package toan.bigo.dijkstra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Để hoàn thành nhiệm vụ trong thời gian ngắn nhất, ta cần:

Đặt bom trong thời gian ngắn nhất.
Từ các tòa nhà di chuyển đến điểm tập trung tốn ít thời gian nhất.
Vậy ta cần tìm đường từ tòa bắt đầu đến tất cả các tòa còn lại ngắn nhất có thể để đi đặt bom, dùng Dijkstra đi từ tòa bắt đầu.

Đồng thời, cũng cần tìm đường ngắn nhất từ các tòa đến toà tập trung hay nói cách khác là từ tòa tập trung đến các các tòa còn lại, ta lại dùng Dijkstra đi từ tòa tập trung.

Ta đã có đường đi ngắn nhất đến tất cả các đỉnh, đồng nghĩa với việc các tòa nhà đã bị đặt bom. Lính thì vô số, nên cho mỗi người từ tòa hiện tại phân đến các tòa lân cận tốn thời gian là như nhau, vậy thời gian để hoàn thành nhiệm vụ là dựa vào người đi lâu nhất. Do đó ta xét tại mỗi tòa nhà, đi từ tòa bắt đầu đến đó và từ đó đến tòa tập trung xem đường nào lâu nhất thì đó là thời gian ngắn nhất để hoàn thành nhiệm vụ.

Độ phức tạp: \mathcal {O} \left(T * V * E * log(V) \right)O(T∗V∗E∗log(V)) với VV là số lượng đỉnh của đồ thị (số tòa nhà), EE là số lượng cạnh của đồ thị (số đường bộ). TT là số lượng bộ test của dữ liệu.
 */
public class Commandos {

    static final int MAX = 105;
    static final int INF = (int)1e9 + 7;
    static int N, R;

    static class Node implements Comparable<Node> {
        int id, weight;

        public Node(int _id, int _weight) {
            this.id = _id;
            this.weight = _weight;
        }

        @Override
        public int compareTo(Node other) {
            return this.weight - other.weight;
        }
    }

    static ArrayList<Node> graph[] = new ArrayList[MAX];

    public static int[] Dijkstra(int s) {
        int[] dist = new int[N];
        Arrays.fill(dist, INF);

        PriorityQueue<Node> pq = new PriorityQueue<>();
        pq.add(new Node(s, 0));
        dist[s] = 0;

        while (!pq.isEmpty()) {
            Node top = pq.poll();
            int u = top.id;
            int w = top.weight;

            if (w > dist[u]) {
                continue;
            }

            for (Node neighbor : graph[u]) {
                if (w + neighbor.weight < dist[neighbor.id]) {
                    dist[neighbor.id] = w + neighbor.weight;
                    pq.add(new Node(neighbor.id, dist[neighbor.id]));
                }
            }
        }

        return dist;
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();

        for (int t = 1; t <= T; t++) {
            N = sc.nextInt();
            R = sc.nextInt();

            for (int i = 0; i < N; i++) {
                graph[i] = new ArrayList<Node>();
            }

            for (int i = 0; i < R; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                graph[u].add(new Node(v, 1));
                graph[v].add(new Node(u, 1));
            }

            int s = sc.nextInt();
            int d = sc.nextInt();

            int[] distS = Dijkstra(s);
            int[] distD = Dijkstra(d);
            int res = 0;

            for (int i = 0; i < N; i++) {
                res = Math.max(res, distS[i] + distD[i]);
            }

            System.out.println("Case " + t + ": " + res);
        }
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 105;
    static final int INF = (int)1e9 + 7;
    static int N, R;

    static class Node implements Comparable<Node> {
        int id, weight;

        public Node(int _id, int _weight) {
            this.id = _id;
            this.weight = _weight;
        }

        @Override
        public int compareTo(Node other) {
            return this.weight - other.weight;
        }
    }

    static ArrayList<Node> graph[] = new ArrayList[MAX];

    public static int[] Dijkstra(int s) {
        int[] dist = new int[N];
        Arrays.fill(dist, INF);

        PriorityQueue<Node> pq = new PriorityQueue<>();
        pq.add(new Node(s, 0));
        dist[s] = 0;

        while (!pq.isEmpty()) {
            Node top = pq.poll();
            int u = top.id;
            int w = top.weight;

            if (w > dist[u]) {
                continue;
            }

            for (Node neighbor : graph[u]) {
                if (w + neighbor.weight < dist[neighbor.id]) {
                    dist[neighbor.id] = w + neighbor.weight;
                    pq.add(new Node(neighbor.id, dist[neighbor.id]));
                }
            }
        }

        return dist;
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();

        for (int t = 1; t <= T; t++) {
            N = sc.nextInt();
            R = sc.nextInt();

            for (int i = 0; i < N; i++) {
                graph[i] = new ArrayList<Node>();
            }

            for (int i = 0; i < R; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                graph[u].add(new Node(v, 1));
                graph[v].add(new Node(u, 1));
            }

            int s = sc.nextInt();
            int d = sc.nextInt();

            int[] distS = Dijkstra(s);
            int[] distD = Dijkstra(d);
            int res = 0;

            for (int i = 0; i < N; i++) {
                res = Math.max(res, distS[i] + distD[i]);
            }

            System.out.println("Case " + t + ": " + res);
        }
    }
}
 */