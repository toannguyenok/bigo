package toan.bigo.dijkstra;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Bài này khá đơn giản, bạn đọc tốt dữ liệu đầu vào, mỗi ví dụ bạn cần tìm đường đi ngắn nhất bằng thuật toán Dijkstra. Mỗi lần xong bạn sẽ in kết quả ra và xóa các tham số cần thiết trong graph, dist.

Độ phức tạp: \mathcal {O} \left (Q * E * log(V) \right)O(Q∗E∗log(V)) với QQ là số lượng bộ test, EE là số lượng cạnh (cung) của đồ thị, VV là số lượng đỉnh của đồ thị.
 */
public class SendingEmail {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int testCase = sc.nextInt();
        for (int i = 0; i < testCase; i++) {
            findResult(sc, i);
        }
    }

    private static void findResult(Scanner sc, int testCase) {
        int n = sc.nextInt();
        int m = sc.nextInt();
        int s = sc.nextInt();
        int t = sc.nextInt();
        ArrayList<ArrayList<Node>> graph = new ArrayList<>();
        ArrayList<Integer> dist = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
            dist.add(Integer.MAX_VALUE);
        }

        for (int i = 0; i < m; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            int w = sc.nextInt();
            graph.get(u).add(new Node(v, w));
            graph.get(v).add(new Node(u, w));
        }
        ArrayList<Integer> newDist = dijkstra(graph, s, dist);
        int ans = newDist.get(t);
        if (ans == Integer.MAX_VALUE) {
            System.out.println("Case #" + (testCase + 1) + ": unreachable");
        } else {
            System.out.println("Case #" + (testCase + 1) + ": " + ans);
        }
    }

    private static ArrayList<Integer> dijkstra(ArrayList<ArrayList<Node>> graph, int start, ArrayList<Integer> dist) {
        ArrayList<Integer> result = new ArrayList<>(dist);
        PriorityQueue<Node> heap = new PriorityQueue<>();
        heap.add(new Node(start, 0));
        result.set(start, 0);

        while (!heap.isEmpty()) {
            Node currentNode = heap.remove();
            if (result.get(currentNode.u) != currentNode.w) {
                continue;
            }
            for (int i = 0; i < graph.get(currentNode.u).size(); i++) {
                Node nextNode = graph.get(currentNode.u).get(i);
                if (currentNode.w + nextNode.w < result.get(nextNode.u)) {
                    result.set(nextNode.u, currentNode.w + nextNode.w);
                    heap.add(new Node(nextNode.u, result.get(nextNode.u)));
                }
            }
        }
        return result;
    }

    private static class Node implements Comparable<Node> {
        int u;
        int w;

        public Node(int u, int w) {
            this.u = u;
            this.w = w;
        }

        @Override
        public int compareTo(Node o) {
            return this.w - o.w;
        }
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 20005;
    static final int INF = (int)1e9 + 7;
    static int Q;
    static int[] dist = new int[MAX];

    static class Node implements Comparable<Node> {
        int id, weight;

        public Node(int _id, int _weight) {
            this.id = _id;
            this.weight = _weight;
        }

        @Override
        public int compareTo(Node other) {
            return this.weight - other.weight;
        }
    }

    static ArrayList<Node> graph[] = new ArrayList[MAX];

    public static void Dijkstra(int s, int f) {
        PriorityQueue<Node> pq = new PriorityQueue<>();
        pq.add(new Node(s, 0));
        dist[s] = 0;

        while (!pq.isEmpty()) {
            Node top = pq.poll();
            int u = top.id;
            int w = top.weight;

            if (u == f) {
                break;
            }

            if (w > dist[u]) {
                continue;
            }

            for (Node neighbor : graph[u]) {
                if (w + neighbor.weight < dist[neighbor.id]) {
                    dist[neighbor.id] = w + neighbor.weight;
                    pq.add(new Node(neighbor.id, dist[neighbor.id]));
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Q = sc.nextInt();

        for (int t = 1; t <= Q; t++) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            int S = sc.nextInt();
            int T = sc.nextInt();

            for (int i = 0; i < n; i++) {
                graph[i] = new ArrayList<Node>();
                dist[i] = INF;
            }

            for (int i = 0; i < m; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                int w = sc.nextInt();
                graph[u].add(new Node(v, w));
                graph[v].add(new Node(u, w));
            }

            Dijkstra(S, T);

            System.out.print("Case #" + t + ": ");
            System.out.println(dist[T] != INF ? dist[T] : "unreachable");
        }
    }
}
 */