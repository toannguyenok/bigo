package toan.bigo.dijkstra;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Đầu tiên bạn sẽ gắn toàn bộ đường đi một chiều vào đồ thị sau đó lần lượt gắn từng đường đi hai chiều vào, nếu đường nào cho ra đường đi ngắn nhất thì bạn chọn đó là kết quả cuối cùng.

Lưu ý: Để làm được việc này thì mỗi lần gắn đường đi hai chiều vào bạn phải chạy lại thuật toán Dijkstra. Việc này sẽ làm bài bạn bị quá thời gian (TLE). Bạn phải cải tiến thuật toán Dijkstra và cải tiến luôn việc không thể gọi Dijkstra mỗi lần gắn đường đi hai chiều mới vào bằng cách sau:

Tính Dijkstra từ SS \to→ TT và tính chiều ngược lại T \to ST→S (gọi Dijkstra hai lần).

Sau khi chạy Dijkstra ta có:

distS[u]: tìm đường đi từ đỉnh SS đến đỉnh uu (trong đồ thị bên dưới là 1 \to 51→5).
distT[v]: tìm đường đi từ đỉnh vv đến đỉnh TT (trong đồ thị là 3 \to 43→4).
Gắn từng đường đi hai chiều vào, lúc này dùng công thức bên dưới, công thức này giống như việc gọi Dijkstra lại mỗi lần gắn đường đi hai chiều vào.

Sau khi gắn dd vào, nếu tổng này nhỏ hơn đường đi hiện tại đang có thì nó chính là đường đi ngắn nhất.

int a = distS[u] + d + distT[v];
int b = distS[v] + d + distT[u];
result = min(result, min(a, b));
Độ phức tạp: \mathcal {O} \left(T * E * log(V) \right)O(T∗E∗log(V)) với EE là số lượng cạnh của đồ thị, VV là số lượng đỉnh của đồ thị và TT là số lượng bộ test trong từng dữ liệu đầu vào.
 */
public class TrafficNetwork {
    static final int INF = (int)1e9 + 7;
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int testCase = sc.nextInt();
        ArrayList<Long> result = new ArrayList<>();
        for (int i = 0; i < testCase; i++) {
            result.add(findResult(sc));
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i));
        }
    }

    private static long findResult(Scanner sc) {
        int n = sc.nextInt();
        int mOneWay = sc.nextInt();
        int kTwoWay = sc.nextInt();
        int start = sc.nextInt() - 1;
        int end = sc.nextInt() - 1;
        ArrayList<ArrayList<Node>> graphS = new ArrayList<>();
        ArrayList<Integer> distS = new ArrayList<>();

        ArrayList<ArrayList<Node>> graphT = new ArrayList<>();
        ArrayList<Integer> distT = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graphS.add(new ArrayList<>());
            distS.add(INF);
            graphT.add(new ArrayList<>());
            distT.add(INF);
        }

        for (int i = 0; i < mOneWay; i++) {
            int u = sc.nextInt() - 1;
            int v = sc.nextInt() - 1;
            int w = sc.nextInt();
            graphS.get(u).add(new Node(v, w));
            graphT.get(v).add(new Node(u, w));
        }
        dijkstra(graphS, start, distS);
        dijkstra(graphT, end, distT);
        long res = distS.get(end);
        for (int i = 0; i < kTwoWay; i++) {
            int u = sc.nextInt() - 1;
            int v = sc.nextInt() - 1;
            int d = sc.nextInt();
            long a = distS.get(u) + d + distT.get(v);
            long b = distS.get(v) + d + distT.get(u);
            res = Math.min(res, Math.min(a, b));
        }

        if (res >= INF) {
            return -1;
        } else {
            return res;
        }
    }

    private static void dijkstra(ArrayList<ArrayList<Node>> graph, int start, ArrayList<Integer> dist) {
        PriorityQueue<Node> heap = new PriorityQueue<>();
        heap.add(new Node(start, 0));
        dist.set(start, 0);

        while (!heap.isEmpty()) {
            Node currentNode = heap.remove();
            if (dist.get(currentNode.u) < currentNode.w) {
                continue;
            }
            for (int i = 0; i < graph.get(currentNode.u).size(); i++) {
                Node nextNode = graph.get(currentNode.u).get(i);
                if (currentNode.w + nextNode.w < dist.get(nextNode.u)) {
                    dist.set(nextNode.u, currentNode.w + nextNode.w);
                    heap.add(new Node(nextNode.u, dist.get(nextNode.u)));
                }
            }
        }
    }

    private static class Node implements Comparable<Node> {
        int u;
        int w;

        public Node(int u, int w) {
            this.u = u;
            this.w = w;
        }

        @Override
        public int compareTo(Node o) {
            return this.w - o.w;
        }
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 10005;
    static final int INF = (int)1e9 + 7;
    static int n, m, k, s, t, u, v, d;

    static class Node implements Comparable<Node> {
        int id, weight;

        public Node(int _id, int _weight) {
            this.id = _id;
            this.weight = _weight;
        }

        @Override
        public int compareTo(Node other) {
            return this.weight - other.weight;
        }
    }

    static ArrayList<Node> graphS[] = new ArrayList[MAX], graphT[] = new ArrayList[MAX];
    static int[] distS = new int[MAX], distT = new int[MAX];

    public static void Dijkstra(int s, int[] dist, ArrayList<Node> graph[]) {
        PriorityQueue<Node> pq = new PriorityQueue<>();
        pq.add(new Node(s, 0));
        dist[s] = 0;

        while (!pq.isEmpty()) {
            Node top = pq.poll();
            int u = top.id;
            int w = top.weight;

            if (w > dist[u]) {
                continue;
            }

            for (Node neighbor : graph[u]) {
                if (w + neighbor.weight < dist[neighbor.id]) {
                    dist[neighbor.id] = w + neighbor.weight;
                    pq.add(new Node(neighbor.id, dist[neighbor.id]));
                }
            }
        }
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();

        while (T-- > 0) {
            n = sc.nextInt();
            m = sc.nextInt();
            k = sc.nextInt();
            s = sc.nextInt();
            t = sc.nextInt();

            for (int i = 1; i <= n; i++) {
                graphS[i] = new ArrayList<Node>();
                graphT[i] = new ArrayList<Node>();
                distS[i] = INF;
                distT[i] = INF;
            }

            for (int i = 0; i < m; i++) {
                u = sc.nextInt();
                v = sc.nextInt();
                d = sc.nextInt();
                graphS[u].add(new Node(v, d));
                graphT[v].add(new Node(u, d));
            }

            Dijkstra(s, distS, graphS);
            Dijkstra(t, distT, graphT);
            int res = distS[t];

            for (int i = 0; i < k; i++) {
                u = sc.nextInt();
                v = sc.nextInt();
                d = sc.nextInt();
                int a = distS[u] + d + distT[v];
                int b = distS[v] + d + distT[u];
                res = Math.min(res, Math.min(a, b));
            }

            System.out.println(res != INF ? res : -1);
        }
    }
}
 */