package toan.bigo.dijkstra;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Có hai cách để giải quyết bài này.

Cách 1: Do số lượng đỉnh NN bé (100100 đỉnh) nên bạn có thể chạy Dijkstra ở mỗi đỉnh rồi tìm đường đi từ các đỉnh đến đỉnh thoát ra. Nếu chi phí \le T≤T thì bạn sẽ tăng số lượng con chuột lên.

Cách 2: Cách này bạn chỉ chạy Dijsktra 11 lần, bạn sẽ chạy Dijkstra từ đỉnh thoát ra. Tuy nhiên trước khi chạy Dijkstra từ đỉnh này bạn phải quay ngược hướng toàn bộ đồ thị lại. Vì bạn cần tìm đường đi từ các đỉnh khác tới lối thoát, giờ bạn tìm đường đi từ đỉnh lối thoát đến các đỉnh khác, thì bạn cần phải quay ngược hướng các đồ thị lại để tìm đường đi. Sau đó xét các chi phí, chi phí nào \le T≤T thì tăng số lượng chuột có thể thoát lên.

Độ phức tạp: \mathcal {O} \left(V * E * log(V) \right)O(V∗E∗log(V)) cho cách 11 và \mathcal {O} \left(E * log(V) \right)O(E∗log(V)) cho cách 22 với EE là số lượng cạnh (cung), VV là số lượng đỉnh trong đồ thị.
 */
public class MiceAndMaze {

    private static ArrayList<ArrayList<Node>> graph = new ArrayList<>();
    private static ArrayList<Integer> dist = new ArrayList<>();
    private static PriorityQueue<Node> heap = new PriorityQueue<>();

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
            dist.add(Integer.MAX_VALUE);
        }
        int exist = sc.nextInt() - 1;
        int countdownTimer = sc.nextInt();
        int m = sc.nextInt();
        for (int i = 0; i < m; i++) {
            int u = sc.nextInt() - 1;
            int v = sc.nextInt() - 1;
            int w = sc.nextInt();
            graph.get(v).add(new Node(u, w));
        }

        dijkstra(exist);

        int ans = 0;
        for (Integer integer : dist) {
            if (integer <= countdownTimer) {
                ans++;
            }
        }
        System.out.println(ans + "");
    }

    private static void dijkstra(int start) {
        dist.set(start, 0);
        heap.add(new Node(start, 0));
        while (!heap.isEmpty()) {
            Node currentNode = heap.remove();
            if (dist.get(currentNode.u) != currentNode.w) {
                continue;
            }
            for (int i = 0; i < graph.get(currentNode.u).size(); i++) {
                Node nextNode = graph.get(currentNode.u).get(i);
                if (currentNode.w + nextNode.w < dist.get(nextNode.u)) {
                    dist.set(nextNode.u, currentNode.w + nextNode.w);
                    heap.add(new Node(nextNode.u, dist.get(nextNode.u)));
                }
            }
        }
    }

    private static class Node implements Comparable<Node> {
        int u;
        int w;

        public Node(int u, int w) {
            this.u = u;
            this.w = w;
        }

        @Override
        public int compareTo(Node o) {
            return this.w - o.w;
        }
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 105;
    static final int INF = (int)1e9 + 7;
    static final int[] dist = new int[MAX];

    static class Node implements Comparable<Node> {
        int id, weight;

        public Node(int _id, int _weight) {
            this.id = _id;
            this.weight = _weight;
        }

        @Override
        public int compareTo(Node other) {
            return this.weight - other.weight;
        }
    }

    static ArrayList<Node> graph[] = new ArrayList[MAX];

    public static void Dijsktra(int s) {
        PriorityQueue<Node> pq = new PriorityQueue<>();
        pq.add(new Node(s, 0));
        dist[s] = 0;

        while (!pq.isEmpty()) {
            Node top = pq.poll();
            int u = top.id;
            int w = top.weight;

            for (Node neighbor : graph[u]) {
                if (w + neighbor.weight < dist[neighbor.id]) {
                    dist[neighbor.id] = w + neighbor.weight;
                    pq.add(new Node(neighbor.id, dist[neighbor.id]));
                }
            }
        }
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int E = sc.nextInt();
        int T = sc.nextInt();
        int M = sc.nextInt();

        for (int i = 0; i <= N; i++) {
            dist[i] = INF;
            graph[i] = new ArrayList<Node>();
        }

        for (int i = 0; i < M; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            int w = sc.nextInt();
            graph[v].add(new Node(u, w));
        }

        Dijsktra(E);

        int count = 0;
        for (int i = 1; i <= N; i++) {
            if (dist[i] <= T) {
                count++;
            }
        }

        System.out.print(count);
    }
}
 */