package toan.bigo.dijkstra;


import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Nhận xét: Để được đường đi gần ngắn nhất, ta cần một đồ thị không bao gồm các cạnh chứa trong những đường đi ngắn nhất và chạy Dijkstra trên đồ thị mới. Do đó mục tiêu của ta là phải loại bỏ các cạnh này ra khỏi đồ thị ban đầu, hay nói cách khác là kiểm tra một cạnh có thuộc bất kỳ đường đi ngắn nhất nào hay chưa để quyết định đưa vào đồ thị mới.
Đầu tiên ta sẽ chạy Dijkstra 2 lần từ SS và từ DD để lưu lại đường đi ngắn nhất từ đỉnh SS và DD đến một đỉnh i bất kỳ trong đồ thị.

Sau khi chạy xong, ta sẽ được độ dài đường đi ngắn nhất từ S \rightarrow DS→D là distS[D]distS[D].

Lần lượt duyệt qua từng đỉnh uu trong đồ thị, kiểm tra xem cách đỉnh kề vv của uu có chi phí là ww có thuộc đường đi ngắn nhất bằng cách so sánh tổng (distS[u] + w + distD[v])(distS[u]+w+distD[v]) với độ dài đường đi ngắn nhất vừa tìm được.

Nếu bằng nghĩa là cạnh (u, v, w)(u,v,w) thuộc đường đi ngắn nhất, ta bỏ qua cạnh đó.
Ngược lại ta đưa cạnh (u, v, w)(u,v,w) vào đồ thị mới.
Cuối cùng chỉ cần chạy Dijkstra trên đồ thị mới là ta sẽ có đường đường đi gần ngắn nhất không bao gồm các cạnh thuộc đường đi ngắn nhất.

Độ phức tạp: O(T * ElogV)O(T∗ElogV) với EE là số lượng cạnh của đồ thị, VV là số lượng đỉnh của đồ thị và TT là số lượng bộ test trong từng dữ liệu đầu vào.
 */
public class AlmostShortestPath {

    static final int MAX = 505;
    static final int INF = (int)1e9 + 7;

    static class Node implements Comparable<Node> {
        int id, weight;

        public Node(int _id, int _weight) {
            this.id = _id;
            this.weight = _weight;
        }

        @Override
        public int compareTo(Node other) {
            return this.weight - other.weight;
        }
    }

    static ArrayList<Node> graph[] = new ArrayList[MAX];
    static ArrayList<Node> graphS[] = new ArrayList[MAX];
    static ArrayList<Node> graphD[] = new ArrayList[MAX];
    static int[] dist = new int[MAX];
    static int[] distS = new int[MAX];
    static int[] distD = new int[MAX];

    public static void Dijkstra(int s, int[] dist, ArrayList<Node> graph[]) {
        PriorityQueue<Node> pq = new PriorityQueue<>();
        pq.add(new Node(s, 0));
        dist[s] = 0;

        while (!pq.isEmpty()) {
            Node top = pq.poll();
            int u = top.id;
            int w = top.weight;

            if (w > dist[u]) {
                continue;
            }

            for (Node neighbor : graph[u]) {
                if (w + neighbor.weight < dist[neighbor.id]) {
                    dist[neighbor.id] = w + neighbor.weight;
                    pq.add(new Node(neighbor.id, dist[neighbor.id]));
                }
            }
        }
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);

        while (true) {
            int N = sc.nextInt();
            int M = sc.nextInt();

            if (N == 0 && M == 0) {
                break;
            }

            for (int i = 0; i < N; i++) {
                graph[i] = new ArrayList<Node>();
                graphS[i] = new ArrayList<Node>();
                graphD[i] = new ArrayList<Node>();
                dist[i] = INF;
                distS[i] = INF;
                distD[i] = INF;
            }

            int S = sc.nextInt();
            int D = sc.nextInt();

            for (int i = 0; i < M; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                int w = sc.nextInt();
                graphS[u].add(new Node(v, w));
                graphD[v].add(new Node(u, w));
            }

            Dijkstra(S, distS, graphS);
            Dijkstra(D, distD, graphD);
            int shortestPath = distS[D];

            for (int u = 0; u < N; u++) {
                for (Node neighbor : graphS[u]) {
                    int v = neighbor.id;
                    int w = neighbor.weight;

                    if (distS[u] + w + distD[v] != shortestPath) {
                        graph[u].add(neighbor);
                    }
                }
            }

            Dijkstra(S, dist, graph);
            System.out.println(dist[D] != INF ? dist[D] : -1);
        }
    }
}
/*
import java.util.*;

public class Main {
	static final int MAX = 505;
	static final int INF = (int)1e9 + 7;

	static class Node implements Comparable<Node> {
		int id, weight;

		public Node(int _id, int _weight) {
			this.id = _id;
			this.weight = _weight;
		}

		@Override
		public int compareTo(Node other) {
			return this.weight - other.weight;
		}
	}

	static ArrayList<Node> graph[] = new ArrayList[MAX];
	static ArrayList<Node> graphS[] = new ArrayList[MAX];
	static ArrayList<Node> graphD[] = new ArrayList[MAX];
	static int[] dist = new int[MAX];
	static int[] distS = new int[MAX];
	static int[] distD = new int[MAX];

	public static void Dijkstra(int s, int[] dist, ArrayList<Node> graph[]) {
		PriorityQueue<Node> pq = new PriorityQueue<>();
		pq.add(new Node(s, 0));
		dist[s] = 0;

		while (!pq.isEmpty()) {
			Node top = pq.poll();
			int u = top.id;
			int w = top.weight;

			if (w > dist[u]) {
				continue;
			}

			for (Node neighbor : graph[u]) {
				if (w + neighbor.weight < dist[neighbor.id]) {
					dist[neighbor.id] = w + neighbor.weight;
					pq.add(new Node(neighbor.id, dist[neighbor.id]));
				}
			}
		}
	}

	public static void main(String[] agrs) {
		Scanner sc = new Scanner(System.in);

		while (true) {
			int N = sc.nextInt();
			int M = sc.nextInt();

			if (N == 0 && M == 0) {
				break;
			}

			for (int i = 0; i < N; i++) {
				graph[i] = new ArrayList<Node>();
				graphS[i] = new ArrayList<Node>();
				graphD[i] = new ArrayList<Node>();
				dist[i] = INF;
				distS[i] = INF;
				distD[i] = INF;
			}

			int S = sc.nextInt();
			int D = sc.nextInt();

			for (int i = 0; i < M; i++) {
				int u = sc.nextInt();
				int v = sc.nextInt();
				int w = sc.nextInt();
				graphS[u].add(new Node(v, w));
				graphD[v].add(new Node(u, w));
			}

			Dijkstra(S, distS, graphS);
			Dijkstra(D, distD, graphD);
			int shortestPath = distS[D];

			for (int u = 0; u < N; u++) {
				for (Node neighbor : graphS[u]) {
					int v = neighbor.id;
					int w = neighbor.weight;

					if (distS[u] + w + distD[v] != shortestPath) {
						graph[u].add(neighbor);
					}
				}
			}

			Dijkstra(S, dist, graph);
			System.out.println(dist[D] != INF ? dist[D] : -1);
		}
	}
}
 */