package toan.bigo.dijkstra;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Bạn sẽ sử dụng thuật toán Dijkstra để tìm đường đi ngắn nhất từ A đến mọi thành phố và từ B đến mọi thành phố.

Gọi distA[i]distA[i] là khoảng cách ngắn nhất từ AA đến 1 thành phố ii bất kỳ.
Gọi distB[i]distB[i] là khoảng cách ngắn nhất từ BB đến 1 thành phố ii bất kỳ.
Như vậy bạn sẽ duyệt qua kk thành phố có bán chocolate và tìm min trong tổng khoảng cách từ AA đến thành phố đó và BB đến thành phố đó.

Một điều cần chú ý là khoảng cách từ BB đến thành phố bán chocolate không được vượt quá xx vì như vậy chocolate sẽ tan. Do bài này test khá lớn, nên cần lưu ý việc nhập/xuất dữ liệu. Đồng thời, trong thuật Dijkstra, khi gặp một khoảng cách tốt hơn/đường đi ngắn hơn thì thì lưu lại khoảng cách đó dist[v] = wdist[v]=w và cặp (w, v)(w,v) được thêm vô hàng đợi. Vậy thì những cặp đã thêm vô hàng đợi trước đó có cùng đỉnh vv, chắc chắn khoảng cách đi kèm sẽ lớn hơn ww của cặp vừa thêm vào. Nên bạn có thể bỏ qua những cặp đó (w > dist[v])(w>dist[v]) không cần xét.

Độ phức tạp: O(ElogV)O(ElogV) với EE là số lượng cạnh (cung) của đồ thị, VV là số lượng đỉnh của đồ thị.
 */
public class ChocolateJourney {

    private static int INF = (int) 1e7 + 10;

    private static class Node implements Comparable<Node> {
        int u;
        int w;

        public Node(int u, int w) {
            this.u = u;
            this.w = w;
        }

        @Override
        public int compareTo(Node o) {
            return this.w - o.w;
        }
    }

    private static ArrayList<ArrayList<Node>> graph = new ArrayList<>();
    private static ArrayList<Integer> chocolate = new ArrayList<>();

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
            graph.clear();
            chocolate.clear();

            int n = sc.nextInt();
            int m = sc.nextInt();
            int k = sc.nextInt();
            int x = sc.nextInt();
            for (int i = 0; i < k; i++) {
                chocolate.add(sc.nextInt() - 1);
            }

            for (int i = 0; i < n; i++) {
                graph.add(new ArrayList<>());
            }

            for (int i = 0; i < m; i++) {
                int u = sc.nextInt() - 1;
                int v = sc.nextInt() - 1;
                int weight = sc.nextInt();
                graph.get(u).add(new Node(v, weight));
                graph.get(v).add(new Node(u, weight));
            }
            int s = sc.nextInt() - 1;
            int d = sc.nextInt() - 1;

            ArrayList<Integer> distA = dijkstra(graph, s, n);
            ArrayList<Integer> distB = dijkstra(graph, d, n);

            int res = INF;
            for (int i = 0; i < chocolate.size(); i++) {
                if (distB.get(chocolate.get(i)) < x) {
                    res = Math.min(res, distA.get(chocolate.get(i)) + distB.get(chocolate.get(i)));
                }
            }
        System.out.print(res < INF ? res : -1);
    }

    private static ArrayList<Integer> dijkstra(ArrayList<ArrayList<Node>> graph, int s, int n) {
        ArrayList<Integer> dist = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            dist.add(INF);
        }
        dist.set(s, 0);
        PriorityQueue<Node> heap = new PriorityQueue<>();
        heap.add(new Node(s, 0));

        while (!heap.isEmpty()) {
            Node current = heap.remove();
            if (current.w > dist.get(current.u)) {
                continue;
            }

            for (int i = 0; i < graph.get(current.u).size(); i++) {
                Node neighbor = graph.get(current.u).get(i);
                if (current.w + neighbor.w < dist.get(neighbor.u)) {
                    dist.set(neighbor.u, current.w + neighbor.w);
                    heap.add(new Node(neighbor.u, dist.get(neighbor.u)));
                }
            }
        }

        return dist;
    }
}
/*
import java.util.*;

public class Main {
	static final int MAX = 100000 + 5;
	static final int INF = (int)1e9;
	static int N, M;

	static class Node implements Comparable<Node> {
		int id, weight;

		public Node(int _id, int _weight) {
			this.id = _id;
			this.weight = _weight;
		}

		@Override
		public int compareTo(Node other) {
			return this.weight - other.weight;
		}
	}

	static ArrayList<Node> graph[] = new ArrayList[MAX];

	public static int[] Dijkstra(int s) {
		int[] dist = new int[N + 1];
		Arrays.fill(dist, INF);

		PriorityQueue<Node> pq = new PriorityQueue<>();
		pq.add(new Node(s, 0));
		dist[s] = 0;

		while (!pq.isEmpty()) {
			Node top = pq.poll();
			int u = top.id;
			int w = top.weight;

			if (w > dist[u]) {
				continue;
			}

			for (Node neighbor : graph[u]) {
				if (w + neighbor.weight < dist[neighbor.id]) {
					dist[neighbor.id] = w + neighbor.weight;
					pq.add(new Node(neighbor.id, dist[neighbor.id]));
				}
			}
		}

		return dist;
	}

	public static void main(String[] agrs) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		M = sc.nextInt();
		int k = sc.nextInt();
		int x = sc.nextInt();
		ArrayList<Integer> cities = new ArrayList<>();

		for (int i = 0; i < k; i++) {
			cities.add(sc.nextInt());
		}

		for (int i = 0; i <= N; i++) {
			graph[i] = new ArrayList<Node>();
		}

		for (int i = 0; i < M; i++) {
			int u = sc.nextInt();
			int v = sc.nextInt();
			int d = sc.nextInt();
			graph[u].add(new Node(v, d));
			graph[v].add(new Node(u, d));
		}

		int A = sc.nextInt();
		int B = sc.nextInt();
		int[] distA = Dijkstra(A);
		int[] distB = Dijkstra(B);
		int res = INF;

		for (int city : cities) {
			if (distB[city] <= x) {
				res = Math.min(res, distA[city] + distB[city]);
			}
		}

		System.out.print(res < INF ? res : -1);
	}
}
 */