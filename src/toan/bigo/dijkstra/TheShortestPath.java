package toan.bigo.dijkstra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
ViEng
Bài này ý tưởng giải thì không khó, cái khó là bạn phải chuyển dữ liệu từ chuỗi về số để có thể chạy thuật toán Dijkstra cho từng trường hợp. Cách giải quyết như sau:

Bỏ các tên thành phố vào một mảng danh sách các thành phố (mảng chuỗi). Sau đó bỏ lần lượt từng đường đi vào graph bình thường. Khi tìm đường đi giữa 22 thành phố với nhau chỉ cần tìm lại index của thành phố đó ban đầu khi bỏ vào mảng thành phố là xong.

Độ phức tạp: \mathcal {O} \left(s * p * E * log(V) \right)O(s∗p∗E∗log(V)) với ss là số lượng bộ test, pp là số lượng truy vấn tìm đường đi ngắn nhất giữa 22 thành phố, EE là số lượng cạnh (cung) của đồ thị và VV là số lượng đỉnh của đồ thị trong mỗi bộ test.
 */
public class TheShortestPath {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int testCase = sc.nextInt();
        for (int i = 0; i < testCase; i++) {
            findResult(sc);
        }
    }

    private static void findResult(Scanner sc) {
        int cities = sc.nextInt();
        ArrayList<ArrayList<Node>> graph = new ArrayList<>();
        ArrayList<Integer> dist = new ArrayList<>();
        HashMap<String, Integer> hashPosition = new HashMap<>();
        for (int i = 0; i < cities; i++) {
            graph.add(new ArrayList<>());
            dist.add(Integer.MAX_VALUE);
        }

        for (int i = 0; i < cities; i++) {
            String cityName = sc.next();
            hashPosition.put(cityName, i);
            int connection = sc.nextInt();
            for (int j = 0; j < connection; j++) {
                int v = sc.nextInt() - 1;
                int w = sc.nextInt();
                graph.get(i).add(new Node(v, w));
            }
        }
        int numberPath = sc.nextInt();
        for (int i = 0; i < numberPath; i++) {
            String start = sc.next();
            String end = sc.next();
            ArrayList<Integer> newDist = dijkstra(graph, hashPosition.get(start), dist);
            System.out.println(newDist.get(hashPosition.get(end)) + "");
        }
    }

    private static ArrayList<Integer> dijkstra(ArrayList<ArrayList<Node>> graph, int start, ArrayList<Integer> dist) {
        ArrayList<Integer> result = new ArrayList<>(dist);
        PriorityQueue<Node> heap = new PriorityQueue<>();
        heap.add(new Node(start, 0));
        result.set(start, 0);

        while (!heap.isEmpty()) {
            Node currentNode = heap.remove();
            if (result.get(currentNode.u) != currentNode.w) {
                continue;
            }
            for (int i = 0; i < graph.get(currentNode.u).size(); i++) {
                Node nextNode = graph.get(currentNode.u).get(i);
                if (currentNode.w + nextNode.w < result.get(nextNode.u)) {
                    result.set(nextNode.u, currentNode.w + nextNode.w);
                    heap.add(new Node(nextNode.u, result.get(nextNode.u)));
                }
            }
        }
        return result;
    }

    private static class Node implements Comparable<Node> {
        int u;
        int w;

        public Node(int u, int w) {
            this.u = u;
            this.w = w;
        }

        @Override
        public int compareTo(Node o) {
            return this.w - o.w;
        }
    }
}
/*
import java.util.*;
import java.io.*;

public class Main {
    static final int MAX = 10005;
    static final int INF = (int)1e9 + 7;
    static int[] dist = new int[MAX];
    static ArrayList<String> cities = new ArrayList<>();

    static class Node implements Comparable<Node> {
        int id, weight;

        public Node(int _id, int _weight) {
            this.id = _id;
            this.weight = _weight;
        }

        @Override
        public int compareTo(Node other) {
            return this.weight - other.weight;
        }
    }

    static ArrayList<Node> graph[] = new ArrayList[MAX];

    public static void Dijkstra(int s, int f) {
        Arrays.fill(dist, INF);
        PriorityQueue<Node> pq = new PriorityQueue<>();

        pq.add(new Node(s, 0));
        dist[s] = 0;

        while (!pq.isEmpty()) {
            Node top = pq.poll();
            int u = top.id;
            int w = top.weight;

            if (u == f) {
                break;
            }

            for (Node neighbor : graph[u]) {
                if (w + neighbor.weight < dist[neighbor.id]) {
                    dist[neighbor.id] = w + neighbor.weight;
                    pq.add(new Node(neighbor.id, dist[neighbor.id]));
                }
            }
        }
    }

    public static void main(String[] args) {
        MyScanner in = new MyScanner();
        int T = in.nextInt();

        while (T-- > 0) {
            int N = in.nextInt();

            for (int i = 1; i <= N; i++) {
                graph[i] = new ArrayList<Node>();
            }
            cities.clear();

            for (int u = 1; u <= N; u++) {
                String name = in.next();
                cities.add(name);
                int neighbors = in.nextInt();

                for (int i = 0; i < neighbors; i++) {
                    int v = in.nextInt();
                    int w = in.nextInt();
                    graph[u].add(new Node(v, w));
                }
            }

            int Q = in.nextInt();
            for (int i = 0; i < Q; i++) {
                String sCity = in.next();
                String fCity = in.next();
                int s = cities.indexOf(sCity) + 1;
                int f = cities.indexOf(fCity) + 1;
                Dijkstra(s, f);
                System.out.println(dist[f]);
            }
        }
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
                br = new BufferedReader(new InputStreamReader(System.in));
        }
        String next() {
                while (st == null || !st.hasMoreElements()) {
                        try {
                                st = new StringTokenizer(br.readLine());
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                }
                return st.nextToken();
        }
        int nextInt() {
                return Integer.parseInt(next());
        }
        long nextLong() {
                return Long.parseLong(next());
        }
        double nextDouble() {
                return Double.parseDouble(next());
        }
        String nextLine() {
                String str = "";
                try {
                        str = br.readLine();
                } catch (IOException e) {
                        e.printStackTrace();
                }
                return str;
        }
    }
}
 */
