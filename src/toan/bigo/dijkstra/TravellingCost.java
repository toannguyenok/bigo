package toan.bigo.dijkstra;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Đề cho biết các địa điểm được đánh số từ 00 đến 500500 và danh sách các đường đi, vậy ta có thể thấy đây là một đồ thị có 501501 đỉnh và các con đường sẽ là các cạnh của đồ thị.

Từ danh sách cạnh, tạo đồ thị, sau đó chạy Dijkstra từ đỉnh UU. Sau khi chạy xong dựa vào mảng chi phí distdist để tìm chi phí đến các đỉnh cần tìm. Nếu đỉnh nào chi phí là vô cực (INF) thì sẽ không có đường đi đến đó.

Độ phức tạp: \mathcal {O} \left( E*log(V) \right)O(E∗log(V)) với EE là số lượng cạnh (cung), VV là số lượng đỉnh trong đồ thị.

Mã nguồn tham khảo
 */
public class TravellingCost {

    private static final ArrayList<Integer> dist = new ArrayList<>();
    private static final PriorityQueue<Node> heap = new PriorityQueue<>();

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        ArrayList<ArrayList<Node>> graph = new ArrayList<>();

        for (int i = 0; i < 500+ 1; i++) {
            graph.add(new ArrayList<>());
            dist.add(Integer.MAX_VALUE);
        }
        for (int i = 0; i < n; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            int w = sc.nextInt();
            graph.get(u).add(new Node(v, w));
            graph.get(v).add(new Node(u, w));
        }

        int start = sc.nextInt();
        int query = sc.nextInt();
        dijkstra(start, graph);
        for (int i = 0; i < query; i++) {
            int nextLocation = sc.nextInt();
            if (dist.get(nextLocation) == Integer.MAX_VALUE) {
                System.out.println("NO PATH");
            } else {
                System.out.println(dist.get(nextLocation) + "");
            }
        }
    }

    private static void dijkstra(int start, ArrayList<ArrayList<Node>> graph) {
        dist.set(start, 0);
        heap.add(new Node(start, 0));
        while (!heap.isEmpty()) {
            Node currentNode = heap.remove();
            if (dist.get(currentNode.u) != currentNode.w) {
                continue;
            }
            for (int i = 0; i < graph.get(currentNode.u).size(); i++) {
                int nextU = graph.get(currentNode.u).get(i).u;
                int nextW = graph.get(currentNode.u).get(i).w;
                if (currentNode.w + nextW < dist.get(nextU)) {
                    dist.set(nextU, currentNode.w + nextW);
                    heap.add(new Node(nextU, dist.get(nextU)));
                }
            }
        }
    }

    private static class Node implements Comparable<Node> {
        int u;
        int w;

        public Node(int u, int w) {
            this.u = u;
            this.w = w;
        }

        @Override
        public int compareTo(Node o) {
            return this.w - o.w;
        }
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 505;
    static final int INF = (int)1e9 + 7;
    static final int[] dist = new int[MAX];

    static class Node implements Comparable<Node> {
        int id, weight;

        public Node(int _id, int _weight) {
            this.id = _id;
            this.weight = _weight;
        }

        @Override
        public int compareTo(Node other) {
            return this.weight - other.weight;
        }
    }

    static ArrayList<Node> graph[] = new ArrayList[MAX];

    public static void Dijsktra(int s) {
        PriorityQueue<Node> pq = new PriorityQueue<>();
        pq.add(new Node(s, 0));
        dist[s] = 0;

        while (!pq.isEmpty()) {
            Node top = pq.poll();
            int u = top.id;
            int w = top.weight;

            for (Node neighbor : graph[u]) {
                if (w + neighbor.weight < dist[neighbor.id]) {
                    dist[neighbor.id] = w + neighbor.weight;
                    pq.add(new Node(neighbor.id, dist[neighbor.id]));
                }
            }
        }
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();

        for (int i = 0; i < MAX; i++) {
            dist[i] = INF;
            graph[i] = new ArrayList<Node>();
        }

        for (int i = 0; i < N; i++) {
            int A = sc.nextInt();
            int B = sc.nextInt();
            int W = sc.nextInt();
            graph[A].add(new Node(B, W));
            graph[B].add(new Node(A, W));
        }

        int S = sc.nextInt();
        Dijsktra(S);
        int Q = sc.nextInt();

        for (int i = 0; i < Q; i++) {
            int V = sc.nextInt();
            System.out.println(dist[V] != INF ? dist[V] : "NO PATH");
        }
    }
}
 */