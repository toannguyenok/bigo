package toan.bigo.sort;

import java.util.Arrays;
import java.util.Scanner;

/*
Nhận xét:

Ta có thể phát biểu lại bài toán trên như sau: Tìm các giá trị xx sao cho xx có thể tách mảng hh ra làm hai phần AA và BB, trong đó các giá trị trong AA luôn lớn hơn xx và giá trị trong BB luôn nhỏ hơn hoặc bằng xx.
Gọi min(A)min(A) là giá trị nhỏ nhất trong phần AA và max(B)max(B) là giá trị lớn nhất trong phần BB. Như vậy, số cách chọn xx chính bằng max(B)max(B) - min(A)min(A).
Ta hình thành cách giải của bài này như sau:

Bước 11: Đưa thông tin độ phức tạp của từng công việc vào một mảng, tạm gọi là hh.
Bước 22: Sắp xếp mảng hh theo thứ tự tăng dần về độ khó. Với mảng được đánh số từ 00, lúc này:
max(B)max(B) tương ứng với h_{b - 1}h
​b−1
​​ .
min(A)min(A) tương ứng với h_bh
​b
​​ .
Bước 33: In ra số cách chọn xx bằng công thức h_b - h_{b - 1}h
​b
​​ −h
​b−1
​​ .
Độ phức tạp: O(nlogn)O(nlogn) với nn là số lượng công việc.
 */
public class Chores {

    public static void main(String[] args) {
        // write your code here
        int n;
        int a;
        int b;

        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        a = sc.nextInt();
        b = sc.nextInt();
        int[] chores = new int[n];
        for (int i = 0; i < n; i++) {
            chores[i] = sc.nextInt();
        }

        Arrays.sort(chores);
        int right = chores[n - a];
        int left = chores[b - 1];
        int x = 0;
        if (right >= left) {
            x = right - left;
        }

        System.out.print(x + "");
    }
}

/*
import java.util.Scanner;
import java.util.Arrays;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(), a = sc.nextInt(), b = sc.nextInt();
        int[] h = new int[n];

        for (int i = 0; i < n; i++) {
            h[i] = sc.nextInt();
        }

        Arrays.sort(h);
        System.out.print(h[b] - h[b - 1]);
    }
}
 */
