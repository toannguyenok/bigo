package toan.bigo.sort;

import java.util.Arrays;
import java.util.Scanner;

/*
Nhận xét:

Trước hết, ta thấy rằng hoàn toàn có thể biết được mảng kết quả sau khi đã được sắp tăng dần bằng việc áp dụng thuật toán sắp xếp đối với mảng ban đầu.
Đoạn con mà khi đảo ngược nó, ta thu được mảng tăng dần sẽ bắt đầu từ vị trí đầu tiên mà mảng kết quả khác với mảng ban đầu. Tương tự, đoạn con đó sẽ kết thúc tại vị trí cuối cùng mà mảng kết quả khác với mảng ban đầu.
Tuy nhiên, việc đảo ngược đoạn con vừa tìm được chưa chắc sẽ cho ta một mảng tăng dần. Do đó, ta cần so sánh mảng sau khi đã đảo ngược đoạn con với mảng kết quả tăng dần để đưa ra đáp án.
Như vậy, các bước thực hiện của ta như sau:

Bước 1: Đưa các phần tử vào mảng.
Bước 2: Sắp xếp lại mảng tăng dần, đây chính là mảng kết quả nói trên.
Bước 3: Chạy song song từ trái sang phải trên hai mảng ban đầu và mảng đã sắp để tìm vị trí đầu tiên sai khác, tạm gọi là ll.
Bước 4: Làm tương tự từ phải sang trái để tìm vị trí cuối cùng sai khác, tạm gọi là rr.
Bước 5: Đảo ngược đoạn con [l, r][l,r].
Bước 6: So sánh mảng sau khi đã đảo ngược đoạn con với mảng kết quả, nếu trùng khớp thì in yes và đoạn [l, r][l,r], ngược lại in no.
Độ phức tạp: O(nlogn)O(nlogn) với nn là số lượng phần tử trong mảng.
 */
public class SortTheArray {

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arrays = new int[n];
        int[] arraysSorted = new int[n];
        for (int i = 0; i < n ; i++) {
            int number = sc.nextInt();
            arrays[i] = number;
            arraysSorted[i] = number;
        }

        Arrays.sort(arraysSorted);

        int left = 0;
        int right = 0;

        for (int i = 0; i < n; i++) {
            if (arraysSorted[i] != arrays[i]) {
                left = i;
                break;
            }
        }

        for (int i = n - 1; i >= 0; i--) {
            if (arraysSorted[i] != arrays[i]) {
                right = i;
                break;
            }
        }

        //swap
        for (int i = left, j = right; i < j; i++, j--) {
            int temp = arrays[i];
            arrays[i] = arrays[j];
            arrays[j] = temp;
        }


        if (!Arrays.equals(arrays, arraysSorted)) {
            System.out.print("no");
            return;
        }

        System.out.println("yes");
        System.out.print((left + 1) + " " + (right + 1));
    }
}
/*
import java.util.Scanner;
import java.util.Arrays;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n], sorted_a = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
            sorted_a[i] = a[i];
        }

        Arrays.sort(sorted_a);
        int l = 0, r = 0;

        for (int i = 0; i < n; i++) {
            if (a[i] != sorted_a[i]) {
                l = i;
                break;
            }
        }

        for (int i = n - 1; i >= 0; i--) {
            if (a[i] != sorted_a[i]) {
                r = i;
                break;
            }
        }

        for (int i = l, j = r; i < j; i++, j--) {
            int temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }

        if (!Arrays.equals(a, sorted_a)) {
            System.out.print("no");
            return;
        }

        System.out.println("yes");
        System.out.print((l + 1) + " " + (r + 1));
    }
}
 */