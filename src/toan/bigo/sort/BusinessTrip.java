package toan.bigo.sort;

import java.util.Arrays;
import java.util.Scanner;

/*
Hướng dẫn giải
Nhận xét:

Ta thấy rằng để cây nhanh chóng đạt chiều cao cần thiết, ta sẽ ưu tiên tưới nước vào những tháng mà cây phát triển mạnh mẽ nhất.
Do đó, ta hình thành cách giải như sau:

Bước 1: Đưa thông tin độ tăng chiều cao của cây ở mỗi tháng vào một mảng.
Bước 2: Sắp xếp mảng giảm dần, tháng nào cây phát triển mạnh hơn sẽ đứng trước.
Bước 3: Sử dụng một biến đếm số lượng tháng cần phải tưới nước và lần lượt duyệt qua mảng đã sắp: Nếu chiều cao cần tăng k vẫn còn lớn hơn 0, tức cây vẫn cần phải phát triển tiếp, ta tăng biến đếm số lượng tháng cần tưới lên 1. Đồng thời trừ độ cao cây phát triển được vào k chiều cao cần phát triển.
Bước 4: In ra kết quả. Nếu lúc này mà k > 0 tức là đã tưới nước hết 12 tháng mà cây vẫn không đạt yêu cầu, ta in -1. Ngược lại, in ra số tháng đã lưu.
Độ phức tạp: O(NlogN)O(NlogN) với N = 12 là số lượng tháng.
 */
public class BusinessTrip {
    public static void main(String[] args) {
        // write your code here
        int k;

        Scanner sc = new Scanner(System.in);
        k = sc.nextInt();
        int total = 0;
        int[] growth = new int[12];
        for (int i = 0; i < 12; i++) {
            int value = sc.nextInt();
            total = total + value;
            growth[i] = value;
        }
        if (total < k) {
            System.out.print("-1");
            return;
        }

        if (k == 0) {
            System.out.print("0");
            return;
        }

        total = 0;
        int result = 0;
        Arrays.sort(growth);
        for (int i = 12 - 1; i >= 0; i--) {
            total = total + growth[i];
            result++;
            if (total >= k) {
                break;
            }
        }

        System.out.print(result + "");
    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        ArrayList<Integer> a = new ArrayList<>();

        for (int i = 0; i < 12; i++) {
            a.add(sc.nextInt());
        }

        Collections.sort(a, Collections.reverseOrder());
        int n_months = 0;

        for (int height : a) {
            if (k <= 0) {
                break;
            }

            n_months++;
            k -= height;
        }

        System.out.print(k <= 0 ? n_months : -1);
    }
}
 */