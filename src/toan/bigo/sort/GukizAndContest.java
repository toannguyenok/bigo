package toan.bigo.sort;

import java.util.Arrays;
import java.util.Scanner;

/*
Nhận xét:

Ta thấy rằng vì những người có cùng điểm số sẽ cùng hạng, do đó tương ứng với mỗi giá trị điểm số, ta sẽ chỉ có một giá trị thứ hạng cố định.
Để nhanh chóng truy xuất được thứ hạng của một học sinh, hay nói cách khác là trả lời cho câu hỏi với điểm số như vậy thì học sinh đó sẽ được hạng bao nhiêu, ta lập bằng lưu thứ hạng theo điểm số.
Nếu sắp điểm số theo thứ tự từ cao đến thấp, thì thứ hạng của một điểm số chính bằng vị trí mà điểm số đó xuất hiện lần đầu tiên (vị trí tính từ 11).
Ta có cách giải của bài này như sau:

Bước 1: Đưa thông tin điểm số vào một mảng - mảng này sẽ là mảng gốc mà ta sẽ duyệt lại cuối cùng để đưa ra kết quả.
Bước 2: Sao chép thông tin điểm số trong mảng gốc vào một mảng mới và sắp xếp theo thứ tự từ cao đến thấp.
Bước 3: Khởi tạo mảng lưu thứ hạng theo điểm số với giá trị mặc định bằng 00.
Bước 4: Sử dụng vòng lặp ii duyệt qua điểm số đã được sắp xếp trên:
Nếu thứ hạng của điểm số hiện tại chưa được ghi nhận (tức thứ hạng bằng 00):
Cập nhật thứ hạng của điểm số đó bằng (1 + i)(1+i).
Ngược lại, không làm gì.
Bước 5: Duyệt lại mảng gốc ban đầu, in ra thứ hạng theo điểm số tương ứng.
Độ phức tạp: O(nlogn)O(nlogn) với nn là số lượng sinh viên tham gia kiểm tra.
 */
public class GukizAndContest {
    public static void main(String[] args) {
        // write your code here
        int n;

        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        int[] rating = new int[n];
        int[] sorted = new int[n];
        int[] result = new int[n];
        for (int i = 0; i < n; i++) {
            int value = sc.nextInt();
            rating[i] = value;
            sorted[i] = value;
        }


        Arrays.sort(sorted);
        for (int i = 0; i < n; i++) {
            for (int j = n - 1; j >= 0; j--) {
                if (sorted[j] <= rating[i]) {
                    result[i] = n -1 - j;
                    break;
                }
            }
            result[i] = result[i] + 1;
        }

        for(int i = 0;i<n;i++) {
            System.out.print(result[i] + " ");
        }
    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        ArrayList<Integer> ratings = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            ratings.add(sc.nextInt());
        }

        ArrayList<Integer> sorted_ratings = new ArrayList<>(ratings);
        Collections.sort(sorted_ratings, Collections.reverseOrder());
        int[] ranked = new int[2005];

        for (int i = 0; i < n; i++) {
            int rating = sorted_ratings.get(i);
            if (ranked[rating] == 0) {
                ranked[rating] = i + 1;
            }
        }

        for (int rating : ratings) {
            System.out.print(ranked[rating] + " ");
        }
    }
}
 */