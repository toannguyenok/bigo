package toan.bigo.sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
Nhận xét:

Vì lượng trà châm cho khách nam luôn nhiều hơn nữ, do đó để tối ưu lượng trà mà mỗi vị khách nhận được, ta sẽ dành nn ly có dung tích nhỏ nhất cho khách nữ và nn ly còn lại có dung tích lớn hơn cho khách nam.
Mặt khác, mỗi khách nữ đều nhận được một lượng trà như nhau, do đó lượng trà tối đa mà ta có thể châm cho khách nữ sẽ bằng dung tích của ly trà nhỏ nhất trong số nn ly nhỏ nhất - ta tạm gọi là xx. Tương tự, lượng trà tối đa có thể châm cho khách nam sẽ bằng dung tích của ly trà nhỏ nhất trong số nn ly lớn hơn - tạm gọi là yy.
Gọi mm là lượng trà mà ta sẽ rót cho mỗi khách nữ. Lượng trà được châm cho nam phải nhiều gấp đôi nữ nên có thể được biểu diễn là 2*m2∗m. Ta có điều kiện sau: m \le xm≤x và 2*m \le y2∗m≤y. Từ đây suy ra được mm lớn nhất khi m = min(x, y/2)m=min(x,y/2).
Với mm vừa tìm được, ta suy ra tổng lượng trà cần chuẩn bị chính bằng m*n + 2*m*n = 3*m*nm∗n+2∗m∗n=3∗m∗n. Tuy nhiên, tổng lượng trà vừa tính có thể vượt quá dung tích tối đa của ấm trà, do đó ta cần phải so sánh để đưa ra kết quả phù hợp.
Như vậy, ta hình thành các bước giải của bài này như sau:

Bước 1: Đưa thông tin của 2*n2∗n ly trà vào một mảng.
Bước 2: Sắp xếp mảng các ly trà theo dung tích tăng dần. Như vậy, xx sẽ nằm ở vị trí đầu tiên và yy sẽ nằm ở vị trí thứ nn trong dãy.
Bước 3: Tính lượng trà tối đa mà ta có thể rót cho mỗi khách nữ là m = min(x, y/2)m=min(x,y/2). Dựa vào mm, tính tổng lượng trà cần chuẩn bị bằng công thức 3*m*n3∗m∗n.
Bước 4: So sánh tổng lượng trà đã tính được ở trên với dung tích tối đa của ấm và in kết quả.
Độ phức tạp: O(nlogn)O(nlogn) với nn là số lượng khách nam/nữ.
 */
public class PashaAndTea {

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(), w = sc.nextInt();
        ArrayList<Integer> cups = new ArrayList<>();

        for (int i = 0; i < n * 2; i++) {
            cups.add(sc.nextInt());
        }

        Collections.sort(cups);

        double m = Math.min(1.0 * cups.get(0), 1.0 * cups.get(n) / 2);
        double max_amount = 3 * m * n;
        System.out.print(Math.min(max_amount, w));
    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.Math;
import java.util.Collections;

public class Main {
	public static void main(String[] agrs) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt(), w = sc.nextInt();
		ArrayList<Integer> cups = new ArrayList<>();

		for (int i = 0; i < n * 2; i++) {
			cups.add(sc.nextInt());
		}

		Collections.sort(cups);

		double m = Math.min(1.0 * cups.get(0), 1.0 * cups.get(n) / 2);
		double max_amount = 3 * m * n;
		System.out.print(Math.min(max_amount, w));
	}
}
 */
