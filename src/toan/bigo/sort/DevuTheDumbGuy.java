package toan.bigo.sort;

import java.util.Arrays;
import java.util.Scanner;

/*
Nhận xét:

Vì càng về sau tốc độ đọc càng được cải thiện, do đó để tối ưu thời gian học ta sẽ ưu tiên đọc các môn có ít chương trước và để dành các môn nhiều chương hơn học sau.
Như vậy, cách giải của bài này như sau:

Bước 1: Đưa thông tin số chương mỗi môn cần đọc vào một mảng.
Bước 2: Sắp xếp mảng tăng dần.
Bước 3: Lần lượt duyệt qua các môn học với số chương từ nhỏ đến lớn:
Ở mỗi môn, ta cần một khoảng thời gian bằng (x \timesx× số chương của môn đó) để hoàn tất. Cộng dồn thời gian này vào một biến kết quả lưu tổng thời gian tối thiểu.
Sau khi học xong một môn thì tốc được cải thiện, do đó ta giảm xx đi 11 giờ. Tuy nhiên, ta cần kiểm tra xem xx đã đạt mức tối thiểu là 11 giờ/chương chưa. Nếu rồi thì ta không cần phải giảm thêm nữa.
Bước 3: In kết quả.
Độ phức tạp: O(nlogn)O(nlogn) với nn là số môn học.
 */
public class DevuTheDumbGuy {

    public static void main(String[] args) {
        // write your code here
        int n;
        long x;


        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        x = sc.nextLong();
        long[] chapter = new long[n];
        for (int i = 0; i < n; i++) {
            chapter[i] = sc.nextInt();
        }

        long total = 0;
        Arrays.sort(chapter);
        for(int i = 0;i<n;i++)
        {
            total = total + x * chapter[i];
            x = Math.max(1,x - 1);
        }

        System.out.print(total + "");
    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int x = sc.nextInt();
		ArrayList<Integer> c = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			c.add(sc.nextInt());
		}

		Collections.sort(c);

		long min_time = 0;

		for (int chapters : c) {
			min_time += 1L * chapters * x;
			if (x > 1) {
				x--;
			}
		}

		System.out.print(min_time);
	}
}
 */