package toan.bigo;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class DeepLinkCount {

    private static final int BASE = 257;
    private static final int TABLE_SIZE = (int) (1e4 + 1);

    public static class DeepLink {
        String string;
        long userCount = 1;
        long clickCount = 1;

        public DeepLink(String string, long userCount, long clickCount) {
            this.string = string;
            this.userCount = userCount;
            this.clickCount = clickCount;
        }
    }

    public static void main(String[] args) {
        String userHome = System.getProperty("user.home");
        String csvFile = userHome + "/Downloads/bq-results-20201128-103359-95vp21ej6v3p2.csv";
        ArrayList<DeepLink> input = new ArrayList<>();

        int count = 0;
        try (Scanner scanner = new Scanner(new FileReader(csvFile))) {
            while (scanner.hasNextLine()) {
                count++;
                String stringTemp = scanner.nextLine();
                if (count > 1) {
                    String[] temp = stringTemp.split(",");
                    try {
                        input.add(new DeepLink(temp[0], Long.parseLong(temp[1]), Long.parseLong(temp[2])));
                    } catch (Exception e) {

                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Trie trie = new Trie();
        ArrayList<DeepLink> output = cleanData(input);
        for (int i = 0; i < output.size(); i++) {
            trie.addWord(output.get(i));
        }
        String t = "";
        System.out.println(trie.getCount("use_card"));
    }

    public static ArrayList<DeepLink> cleanData(ArrayList<DeepLink> input) {
        ArrayList<DeepLink> result = new ArrayList<>();
        for (int i = 0; i < input.size(); i++) {
            DeepLink temp = input.get(i);
            String stringTemp = temp.string;
            if (stringTemp.contains("one://vinid.net/")) {
                stringTemp = stringTemp.replace("one://vinid.net/", "");
            }
            if (stringTemp.contains("one://superapp/")) {
                stringTemp = stringTemp.replace("one://superapp/", "");
            }
            temp.string = stringTemp;
            result.add(temp);
        }
        return result;
    }

    private static class Node {

        public Node[] child;
        public String string = "";
        long userCount;
        long clickCount;

        public Node(String string, long userCount, long clickCount) {
            child = new Node[TABLE_SIZE];
            this.string = string;
            this.userCount = userCount;
            this.clickCount = clickCount;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return Objects.equals(string, node.string);
        }

        @Override
        public int hashCode() {
            return Objects.hash(child, string, userCount, clickCount);
        }
    }

    private static class Trie {

        private Node root;

        public Trie() {
            root = new Node("root", 0, 0);
        }

        public void addWord(DeepLink deepLink) {
            String link = deepLink.string;
            if (!link.isBlank()) {
                Node temp = root;
                String[] arrayWord = link.split("/");
                for (int i = 0; i < arrayWord.length; i++) {
                    int index = polyHash(arrayWord[i]);
                    Node x;
                    if (temp.child[index] == null) {
                        x = new Node(arrayWord[i], deepLink.userCount, deepLink.clickCount);
                    } else {
                        x = temp.child[index];
                        x.userCount = x.userCount + deepLink.userCount;
                        x.clickCount = x.clickCount + deepLink.clickCount;
                    }
                    temp.child[index] = x;
                    temp = x;
                }
            }
        }

        public long getCount( String word)
        {
            String[] arrayWord = word.split("/");
            Node node = root;
            for(int i = 0;i< arrayWord.length;i++)
            {
                node = getNode(node,arrayWord[i]);
            }
            return node.clickCount;
        }

        public Node getNode(Node node, String word)
        {
            return node.child[polyHash(word)];
        }
    }

    private static int polyHash(String keys) {
        int hashValue = 0;
        for (int i = 0; i < keys.length(); i++)
            hashValue = ((hashValue * BASE + (keys.charAt(i) - 'a' + 1)) % TABLE_SIZE + TABLE_SIZE) % TABLE_SIZE;
        return hashValue;
    }

}
