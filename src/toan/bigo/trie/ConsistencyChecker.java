package toan.bigo.trie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class ConsistencyChecker {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int queries = sc.nextInt();
        ArrayList<String> ans = new ArrayList<>();
        for (int q = 0; q < queries; q++) {
            int n;
            n = sc.nextInt();
            Trie trie = new Trie();
            boolean consistency = true;
            for (int i = 0; i < n; i++) {
                String word = sc.next();
                boolean temp = trie.addWord(word);
                if (!temp) {
                    consistency = false;
                }
            }
            if (consistency) {
                ans.add("YES");
            } else {
                ans.add("NO");
            }
        }
        for (int i = 0; i < ans.size(); i++) {
            System.out.println("Case " + (i + 1) + ": " + ans.get(i));
        }
    }

    private static class Node {
        static final int MAX = 11;
        public Node[] child;
        int countWorld;

        public Node() {
            countWorld = 0;
            child = new Node[MAX];
        }
    }

    private static class Trie {
        private Node root;

        public Trie() {
            root = new Node();
        }

        public boolean addWord(String s) {
            int index = -1;
            Node temp = root;
            for (int i = 0; i < s.length(); i++) {
                index = Integer.parseInt(String.valueOf(s.charAt(i)));
                if (temp.child[index] == null) {
                    Node x = new Node();
                    temp.child[index] = x;
                }
                temp = temp.child[index];
                if (temp.countWorld > 0) {
                    return false;
                }
            }
            for (int j = 0; j < temp.child.length; j++) {
                if (temp.child[j] != null) {
                    return false;
                }
            }
            temp.countWorld++;
            return true;
        }
    }
}
