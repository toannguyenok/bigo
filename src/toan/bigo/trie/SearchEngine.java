package toan.bigo.trie;

import java.util.ArrayList;
import java.util.Scanner;

public class SearchEngine {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n, m;
        n = sc.nextInt();
        m = sc.nextInt();
        Trie trie = new Trie();
        ArrayList<Integer> ans = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            String word = sc.next();
            int priority = sc.nextInt();
            trie.addWord(word, priority);
        }
        for (int i = 0; i < m; i++) {
            String word = sc.next();
            ans.add(trie.findWord(word));
        }
        for (int i = 0; i < ans.size(); i++) {
            System.out.println("" + ans.get(i));
        }
    }

    private static class Node {
        static final int MAX = 52;
        public Node[] child;
        int priority;

        public Node() {
            priority = -1;
            child = new Node[MAX];
        }
    }

    private static class Trie {
        private Node root;

        public Trie() {
            root = new Node();
        }

        public void addWord(String s, int priority) {
            int ch;
            Node temp = root;
            for (int i = 0; i < s.length(); i++) {
                ch = s.charAt(i);
                int index;
                if (ch > 'Z') {
                    index = ch - 'a' + 26;
                } else {
                    index = ch - 'A';
                }
                if (temp.child[index] == null) {
                    Node x = new Node();
                    temp.child[index] = x;
                }
                temp = temp.child[index];
                temp.priority = Math.max(temp.priority, priority);
            }
        }

        public int findWord(String s) {
            int ch;
            Node temp = root;
            for (int i = 0; i < s.length(); i++) {
                ch = s.charAt(i);
                int index;
                if (ch > 'Z') {
                    index = ch - 'a' + 26;
                } else {
                    index = ch - 'A';
                }
                if (temp.child[index] == null) {
                    temp.child[index] = new Node();
                }
                temp = temp.child[index];
            }
            return temp.priority;
        }
    }
}
