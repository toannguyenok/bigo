package toan.bigo.trie;

import java.util.ArrayList;
import java.util.Scanner;

public class Contacts {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        ArrayList<Integer> ans = new ArrayList<>();
        Trie trie = new Trie();
        for (int i = 0; i < n; i++) {
            String cmd = sc.next();
            String word = sc.next();
            if (cmd.equals("add")) {
                trie.addWord(word);
            } else {
                ans.add(trie.findWord(word));
            }
        }

        for (int i = 0; i < ans.size(); i++) {
            System.out.println("" + ans.get(i));
        }
    }

    private static class Node {
        static final int MAX = 26;
        public Node[] child;
        int countWorld;

        public Node() {
            countWorld = 0;
            child = new Node[MAX];
        }
    }

    private static class Trie {
        public static final int MAX = 26;
        private Node root;

        public Trie() {
            root = new Node();
        }

        public void addWord(String s) {
            int ch;
            Node temp = root;
            for (int i = 0; i < s.length(); i++) {
                ch = s.charAt(i) - 'a';
                if (temp.child[ch] == null) {
                    Node x = new Node();
                    temp.child[ch] = x;
                }
                temp = temp.child[ch];
                temp.countWorld++;
            }
        }

        public int findWord(String s) {
            int ch;
            Node temp = root;
            for (int i = 0; i < s.length(); i++) {
                ch = s.charAt(i) - 'a';
                if (temp.child[ch] == null) {
                    return 0;
                }
                temp = temp.child[ch];
            }
            return temp.countWorld;
        }
    }
}
