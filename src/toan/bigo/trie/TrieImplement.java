package toan.bigo.trie;

public class TrieImplement {

    public static void main(String[] args) {

    }

    private static class Node {
        static final int MAX = 26;
        public Node[] child;
        int countWorld;

        public Node() {
            countWorld = 0;
            child = new Node[MAX];
        }
    }

    private static class Trie {
        public static final int MAX = 26;
        private Node root;

        public Trie() {
            root = new Node();
        }

        public void addWord(String s) {
            int ch;
            Node temp = root;
            for (int i = 0; i < s.length(); i++) {
                ch = s.charAt(i) - 'a';
                if (temp.child[ch] == null) {
                    Node x = new Node();
                    temp.child[ch] = x;
                }
                temp = temp.child[ch];
            }
            temp.countWorld++;
        }

        public boolean findWord(String s) {
            int ch;
            Node temp = root;
            for (int i = 0; i < s.length(); i++) {
                ch = s.charAt(i) - 'a';
                if (temp.child[ch] == null) {
                   return false;
                }
                temp = temp.child[ch];
            }
            return temp.countWorld > 0;
        }
    }
}
