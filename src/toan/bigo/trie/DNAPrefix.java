package toan.bigo.trie;

import java.util.ArrayList;
import java.util.Scanner;

public class DNAPrefix {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int queries = sc.nextInt();
        ArrayList<Integer> ans = new ArrayList<>();
        for (int q = 0; q < queries; q++) {
            int n;
            n = sc.nextInt();
            Trie trie = new Trie();
            int result = -1;
            for (int i = 0; i < n; i++) {
                String word = sc.next();
                result = Math.max(result, trie.addWord(word));
            }
            ans.add(result);
        }
        for (int i = 0; i < ans.size(); i++) {
            System.out.println("Case " + (i + 1) + ": " + ans.get(i));
        }
    }

    private static class Node {
        static final int MAX = 26;
        public Node[] child;
        int countWorld;

        public Node() {
            countWorld = 0;
            child = new Node[MAX];
        }
    }

    private static class Trie {
        public static final int MAX = 26;
        private Node root;

        public Trie() {
            root = new Node();
        }

        public int addWord(String s) {
            int result = -1;
            int ch;
            Node temp = root;
            for (int i = 0; i < s.length(); i++) {
                ch = s.charAt(i) - 'A';
                if (temp.child[ch] == null) {
                    Node x = new Node();
                    temp.child[ch] = x;
                }
                temp = temp.child[ch];
                temp.countWorld++;
                result = Math.max(result, (i + 1) * temp.countWorld);
            }
            return result;
        }
    }
}
