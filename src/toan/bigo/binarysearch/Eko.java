package toan.bigo.binarysearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Eko {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        long expectCut = sc.nextLong();
        ArrayList<Integer> input = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            input.add(sc.nextInt());
        }
        Collections.sort(input);
        int start = 0;
        int end = input.get(input.size() - 1);
        int result = findResult(input, start, end, expectCut);
        System.out.println(result);
    }

    private static int findResult(ArrayList<Integer> input, int start, int end, long expectCut) {
        int result = 0;
        while (start <= end) {
            int mid = (start + end) / 2;
            long cutted = 0;
            for (int i = 0; i < input.size(); i++) {
                if (input.get(i) - mid > 0) {
                    cutted += input.get(i) - mid;
                }
            }
            if (cutted > expectCut) {
                start = mid + 1;
                if (mid > result) {
                    result = mid;
                }
            } else if (cutted < expectCut) {
                end = mid - 1;
            } else {
                result = mid;
                return result;
            }
        }
        return result;
    }
}
