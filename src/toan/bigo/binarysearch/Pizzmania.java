package toan.bigo.binarysearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Pizzmania {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> ans = new ArrayList<>();
        int testCase = sc.nextInt();
        for (int k = 0; k < testCase; k++) {
            int n = sc.nextInt();
            ArrayList<Integer> input = new ArrayList<>();
            int costPerPiz = sc.nextInt();
            for (int i = 0; i < n; i++) {
                input.add(sc.nextInt());
            }
            Collections.sort(input);
            ans.add(findCount(input, 0, input.size() - 1, costPerPiz));
        }
        for (int i = 0; i < ans.size(); i++) {
            System.out.println(ans.get(i));
        }
    }

    private static int findCount(ArrayList<Integer> intput, int left, int right, int cost)
    {
        int count = 0;
        while (left < right)
        {
            int total = intput.get(left) + intput.get(right);
            if(total == cost)
            {
                count++;
                left = left + 1;
                right = right - 1;
            }else if(total < cost)
            {
                left = left + 1;
            }else
            {
                right = right - 1;
            }
        }
        return count;
    }

}
