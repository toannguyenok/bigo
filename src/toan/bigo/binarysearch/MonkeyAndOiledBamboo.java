package toan.bigo.binarysearch;

import java.util.ArrayList;
import java.util.Scanner;

public class MonkeyAndOiledBamboo {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> ans = new ArrayList<>();
        int testCase = sc.nextInt();
        for (int t = 0; t < testCase; t++) {
            int n = sc.nextInt();
            ArrayList<Integer> input = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                input.add(sc.nextInt());
            }
            int left = 0;
            int right = input.get(input.size() - 1);
            int k = 0;
            while (left <= right) {
                int mid = (int) ((left + right) / 2);
                if (checkIsValidK(input, mid)) {
                    right = mid -1;
                    k = mid;
                } else {
                    left = mid + 1;
                }
            }
            ans.add(k);
        }
        for (int i = 0; i < ans.size(); i++) {
            System.out.println("Case " + (i + 1) + ": " + ans.get(i));
        }
    }

    private static boolean checkIsValidK(ArrayList<Integer> input, int k) {
        int last = 0;
        for (int i = 0; i < input.size(); i++) {
            int jump = input.get(i) - last;
            if (jump > k) {
                return false;
            } else if (jump == k) {
                k--;
            }
            last = input.get(i);
        }
        if (k >= 0) {
            return true;
        }
        return false;
    }
}
