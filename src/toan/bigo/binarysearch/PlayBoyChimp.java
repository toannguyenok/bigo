package toan.bigo.binarysearch;

import java.util.ArrayList;
import java.util.Scanner;

public class PlayBoyChimp {

    private static int lowerBound(ArrayList<Integer> input, int left, int right, int x) {
        int pos = right;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (input.get(mid) >= x) {
                pos = mid;
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return pos;
    }

    private static int upperBound(ArrayList<Integer> input, int left, int right, int x) {
        int pos = right;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (input.get(mid) > x) {
                pos = mid;
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return pos;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        ArrayList<Integer> input = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            input.add(sc.nextInt());
        }
        int queries = sc.nextInt();
        ArrayList<Result> ans = new ArrayList<>();
        for (int i = 0; i < queries; i++) {
            int x = sc.nextInt();
            int left = 0;
            int right = input.size() - 1;
            int lowerIndex = lowerBound(input, left, right, x);
            int upperIndex = upperBound(input, left, right, x);
            int lower = -1;
            int upper = -1;
            if (input.get(lowerIndex) >= x) {
                lowerIndex = lowerIndex - 1;
            }
            if (lowerIndex >= 0 && input.get(lowerIndex) < x) {
                lower = input.get(lowerIndex);
            }

            if (input.get(upperIndex) > x) {
                upper = input.get(upperIndex);
            }
            ans.add(new Result(lower, upper));
        }
        for (int i = 0; i < ans.size(); i++) {
            System.out.println(ans.get(i).getLowerValue() + " " + ans.get(i).getUpperValue());
        }
    }

    private static class Result {
        int lower;
        int upper;

        public Result(int lower, int upper) {
            this.lower = lower;
            this.upper = upper;
        }

        public String getUpperValue() {
            if (upper == -1) {
                return "X";
            }
            return upper + "";
        }

        public String getLowerValue() {
            if (lower == -1) {
                return "X";
            }
            return lower + "";
        }

    }
}
