package toan.bigo.binarysearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class WhereIsTheMarble {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        ArrayList<ArrayList<String>> ans = new ArrayList<>();
        while (true) {
            int n = sc.nextInt();
            int queries = sc.nextInt();
            if (n == 0 && queries == 0) {
                break;
            }

            ArrayList<Integer> input = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                input.add(sc.nextInt());
            }
            Collections.sort(input);
            ArrayList<String> ansInTestCase = new ArrayList<>();
            int left = 0;
            int right = input.size() - 1;
            for (int i = 0; i < queries; i++) {
                int x = sc.nextInt();

                int position = binarySearchFirst(input, left, right, x);
                if (position < 0) {
                    ansInTestCase.add(x + " not found");
                } else {
                    ansInTestCase.add(x + " found at " + (position + 1));
                }
            }
            ans.add(ansInTestCase);
        }
        for (int i = 0; i < ans.size(); i++) {
            System.out.println("CASE# " + (i + 1) + ":");
            for (int j = 0; j < ans.get(i).size(); j++) {
                System.out.println(ans.get(i).get(j));
            }
        }
    }


    private static int binarySearchFirst(ArrayList<Integer> input, int left, int right, int x) {
        if (left <= right) {
            int mid = left + (right - left) / 2;
            if ((mid == left || x > input.get(mid - 1)) && input.get(mid) == x) {
                return mid;
            } else if (x > input.get(mid)) {
                return binarySearchFirst(input, (mid + 1), right, x);
            } else {
                return binarySearchFirst(input, left, (mid - 1), x);
            }
        }
        return -1;
    }
}
