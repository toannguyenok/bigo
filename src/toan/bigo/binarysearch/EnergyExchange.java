package toan.bigo.binarysearch;

import java.util.ArrayList;
import java.util.Scanner;

public class EnergyExchange {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        ArrayList<Integer> input = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            input.add(sc.nextInt());
        }
        input.sort((o1, o2) -> o2 - o1);
        float left = input.get(input.size() - 1);
        float right = input.get(0);

        float output = Float.MAX_VALUE;
        while (left < right) {
            float mid = (right - left) / 2f;
            if (mid < Math.pow(10.0, -6.0)) {
                break;
            }
            if (checkIsValidEnergy(input, mid, k)) {
                left = mid;
                output = mid;
            } else {
                right = mid;
            }
        }
        System.out.println(output);
    }

    private static boolean checkIsValidEnergy(ArrayList<Integer> input, float energy, int k) {
        float store = 0;
        for (int i = 0; i < input.size(); i++) {
            if (input.get(i) > energy) {
                store = input.get(i) - energy;
            } else if (input.get(i) < energy) {
                float x = energy - input.get(i);
                store = store - x - (x * k) / 100f;
                if (store < 0f) {
                    return false;
                }
            }

        }
        if (store >= 0f) {
            return true;
        }
        return false;
    }
}
