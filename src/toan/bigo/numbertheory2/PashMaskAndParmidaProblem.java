package toan.bigo.numbertheory2;

/*
Hướng dẫn giải
Nhận xét: Tất cả các giá trị f(1, i, a[i])f(1,i,a[i]) đều có L = 1L=1 và X = a[i]X=a[i]. Như vậy giá trị hàm ff đó có ý nghĩa “ số a[i]a[i] đã xuất hiện bao nhiêu lần trong ii vị trí đầu tiên của dãy?”. Tương tự như vậy với f(j, n, a[j])f(j,n,a[j]), hàm ff này có ý nghĩa “số a[j]a[j] đã xuất hiện bao nhiêu lần tính từ vị trí jj đến cuối của dãy?”.

Chỉ có NN vị trí ii và jj có thể. Như vậy ta có thể tính ra tất cả 2*N2∗N giá trị đó bằng cấu trúc dữ liệu map/dict. Gọi dãy các giá trị của f(1, i, a[i])f(1,i,a[i]) là PP, và f(j, n, a[j])f(j,n,a[j]) là SS. Bài toán ta cần tìm bây giờ là, “Cho 2 dãy số PP và SS. Có bao nhiêu vị trí ii, jj trong đó i < ji<j, và P[i] > S[j]P[i]>S[j].”

Ta sẽ áp dụng tư tưởng Divide and Conquer dựa trên thuật toán Merge Sort để giải quyết bài toán này.

Nhắc lại thuật toán Merge Sort: bắt đầu từ toàn bộ dãy, ta gọi hàm mergeSort(1, N). Với mỗi lần gọi mergeSort(L, R), ta gọi đệ quy trên hai đoạn con mergeSort(L, mid) và mergeSort(mid+1, R). Sau đó ta gộp hai đoạn con đó lại bằng thuật toán tương tự two-pointer. Ta sẽ thực hiện merge sort trên cả hai dãy PP và SS, và lợi dụng việc merge hai dãy để thực hiện việc đếm số lượng cặp.

Xét bài toán sau: “Cho hai dãy số AA và BB đã sắp xếp không giảm, đếm số lượng cặp chỉ số ii, jj sao cho A[i] > B[j]A[i]>B[j]”. Đây là một bài toán giải quyết được bằng thuật toán two-pointer: ta đặt một pointer pp ở dãy AA, và một pointer qq ở dãy BB. Khi ta tiến pointer qq lên một bước, thì ta phải tiến pointer pp lên sao cho A[p] > B[q]A[p]>B[q] thì dừng. Sau đó tất cả các chỉ số ii, jj sao cho j = qj=q, và i >= pi>=p là các cặp chỉ số thỏa. Dễ dàng tính số lượng chỉ số ii thỏa mãn, và cộng vào đáp án.

Ta có thể áp dụng bài toán trên để đưa ra lời giải hoàn chỉnh cho bài toán này. Hai dãy con đã sắp xếp không giảm ở đây là P[L..mid]P[L..mid] và S[mid+1…R]. Do đoạn P[L..mid]P[L..mid] nằm hoàn toàn trước đoạn S[mid+1…R], nên điều kiện i < ji<j được đảm bảo. Sau khi đếm xong, ta merge sort riêng cho mỗi dãy.

Độ phức tạp: O(NlogN)O(NlogN) với NN là độ dài của dãy
 */
public class PashMaskAndParmidaProblem {
}

/*
import java.io.*;
import java.util.*;

public class Main {
    static class FastInput
    {
        final private int BUFFER_SIZE = 1 << 16;

        private DataInputStream din;
        private byte[] buffer;
        private int bufferPointer, bytesRead;

        public FastInput(InputStream in)
        {
            din = new DataInputStream(in);
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }

        public long nextLong() throws Exception
        {
            long ret = 0;
            byte c = read();
            while (c <= ' ') c = read();
            boolean neg = c == '-';
            if (neg) c = read();
            do
            {
             ret = ret * 10 + c - '0';
             c = read();
            } while (c > ' ');
            if (neg) return -ret;
            return ret;
        }

        //reads in the next string
        public String next() throws Exception
        {
            StringBuilder ret =  new StringBuilder();
            byte c = read();
            while (c <= ' ') c = read();
            do
            {
                ret = ret.append((char)c);
                c = read();
            } while (c > ' ');
            return ret.toString();
        }

        public int nextInt() throws Exception
        {
            int ret = 0;
            byte c = read();
            while (c <= ' ') c = read();
            boolean neg = c == '-';
            if (neg) c = read();
            do
            {
                ret = ret * 10 + c - '0';
                c = read();
            } while (c > ' ');
            if (neg) return -ret;
            return ret;
        }

        private void fillBuffer() throws Exception
        {
            bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
            if (bytesRead == -1) buffer[0] = -1;
        }

        private byte read() throws Exception
        {
            if (bufferPointer == bytesRead) fillBuffer();
            return buffer[bufferPointer++];
        }
    }
    public static void merge(int[] f, int l, int mid, int r) {
        int[] res = new int[r - l + 1];
        int i = l, j = mid + 1, k = 0;
        while (i <= mid && j <= r) {
            if (f[i] < f[j]) {
                res[k++] = f[i++];
            }
            else {
                res[k++] = f[j++];
            }
        }
        while (i <= mid) {
            res[k++] = f[i++];
        }
        while (j <= r) {
            res[k++] = f[j++];
        }
        for (i = l; i <= r; ++i) {
            f[i] = res[i - l];
        }
    }
    public static long solve(int[] fl, int[] fr, int l, int r) {
        if (l == r) {
            return 0;
        }

        int mid = (l + r) / 2;
        long res = solve(fl, fr, l, mid) + solve(fl, fr, mid + 1, r);

        for (int i = l, j = mid + 1; i <= mid; ++i) {
            while (j <= r && fr[j] < fl[i]) {
                ++j;
            }
            res += j - mid - 1;
        }

        merge(fl, l, mid, r);
        merge(fr, l, mid, r);
        return res;
    }
	public static void main(String args[]) throws Exception {
        FastInput sc = new FastInput(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        int[] fl = new int[n];
        int[] fr = new int[n];
        Map<Integer, Integer> cnt = new HashMap<>();

        for (int i = 0; i < n; ++i) {
            a[i] = sc.nextInt();
            if (cnt.containsKey(a[i])) {
                cnt.put(a[i], cnt.get(a[i]) + 1);
            }
            else {
                cnt.put(a[i], 1);
            }
            fl[i] = cnt.get(a[i]);
        }

        cnt.clear();
        for (int i = n - 1; i >= 0; --i) {
            if (cnt.containsKey(a[i])) {
                cnt.put(a[i], cnt.get(a[i]) + 1);
            }
            else {
                cnt.put(a[i], 1);
            }
            fr[i] = cnt.get(a[i]);
        }

        System.out.print(solve(fl, fr, 0, n - 1));
    }
}
 */

/*
#include <bits/stdc++.h>
#define INF 1000000000
#define MAXN 1000006
using namespace std;
int b[MAXN];
int c[MAXN];
void Merge(int a[], int l, int r) {
    int nB = 0, nC = 0;
    int mid = (l + r) >> 1;
    for (int i = l; i <= mid; i++) {
        b[nB++] = a[i];
    }
    for (int i = mid + 1; i <= r; i++) {
        c[nC++] = a[i];
    }
    b[nB] = c[nC] = INF;
    int i = 0, j = 0, k = l;
    while (i < nB || j < nC) {
        if (b[i] < c[j]) {
            a[k++] =b [i++];
        } else {
            a[k++] = c[j++];
        }
    }
}

long long Solve(int fl[], int fr[], int l, int r) {
    if (l == r) {
        return 0;
    }

    int mid = (l + r) >> 1;
    long long res = Solve(fl, fr, l, mid) + Solve(fl, fr, mid + 1, r);
    int i = l, j = mid + 1;
    while (i <= mid) {
        while (j <= r && fr[j] < fl[i]) {
            j++;
        }
        res += j - mid - 1;
        i +=1;
    }
    Merge(fl, l, r);
    Merge(fr, l, r);
    return res;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, a[MAXN], fL[MAXN], fR[MAXN];
    map<int, int> cnt1, cnt2;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> a[i];
        cnt1[a[i]] = 0;
        cnt2[a[i]] = 0;
    }

    for (int i = 0; i < n; i++) {
        cnt1[a[i]] = cnt1[a[i]] + 1;
        fL[i] = cnt1[a[i]];
    }
    for (int i = n - 1; i >=0; i--) {
        cnt2[a[i]] = cnt2[a[i]] + 1;
        fR[i] = cnt2[a[i]];
    }
    cout << Solve(fL, fR, 0, n - 1);

}
 */