package toan.bigo.numbertheory2;

import java.util.Scanner;

/*
Hướng dẫn giải
Nhận xét:

Các giá trị Answer(x,y)Answer(x,y) với x=yx=y có thể được suy ra từ Answer(1,1)Answer(1,1) nên không cần phải tính nữa.

Giả sử x<yx<y, đặt d=gcd(x,y)d=gcd(x,y).

Nếu d>1d>1 thì Answer(x,y)Answer(x,y) có thể được suy ra từ Answer(x/d,y/d)Answer(x/d,y/d) nên không cần tính nó.
Nếu d=1d=1 thì không thể suy ra Answer(x,y)Answer(x,y) từ giá trị nào trước đó, nên ta phải tốn 11 phép tính cho Answer(x,y)Answer(x,y) (và tương tự, 11 phép tính cho Answer(y,x)Answer(y,x), vậy là 22 phép tính).
Như vậy, với một giá trị yy cố định, số phép tính Answer(x,y)Answer(x,y) (với x<yx<y) cần thực hiện chính là phi(y)phi(y), tức là số các số bé hơn yy và nguyên tố cùng nhau với yy.

Thuật toán:

Khởi tạo kết quả bằng 11.
Với mỗi giá trị yy từ 22 tới NN, cộng 2 * phi(y)2∗phi(y) vào kết quả.
Độ phức tạp: O(T * N * \sqrt{N})O(T∗N∗√
​N
​
​​ ) với TT là số test. Lưu ý, cần phải cache lại giá trị phi để tránh phải tính phiphi của một số rất nhiều lần, dẫn tới TLE.
 */
public class SendATable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        while (flag) {
            int n = scanner.nextInt();
            if (n == 0) {
                flag = false;
            } else {
                int counter = 1;
                for (int i = 2; i <= n; i++) {
                    counter += 2 * phi(i);
                }
                System.out.println(counter);
            }
        }
    }

    public static int phi(int n) {
        int result = n;
        for (int i = 2; i * i <= n; i++) {
            if (n % i == 0) {
                while (n % i == 0) {
                    n = n / i;
                }
                result = result / i * (i - 1);
            }
        }
        if (n > 1) {
            result = result / n * (n - 1);
        }
        return result;
    }
}
/*
#include <iostream>

using namespace std;

const int N = 50001;
int phi_cache[N] = {0};

int eulerPhi(int n) {
    if (phi_cache[n] > 0) {
        return phi_cache[n];
    }

    int res = n, m = n;
    for (int i = 2; i * i <= n; ++i) {
        if (n % i == 0) {
            while (n % i == 0) {
                n /= i;
            }
			res = res / i * (i - 1);
        }
    }

    if (n > 1) {
        res = res / n * (n - 1);
    }

    return phi_cache[m] = res;
}

int main() {
    int n;
    while (true) {
        cin >> n;
        if (n == 0) {
            break;
        }

        int res = 1;
        for (int y = 2; y <= n; ++y) {
            res += eulerPhi(y) * 2;
        }
        cout << res << endl;
    }

    return 0;
}
 */

/*
import java.util.Scanner;

public class Main {
    private static final int N = 50001;
    private static int[] phi_cache;

    private static int eulerPhi(int n) {
        if (phi_cache[n] > 0) {
            return phi_cache[n];
        }

        int res = n, m = n;
        for (int i = 2; i * i <= n; ++i) {
            if (n % i == 0) {
                while (n % i == 0) {
                    n /= i;
                }
                res = res / i * (i - 1);
            }
        }

        if (n > 1) {
            res = res / n * (n - 1);
        }

        return phi_cache[m] = res;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        phi_cache = new int[N];
        int n;

        while (true) {
            n = sc.nextInt();
            if (n == 0) {
                break;
            }

            int res = 1;
            for (int y = 2; y <= n; ++y) {
                res += eulerPhi(y) * 2;
            }
            System.out.println(res);
        }

        sc.close();
    }
}
 */