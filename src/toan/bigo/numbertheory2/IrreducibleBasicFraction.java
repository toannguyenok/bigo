package toan.bigo.numbertheory2;

import java.util.Scanner;

/*
Phân số \frac{m}{n}
​n
​
​m
​​  tối giản khi và chỉ khi gcd(m, n) = 1gcd(m,n)=1, khi đó mm và nn là nguyên tố cùng nhau. Vậy, để đếm số lượng phân số \frac{m}{n}
​n
​
​m
​​  cơ bản tối giản với 0 \le m < n0≤m<n, ta đếm số lượng số nguyên tố cùng nhau với nn mà nhỏ hơn nn, chính là kết quả của hàm phi Euler của nn.

Độ phức tạp: O(T * \sqrt{n})O(T∗√
​n
​
​​ ) với TT là số lượng test cases.
 */
public class IrreducibleBasicFraction {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        while (flag) {
            int n = scanner.nextInt();
            if (n == 0) {
                flag = false;
            } else {
                System.out.println(phi(n));
            }
        }
    }

    public static int phi(int n) {
        int result = n;
        for (int i = 2; i * i <= n; i++) {
            if (n % i == 0) {
                while (n % i == 0) {
                    n = n / i;
                }
                result = result / i * (i - 1);
            }
        }
        if (n > 1) {
            result = result / n * (n - 1);
        }
        return result;
    }
}

/*
#include <iostream>
using namespace std;

int eulerPhi(int n) {
    int result = n;

    for (int i = 2; i * i <= n; ++i) {
        if (n % i == 0) {
            while (n % i == 0) {
                n /= i;
            }
            result = result / i * (i - 1);
        }
    }

    if (n > 1) {
        result = result / n * (n - 1);
    }

    return result;
}

int main() {
    int n;
    while (cin >> n) {
        if (n == 0) {
            break;
        }
        cout << eulerPhi(n) << endl;
    }
    return 0;
}
 */

/*
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (true) {
            int n = in.nextInt();
            if (n == 0) {
                return;
            }
            System.out.println(eulerPhi(n));
        }
    }

    public static int eulerPhi(int n) {
        int res = n;

        for (int i = 2; i * i <= n; i++) {
            if (n % i == 0) {
                while (n % i == 0) {
                    n /= i;
                }
                res -= res / i;
            }
        }

        if (n > 1) {
            res -= res / n;
        }

        return res;
    }
}
 */