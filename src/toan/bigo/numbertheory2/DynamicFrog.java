package toan.bigo.numbertheory2;

/*
Hướng dẫn giải
Để đơn giản, ta xem bờ bên trái và bờ bên phải là hai tảng đá lớn có tọa độ lần lượt là 00 và DD.

Ta nhận xét rằng, bước di chuyển tối ưu của chú ếch sẽ luôn đi qua các tảng đá lớn. Thật vậy, gọi x_a,x_bx
​a
​​ ,x
​b
​​  là tọa độ của hai tảng đá liên tiếp trên bước di chuyển tối ưu của chú ếch. Giả sử tồn tại một tảng đá lớn có tọa độ x_ix
​i
​​  sao cho x_a<x_i<x_bx
​a
​​ <x
​i
​​ <x
​b
​​  thì:

Nếu không sử dụng tảng đá lớn thì chú ếch sẽ thực hiện bước nhảy độ dài x_b-x_ax
​b
​​ −x
​a
​​ .
Nếu sử dụng tảng đá lớn thì chú ếch sẽ thực hiện hai bước nhảy độ dài lần lượt là x_i-x_ax
​i
​​ −x
​a
​​  và x_b-x_ix
​b
​​ −x
​i
​​ .
Do max(x_i - x_a, x_b - x_i) < x_b - x_amax(x
​i
​​ −x
​a
​​ ,x
​b
​​ −x
​i
​​ )<x
​b
​​ −x
​a
​​  nên việc sử dụng tảng đá lớn sẽ không làm kết quả tồi đi.

Do đó, ta có thể xét hai tảng đá lớn liên tục ll và rr (các tảng đá nằm giữa sẽ là các tảng đá nhỏ), tìm cách nhảy từ tảng đá ll đến tảng đá rr rồi quay trở về tảng đá ll sao cho bước nhảy dài nhất là nhỏ nhất có thể. Sau đó, ta chỉ cần in ra giá trị lớn nhất trong các bước nhảy dài nhất này.

Với một dãy tảng đá x_l,x_{l+1},...,x_rx
​l
​​ ,x
​l+1
​​ ,...,x
​r
​​ , nếu l+1=rl+1=r thì đáp án là x_r-x_lx
​r
​​ −x
​l
​​ . Ngược lại, gọi S = max(x_{i+2} - x_i)S=max(x
​i+2
​​ −x
​i
​​ ) với l \le i \le r-2l≤i≤r−2. Ta nhận xét rằng đáp án không thể nhỏ hơn SS.

Giả sử tồn tại cách nhảy có bước nhảy dài nhất là D\ (D<S)D (D<S). Khi đó, tồn tại chỉ số i \in [l, r-2]i∈[l,r−2] sao cho x_{i+2} - x_i > Dx
​i+2
​​ −x
​i
​​ >D. Từ tảng đá j \leq ij≤i, tảng đá xa nhất về bên phải mà chú ếch có thể nhảy đến là i+1i+1. Từ một vị trí k \geq i+2k≥i+2, tảng đá xa nhất về bên trái mà chú ếch có thể nhảy đến là i+1i+1. Do đó, trong cả hai lượt di chuyển (lượt đi và lượt về) chú ếch đều buộc phải đi qua tảng đá i+1i+1, mâu thuẫn với giả thiết là tảng đá i+1i+1 chỉ được sử dụng một lần.

Ta cũng thấy rằng có thể xây dựng một cách nhảy sao cho bước nhảy dài nhất có độ dài SS. Có hai trường hợp:

r-lr−l chẵn: x_l \to x_{l+2} \to x_{l+4} \to \cdots \to x_{r-2} \to x_r \to x_{r-1} \to x_{r-3} \to \cdots \to x_{l+3} \to x_{l+1} \to x_lx
​l
​​ →x
​l+2
​​ →x
​l+4
​​ →⋯→x
​r−2
​​ →x
​r
​​ →x
​r−1
​​ →x
​r−3
​​ →⋯→x
​l+3
​​ →x
​l+1
​​ →x
​l
​​ .
r-lr−l lẻ: x_l \to x_{l+2} \to x_{l+4} \to \cdots \to x_{r-3} \to x_{r-1} \to x_r \to x_{r-2} \to x_{r-4} \to \cdots \to x_{l+2} \to x_lx
​l
​​ →x
​l+2
​​ →x
​l+4
​​ →⋯→x
​r−3
​​ →x
​r−1
​​ →x
​r
​​ →x
​r−2
​​ →x
​r−4
​​ →⋯→x
​l+2
​​ →x
​l
​​ .
Độ phức tạp: O(n)O(n) với nn là số tảng đá.
 */
public class DynamicFrog {
}

/*
#include <bits/stdc++.h>

using namespace std;

const int MAXN = 105;

int nTest, n, D, x[MAXN];
bool isLarge[MAXN];

int getMinimaxLeap(int l, int r) {
    if (l+1 == r)
        return x[r] - x[l];

    int leap = 0;
    for(int i = l; i < r-1; ++i)
        leap = max(leap, x[i+2] - x[i]);
    return leap;
}

int main() {
    cin >> nTest;
    for(int iTest = 1; iTest <= nTest; ++iTest) {
        cin >> n >> D;
        for(int i = 1; i <= n; ++i) {
            string s;
            cin >> s;

            isLarge[i] = (s[0] == 'B');
            x[i] = stoi(s.substr(2));
        }

        isLarge[0] = true;
        x[0] = 0;

        isLarge[n+1] = true;
        x[n+1] = D;

        int ans = 0;
        int lastLarge = 0;
        for(int i = 1; i <= n+1; ++i) {
            if (isLarge[i]) {
                ans = max(ans, getMinimaxLeap(lastLarge, i));
                lastLarge = i;
            }
        }
        printf("Case %d: %d\n", iTest, ans);
    }
}
 */

/*
import java.util.*;

public class sol{
    public static final int MAX_N = 105;
    public static boolean isLarge[];
    public static int x[], nTest, n, D;

    public static int getMinimaxLeap(int l, int r){
        if (l + 1 == r)
            return x[r] - x[l];
        int leap = 0;
        for (int i = l; i < r - 1; i++)
            leap = Math.max(leap, x[i + 2] - x[i]);
        return leap;
    }
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        nTest = scanner.nextInt();
        for (int iTest = 1; iTest <= nTest; iTest++)
        {
            n = scanner.nextInt();
            D = scanner.nextInt();
            isLarge = new boolean[n + 2];
            x = new int[n + 2];
            for (int i = 1; i <= n; i++)
            {
                String s = scanner.next();
                isLarge[i] = (s.charAt(0) == 'B');
                x[i] = Integer.parseInt(s.substring(2));
            }

            isLarge[0] = true;
            x[0] = 0;
            isLarge[n + 1] = true;
            x[n + 1] = D;

            int ans = 0;
            int lastLarge = 0;
            for (int i = 1; i <= n + 1; i ++)
            {
                if (isLarge[i]){
                    ans = Math.max(ans, getMinimaxLeap(lastLarge, i));
                    lastLarge = i;
                }
            }

            System.out.println("Case " + iTest + ": " + ans);
        }
    }
}
 */
