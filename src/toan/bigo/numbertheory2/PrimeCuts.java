package toan.bigo.numbertheory2;

/*
Hướng dẫn giải
Do giới hạn của bài này nhỏ nên ta sẽ trước khi xử lý bất kì trường hợp nào, ta sẽ dùng sàng Eratosthenes để tìm toàn bộ các số nguyên tố. Lưu ý rằng ở bài này số 11 cũng được xem là một số nguyên tố.
Với mỗi trường hợp, ta sẽ duyệt từ 11 tới NN và bỏ toàn bộ các số nguyên tố vào 11 mảng:
Nếu kích cỡ của mảng bé hơn 2*C - 12∗C−1 thì ta in ra toàn bộ mảng các số nguyên tố ấy.
Nếu ngược lại thì ta sẽ phải in ra 2*C2∗C số ở trung tâm nếu như kích cỡ của mảng là số chẵn và 2*C - 12∗C−1 số ở trung tâm nếu như dãy có kích cỡ là số lẻ. Gọi mm là kích cỡ của mảng các số nguyên tố từ 11 đến nn.
Ở đây ta để ý rằng đoạn các số ở trung tâm sẽ luôn bắt đầu từ m/2 - C + (m \% 2)m/2−C+(m%2), lý do là vì ta sẽ đi từ trung tâm của dãy qua trái CC số, nếu như mm là số lẻ thì m \% 2 = 1m%2=1 và ta sẽ bắt đầu dãy số từ vị trí m/2 - C + 1m/2−C+1, nếu như mm là số chẵn thì ta bắt đầu từ vị trí m/2 – C
Ngoài ra, ta còn thấy rằng dù mm là chẵn hay lẻ thì index cuối cùng trong đoạn số cần in sẽ luôn là m/2 + C – 1.
Như vậy dãy của ta sẽ đi từ index m/2 - C + (m\%2)m/2−C+(m%2) cho đến m/2 + C -1m/2+C−1.
Độ phức tạp: O(\sqrt{N}*log(log(N)))O(√
​N
​
​​ ∗log(log(N))) với NN đề cho.


 */
public class PrimeCuts {
}

/*
#include <iostream>
#include <vector>
#include <string.h>

using namespace std;

bool isPrime[1001];

int main() {
    memset(isPrime, true, sizeof(isPrime));
    isPrime[0] = false;
    isPrime[1] = true;

    for (int i = 2; i * i <= 1000; ++i) {
        if (isPrime[i]) {
            for (int j = i * 2; j <= 1000; j += i) {
                isPrime[j] = false;
            }
        }
    }

    int n, c;
    while (cin >> n >> c) {
        vector<int> primes;
        for (int i = 1; i <= n; ++i) {
            if (isPrime[i]) {
                primes.push_back(i);
            }
        }

        int m = primes.size();
        cout << n << ' ' << c << ":";

        if (m < (2 * c - 1)) {
            for (int prime : primes) {
                cout << ' ' << prime;
            }
        }
        else {
            for (int i = m / 2 - c + (m % 2); i <= m / 2 + c - 1; ++i) {
                cout << ' ' << primes[i];
            }
        }
        cout << endl << endl;
    }

    return 0;
}
 */

/*
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        boolean[] isPrime = new boolean[1001];
        Scanner sc = new Scanner(System.in);

        Arrays.fill(isPrime, true);
        isPrime[0] = false;
        isPrime[1] = true;

        for (int i = 2; i <= 1000; i++) {
            if (isPrime[i]) {
                for (int j = i * 2; j <= 1000; j += i) {
                    isPrime[j] = false;
                }
            }
        }

        int n, c;
        while (sc.hasNextInt()) {
            n = sc.nextInt();
            c = sc.nextInt();

            ArrayList<Integer> primes = new ArrayList<>();
            for (int i = 1; i <= n; ++i) {
                if (isPrime[i]) {
                    primes.add(i);
                }
            }

            int m = primes.size();
            System.out.print(n + " " + c + ":");

            if (m < (2 * c - 1)) {
                for (int prime : primes) {
                    System.out.print(" " + prime);
                }
            }
            else {
                for (int i = m / 2 - c + (m % 2); i <= m / 2 + c - 1; ++i) {
                    System.out.print(" " + primes.get(i));
                }
            }
            System.out.print("\n\n");
        }

        sc.close();
    }
}
 */