package toan.bigo.exam;

import java.util.Scanner;

/*
Cách 1: Duyệt vòng lặp qua w lần, mỗi lần tăng số tiền mua chuối lên theo biến i. Tính tổng số tiền cần mua. Nếu số tiền cần mua nhỏ hơn n thì xuất ra 0. Ngược lại sẽ xuất ra “số tiền – n”.

Cách 2: Biết rằng để mua được w chuối thì anh ta phải chuẩn bị số tiền là k * 1 + k * 2 + … + k * w. Đặt k làm nhân tử chung, ta được k * (1 + 2 + 3 + … + w). Lúc này ta có thể áp dụng công thức tính tổng các số từ 1 đến n trong toán học: \sum_{k=1}^{n} \frac{n(n+1)}{2}∑
​k=1
​n
​​
​2
​
​n(n+1)
​​

Độ phức tạp: O(w) với w là số lượng chuối muốn mua với cách 1 và O(1) với cách 2.
 */
public class SoldierAndBananas {
    public static void main(String[] args) {

        int k;
        int n;
        int w;

        Scanner sc = new Scanner(System.in);
        k = sc.nextInt();
        n = sc.nextInt();
        w = sc.nextInt();
        int total = 0;
        for (int i = 0; i < w; i++) {
            total = total + (i + 1) * k;
        }
        if (total <= n) {
            System.out.println("0");
        } else {
            int ans = total - n;
            System.out.println(ans);
        }
    }
}
/*
import java.util.*;
import java.io.*;

public class Main
{
    static PrintWriter out;

    public static void main(String[] args) {
        MyScanner in = new MyScanner();
        out = new PrintWriter(new BufferedOutputStream(System.out), true);
        int k,n,w;
        long res = 0;
        k = in.nextInt();
        n = in.nextInt();
        w = in.nextInt();
        for (int i = 1; i <= w; i++)
    	{
                res += i*k;
    	}
    	if (n >= res)
                out.printf("%d",0);
    	else
                out.printf("%d",res - n);
    }
    public static class Pair<F,S>{

        public F getFirst() {
            return first;
        }

        public void setFirst(F first) {
            this.first = first;
        }

        public S getSecond() {
            return second;
        }

        public void setSecond(S second) {
            this.second = second;
        }

        public Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }
        public F first;
        public S second;
    }
    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
                br = new BufferedReader(new InputStreamReader(System.in));
        }
        String next() {
                while (st == null || !st.hasMoreElements()) {
                        try {
                                st = new StringTokenizer(br.readLine());
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                }
                return st.nextToken();
        }
        int nextInt() {
                return Integer.parseInt(next());
        }
        long nextLong() {
                return Long.parseLong(next());
        }
        double nextDouble() {
                return Double.parseDouble(next());
        }
        String nextLine() {
                String str = "";
                try {
                        str = br.readLine();
                } catch (IOException e) {
                        e.printStackTrace();
                }
                return str;
        }
    }
}
 */