package toan.bigo.exam;

import java.util.Scanner;

/*
Do một ký tự chỉ cần xuất hiện hoa hay thường đều được tính là có xuất hiện, do đó để dễ dàng ta chuyển toàn bộ ký tự trong chuỗi thành in hoa (hoặc thường).
Dùng một mảng đánh dấu để kiểm tra các ký tự đã xuất hiện trước đây hay chưa (mảng có kích thước 26 tương ứng với số lượng ký tự trong bảng chữ cái).

Nếu ký tự đó chưa xuất hiện thì tăng biến đếm số lượng ký tự phân biệt lên 1, đồng thời đánh dấu ký tự đó đã xuất hiện rồi.
Ngược lại, ta không làm gì cả.
Cuối cùng chỉ cần kiểm tra biến đếm số lượng ký tự phân biệt có đúng bằng số lượng chữ cái trong bảng chữ cái (bằng 26) hay không. Nếu có in “YES”, ngược lại in “NO”.

Độ phức tạp: O(N) với N là độ dài của chuỗi.
 */
public class Pangram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] symbolCount = new int[26];
        if (n < 26) {
            System.out.println("NO");
        } else {
            String str = sc.next();
            for (int i = 0; i < str.length(); i++) {
                char symbol = str.charAt(i);
                if (symbol >= 65 && symbol <= 90) {
                    symbol = (char) (symbol + 32);
                }
                symbolCount[symbol - (int)'a']++;
            }
            for (int i = 0; i < symbolCount.length; i++) {
                if (symbolCount[i] == 0) {
                    System.out.println("NO");
                    return;
                }
            }
            System.out.println("YES");
        }
    }
}
/*
import java.util.*;

public class Main {
	public static void main(String[] agrs) {
		Scanner sc = new Scanner(System.in);
		boolean[] alphabet = new boolean[26];
		int n = sc.nextInt();
		String text = sc.next().toUpperCase();

		int count = 0;

		for (char c : text.toCharArray()) {
			if (!alphabet[c - 'A']) {
				count++;
				alphabet[c - 'A'] = true;
			}
		}

		System.out.print(count == 26 ? "YES" : "NO");
	}
}
 */