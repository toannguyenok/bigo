package toan.bigo.exam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
Phần tử trung vị là phần tử nằm giữa mảng khi sắp xếp lại, đặc biệt trong bài này mảng luôn có số lượng phần tử là lẻ nên sẽ chỉ có 1 phần tử trung vị.
Do đó ta chỉ cần sắp xếp lại mảng tăng dần (hoặc giảm dần). Phần tử ở vị trí n / 2 (mảng đánh dấu từ 0) chính là kết quả cần tìm.
Độ phức tạp: O(nlogn) với n là số lượng phần tử, trong đó chi phí sắp xếp tốn O(nlogn), việc lấy phần tử ở vị trí n/2 chỉ tốn O(1).
 */
public class FindTheMedian {
    public static void main(String[] args) {

        int n;

        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();

        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int number = sc.nextInt();
            arrayList.add(number);
        }
        Collections.sort(arrayList);
        int ans = arrayList.get((arrayList.size() ) /2);
        System.out.println(ans + "");
    }
}
/*
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int a[] = new int[n];

        for (int i = 0; i < n; i++)
            a[i] = sc.nextInt();

        Arrays.sort(a);
        System.out.print(a[n / 2]);
    }
}
 */