package toan.bigo.exam;

import java.util.*;

/*
Từ ô bắt đầu, lan BFS sang các ô kề nó theo 4 hướng trên dưới trái phải. Mỗi bước đi dùng mảng lưu lại độ dài đường đi từ ô xuất phát S đến ô hiện tại. Ta có thể kết hợp mảng này với ma trận đề cho bằng cách quy ước như sau:

graph[x][y] = 0 nếu ta chưa viếng thăm ô (x, y).
graph[x][y] = -1 nếu tại đó có bom.
graph[x][y] = a với a là độ dài đường đi ngắn nhất từ ô xuất phát S đến ô (x, y), a \geq≥ 1.
Độ phức tạp: O(T * R * C) với T là số lượng bộ dữ liệu, R và C lần lượt là số dòng và cột của từng bộ dữ liệu. Vì trong trường hợp xấu nhất, đồ thị sẽ có R * C đỉnh và mỗi đỉnh sẽ nối với 4 đỉnh khác. Nên tổng quát độ phức tạp thuật toán BFS là O(V + E) thì độ phức tạp sẽ là O(R * C).
 */
public class Bombs {

    private static int[] dx = new int[]{0, 0, 1, -1};
    private static int[] dy = new int[]{1, -1, 0, 0};
    static ArrayList<ArrayList<Integer>> dist = new ArrayList<>();
    static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int row = -1;
        int column = -1;
        while (row != 0 && column != 0) {
            row = sc.nextInt();
            column = sc.nextInt();
            if (row != 0 && column != 0) {
                for (int i = 0; i < row; i++) {
                    graph.add(new ArrayList<>());
                    dist.add(new ArrayList<>());
                    for (int j = 0; j < column; j++) {
                        graph.get(i).add(0);
                        dist.get(i).add(Integer.MAX_VALUE);
                    }
                }
                int bomb = sc.nextInt();
                for (int i = 0; i < bomb; i++) {
                    int rowPosition = sc.nextInt();
                    int numberBomb = sc.nextInt();
                    for (int j = 0; j < numberBomb; j++) {
                        int columnPosition = sc.nextInt();
                        graph.get(rowPosition).set(columnPosition, 1);
                    }
                }
                int rowStart = sc.nextInt();
                int columnStart = sc.nextInt();
                int rowEnd = sc.nextInt();
                int columnEnd = sc.nextInt();
                System.out.println(findShortestReach(graph, row, column, rowStart, columnStart, rowEnd, columnEnd) + "");
                graph.clear();
                dist.clear();
            }
        }
    }

    public static int findShortestReach(ArrayList<ArrayList<Integer>> graph,
                                                       int row,
                                                       int column,
                                                       int rowStart,
                                                       int columnStart,
                                                       int rowEnd,
                                                       int columnEnd) {
        PriorityQueue<Node> heap = new PriorityQueue<>();
        heap.add(new Node(rowStart, columnStart,0));

        while (!heap.isEmpty()) {
            Node currentNode = heap.remove();
            for (int i = 0; i < 4; i++) {
                int newRow = dx[i] + currentNode.row;
                int newColumn = dy[i] + currentNode.column;


                if (newRow >= 0 &&
                        newRow < row  &&
                        newColumn >= 0 &&
                        newColumn < column  &&
                        graph.get(newRow).get(newColumn) == 0) {

                    if (currentNode.w + 1 < dist.get(newRow).get(newColumn)) {
                        dist.get(newRow).set(newColumn, currentNode.w + 1);
                        heap.add(new Node(newRow,newColumn, dist.get(newRow).get(newColumn)));
                    }
                }
            }
        }
        return dist.get(rowEnd).get(columnEnd);
    }

    private static class Node  implements Comparable<Node> {
        int row;
        int column;
        int w;

        public Node(int row, int column, int w) {
            this.row = row;
            this.column = column;
            this.w = w;
        }

        @Override
        public int compareTo(Node o) {
            return this.w - o.w;
        }
    }
}
/*
import java.util.*;

public class Main {
	static int[] dr = {0, 0, 1, -1};
	static int[] dc = {1, -1, 0, 0};
	static int R, C;

	static class Cell {
		int r, c;

		public Cell(int _r, int _c) {
			this.r = _r;
			this.c = _c;
		}
	}

	public static boolean isValid(int r, int c) {
		return r >= 0 && c >= 0 && r < R && c < C;
	}

	public static int BFS(Cell s, Cell f, int[][] graph) {
		Queue<Cell> q = new LinkedList<>();
		q.add(s);
		graph[s.r][s.c] = 1;

		while (!q.isEmpty()) {
			Cell u = q.poll();

			if (u.r == f.r && u.c == f.c) {
				break;
			}

			for (int i = 0; i < 4; i++) {
				int r = u.r + dr[i];
				int c = u.c + dc[i];

				if (isValid(r, c) && graph[r][c] == 0) {
					graph[r][c] = graph[u.r][u.c] + 1;
					q.add(new Cell(r, c));
				}
			}
		}

		return graph[f.r][f.c] - 1;
	}

	public static void main(String[] agrs) {
		Scanner sc = new Scanner(System.in);

		while (true) {
			R = sc.nextInt();
			C = sc.nextInt();

			if (R == 0 && C == 0) {
				break;
			}

			int[][] graph = new int[R][C];
			int rows = sc.nextInt();

			for (int i = 0; i < rows; i++) {
				int row = sc.nextInt();
				int n = sc.nextInt();

				for (int j = 0; j < n; j++) {
					int col = sc.nextInt();
					graph[row][col] = -1;
				}
			}

			Cell s = new Cell(sc.nextInt(), sc.nextInt());
			Cell f = new Cell(sc.nextInt(), sc.nextInt());
			System.out.println(BFS(s, f, graph));
		}
	}
}
 */