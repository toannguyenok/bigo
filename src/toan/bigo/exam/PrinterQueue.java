package toan.bigo.exam;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

/*
Các tác vụ này được xử lý như một hàng đợi nên ta sẽ sử dụng một CTDL hàng đợi (queue) để lưu thông tin mỗi tác vụ bao gồm số thứ tự và độ ưu tiên của tác vụ đó.
Đồng thời để quyết định một tác vụ có được in ấn hay không ta dựa vào độ ưu tiên của tác vụ ấy, do đó ta sử dụng thêm một CTDL hàng đợi ưu tiên (priority queue).
Tiến hành xử lý từng tác vụ trong hàng đợi:

Nếu tác vụ đó có độ ưu tiên trùng với độ ưu tiên lớn nhất nằm ở đầu hàng đợi ưu tiên thì ta sẽ xử lý tác vụ này, đồng thời so sánh số thứ tự của tác vụ đó với vị trí tác vụ của bạn. Nếu trùng thì dừng lại và không in nữa.
Ngược lại, đưa tác vụ đó vào cuối hàng đợi.
Độ phức tạp: O(T * NlogN) với T là số lượng bộ test, N là số lượng tác vụ của từng bộ test.
 */
public class PrinterQueue {
        static class Job {
            int pos, priority;

            public Job(int _pos, int _priority) {
                this.pos = _pos;
                this.priority = _priority;
            }
        }

        public static void main(String[] agrs) {
            Scanner sc = new Scanner(System.in);
            int Q = sc.nextInt();

            while (Q-- > 0) {
                Queue<Job> q = new LinkedList<>();
                PriorityQueue<Integer> pq = new PriorityQueue<>();

                int n = sc.nextInt();
                int m = sc.nextInt();

                for (int i = 0; i < n; i++) {
                    int priority = sc.nextInt();
                    q.add(new Job(i, priority));
                    pq.add(-priority);
                }

                int count = 0;

                while (!pq.isEmpty()) {
                    if (-pq.peek() == q.peek().priority) {
                        count++;

                        if (q.peek().pos == m) {
                            break;
                        }

                        q.remove();
                        pq.remove();
                    }
                    else {
                        q.add(q.poll());
                    }
                }

                System.out.println(count);
            }
        }
}
/*
import java.util.*;

public class Main {
	static class Job {
		int pos, priority;

		public Job(int _pos, int _priority) {
			this.pos = _pos;
			this.priority = _priority;
		}
	}

	public static void main(String[] agrs) {
		Scanner sc = new Scanner(System.in);
		int Q = sc.nextInt();

		while (Q-- > 0) {
			Queue<Job> q = new LinkedList<>();
			PriorityQueue<Integer> pq = new PriorityQueue<>();

			int n = sc.nextInt();
			int m = sc.nextInt();

			for (int i = 0; i < n; i++) {
				int priority = sc.nextInt();
				q.add(new Job(i, priority));
				pq.add(-priority);
			}

			int count = 0;

			while (!pq.isEmpty()) {
				if (-pq.peek() == q.peek().priority) {
					count++;

					if (q.peek().pos == m) {
						break;
					}

					q.remove();
					pq.remove();
				}
				else {
					q.add(q.poll());
				}
			}

			System.out.println(count);
		}
	}
}
 */