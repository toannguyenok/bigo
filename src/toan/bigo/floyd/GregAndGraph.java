package toan.bigo.floyd;

import java.util.ArrayList;
import java.util.Scanner;

public class GregAndGraph {

    private static ArrayList<ArrayList<Integer>> dist = new ArrayList<>();
    private static ArrayList<Integer> removed = new ArrayList<>();
    private static ArrayList<Integer> ans = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            dist.add(new ArrayList<>());
            for (int j = 0; j < n; j++) {
                int weight = sc.nextInt();
                dist.get(i).add(weight);
            }
        }
        for (int i = 0; i < n; i++) {
            removed.add(sc.nextInt() - 1);
        }

        for (int index = n - 1; index >= 0; index--) {
            int k = removed.get(index);
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    dist.get(i).set(j, Math.min(dist.get(i).get(j), dist.get(i).get(k) + dist.get(k).get(j)));
                }
            }

            int result = 0;
            for (int i = index; i < n; i++) {
                int u = removed.get(i);
                for (int j = index; j < n; j++) {
                    int v = removed.get(j);
                    result = result + dist.get(u).get(v);
                }
            }
            ans.add(0,result);
        }
        for(int i = 0;i<ans.size();i++)
        {
            System.out.print(ans.get(i) + " ");
        }
    }
}
