package toan.bigo.floyd;

import java.util.ArrayList;
import java.util.Scanner;

public class EventOrganizer {

    private static ArrayList<ArrayList<Integer>> dist = new ArrayList<>();
    private static final int MAX = 49;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int testCase = sc.nextInt();
        ArrayList<Integer> ans = new ArrayList<>();
        for (int tc = 0; tc < testCase; tc++) {
            dist.clear();
            for (int i = 0; i < MAX; i++) {
                dist.add(new ArrayList<>());
                for (int j = 0; j < MAX; j++) {
                    dist.get(i).add(0);
                }
            }
            int n = sc.nextInt();
            for (int i = 0; i < n; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                int w = sc.nextInt();
                if (w > dist.get(u).get(v)) {
                    dist.get(u).set(v, w);
                }
            }
            floyd();
            ans.add(dist.get(0).get(MAX - 1));
        }
        for (int i = 0; i < ans.size(); i++) {
            System.out.print(ans.get(i) + " ");
        }
    }

    private static void floyd() {
        for (int k = 0; k < MAX; k++) {
            for (int i = 0; i < MAX; i++) {
                for (int j = 0; j < MAX; j++) {
                    if(i <= k && k<= j) {
                        dist.get(i).set(j, Math.max(dist.get(i).get(j), dist.get(i).get(k) + dist.get(k).get(j)));
                    }
                }
            }
        }
    }
}
