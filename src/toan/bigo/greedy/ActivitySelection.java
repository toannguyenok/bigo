package toan.bigo.greedy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ActivitySelection {

    private static class Activity {
        int start;
        int end;

        public Activity(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    public static void main(String[] args) {
    }

    public static ArrayList<Activity> activitySelection(ArrayList<Activity> input) {
        Collections.sort(input, new Comparator<Activity>() {
            @Override
            public int compare(Activity o1, Activity o2) {
                return o1.end - o2.end;
            }
        });

        ArrayList<Activity> result = new ArrayList<>();
        int lastEnd = 0;
        for (int i = 0; i < input.size(); i++) {
            if (input.get(i).start >= lastEnd) {
                result.add(input.get(i));
                lastEnd = input.get(i).end;
            }
        }
        return result;
    }
}
