package toan.bigo.greedy;

import java.util.Arrays;
import java.util.Scanner;

public class MakeJumps2 {

    private static int[] dx = {-2, -2, -1, -1, 1, 1, 2, 2};
    private static int[] dy = {-1, 1, -2, 2, -2, 2, -1, 1};

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        int test = 0;
        while (flag) {
            int changes = scanner.nextInt();
            if (changes == 0) {
                flag = false;
            } else {
                test++;
                boolean[][] graph = new boolean[10][10];
                for (int i = 0; i < graph.length; i++) {
                    Arrays.fill(graph[i], false);
                }
                int totalLen = 0;
                int startCol = 0;
                for (int i = 0; i < changes; i++) {
                    int start = scanner.nextInt();
                    int length = scanner.nextInt();
                    if (i == 0) {
                        startCol = start;
                    }
                    totalLen += length;
                    for (int j = 0; j < length; j++) {
                        graph[i][start + j] = true;
                    }
                }
                int result = totalLen - makeJumps(graph, 0, startCol);
                if (result == 1)
                    System.out.printf("Case %d, %d square can not be reached.\n", test, result);
                else
                    System.out.printf("Case %d, %d squares can not be reached.\n", test, result);
            }
        }
    }

    private static boolean checkDxDy(boolean[][] graph, int x, int y) {
        if (x >= 0 && x < 10 && y >= 0 && y < 10) {
            return graph[x][y];
        }
        return false;
    }


    private static int makeJumps(boolean[][] graph, int x, int y) {
        int path = 0;
        graph[x][y] = false;
        for (int i = 0; i < 8; i++) {
            int newX = x + dx[i];
            int newY = y + dy[i];
            if (checkDxDy(graph, newX, newY)) {
                path = Math.max(path, makeJumps(graph, newX, newY));
            }
        }
        graph[x][y] = true;
        return path + 1;
    }
}
