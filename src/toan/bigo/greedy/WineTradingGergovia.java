package toan.bigo.greedy;

import java.util.ArrayList;
import java.util.Scanner;

/*
Hướng dẫn giải
Nhận xét:

Mỗi người cần mua thì đều chỉ cần mua từ những người gần mình nhất, và cũng tương tự với những người bán. Điều này có nghĩa là, giả sử ta xét từng người từ người đầu tiên, khi ta gặp một người muốn bán thì ta sẽ ưu tiên bán cho những người chưa mua đủ trước đó, nếu còn dư thì ta mới để dành bán cho những người ở sau.

Gọi tổng lượng rượu tích lũy của ii người đầu là SS:

Nếu S < 0S<0 có nghĩa là ta đang tập trung -S−S chai rượu cần bán tại nhà ii.
Nếu S \ge 0S≥0 có nghĩa là ta đang tập trung một số người trước đó có tổng lượng rượu cần mua là SS (ai mua đủ rồi thì có thể quay về).
Khi đi từ nhà ii sang nhà i+1i+1:

Ta di chuyển rượu từ nhà ii sang nhà i+1i+1 (khi đó mỗi chai rượu cần 11 đơn vị năng lượng nên ta cần thêm |S|∣S∣ đơn vị năng lượng vào kết quả.
Ta cộng a_{i+1}a
​i+1
​​  vào S. Nghĩa là nếu a_{i+1} \ge 0a
​i+1
​​ ≥0 thì ta sẽ thêm người i+1i+1 vào nhóm những người đang đợi mua rượu, còn a_{i+1} < 0a
​i+1
​​ <0 thì ta sẽ bán cho một số người ở trước đó đang cần mua, lúc này họ có thể đi về nên SS giảm đi một lượng bằng -a_{i+1}−a
​i+1
​​ , nếu còn dư thì mang đi bán cho các nhà phía sau.
Độ phức tạp: O(n)O(n) với nn là số người trong thị trấn.
 */
public class WineTradingGergovia {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        int n;
        while (flag) {
            n = scanner.nextInt();
            if (n == 0) {
                flag = false;
            } else {
                ArrayList<Integer> input = new ArrayList<>();
                for(int i = 0;i<n;i++)
                {
                    int value = scanner.nextInt();
                    input.add(value);
                }
                long totalUnit = 0;
                long totalWine = 0;
                for(int i = 0;i <n;i++)
                {
                    totalWine = totalWine +  input.get(i);
                    totalUnit = totalUnit + Math.abs(totalWine);
                }
                System.out.println(totalUnit);
            }
        }
    }
}

/*
import java.io.*;
import java.util.*;

public class Main {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
        while (true) {
            int n = sc.nextInt();
            if (n == 0) {
                break;
            }

            long res = 0, s = 0;
            for (int i = 0, x; i < n; ++i) {
                x = sc.nextInt();
                res += Math.abs(s);
                s += x;
            }
            System.out.println(res);
        }
	}
}
 */

/*
#include <iostream>
using namespace std;

int main() {
    int n;
    while (true) {
        cin >> n;
        if (n == 0) {
            break;
        }

        long long res = 0, s = 0;
        for (int i = 0, x; i < n; ++i) {
            res += abs(s);
            cin >> x;
            s += x;
        }

        cout << res << endl;
    }

    return 0;
}
 */