package toan.bigo.greedy;

/*
Hướng dẫn giải
Tóm tắt đề bài
Cho một bàn cờ vua có NN dòng, số dòng và số cột của bàn cờ không vượt quá 1010. Tuy nhiên tại mỗi dòng không phải luôn có đủ các ô như bình thường mà nó chỉ có yy ô bắt đầu từ vị trí $$ nào đó. Hỏi xem nếu dùng 11 quân mã di chuyển trên bàn cờ, thì số ô ít nhất mà ta không thể đi đến được là bao nhiêu, biết rằng mỗi ô chỉ đến tối đa 11 lần trong hình trình, các ô được đến phải nằm trong bàn cờ, trong các bước di chuyển có thể nhảy ra khỏi bàn cờ nhưng vẫn phải kết thúc bước nhảy ở 11 ô thuộc bàn cờ.

Giải thích ví dụ
Bên dưới là một cách đi với độ dài dài nhất (2424 bước), tức còn 44 ô không thể đi đến được. Thứ tự các bước đi tương ứng với các số trong ô.



Lời giải
Ta thấy rằng, để số ô không thể đến được là ít nhất, thì đường đi của con mã phải là dài nhất, như vậy nhiệm vụ ta cần chính là tìm được đường đi dài nhất của con mã.

Tuy nhiên, với thuật toán DFS bình thường thì mỗi ô khi đi qua 11 lần rồi thì sau này sẽ không được duyệt nữa. Nhưng ở bài toán này, sẽ có trường hợp mà hướng duyệt trước sẽ cho ra đường đi xấu, khi ta duyệt sang hướng khác, thì nó sẽ có đường đi tốt hơn, nhưng lại bị chặn bởi các ô đã duyệt trước đó. Ví dụ như trong hình ví dụ trên, nếu tại bước 1919 (ô (3,1)(3,1), tọa độ tính từ 00), mình không di chuyên lên ô 2020 trước mà lại di chuyển đến ô 2222 rồi mới đến ô 2020, thì lúc này ta sẽ hết đường đi, tức chỉ đi được 2121 ô. Thì lúc này, khi quay lại ô 1919, mình không thể đi sang 11 hướng khác (là đi đến ô 2020), vì lúc này ô 2020 đã bị đánh dấu là duyệt rồi. Như vậy, để duyệt được sang ô 2020, thì lúc DFS đến 2222, sau khi duyệt xuống ô 2020 xong, mình phải giải phóng (đánh dấu visited lại cho ô 2020 là falsefalse), thì như vậy từ ô 1919 mình mới xuống được 2020. Tức hàm đệ quy DFS của chúng ta sẽ có cấu trúc như sau:

Function DFS(int u, int v):
// u, v là tọa độ ô đang xét, tương tự như đỉnh v
    visited[u][v] = true;
    Duyệt các ô (x, y) kề với (u, v) theo quân mã:
        nếu ô (x, y) trong giới hạn bàn cờ, có thể đến được và chưa duyệt:
            DFS(x, y);

    // Điểm thay đổi so với thuật DFS cơ bản
    visited[u][v] = false // giải phóng đỉnh (u, v)
Tuy nhiên, trong quá trình duyệt, ta cần phải lưu lại được độ dài đường đi dài nhất của quân mã. Thì lúc này sẽ có 2 cách thực thi:

Thêm một tham số là cnt để lưu lại số bước đi khi đi đến ô (u, v)(u,v) đang xét. Tức hàm DFS sẽ được khai báo thành DFS(int u, int v, int cnt). Thì sau mỗi bước duyệt đến ô (u, v)(u,v) mình chỉ việt cập nhật lại result theo cnt (result khai báo ngoài hàm).
Biến hàm DFS thành $$ hàm có giá trị trả về, và giá trị trả về của DFS(u, v) là độ dài đường đi dài nhất mà tính từ đỉnh (u, v)(u,v). Thì theo như cấu trúc hàm đã nêu ở trên, DFS(u, v) = max(DFS(x, y)) + 1 với mọi cặp (x,y)(x,y) kề với (u, v)(u,v) và có thể duyệt. Để lấy kết quả ta chỉ việc gọi DFS(0,0).
Một lưu ý là nếu kết quả bằng 1, thì xuất ra “square”, còn lại thì xuất ra “squares”.

Độ phức tạp
Rất khó để đánh giá được độ phức tạp cụ thể cho một bài toán BackTracking. Ở đây analysis chỉ đánh giá trong trường hợp xấu nhất có thể. Với mỗi một ô có 88 trạng thái đến hoặc không đến, do đó độ phức tạp là O(8^{mn})O(8
​mn
​​ ). Mỗi ô có 8 cách đi, nhưng những ô nào bị đánh dấu rồi sẽ không đi đến nữa.
 */
public class MakeJumps {
}

/*import java.util.*;
import java.io.*;
import java.lang.*;

public class MKJUMPS {
    static boolean[][] graph;
    static int[] direct_x = { -2, -2, -1, -1, 1, 1, 2, 2 };
    static int[] direct_y = { -1, 1, -2, 2, -2, 2, -1, 1 };

    static boolean check(int x, int y) {
        if (0 <= x && x < 10 && 0 <= y && y < 10)
            return true;
        return false;
    }

    static int DFS(int x, int y) {
        int path = 0;
        graph[x][y] = false;
        for (int i = 0; i < 8; i++) {
            int ux = x + direct_x[i];
            int uy = y + direct_y[i];
            if (check(ux, uy) && graph[ux][uy])
                path = Math.max(path, DFS(ux, uy));
        }
        graph[x][y] = true;
        return path + 1;
    }

    public static void main(String[] args) {
        int test = 1;
        Scanner sc = new Scanner(System.in);
        while (true) {
            graph = new boolean[10][10];
            int n = sc.nextInt();
            int area = 0;
            if (n == 0)
                break;

            int start_col = 0;
            for (int i = 0; i < n; i++) {
                int start = sc.nextInt();
                int len = sc.nextInt();
                if (i == 0)
                    start_col = start;

                area += len;
                for (int j = 0; j < len; j++)
                    graph[i][start + j] = true;
            }
            int result = area - DFS(0, start_col);
            if (result == 1)
                System.out.printf("Case %d, %d square can not be reached.\n", test, result);
            else
                System.out.printf("Case %d, %d squares can not be reached.\n", test, result);
            test++;
        }
    }
}

 */

/*
#include <bits/stdc++.h>

using namespace std;

int n;
int area;
vector<vector<bool>> graph; // graph[i][j] = False -> not used
vector<int> direct_x = {-2, -2, -1, -1, 1, 1, 2, 2};
vector<int> direct_y = {-1, 1, -2, 2, -2, 2, -1, 1};

bool check(int x, int y)
{
    if (0 <= x && x < 10 && 0 <= y && y < 10)
        return true;
    return false;
}

int DFS(int x, int y)
{
    int path = 0;
    graph[x][y] = false;
    for (int i = 0; i < 8; i++)
    {
        int ux = x + direct_x[i];
        int uy = y + direct_y[i];
        if (check(ux, uy) && graph[ux][uy])
            path = max(path, DFS(ux, uy));
    }
    graph[x][y] = true;
    return path + 1;
}

int main()
{
    int test = 1;
    while (true)
    {
        cin >> n;
        area = 0;
        if (n == 0)
            break;
        graph.resize(10, vector<bool>(10, false));
        int start_col = 0;
        for (int i = 0; i < n; i++)
        {
            int start, len;
            cin >> start >> len;
            if (i == 0)
                start_col = start;

            area += len;
            for (int j = 0; j < len; j++)
                graph[i][start + j] = true;
        }
        int result = area - DFS(0, start_col);
        if (result == 1)
            printf("Case %d, %d square can not be reached.\n", test, result);
        else
            printf("Case %d, %d squares can not be reached.\n", test, result);
        graph.clear();
        test++;
    }
}
 */