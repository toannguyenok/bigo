package toan.bigo.greedy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
Để tổng lợi nhuận thu về là lớn nhất, ta sẽ thực hiện đổi dấu trên các số âm theo thứ tự từ bé đến lớn (vì khi đổi dấu số âm càng nhỏ thì kết quả thu được là một số dương càng lớn). Nếu đã đổi dấu toàn bộ số âm mà vẫn chưa đủ kk lần đổi dấu theo yêu cầu, ta xét:

Nếu số lần đổi dấu còn lại là chẵn, ta có thể giữ nguyên lợi nhuận hiện tại bằng cách thực hiện đổi dấu trên một số duy nhất bất kỳ.

Ngược lại nếu số lần đổi dấu còn lại là lẻ, để lợi nhuận giảm đi ít nhất có thể ta sẽ thực hiện đổi dấu trên số nhỏ nhất hiện có trong dãy, tạm gọi là xx. Khi này, tổng lợi nhuận sẽ giảm đi một lượng là 2 * x2∗x.

Độ phức tạp: O(n)O(n) với nn là số lượng số nguyên trong dãy.
 */
public class RomaAndChangingSign {

    public static void main(String[] args) {
        int n;
        int k;
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        k = scanner.nextInt();
        ArrayList<Integer> input = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            int value = scanner.nextInt();
            input.add(value);
        }
        long result = 0;

        for (int i = 0; i < n; i++) {
            if (k == 0) {
                break;
            }
            if (input.get(i) < 0) {
                input.set(i, input.get(i) * -1);
                k--;
            }
        }
        Collections.sort(input);
        if (k != 0) {
            if (k % 2 != 0) {
                int lastItem = input.get(0);
                input.set(0, lastItem * (-1));
            }
        }
        for (int i = 0; i < n; i++) {
           result += input.get(i);
        }
        System.out.println(result);
    }
}

/*
#include <iostream>
#include <math.h>
#include <vector>

using namespace std;

int main() {
	int n, k, res = 0, t, min_income = 1e5;
	vector<int> incomes;
	cin >> n >> k;
	for (int i = 0; i < n; ++i) {
		cin >> t;
		incomes.push_back(t);
		res += t;
		min_income = min(min_income, abs(t));
	}

	int i = 1, idx = 0;
	for (; i <= k; ++i) {
		if(idx >= n || incomes[idx] >= 0)
			break;
		res += incomes[idx] * -2;
		idx++;
	}
	if (i <= k) {
		if ((i - k + 1) % 2 != 0)
			res -= min_income * 2;
	}

	cout << res;
	return 0;
}
 */

/*
import java.util.*;
import java.lang.*;
import java.io.*;

public class S
{
	public static void main (String[] args) throws java.lang.Exception
	{
		Scanner in = new Scanner(System.in);

		int n = in.nextInt();
		int k = in.nextInt();

		int res = 0, min_income = 100000;
		int[] incomes = new int[n];

		for(int i = 0; i < n; ++i) {
			int t = in.nextInt();
			incomes[i] = t;
			res += t;
			min_income = Math.min(min_income, Math.abs(t));
		}

		int i = 1, idx = 0;
		for (; i <= k; ++i) {
			if (idx >= n || incomes[idx] >= 0)
				break;
			res += incomes[idx] * -2;
			idx++;
		}

		if (i <= k) {
			if ((i - k + 1) % 2 != 0)
				res -= min_income * 2;
		}

		System.out.println(res);
	}
}
 */