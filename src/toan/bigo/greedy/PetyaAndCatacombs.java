package toan.bigo.greedy;

import java.util.Scanner;

/*
Hướng dẫn giải
Ở bài này, đề yêu cầu tìm số phòng ít nhất có thể. Ta dễ dàng thấy được căn phòng số 11 sẽ luôn luôn phải đi qua, nhiệm vụ của chúng ta là tìm những căn phòng còn lại. Rõ ràng số phòng đó là tổng số lượng các phút không được ghi nhận lại tính từ phút thứ 00 đến phút thứ n - 1n−1.

Bước 1: Đọc dữ liệu vào, đồng thời dùng một mảng để đánh dấu các phút đã được ghi lại.
Bước 2: Khởi tạo biến count = 1=1 tương ứng với phòng số một chắc chắn được chọn.
Bước 3: Duyệt từ 00 đến n - 1n−1, kiểm tra phút nào chưa được đánh dấu thì tăng count lên 11.
Bước 4: In count là kết quả cần tìm.
Độ phức tạp: O(n)O(n) với nn là số lượng ghi chú.
 */
public class PetyaAndCatacombs {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        boolean[] room = new boolean[n + 1];

        for (int i = 0; i < n; i++) {
            int x = sc.nextInt();
            room[x] = true;
        }

        int count = 1;
        for (int i = 0; i < n; i++) {
            if (!room[i]) {
                count++;
            }
        }

        System.out.println(count);
    }
}

/*
import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        boolean[] room = new boolean [n + 1];

        for (int i = 0; i < n; i++) {
            int x = sc.nextInt();
            room[x] = true;
        }

        int count = 1;
        for (int i = 0; i < n; i++) {
            if (!room[i]) {
                count++;
            }
        }

        System.out.println(count);
    }
}
 */

/*
#include <iostream>
#include <vector>
using namespace std;

int main() {
	int n, x;
	cin >> n;

	vector<int> room(n + 1);
	for (int i = 0; i < n; i++) {
		cin >> x;
		room[x] = true;
	}

	int count = 1;
	for (int i = 0; i < n; i++) {
		if (!room[i]) {
			count++;
		}
	}

	cout << count;
	return 0;
}
 */