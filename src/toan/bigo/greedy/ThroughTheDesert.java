package toan.bigo.greedy;


/*
Hướng dẫn giải
Giải thích

Test case số 1 chỉ có 2 sự kiện, theo đó xe tải cần đi 100km và cần dùng 10 lít xăng. Đây cũng là dung tích nhỏ nhất của thùng xăng để băng qua sa mạc thành công. Ở test case số 2, ta cần 5 lít xăng để đi hết 100km đầu tiên. Từ vị trí 100km đến 200km (cũng là đích đến), ta cần 30 lít xăng. Như vậy, dung tích nhỏ nhất của thùng xăng thỏa yêu cầu là 35. Tương tự với test case số 3, ta cần 77.5 lít xăng để di chuyển từ vị trí bắt đầu đến trạm xăng ở vi trí 50km và cần thêm 81 lít xăng để đi từ trạm xăng đến vạch đích. Vậy để bảo đảm xe có thể đến đích an toàn, dung tích thùng xăng nhỏ nhất phải là 81 lít (vì nếu dung tích thùng xăng nhỏ hơn 81 thì dù ở trạm xăng được bơm đầy thùng, xe cũng không thể đến được vạch đích).

Hướng dẫn giải

Tại mỗi sự kiện diễn ra, ta gọi total là dung tích xe cần có tại mỗi sự kiện đó. Như vậy kết quả bài toán chính là giá trị lớn nhất của các total. Để tính total ta có công thức :

total += (n / 100 + leakCount) * (pivot – lastPivot)
trong đó: n là số lít xăng xe tiêu thụ trên 100km, leakCount là số lần thủng xe, pivot là cột mốc của sự kiện hiện tại và lastPivot chính là cột mốc của sự kiện trước đó.

Ở mỗi sự kiện, ta cập nhật các tham số để tính total như sau:

FUEL: ta chỉ cần cập nhật n bằng cách đọc giá trị n mới vào.
LEAK: tăng leakCount lên 1.
GAS: đổ đầy xăng bằng cách cho total = 0.
MECHANIC: cho leakCount = 0.
GOAL: In ra giá trị lớn nhất của total và gán tất cả các giá trị trên về 0.
 */
public class ThroughTheDesert {
}

/*
import java.text.DecimalFormat;
import java.util.Scanner;

public class Sol{

    private static final DecimalFormat df = new DecimalFormat("0.000");
    private static final String FUEL = "Fuel";
    private static final String GOAL = "Goal";
    private static final String LEAK = "Leak";
    private static final String GAS = "Gas";
    private static final String MECHANIC = "Mechanic";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double total = 0;
        double tank = 0;
        double lastPivot = 0;
        double n = 0;
        double leakCount = 0;
        while (true) {
            String line = scanner.nextLine();
            String[] split = line.split(" ");
            double pivot = Double.parseDouble(split[0]);
            total += (n / 100.0 + leakCount) * (pivot - lastPivot);
            tank = Math.max(tank, total);
            lastPivot = pivot;
            switch (split[1]) {
                case FUEL:
                    n = Double.parseDouble(split[3]);
                    if (n == 0)
                        return;
                    break;
                case LEAK:
                    leakCount++;
                    break;
                case GAS:
                    total = 0;
                    break;
                case MECHANIC:
                    leakCount = 0;
                    break;
                case GOAL:
                    System.out.println(df.format(tank));
                    total = 0;
                    lastPivot = 0;
                    leakCount = 0;
                    n = 0;
                    tank = 0;
                    break;
            }
        }
    }
}
 */

/*
#include <bits/stdc++.h>

using namespace std;

const string FUEL = "Fuel";
const string GOAL = "Goal";
const string LEAK = "Leak";
const string GAS = "Gas";
const string MECHANIC = "Mechanic";

int main()
{
    double total = 0, tank = 0, lastPivot = 0, n = 0, leakCount = 0;
    string s;
    int pivot;

    while (true){
        cin >> pivot >> s;
        total += (n / 100.0 + leakCount) * (pivot - lastPivot);
        tank = max(tank, total);
        lastPivot = pivot;
        if (s == FUEL){
            cin >> s >> n;
            if (n == 0){
                return 0;
            }
        }
        else if (s == LEAK){
            leakCount ++;
        }
        else if (s == GAS) {
            cin >> s;
            total = 0;
        }
        else if (s == MECHANIC) {
            leakCount = 0;
        }
        else
        {
            cout << fixed << setprecision(3) << tank << endl;
            total = 0;
            lastPivot = 0;
            leakCount = 0;
            n = 0;
            tank = 0;
        }
    }
    return 0;
}
 */