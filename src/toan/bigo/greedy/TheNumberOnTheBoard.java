package toan.bigo.greedy;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/*
ận xét:

Nếu tổng chữ số ban đầu đã thỏa điều kiện lớn hơn hoặc bằng số kk → ta không cần phải thay đổi bất cứ chữ số nào trong số ban đầu.

Ngược lại, để tối thiểu số lượng chữ số cần thay đổi thì phải làm cho tổng chữ số tăng lên càng nhanh càng tốt. Để làm được điều này, ta sắp xếp các chữ số trong số nn ban đầu theo thứ tự từ bé đến lớn và lần lượt thay đổi các chữ số thành chữ số lớn nhất là 99 cho đến khi nào tổng các chữ số thỏa điều kiện lớn hơn hoặc bằng số kk. In ra kết quả cần tìm.

Độ phức tạp: O(N \log(N))O(Nlog(N)) với NN là số lượng chữ số của số nn.
 */
public class TheNumberOnTheBoard {

    public static void main(String[] args) {
        String n;
        long k;
        Scanner scanner = new Scanner(System.in);
        k = scanner.nextLong();
        n = scanner.next();
        int sum = 0;
        for (int i = 0; i < n.length(); i++) {
            sum = sum + (n.charAt(i) - '0');
        }
        char[] charArray = n.toCharArray();
        Arrays.sort(charArray);
        String sortedString = new String(charArray);

        int result = 0;
        int i = 0;
        while (sum < k && i < sortedString.length()) {
            if (sortedString.charAt(i) != '9') {
                int temp = 9 - (sortedString.charAt(i) - '0');
                sum += temp;
                result++;
            }
            i++;
        }
        System.out.println(result);
    }
}

/*
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int k;
string n;

int main() {
	cin >> k >> n;

	int digits_sum = 0;
	for (int i = 0; i < n.length(); i++) {
		digits_sum += n[i] - '0';
	}

	sort(n.begin(), n.end());

	int ans = 0;
	while (digits_sum < k) {
		digits_sum += '9' - n[ans];
		ans++;
	}

	cout << ans << endl;
	return 0;
}
 */

/*
import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
		Scanner in = new Scanner(System.in);

		int k = in.nextInt();
		String n = in.next();

		int digits_sum = 0;
		for (int i = 0; i < n.length(); i++) {
			digits_sum += n.charAt(i) - '0';
		}

		n = sort_string(n);

		int ans = 0;
		while (digits_sum < k) {
			digits_sum += '9' - n.charAt(ans);
			ans++;
		}

		System.out.println(ans);
	}

	static String sort_string(String s) {
		char[] chars = s.toCharArray();

		Arrays.sort(chars);
		String sorted = new String(chars);

		return sorted;
	}
}
 */