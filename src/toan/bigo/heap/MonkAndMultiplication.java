package toan.bigo.heap;

import java.util.PriorityQueue;
import java.util.Scanner;

/*
Áp dụng hàng đợi ưu tiên là giải quyết bài này.

Bước 1: Lưu các phần tử đề cho vào một mảng.
Bước 2: Lần lượt duyệt qua các phần tử và bỏ vào hàng đợi ưu tiên.
Nếu như số lượng phần tử trong hàng đợi ưu tiên nhỏ hơn 33 thì ta xuất ra -1−1.
Mỗi lần bỏ phần tử mới vào thì lấy top 33 để nhân với nhau và in ra kết quả.
Độ phức tạp: O(NlogN)O(NlogN) với NN là số lượng phần tử trong mảng.
 */
public class MonkAndMultiplication {

    public static void main(String[] args) {

        int n;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();

        PriorityQueue<Long> heap = new PriorityQueue<>((x, y) -> Long.compare(y, x));

        for (int i = 0; i < n; i++) {
            heap.add(sc.nextLong());
            if (i < 2) {
                System.out.println(-1);
            } else {
                long first = heap.remove();
                long second = heap.remove();
                long third = heap.remove();
                long multiplication = first*second*third;
                System.out.println(multiplication);
                heap.offer(first);
                heap.add(second);
                heap.add(third);
            }
        }
    }
}
/*
import java.util.Scanner;
import java.util.PriorityQueue;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x, n = sc.nextInt();
        PriorityQueue<Integer> pq = new PriorityQueue<>();

        for (int i = 0; i < n; i++) {
            x = sc.nextInt();
            pq.add(-x);

            if (i < 2) {
                System.out.println(-1);
            }
            else {
                int first = -pq.poll();
                int second = -pq.poll();
                int third = -pq.poll();

                System.out.println(1L * first * second * third);

                pq.add(-first);
                pq.add(-second);
                pq.add(-third);
            }
        }
    }
}
 */