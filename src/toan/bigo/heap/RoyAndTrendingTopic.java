package toan.bigo.heap;

import java.util.PriorityQueue;
import java.util.Scanner;

/*
Với mỗi topic bạn sẽ tính toán điểm điểm chênh lệch = new z-score – old z-score. Sau đó lưu vào một cấu trúc topic. Viết thêm một hàm so sánh để phù hợp với yêu cầu đề bài. Sau khi bỏ tất cả topic vào hàng đợi ưu tiên thì sẽ lấy 5 giá trị đầu in ra là kết quả của bài toán.

Độ phức tạp: O(NlogN)O(NlogN) với NN là số lượng topic cần xử lý.
 */
public class RoyAndTrendingTopic {

    public static void main(String[] args) {

        PriorityQueue<Trending> heap = new PriorityQueue<>();

        int topics;
        Scanner sc = new Scanner(System.in);
        topics = sc.nextInt();

        for (int i = 0; i < topics; i++) {
            long id = sc.nextLong();
            long currentZ = sc.nextLong();
            long posts = sc.nextLong();
            long likes = sc.nextLong();
            long comments = sc.nextLong();
            long shares = sc.nextLong();
            long increase = posts*50L + likes*5L  + comments*10L + shares*20L;
            heap.add(new Trending(id,increase , increase - currentZ));
        }
        int counter = 0;
        while (!heap.isEmpty() && counter < 5)
        {
            Trending trending = heap.remove();
            System.out.println(trending.id + " " + trending.zScores);
            counter++;
        }
    }

    private static class Trending implements Comparable<Trending>
    {
        long id;
        long zScores;
        long changeScores;
        public Trending(long id, long zScores, long changeScores)
        {
            this.id = id;
            this.zScores = zScores;
            this.changeScores = changeScores;
        }

        @Override
        public int compareTo(Trending o) {
            if(o.changeScores == this.changeScores)
            {
                return (int) (o.id - this.id);
            }
            return (int) (o.changeScores - this.changeScores);
        }
    }
}
/*
import java.util.*;
import java.io.*;

public class Solution {
    static class Topic {
        int id;
        long old_score, new_score, change;

        public Topic(int _id, long _old_score, long _new_score) {
            id = _id;
            old_score = _old_score;
            new_score = _new_score;
            change = new_score - old_score;
        }
    };

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        PriorityQueue<Topic> pq = new PriorityQueue<>(new Comparator<Topic>() {
            public int compare(Topic a, Topic b) {
                if (a.change < b.change || (a.change == b.change && a.id < b.id)) {
                    return 1;
                }
                else {
                    return -1;
                }
            }
        });

        for (int k = 0; k < n; k++) {
            int id = sc.nextInt();
            long old_score = sc.nextLong();
            long post = sc.nextLong(),          like = sc.nextLong();
            long comment = sc.nextLong(),       share = sc.nextLong();
            long new_score = post * 50 + like * 5 + comment * 10 + share * 20;
            pq.add(new Topic(id, old_score, new_score));
        }

        for (int i = 0; i < 5; i++) {
            Topic t = pq.poll();
            System.out.println(t.id + " " + t.new_score);
        }
    }
}
 */