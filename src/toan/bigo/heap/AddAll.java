package toan.bigo.heap;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Bạn sẽ bỏ toàn bộ NN số vào hàng đợi ưu tiên, mỗi lần cộng hai số hoặc hai nhóm nào lại với nhau bạn sẽ cộng dồn chi phí lại, đồng thời lấy hai số/nhóm đó ra khỏi hàng đợi. Chi phí mới nhận được lại tiếp tục bỏ vào trong hàng đợi ưu tiên. Khi hàng đợi ưu tiên không còn số nào thì kết quả cộng các chi phí lại sẽ là kết quả cần tìm.

Độ phức tạp: O(NlogN)O(NlogN) với NN là chi phí xét toàn bộ các cặp số trong NN số, logNlogN là chi phí push/pop phần tử vào trong hàng đợi ưu tiên
 */
public class AddAll {
    public static void main(String[] args) {

        PriorityQueue<Long> heap = new PriorityQueue<>();
        ArrayList<Long> result = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        int n = -1;
        while (n != 0) {
            n = sc.nextInt();
            if (n != 0) {
                heap.clear();
                for (int i = 0; i < n; i++) {
                    heap.add(sc.nextLong());
                }
                long ans = 0;
                while (heap.size() > 1) {
                    long first = heap.remove();
                    long second = heap.remove();
                    long total = second + first;
                    ans = ans + total;
                    heap.add(total);
                }
                result.add(ans);
            }
        }
        for (long integer : result) {
            System.out.println(integer);
        }
    }
}
/*
import java.util.Scanner;
import java.util.PriorityQueue;

public class Solution {
    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        PriorityQueue<Integer> pq = new PriorityQueue<>();

        while (true) {
            int n = sc.nextInt();

            if (n == 0) {
                break;
            }

            for (int i = 0; i < n; i++) {
                int x = sc.nextInt();
                pq.add(x);
            }

            long ans = 0;

            while (pq.size() > 1) {
                int a = pq.poll();
                int b = pq.poll();
                ans += a + b;
                pq.add(a + b);
            }

            System.out.println(ans);
            pq.remove();
        }
    }
}
 */