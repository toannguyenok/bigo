package toan.bigo.heap;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Hướng dẫn giải: Do yêu cầu của khuyến mãi là lấy phiếu có giá trị lớn nhất và nhỏ nhất nên ta có thể sử dụng 2 heap đồng thời:

Max-heap với top là phiếu có giá trị lớn nhất
Min-heap với top là phiếu có giá trị nhỏ nhất Theo quy định phiếu đã lấy ra thì không bỏ lại vào thùng nên sau khi tính được số tiền phải chi trong ngày của siêu thị bằng cách trừ top của max-heap và min-heap, ta tiến hành pop cả hai phiếu này ra khỏi hàng đợi và tiếp tục xử lý phiếu của những ngày tiếp theo.
Tuy nhiên nếu chỉ làm như vậy thì các phiếu được lấy ra từ max-heap có thể còn trong min-heap và ngược lại. Do đó ta cần đánh dấu lại các phiếu đã được lấy ra khỏi thùng để về sau khi lấy phiếu ra ta bỏ qua những phiếu đã được lấy ra trước đó rồi. Một cách đơn giản để đánh dấu các phiếu đã được lấy ra rồi là ta sẽ đánh dấu dựa trên số thứ tự đơn hàng.

Độ phức tạp: O(N * log(N))O(N∗log(N)) với NN là tổng số phiếu.
 */
public class Promotion {

    private static class Receipt {
        int position;
        int price;

        public Receipt(int position, int price) {
            this.position = position;
            this.price = price;
        }
    }

    static final int MAX = (int) 1e6 + 5;

    public static void main(String[] args) {

        PriorityQueue<Receipt> maxHeap = new PriorityQueue<>(new Comparator<Receipt>() {
            @Override
            public int compare(Receipt o1, Receipt o2) {
                return o2.price - o1.price;
            }
        });

        PriorityQueue<Receipt> minHeap = new PriorityQueue<>(new Comparator<Receipt>() {
            @Override
            public int compare(Receipt o1, Receipt o2) {
                return o1.price - o2.price;
            }
        });

        boolean[] visited = new boolean[MAX];

        Scanner sc = new Scanner(System.in);
        int day = sc.nextInt();
        long total = 0;
        int position = 0;
        for (int i = 0; i < day; i++) {
            int k = sc.nextInt();
            for (int j = 0; j < k; j++) {
                int price = sc.nextInt();
                maxHeap.add(new Receipt(position, price));
                minHeap.add(new Receipt(position, price));
                position++;
            }
            while (visited[maxHeap.peek().position]) {
                maxHeap.remove();
            }
            while (visited[minHeap.peek().position]) {
                minHeap.remove();
            }
            visited[maxHeap.peek().position] = true;
            visited[minHeap.peek().position] = true;
            total += (maxHeap.remove().price - minHeap.remove().price);

        }
        System.out.println(total);
    }
}
/*
import java.util.*;

public class Solution {
    static final int MAX = (int)1e6 + 5;

    static class Bill {
        int number, price;

        public Bill(int _number, int _price) {
            this.number = _number;
            this.price = _price;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean[] taken = new boolean[MAX];
        int n = sc.nextInt();
        int nbills = 0;
        long money = 0;

        PriorityQueue<Bill> maxHeap = new PriorityQueue<>(new Comparator<Bill>() {
            public int compare(Bill a, Bill b) {
                return b.price - a.price;
            }
        });

        PriorityQueue<Bill> minHeap = new PriorityQueue<>(new Comparator<Bill>() {
            public int compare(Bill a, Bill b) {
                return a.price - b.price;
            }
        });

        for (int i = 0; i < n; i++) {
            int k = sc.nextInt();

            for (int j = 0; j < k; j++) {
                int x = sc.nextInt();
                nbills++;

                maxHeap.add(new Bill(nbills, x));
                minHeap.add(new Bill(nbills, x));
            }

            while (taken[maxHeap.peek().number]) {
                maxHeap.remove();
            }

            while (taken[minHeap.peek().number]) {
                minHeap.remove();
            }

            taken[maxHeap.peek().number] = taken[minHeap.peek().number] = true;
            money += maxHeap.poll().price - minHeap.poll().price;
        }

        System.out.print(money);
    }
}
 */