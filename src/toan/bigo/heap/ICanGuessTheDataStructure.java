package toan.bigo.heap;

import java.util.*;
/*Bài này ta chỉ việc đi mô phỏng lại các thao tác ứng với ba cấu trúc được đề cập và kiểm tra.

Đầu tiên, chuẩn bị 33 biến đánh dấu CTDL có thể thuộc 33 loại stackstack, queuequeue hay prioritypriority queuequeue. Ban đầu chưa biết CTDL đó là gì thì ta mặc định 33 biến này đều có giá trị 11 (truetrue). Đồng thời khởi tạo 33 CTDL stackstack, queuequeue và prioritypriority queuequeue.

Với lệnh type = 1type=1, ta đẩy xx vào stackstack, queuequeue và prioritypriority queuequeue.

Với lệnh type = 2type=2, đây là lệnh yêu cầu ta lấy 11 phần tử ra khỏi cấu trúc dữ liệu đó, nên trước khi thực hiện thao tác này, kiểm tra stackstack, queuequeue, prioritypriority queuequeue có rỗng hay không. Nếu như chúng rỗng mà thao tác này lại bảo mình lấy 11 phần tử ra, thì rõ ràng không thể tồn tại cấu trúc dữ liệu này. Do đó ta gán cả 33 biến đánh dấu là 00 (false).

Ngược lại, ta kiểm tra phần tử mà ta lấy ra có bằng xx hay không và cập nhật lại biến đánh dấu.

Kiểm tra các biến đánh dấu và in ra kết quả tương ứng.

Độ phức tạp: O(NlogN)O(NlogN) với NN là số lượng thao tác. Độ phức tạp sẽ đạt cao nhất khi thao tác trên prioritypriority queuequeue.

 */
public class ICanGuessTheDataStructure {

    public static void main(String[] args) {

        PriorityQueue<Integer> heap = new PriorityQueue<>();
        Queue<Integer> queue  = new LinkedList<>();
        Stack<Integer> stack = new Stack<>();
        ArrayList<String> result = new ArrayList<>();
        int isQueue = 1;
        int isStack = 1;
        int isHeap = 1;
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {
            heap.clear();
            queue.clear();
            stack.clear();
            isQueue = isHeap = isStack = 1;
            int n = sc.nextInt();
            for (int i = 0; i < n; i++) {
                int type = sc.nextInt();
                int x = sc.nextInt();
                if (type == 1) {
                    stack.add(x);
                    queue.add(x);
                    heap.add(-x);
                } else {
                    if (stack.empty()) {
                        isStack = isQueue = isHeap = 0;
                        continue;
                    }

                    if (stack.pop() != x) {
                        isStack = 0;
                    }
                    if (queue.poll() != x) {
                        isQueue = 0;
                    }
                    if (heap.poll() != -x) {
                        isHeap = 0;
                    }
                }
            }
            if (isStack + isQueue + isHeap == 0) {
                result.add("impossible");
            } else if (isStack + isQueue + isHeap > 1) {
                result.add("not sure");
            } else if (isStack == 1) {
                result.add("stack");
            } else if (isQueue == 1) {
                result.add("queue");
            } else {
                result.add("priority queue");
            }
        }

        for(String value: result)
        {
            System.out.println(value);
        }
    }
}
/*
import java.util.*;

public class Main {
    static int isStack, isQueue, isPQ;
    static Stack<Integer> s = new Stack<>();
    static Queue<Integer> q = new LinkedList<>();
    static PriorityQueue<Integer> pq = new PriorityQueue<>();
    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {
            int n = sc.nextInt();
            s.clear();
            q.clear();
            pq.clear();
            isStack = isQueue = isPQ = 1;

            for (int i = 0; i < n; i++) {
                int type = sc.nextInt();
                int x = sc.nextInt();

                if (type == 1) {
                    s.add(x);
                    q.add(x);
                    pq.add(-x);
                }
                else {
                    if (s.empty()) {
                        isStack = isQueue = isPQ = 0;
                        continue;
                    }

                    if (s.pop() != x) {
                        isStack = 0;
                    }
                    if (q.poll() != x) {
                        isQueue = 0;
                    }
                    if (pq.poll() != -x) {
                        isPQ = 0;
                    }
                }
            }

            if (isStack + isQueue + isPQ == 0) {
                System.out.println("impossible");
            }
            else if (isStack + isQueue + isPQ > 1) {
                System.out.println("not sure");
            }
            else if (isStack == 1) {
                System.out.println("stack");
            }
            else if (isQueue == 1) {
                System.out.println("queue");
            }
            else {
                System.out.println("priority queue");
            }
        }
    }
}
 */
