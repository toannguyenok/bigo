package toan.bigo.heap;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Trong ba loại truy vấn thì loại 22 là cần lưu ý nhất. Vì để xóa 1 phần tử bất kỳ trong hàng đợi ưu tiên chúng ta sẽ phải pop toàn bộ các giá trị từ đầu cho đến khi nào gặp phần tử cần xóa thì ta sẽ ngưng.

Lúc này chúng ta sẽ phải lưu lại toàn bộ giá trị đã lấy ra, để push lại vào trong hàng đợi ưu tiên.

Ví dụ: hàng đợi ưu tiên đang có 3\ 8\ 9\ 12\ 153 8 9 12 15. Chúng ta muốn xóa giá trị 99 thì phải đem 33 và 88 ra ngoài. Sau đó, xóa 99 đi, rồi thêm 33 và 88 vào lại hàng đợi ưu tiên. Khi thao tác như vậy, nếu phần tử cần xóa nằm ở vị trí cuối cùng của hàng đợi ưu tiên vậy ta phải sử dụng hai vòng lặp, một vòng lặp để xóa và một vòng lặp để thêm lại.

\rightarrow→ Không khả thi vì số lượng phần tử tối đa là 10^510
​5
​​ .

Cách cải tiến:

Để việc xóa không trở nên phức tạp, bạn sẽ dùng thêm một hàng đợi ưu tiên khác (tạm đặt tên là pqRemove). Khi gặp một truy vấn loại 22 thay vì xóa bạn sẽ thêm phần tử cần xóa này vào hàng đợi pqRemove.

Lúc này ở thao tác in phần tử ra màn hình (truy vấn loại 33), mỗi lần in bạn cần phải kiểm tra xem phần tử ở hàng đợi ban đầu có trùng với phần tử ở hàng đợi pqRemove không. Nếu trùng nhau nghĩa là phần tử này trước đó đã bị xóa đi. Lúc này bạn mới xóa phần tử này đi trong hàng đợi ban đầu. Còn nếu không trùng nhau thì bạn vẫn in ra giá trị ra bình thường.

Ví dụ: hàng đợi ban đầu 3\ 8\ 9\ 12\ 153 8 9 12 15. Bạn có một truy vấn xóa phần tử số 99. Thay vì xóa trong hàng đợi ban đầu bạn thêm số 99 vào pqRemove: 99.

Khi đề bài có thao tác in giá trị phần tử ra thì bạn kiểm tra phần tử trên cùng của pq có trùng với pqRemove hay không, nếu không trùng thì in phần tử trên cùng của pq (ở ví dụ trên là số 33). Còn nếu trùng thì đơn giản là chỉ việc xóa phần tử 99 ra khỏi pq và pqRemove.

Độ phức tạp: O(QlogN)O(QlogN) với QQ là số lượng truy vấn, NN là số lượng phần tử được add vào hàng đợi ưu tiên.
 */
public class QHeap1 {
    public static void main(String[] args) {

        PriorityQueue<Integer> heap = new PriorityQueue<>();

        int queries;
        Scanner sc = new Scanner(System.in);
        queries = sc.nextInt();
        ArrayList<Integer> result = new ArrayList<>();

        for (int i = 0; i < queries; i++) {
            int command = sc.nextInt();
            if (command == 1) {
                heap.add(sc.nextInt());
            } else if (command == 2) {
                heap.remove(sc.nextInt());
            } else if (command == 3) {
                result.add(heap.peek());
            }
        }

        for (Integer integer : result) {
            System.out.println(integer);
        }
    }
}
/*
import java.util.Scanner;
import java.util.PriorityQueue;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int Q = sc.nextInt();
        PriorityQueue<Integer> pq = new PriorityQueue<>(), pqRemove = new PriorityQueue<>();

        int type, value;

        for (int k = 0; k < Q; k++) {
            type = sc.nextInt();

            if (type == 1) {
                value = sc.nextInt();
                pq.add(value);
            }
            else if (type == 2) {
                value = sc.nextInt();
                pqRemove.add(value);
            }
            else {
                while (!pqRemove.isEmpty() && (int)pq.peek() == (int)pqRemove.peek()) {
                    pq.remove();
                    pqRemove.remove();
                }

                System.out.println(pq.peek());
            }
        }
    }
}
 */
