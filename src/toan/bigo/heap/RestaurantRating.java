package toan.bigo.heap;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Cách giải đơn thuần là với mỗi thao tác loại 22, ta chỉ cần đi sắp xếp lại mảng chứa danh sách đánh giá hiện tại giảm dần và in ra đánh giá thứ floor(n/3)floor(n/3) (mảng bắt đầu từ 11).

\rightarrow→ Cách này sẽ bị TLE do mỗi lần sort mảng sẽ tốn nhiều thời gian. Thay vào đó ta sử dụng heap chỉ tốn thời gian khi thêm đánh giá mới.

Tuy nhiên dùng heap đơn thuần thì chỉ có thể lấy phần tử trên đầu, mà muốn biết đánh giá thứ floor(n/3)floor(n/3) thì phải thực hiện lấy ra ra từng cái, và sau đó phải đưa vào heap lại.

Ta có một nhận xét khác:

Khi có một đánh giá có điểm số cao hơn so với đánh giá có số điểm thấp nhất hiện tại trong top thì mới được thêm vào top, còn cái thấp nhất sẽ bị loại khỏi top và nằm trong phần còn lại của danh sách đánh giá.
Top được tính là floor(n/3)floor(n/3), nên khi số đánh giá ít hơn 33 thì không có đánh giá nào trong top, khi bằng 3 \le n \le 53≤n≤5 có 1 đánh giá trong top, 6 \le n \le 86≤n≤8 có 2 đánh giá trong top, cứ tiếp tục như vậy. Nói cách khác khi số đánh giá tăng đến khi chia hết cho 33 thì số đánh giá trong top tăng lên 11.
Vậy ta sẽ sử dụng 2 hàng đợi ưu tiên:

Min-heap để lưu top 33 đánh giá tích cực – top của min-heap là đánh giá có số điểm nhỏ nhất hiện tại.
Max-heap lưu phần còn lại.
Khi có một đánh giá mới có số điểm xx, ta so sánh nếu xx lớn hơn top của min-heap thì đem top đó bỏ qua max-heap và đẩy xx vào min-heap. Ngược lại, đẩy xx vào max-heap. Sau đó, nếu số đánh giá hiện tại chia hết cho 33 thì lấy top của max-heap đem qua min-heap để tăng số trong top lên 11. Đánh giá hiện tại có điểm nhỏ nhất trên web là top của min-heap.

Độ phức tạp: O(NlogN)O(NlogN) với NN là số lượng thao tác.


 */
public class RestaurantRating {

    public static void main(String[] args) {

        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });

        PriorityQueue<Integer> minHeapTop3 = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });


        Scanner sc = new Scanner(System.in);
        int operation = sc.nextInt();
        int nReview = 0;
        for (int i = 0; i < operation; i++) {
            int query = sc.nextInt();
            if (query == 1) {
                int review = sc.nextInt();
                nReview++;
                if (!minHeapTop3.isEmpty() && review > minHeapTop3.peek()) {
                    int temp = minHeapTop3.remove();
                    minHeapTop3.add(review);
                    maxHeap.add(temp);
                } else {
                    maxHeap.add(review);
                }
                if (nReview % 3 == 0) {
                    int temp = maxHeap.remove();
                    minHeapTop3.add(temp);
                }
            } else {
                if (minHeapTop3.isEmpty()) {
                    System.out.println("No reviews yet");
                } else {
                    System.out.println("" + minHeapTop3.peek());
                }
            }
        }
    }
}
/*
import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        MyScanner in = new MyScanner();
        PrintWriter out = new PrintWriter(System.out);
        PriorityQueue<Integer> top3 = new PriorityQueue<>();
        PriorityQueue<Integer> rest = new PriorityQueue<>();
        int n = in.nextInt();
        int nreviews = 0;

        while (n-- > 0) {
            int type = in.nextInt();

            if (type == 1) {
                int x = in.nextInt();
                nreviews++;

                if (!top3.isEmpty() && top3.peek() < x) {
                    rest.add(-top3.poll());
                    top3.add(x);
                }
                else {
                    rest.add(-x);
                }

                if (nreviews % 3 == 0) {
                    top3.add(-rest.poll());
                }
            }
            else {
                if (top3.isEmpty()) {
                    out.println("No reviews yet");
                }
                else {
                    out.println(top3.peek());
                }
            }
        }
        out.close();
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st = null;
        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        public MyScanner(InputStream stream) {
            br = new BufferedReader(new InputStreamReader(stream));
        }

        boolean hasNext() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    String tmp = br.readLine();
                    if (tmp == null)
                        return false;
                    st = new StringTokenizer(tmp);
                } catch (IOException e) {
                    return false;
                }
            }
            return true;
        }
        String next() {
            if (hasNext())
                return st.nextToken();
            return null;
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
 */