package toan.bigo.dynamicarray;

import java.util.Arrays;
import java.util.Scanner;

/*
Chạy ngược chuỗi SS từ ký tự cuối về ký tự đầu, xét hai trường hợp sau:

Nếu gặp ký tự ‘z’ thì biến ký tự này thành ký tự ‘a’.

Nếu gặp ký tự khác ‘z’ thì tăng ký tự này lên một bậc, nghĩa là nếu gặp ký tự ‘b’ thì biến thành ‘c’, ký tự là ‘g’ thì biến thành ‘h’. Ngay sau tăng ký tự lên một bậc thì dừng vòng lặp.

Sau khi biến đổi xong hãy so sánh giữa chuỗi kết quả và TT, nếu chuỗi kết quả khác chuỗi TT (nghĩa là chuỗi nhỏ hơn TT) thì in ra chuỗi kết quả, ngược lại in “No such string”.

Độ phức tạp: \mathcal {O} \left( N \right)O(N) với NN là độ dài của 2 chuỗi.
 */
public class VitalyAndStrings {

    public static void main(String[] args) {

        //Khai báo đối tượng Scanner, giúp chúng ta nhận thông tin từ keyboard
        Scanner sc = new Scanner(System.in);
        System.out.print("");
        char s[] = sc.next().toCharArray();
        char t[] = sc.next().toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        int counter = 0;

        for (int i = s.length - 1; i >=0; i--) {
            if (s[i] == 'z') {
                s[i] = 'a';
            }
            else {
                s[i]++;
                break;
            }
        }

        System.out.print(Arrays.equals(s, t) ? "No such string".toCharArray() : s);
    }
}
/*
import java.util.Scanner;
import java.util.Arrays;

public class Main {
    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        char s[] = sc.next().toCharArray();
        char t[] = sc.next().toCharArray();

        for (int i = s.length - 1; i >= 0; i--) {
            if (s[i] == 'z') {
                s[i] = 'a';
            }
            else {
                s[i]++;
                break;
            }
        }

        System.out.print(Arrays.equals(s, t) ? "No such string".toCharArray() : s);
    }
}
 */