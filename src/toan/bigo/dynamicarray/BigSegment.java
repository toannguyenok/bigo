package toan.bigo.dynamicarray;

import java.util.ArrayList;
import java.util.Scanner;

/*
Nhận xét: Đoạn thẳng có thể bao phủ tất cả các đoạn thẳng còn lại trong NN đoạn thẳng đã cho phải có:

Đầu mút trái là giá trị nhỏ nhất trong tất cả các giá trị đầu mút trái của NN đoạn thẳng.
Đầu mút phải là giá trị lớn nhất trong tất cả các giá trị đầu mút phải của NN đoạn thẳng.
Ý tưởng: Đi tìm cặp [[L_iL
​i
​​ , R_iR
​i
​​ ]] biễu diễn đoạn thẳng lớn nhất này. Sau đó kiểm tra xem có đoạn thẳng nào như vậy trong NN đoạn thẳng được cho hay không. Nếu có đoạn nào trùng khớp thì đó chính là đoạn thẳng cần tìm, ta in ra số thứ tự của đoạn thẳng đó. Ngược lại, in ra -1−1.

Ta có các bước giải bài này như sau:

Bước 1: Đưa cặp L_iL
​i
​​ , R_iR
​i
​​  biểu diễn đoạn thẳng thứ ii vào hai mảng động chứa đầu mút trái và đầu mút phải.
Bước 2: Tìm giá trị nhỏ nhất trong mảng chứa đầu mút trái và giá trị lớn nhất trong mảng chứa đầu mút phải.
Bước 3: Duyệt lại từng đoạn thẳng thứ ii trong NN đoạn thẳng:
Nếu đoạn thẳng đó có L_iL
​i
​​  bằng giá trị đầu mút trái nhỏ nhất và R_iR
​i
​​  bằng giá trị đầu mút phải lớn nhất vừa tìm được:
In ra số thứ tự của đoạn thẳng đó và thoát chương trình.
Bước 4: Không có đoạn thẳng nào trùng khớp, do đó in ra -1−1.
Độ phức tạp: \mathcal {O} \left( N \right)O(N) với NN là số lượng đoạn thẳng.
 */
public class BigSegment {

    public static void main(String[] args) {
        // write your code here
        int nSegment;
        ArrayList<Integer> first = new ArrayList();
        ArrayList<Integer> second = new ArrayList();


        Scanner sc = new Scanner(System.in);
        nSegment = sc.nextInt();

        for (int i = 0; i < nSegment; i++) {
            first.add(sc.nextInt());
            second.add(sc.nextInt());
        }

        int min = first.get(0);
        int max = second.get(0);
        for (int i = 1; i < nSegment; i++) {
            min = Math.min(min, first.get(i));
            max = Math.max(max, second.get(i));
        }

        int index = -1;
        for (int i = 0; i < nSegment; i++) {
            if (first.get(i) == min && second.get(i) == max) {
                index = i + 1;
                break;
            }
        }
        System.out.print(index + "");
    }
}

/*
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.Math;

public class Main {
    static final int INF = (int)1e9 + 5;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        ArrayList<Integer> L = new ArrayList<>(), R = new ArrayList<>();

        int left = INF, right = 0;              // Sentinal values

        for (int i = 0; i < n; i++) {
            L.add(sc.nextInt());
            R.add(sc.nextInt());
            left = Math.min(left, L.get(i));
            right = Math.max(right, R.get(i));
        }

        for (int i = 0; i < n; i++) {
            if (left == L.get(i) && right == R.get(i)) {
                System.out.print(i + 1);
                return;
            }
        }

        System.out.print(-1);
    }
}
 */
