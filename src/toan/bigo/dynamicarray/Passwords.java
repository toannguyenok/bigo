package toan.bigo.dynamicarray;

import java.util.Scanner;

/*
Theo cách nhập mật khẩu của Vanya, các mật khẩu có chiều dài nhỏ nhất sẽ được nhập trước, rồi đến các mật khẩu có chiều dài nhỏ thứ hai, cứ tiếp tục như vậy. Đến khi chiều dài bằng chiều dài mật khẩu đúng, thì trường hợp tốt sẽ nhập đúng ngay lần đầu tiên, trường hợp xấu sẽ nhập đúng sau khi đã nhập tất cả mật khẩu cùng chiều dài.

Vậy với nn mật khẩu, ta sẽ đếm xem ứng với mỗi chiều dài thì có bao nhiêu chuỗi cùng chiều dài đó, lưu lại thông tin này.

Sau đó, để tìm thời gian cho trường hợp tốt nhất, đi tính tổng số mật khẩu có chiều dài nhỏ hơn chiều dài mật khẩu đúng, đây cũng là thời gian dành để nhập các mật khẩu. Sau kk lần nhập sai thì bị chặn 5 giây, nên ta lấy tổng vừa tìm được chia cho kk ra được số lần bị chặn, rồi nhân cho 5, ta sẽ biết được tổng số thời gian bị chặn. Ta lấy tổng số mật khẩu tìm được trước đó cộng với tổng thời gian bị chặn sẽ biết được tổng thời gian nhập các mật khẩu sai có chiều dài nhỏ hơn chiều dài mật khẩu đúng.

Thực hiện tương tự để tìm thời gian cho trường hợp xấu nhất, nhưng chỗ tính tổng số mật khẩu, ta cộng thêm số mật khẩu cùng chiều dài với mật khẩu đúng, nhớ trừ đi 1 (trừ mật khẩu đúng).

Lấy kết quả ở cả hai trường hợp ta đã tính được cộng thêm 1 (thời gian nhập mật khẩu đúng) là ra đáp án cuối cùng.

Độ phức tạp: O(n + length(s))O(n+length(s)) với nn là số lượng chuỗi nhập và length(s)length(s) là độ dài chuỗi ss là chuỗi mật khẩu đúng.
 */
public class Passwords {

    public static void main(String[] args) {
        // write your code here
        int n;
        int k;

        int[] sizePasswords = new int[1000];
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        k = sc.nextInt();
        for (int i = 0; i < n; i++) {
            String pass = sc.next();
            sizePasswords[pass.length()]++;
        }
        String password = sc.next();
        int bestTime = 0;
        int worstTime = 0;
        for (int i = 0; i < password.length(); i++) {
            bestTime = bestTime + sizePasswords[i];
        }
        worstTime = bestTime + sizePasswords[password.length()] - 1;
        bestTime = bestTime+ (bestTime / k) * 5;
        worstTime = worstTime + (worstTime / k) * 5;
        System.out.print((bestTime + 1) + " " + (worstTime + 1));
    }
}
/*
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        int[] cnt = new int[101];

        for (int i = 0; i < n; i++) {
            String s = sc.next();
            cnt[s.length()]++;
        }

        String password = sc.next();
        int best_time = 0, worst_time = 0;

        for (int i = 0; i < password.length(); i++) {
            best_time += cnt[i];
        }

        worst_time = best_time + cnt[password.length()] - 1;

        best_time += (best_time / k) * 5;
        worst_time += (worst_time / k) * 5;

        System.out.print((best_time + 1) + " " + (worst_time + 1));
    }
}
 */