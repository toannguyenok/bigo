package toan.bigo.dynamicarray;

import java.util.Scanner;

/*
Trước tiên ta có nhận xét như sau:

Nếu có một ký tự nào trong tt mà không có trong ss thì chắc chắn ta không thể biến đổi chuỗi ss thành chuỗi tt được -> “need tree”.
Nếu có một ký tự nào trong ss mà không có ký tự trong tt thì ta phải bỏ đi ký tự đó -> “automaton”.
Phép biến đổi “array” hoán đổi vị trí của hai ký tự chỉ cần thiết nếu thứ tự xuất hiện của các ký tự trong chuỗi tt không trùng khớp với thứ tự của các ký tự đó trong chuỗi ss.
Để giải quyết trường hợp 1 và 2, ta sử dụng một mảng đếm tần suất xuất hiện của từng ký tự trong chuỗi ss và chuỗi tt. Sau đó duyệt từng ký tự trong bảng chữ cái gồm 26 chữ:

Nếu tần suất xuất hiện của ký tự đó trong chuỗi tt lớn hơn chuỗi ss, nghĩa là chuỗi ss không đủ ký tự để ta biến đổi thành chuỗi tt -> “need tree”.
Nếu tần suất xuất hiện của ký tự đó trong chuỗi tt nhỏ hơn chuỗi ss, nghĩa là sẽ có một số ký tự thừa -> “automaton”.
Ta cần giải quyết trường hợp 3 để có thể đưa ra kết luận là “array” hay “both”.

Bằng cách duyệt qua từng ký tự trong chuỗi tt, ta tìm vị trí của ký tự tương ứng trong chuỗi ss. Nếu các vị trí này tăng dần nghĩa là các ký tự này đã đúng vị trí, ta không cần đổi chỗ. Ngược lại ta kết luận “array”.

Xem xét liệu “automaton” và “array” có cùng thỏa hay không để đưa ra kết luận là “both”.

Độ phức tạp: O(max(length(s), length(t))O(max(length(s),length(t))
 */
public class SuffixStructures {

    public static void main(String[] args) {
        // write your code here
        int[] sCount = new int[26];
        int[] tCount = new int[26];

        String s = "";
        String t = "";
        Scanner sc = new Scanner(System.in);
        s = sc.next();
        t = sc.next();
        for (int i = 0; i < s.length(); i++) {
            sCount[(int) s.charAt(i) - (int) 'a']++;
        }
        for (int i = 0; i < t.length(); i++) {
            tCount[(int) t.charAt(i) - (int) 'a']++;
        }

        boolean needTrue = false;
        boolean automaton = false;
        boolean array = false;
        for (int i = 0; i < 26; i++) {
            if (tCount[i] > sCount[i]) {
                needTrue = true;
            } else if (tCount[i] < sCount[i]) {
                automaton = true;
            }
        }

        int index_found = 0, match = -1;

        for (int i = 0; i < t.length(); i++) {
            index_found = s.indexOf(t.charAt(i), match + 1);

            if (index_found > match) {
                match = index_found;
            }
            else {
                array = true;
                break;
            }
        }

        if (needTrue) {
            System.out.print("need tree");
        }
        else if (automaton && array) {
            System.out.print("both");
        }
        else if (automaton) {
            System.out.print("automaton");
        }
        else {
            System.out.print("array");
        }
    }
}

/*
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.next();
        String t = sc.next();

        boolean need_tree = false, array = false, automaton = false;
        int[] sCount = new int[26];
        int[] tCount = new int[26];

        for (int i = 0; i < s.length(); i++) {
            sCount[s.charAt(i) - 'a']++;
        }

        for (int i = 0; i < t.length(); i++) {
            tCount[t.charAt(i) - 'a']++;
        }

        for (int i = 0; i < 26; i++) {
            if (tCount[i] > sCount[i]) {
                need_tree = true;
            }
            else if (tCount[i] < sCount[i]) {
                automaton = true;
            }
        }

        int index_found = 0, match = -1;

        for (int i = 0; i < t.length(); i++) {
            index_found = s.indexOf(t.charAt(i), match + 1);

            if (index_found > match) {
                match = index_found;
            }
            else {
                array = true;
                break;
            }
        }

        if (need_tree) {
            System.out.print("need tree");
        }
        else if (automaton && array) {
            System.out.print("both");
        }
        else if (automaton) {
            System.out.print("automaton");
        }
        else {
            System.out.print("array");
        }
    }
}
 */
