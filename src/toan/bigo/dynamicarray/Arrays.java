package toan.bigo.dynamicarray;

import java.util.ArrayList;
import java.util.Scanner;

/*
Nhận xét: Để tăng khả năng hai đoạn con được chọn ở hai mảng thỏa yêu cầu đề bài, ta ưu tiên chọn KK phần tử nhỏ nhất trong AA và MM phần tử lớn nhất trong BB. Lúc này muốn kiểm tra điều kiện tất cả phần tử trong mảng con AA đều nhỏ hơn tất cả phần tử trong mảng con BB, ta chỉ việc so sánh phần tử lớn nhất trong mảng con AA và phần tử nhỏ nhất trong mảng con BB. Nếu kết quả là nhỏ hơn, đồng nghĩa với mọi phần tử trong KK phần tử của mảng AA đều nhỏ hơn MM phần tử trong mảng BB, ta in ra “YES”. Ngược lại, in “NO”.

Hơn nữa, cả hai mảng đều đã được sắp xếp không giảm, do đó với mảng đánh số bắt đầu từ 00, ta có:

Phần tử lớn nhất trong KK phần tử nhỏ nhất của AA sẽ nằm ở vị trí KK – 11.
Phần tử nhỏ nhất trong MM phần tử lớn nhất của BB nằm ở vị trí N_BN
​B
​​  – MM.
Như vậy, đáp án của ta chỉ cần dựa vào kết quả của phép so sánh AA[[KK – 11]] << BB[[N_BN
​B
​​  – MM]].

Độ phức tạp: \mathcal {O} \left( 1 \right)O(1)
 */
public class Arrays {

    public static void main(String[] args) {
        // write your code here
        int sizeA, sizeB;
        int k, m;

        ArrayList<Integer> arrayA = new ArrayList<>();
        ArrayList<Integer> arrayB = new ArrayList<>();

        Scanner sc = new Scanner(System.in);
        sizeA = sc.nextInt();
        sizeB = sc.nextInt();
        k = sc.nextInt();
        m = sc.nextInt();
        for (int i = 0; i < sizeA; i++) {
            System.out.print("");
            arrayA.add(sc.nextInt());
        }
        for (int i = 0; i < sizeB; i++) {
            System.out.print("");
            arrayB.add(sc.nextInt());
        }

        if (arrayA.get(k - 1) < arrayB.get(sizeB - m)) {
            System.out.print("YES");
        } else {
            System.out.print("NO");
        }
    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int na = sc.nextInt();
        int nb = sc.nextInt();
        int k = sc.nextInt();
        int m = sc.nextInt();

        ArrayList<Integer> a = new ArrayList<>(), b = new ArrayList<>();

        for (int i = 0; i < na; i++)
            a.add(sc.nextInt());

        for (int i = 0; i < nb; i++)
            b.add(sc.nextInt());

        System.out.print(a.get(k - 1) < b.get(nb - m) ? "YES" : "NO");
    }
}
 */