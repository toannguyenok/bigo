package toan.bigo.dynamicarray;

import java.util.Scanner;


/**
 * Đặt kim đồng hồ của bạn ngay tại vị trí bắt đầu là ký tự ‘a’.
 *
 * Bạn sẽ cho vòng lặp lấy từng ký tự ra, ký tự đầu tiên sẽ trừ với ký tự ‘a’. Nếu từ vị trí ‘a’ đến ký tự đó nhỏ hơn 13 thì bạn sẽ đi theo hướng này. Nếu lớn hơn 13 thì bạn sẽ đi ngược hướng lại để đi được gần hơn.
 *
 * Sau khi chọn bước đi ở trên xong thì bạn sẽ gán ký tự mới này vào thay cho vị trí bắt đầu (vì theo đề bài mỗi bước đi sẽ cập nhật lại vị trí ban đầu của kim đồng hồ).
 *
 * Chạy lần lượt qua hết các ký tự lấy số bước đi cộng lại là kết quả của bài toán.
 *
 * Độ phức tạp: \mathcal{O}\left( N \right)O(N) với NN là độ dài của chuỗi đầu vào.
 */
public class NightAtTheMuseum {
    public static void main(String[] args) {
        String text = "";
        Scanner sc = new Scanner(System.in);
        System.out.print("");
        text = sc.next();

        int charStart = (int)'a';
        int total = 0;

        for (int i = 0; i < text.length(); i++) {
            int character = (int)text.charAt(i);
            int lengthChar = Math.abs(character - charStart);
            if(lengthChar != 0) {
                total = total + Math.min(lengthChar,Math.abs(lengthChar - 26));
            }
            charStart = character;
        }
        System.out.print(total + "");
    }
}

/**
 * import java.util.Scanner;
 *
 * public class Main {
 *     public static void main(String[] agrs) {
 *         Scanner sc = new Scanner(System.in);
 *         String wheel = sc.next();
 *         char pointer = 'a';
 *         int dist, count = 0;
 *
 *         for (char c : wheel.toCharArray()) {
 *             dist = Math.abs(pointer - c);
 *
 *             if (dist < 13) {
 *                 count = count + dist;
 *             }
 *             else {
 *                 count = count + (26 - dist);
 *             }
 *
 *             pointer = c;
 *         }
 *
 *         System.out.print(count);
 *     }
 * }
 */
