package toan.bigo.dynamicarray;

import java.util.ArrayList;
import java.util.Scanner;


/**
 * Bước 1: Đưa toàn bộ giá trị đại diện cho trạng thái mỗi nút áo vào trong mảng động.
 *
 * Bước 2: Xét hai trường hợp:
 *
 * Trường hợp 1: Khi áo có 11 nút. Nếu nút đó là “11” thì in ra “YES”, ngược lại in ra “NO” và thoát khỏi chương trình.
 * Trường hợp 2: Với số nút lớn hơn 11, sử dụng một biến để đếm số lượng nút không cài (số 00). Nếu số lượng nút không cài đúng bằng 11 thì in ra “YES”, ngược lại in ra “NO”.
 * Độ phức tạp: \mathcal{O} \left( N \right)O(N) với NN là số lượng nút áo.
 */
public class Main {

    public static void main(String[] args) {
        // write your code here
        int n;
        ArrayList<Integer> caiao = new ArrayList<Integer>();

        //Khai báo đối tượng Scanner, giúp chúng ta nhận thông tin từ keyboard
        Scanner sc = new Scanner(System.in);
        System.out.print("");
        n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.print("");
            caiao.add(sc.nextInt());
        }

        int countChuaCai = 0;
        for (int i = 0; i < n; i++) {
            if (caiao.get(i) == 0) {
                countChuaCai++;
            }
            if (countChuaCai > 1) {
                break;
            }
        }

        if (n == 1) {
            if (countChuaCai == 1) {
                System.out.print("NO");
            } else {
                System.out.print("YES");
            }
        } else {
            if (countChuaCai == 1) {
                System.out.print("YES");
            } else {
                System.out.print("NO");
            }
        }
    }
}

/**
 * import java.util.ArrayList;
 * import java.util.Scanner;
 *
 * public class Main {
 *     private static boolean checkJacket(ArrayList<Integer> v, int n) {
 *         if (n == 1) {
 *             if (v.get(0) == 1)
 *                 return true;
 *             else
 *                 return false;
 *         }
 *         int count = 0;
 *         for (int i = 0; i < n; i++) {
 *             if (v.get(i) == 0) {
 *                 count++;
 *             }
 *         }
 *         if (count == 1)
 *             return true;
 *         else
 *             return false;
 *     }
 *     public static void main(String[] args) {
 *         Scanner sc = new Scanner(System.in);
 *         int n = sc.nextInt();
 *         int value;
 *         ArrayList<Integer> v = new ArrayList<>();
 *
 *         for (int i = 0; i < n; i++) {
 *             value = sc.nextInt();
 *             v.add(value);
 *         }
 *
 *         boolean result = checkJacket(v, n);
 *         if (result == true)
 *             System.out.println("YES");
 *         else
 *             System.out.println("NO");
 *     }
 * }
 */
