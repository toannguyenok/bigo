package toan.bigo.dynamicarray;

import java.util.ArrayList;
import java.util.Scanner;


/**
 * Bước 1: Đưa toàn bộ các phút gây cấn trong chương trình vào mảng động.
 *
 * Bước 2: Gọi TT == 00 là thời điểm Limak bắt đầu xem chương trình. Xét lần lượt các phút gây cấn thứ ii:
 *
 * Nếu TT ++ 1515 << thời điểm xuất hiện phút gây cấn thứ ii, đồng nghĩa với việc đã qua 1515 phút mà không xuất hiện đoạn gây cấn mới, thì (TT ++ 1515) chính là thời điểm Limak tắt TV -- cũng là số phút tối đa mà cậu xem chương trình.
 * Ngược lại, ta cập nhật giá trị TT là thời gian phút gây cấn mới xuất hiện.
 * Bước 3: So sánh giá trị (TT ++ 1515) với 9090 (số phút tối đa của chương trình) để đưa ra đáp án.
 *
 * Độ phức tạp: \mathcal {O} \left( N \right)O(N) với NN là số lượng đoạn hấp dẫn.
 */
public class BearAndGame {

    public static void main(String[] args) {
        // write your code here
        int n;
        ArrayList<Integer> minuteInterest = new ArrayList<Integer>();

        //Khai báo đối tượng Scanner, giúp chúng ta nhận thông tin từ keyboard
        Scanner sc = new Scanner(System.in);
        System.out.print("");
        n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.print("");
            minuteInterest.add(sc.nextInt());
        }

        int lastMinuteInterest = 0;
        int totalWatch = 0;
        for (int i = 0; i < n; i++) {
            int minute = minuteInterest.get(i);
            if (minute <= lastMinuteInterest + 15) {
                totalWatch = minute;
                lastMinuteInterest = minute;
            } else {
                break;
            }
        }
        totalWatch = Math.min(totalWatch + 15, 90);
        System.out.print(totalWatch + "");
    }
}

/*
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.Math;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        ArrayList<Integer> a = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            a.add(sc.nextInt());
        }

        int t = 0;
        for (int i = 0; i < n; i++) {
            if (t + 15 < a.get(i)) {
                System.out.print(t + 15);
                return;
            }
            else {
                t = a.get(i);
            }
        }

        System.out.print(Math.min(t + 15, 90));
    }
}
 */