package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Arrays;

public class Lis {

    public static ArrayList<Integer> list(int[] input) {
        int length = 0;
        int last = 0;
        ArrayList<Integer> result = new ArrayList<>();
        int[] path = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            result.add(1);
        }
        Arrays.fill(path, -1);

        for (int i = 1; i < input.length; i++) {
            for (int j = 0; j < i; i++) {
                if (input[j] < input[i] && result.get(i) < result.get(j) + 1) {
                    result.set(i, result.get(j) + 1);
                    path[i] = j;
                }
            }
        }

        for (int i = 0; i < input.length; i++) {
            if (length < result.get(i)) {
                length = result.get(i);
                last = i;
            }
        }

        return result;
    }
}
