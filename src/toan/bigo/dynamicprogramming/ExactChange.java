package toan.bigo.dynamicprogramming;

import java.util.Scanner;

/*
Hướng dẫn giải
Ta thấy bản chất bài toán là tìm cách tạo ra một tổng nhỏ nhất sao cho lớn hơn hoặc bằng giá trị của món hàng. Với cách tạo ra tổng nhỏ nhất đó, ta lại cần tìm cách sử dụng ít đồng tiền nhất có thể. Vậy ta có thể quy đổi bài toán về bài toán Coin Change. Các bước giải bài toán như sau:

Bước 1: Tạo mảng solsol với sol[X]sol[X] là số lượng đồng tiền cần có để tạo nên giá trị XX. Trong trường hợp tệ nhất, món hàng cần mua có giá là 1000010000 cents và ta chỉ có trong tay 22 tờ tiền mệnh giá 99999999 cents. Vậy ta phải sử dụng cả 22 tờ 99999999 cents thì mới mua được hàng. Vậy mảng solsol sẽ chứa tối đa 9999*29999∗2 phần tử (có thể khai báo chẵn 2000020000).
Bước 2: Khởi tạo sol[0] = 0sol[0]=0 và tất cả các sol[X] =sol[X]= INFINITY (1 <= X <= 20000)(1<=X<=20000).
Bước 3: Với mỗi đồng tiền có giá trị là coin[i]coin[i] (0 \le i \le n – 1), ta sẽ cập nhật: sol[j + coin[i]]sol[j+coin[i]] = min(sol[j + coin[i]], sol[j] + 1)min(sol[j+coin[i]],sol[j]+1) (0 \le j \le giá trị món hàng)
Bước 4: Trả về kết quả là XX (tổng tiền chi trả) và sol[X]sol[X] (số đồng tiền ít nhất tương ứng tổng tiền chi trả ít nhất). Ta sẽ tìm giá trị XX nhỏ nhất sao cho X \geX≥ giá trị món hàng và sol[X]sol[X] khác INFINITY.
Độ phức tạp: O(T*p*n)O(T∗p∗n) với TT là số testcases, pp là giá trị của một món hàng và nn là số lượng đồng tiền hoặc hối phiếu trong mỗi test case.
 */
public class ExactChange {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCase = scanner.nextInt();
        for (int t = 0; t < testCase; t++) {
            int[] dp = new int[20000];
            int total = scanner.nextInt();
            int n = scanner.nextInt();
            int mx = 0;
            dp[0] = 1;
            for (int i = 0; i < n; i++) {
                int bill = scanner.nextInt();
                mx += bill;
                if (mx > 20000) {
                    mx = 19999;
                }
                for (int j = mx; j >= bill; j--) {
                    if (dp[j] == 0 || dp[j] > dp[j - bill] + 1) {
                        if (dp[j - bill] != 0) {
                            dp[j] = dp[j - bill] + 1;
                        }
                    }
                }
            }
            while (dp[total] == 0) total++;
            System.out.println(total + " " + (dp[total] - 1));
        }
    }
}
/*
#include <iostream>
#include <vector>

using namespace std;

#define INF 1000000
#define MAX 20000

int main()
{
    int tc, n, price_of_item;
    int coin[101];
    int sol[MAX];
    cin >> tc;
    while (tc--)
    {
        cin >> price_of_item;
        cin >> n;
        for (int i = 0; i < n; i++)
        {
            cin >> coin[i];
        }
        sol[0] = 0;
        for (int i = 1; i < MAX; i++)
        {
            sol[i] = INF;
        }
        for (int i = 0; i < n; i++)
        {
            for (int j = price_of_item; j >= 0; j--)
            {
                if (sol[j] != INF)
                {
                    sol[j + coin[i]] = min(sol[j + coin[i]], sol[j] + 1);
                }
            }
        }
        for (int i = price_of_item; i < MAX; i++)
        {
            if (sol[i] < INF)
            {
                cout << i << " " << sol[i] << endl;
                break;
            }
        }
    }
    return 0;
}
 */
