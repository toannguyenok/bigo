package toan.bigo.dynamicprogramming;

public class KMP {

    public static void KMPPreProcesses(String p, int[] prefix) {
        prefix[0] = 0;
        int m = p.length();
        int len = 0;
        int i = 1;
        while (i < m) {
            if (p.charAt(i) == p.charAt(len)) {
                len++;
                prefix[i] = len;
                i++;
            } else {
                if (len != 0) {
                    len = prefix[len - 1];
                } else {
                    prefix[i] = 0;
                    i++;
                }
            }
        }
    }

    public static int kmpSearch(String t, String p, int[] prefix) {
        int n = t.length();
        int m = p.length();
        int i = 0;
        int j = 0;
        while (i < n) {
            if (t.charAt(i) == p.charAt(j)) {
                i++;
                j++;
            }
            if (j == m) {
                return i - j;
            } else if (i < n && p.charAt(j) != t.charAt(i)) {
                if (j != 0) {
                    j = prefix[j - 1];
                } else {
                    i = i + 1;
                }
            }
        }
        return - 1;
    }
}
