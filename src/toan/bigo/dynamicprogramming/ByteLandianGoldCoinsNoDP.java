package toan.bigo.dynamicprogramming;

import java.util.Arrays;
import java.util.Scanner;

public class ByteLandianGoldCoinsNoDP {

    public static int MAX = (int) (10e6 + 10);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            long n = scanner.nextLong();
            System.out.println(calculator(n));
        }
    }

    public static long getCoins(long n, long a) {
        return n / a;
    }

    public static long calculator(long n) {
        if (n < 10) {
            return n;
        }
        long temp = calculator(getCoins(n, 2))
                + calculator(getCoins(n, 3))
                + calculator(getCoins(n, 4));
        return Math.max(n, temp);

    }
}
