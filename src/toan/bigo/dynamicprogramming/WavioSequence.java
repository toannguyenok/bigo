package toan.bigo.dynamicprogramming;

import java.util.*;

/*
Hướng dẫn giải
Với mỗi vị trí ii trong dãy số ban đầu, ta sẽ tìm độ dài của dãy Wavio có tâm đối xứng là ii. Sau đó, ta sẽ tìm dãy có độ dài lớn nhất. Để tìm được độ dài của dãy Wavio có tâm đối xứng là ii, ta sẽ phải tìm ra độ dài dãy con tăng dài nhất từ vị trí 00 đến ii, gọi là ascendingLengthascendingLength và độ dài dãy con giảm dài nhất từ ii đến n - 1n−1, gọi là descendingLengthdescendingLength. Do dãy Wavio phải đối xứng qua ii, độ dài tối đa của dãy Wavio tại ii sẽ là min(ascendingLength, descendingLength) * 2 - 1min(ascendingLength,descendingLength)∗2−1.

Để tìm được dãy con tăng dài nhất từ vị trí 00 đến ii, ta sẽ tìm LIS trên dãy ban đầu cho từng vị trí ii, lưu trong 11 mảng ascendingLengthascendingLength. Còn để tìm được dãy con giảm dài nhất, ta có thể làm 11 cách đơn giản là lật ngược dãy ban đầu lại rồi tìm LIS bắt đầu từ n - 1n−1 đến ii với từng vị trí ii, lưu trong 11 mảng descendingLengthdescendingLength.

Độ phức tạp: O(N^2 * log(N))O(N
​2
​​ ∗log(N)) với NN là số lượng biến trong mảng ban đầu.
 */
public class WavioSequence {

    public static ArrayList<Integer> input = new ArrayList<>();
    private static int[] path;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            input.clear();
            for (int i = 0; i < n; i++) {
                int number = scanner.nextInt();
                input.add(number);
            }
            int[] lisFirst = lis(input);

            Collections.reverse(input);
            int[] lisRightTemp = lis(input);
            int[] lisRight = new int[lisRightTemp.length];
            for (int i = lisRightTemp.length - 1; i >= 0; i--) {
                lisRight[lisRightTemp.length - 1 - i] = lisRightTemp[i];
            }

            int ans = -1;
            for (int i = 0; i < lisFirst.length; i++) {
                ans = Math.max(ans, Math.min(lisFirst[i], lisRight[i]) * 2 - 1);
            }
            System.out.println(ans  );
        }
    }

    public static int[] lis(List<Integer> input) {
        int length = 1;
        ArrayList<Integer> result = new ArrayList<>();
        result.add(0);
        path = new int[input.size()];
        int[] lis = new int[input.size()];
        lis[0] = length;
        Arrays.fill(path, -1);
        for (int i = 1; i < input.size(); i++) {
            if (input.get(i) <= input.get(result.get(0))) {
                result.set(0, i);
            } else if (input.get(i) > input.get(result.get(length - 1))) {
                path[i] = result.get(length - 1);
                result.add(i);
                length++;
            } else {
                int pos = lowerBound(input, result, length, input.get(i));
                path[i] = input.get(pos - 1);
                result.set(pos, i);
            }
            lis[i] = length;
        }
        return lis;
    }

    public static int lowerBound(List<Integer> a, ArrayList<Integer> sub, int n, int x) {
        int left = 0;
        int right = n;
        int pos = n;
        while (left < right) {
            int mid = left + (right - left) / 2;
            int index = sub.get(mid);
            if (a.get(index) >= x) {
                pos = mid;
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return pos;
    }
}

/*
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

#define init_cin cin.tie(NULL), cout.tie(NULL), ios_base::sync_with_stdio(false)

//-----------------------------------------------------------------
// functions
void WavioLIS(const vector<int>& a, vector<int>& lis)
{
    int n = a.size();
    vector<int> sub;
    sub.push_back(a[0]);
    for (int i = 1; i < a.size(); ++i) {
        int pos = lower_bound(sub.begin(), sub.end(), a[i]) - sub.begin();
        if (pos == sub.size()) {
            sub.push_back(a[i]);
        } else {
            sub[pos] = a[i];
        }
        lis[i] = pos + 1;
    }
}
//-----------------------------------------------------------------
// main function
int main()
{
    init_cin;
    int n;
    vector<int> a;
    vector<int> ascendingLength, descendingLength;
    while (cin >> n) {
        a.resize(n);
        ascendingLength.resize(n);
        descendingLength.resize(n);
        for (int i = 0; i < n; ++i) {
            cin >> a[i];
        }
        WavioLIS(a, ascendingLength);
        reverse(a.begin(), a.end());
        WavioLIS(a, descendingLength);
        int maxLength = 1;
        for (int i = 0; i < n; ++i) {
            int minLen = min(ascendingLength[i], descendingLength[n - 1 - i]);
            maxLength = max(maxLength, minLen * 2 - 1);
        }
        cout << maxLength << endl;
    }
    return 0;
}
 */

/*
import java.util.*;
import java.lang.Math;

class Solution {
    public static int lowerBound(ArrayList<Integer> array, int value) {
        int low = 0;
        int high = array.size();
        while (low < high) {
            final int mid = (low + high) / 2;
            if (value <= array.get(mid)) {
                high = mid;
            } else {
                low = mid + 1;
            }
        }
        return low;
    }

    public static void WavioLIS(Integer[] a, int[] lis) {
        int n = a.length;
        ArrayList<Integer> sub = new ArrayList<Integer>();
        sub.add(a[0]);
        for (int i = 1; i < n; i++) {
            int pos = lowerBound(sub, a[i]);
            if (pos == sub.size())
                sub.add(a[i]);
            else
                sub.set(pos, a[i]);
            lis[i] = pos + 1;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        while (true)
            try {
                n = sc.nextInt();
                Integer[] a = new Integer[n];
                int[] ascendingLength = new int[n];
                int[] descendingLength = new int[n];
                for (int i = 0; i < n; i++)
                    a[i] = sc.nextInt();
                WavioLIS(a, ascendingLength);

                List<Integer> list = Arrays.asList(a);
                Collections.reverse(list);
                a = list.toArray(a);

                WavioLIS(a, descendingLength);
                int maxLength = 1;
                for (int i = 0; i < n; i++) {
                    int minLen = Math.min(ascendingLength[i], descendingLength[n - i - 1]);
                    maxLength = Math.max(maxLength, minLen * 2 - 1);
                }
                System.out.println(maxLength);
            } catch (Exception e) {
                sc.close();
                System.exit(0);
            }
    }
}
 */