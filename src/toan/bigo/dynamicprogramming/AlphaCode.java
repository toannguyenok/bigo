package toan.bigo.dynamicprogramming;

/*
Hướng dẫn giải
Giải thích ví dụ:

Với test ví dụ số 11, ta có 66 chuỗi ban đầu mã hóa ra chuỗi 2511425114 là: BEAN, BEAAD, YAAD, YAN, YKD, BEKD.
Với test ví dụ số 33, chỉ có duy nhất 11 chuỗi là CCCCCCCCCC.
Hướng dẫn giải:

Ta gọi f[i] là số lượng cách để tạo ra chuỗi chữ cái ban đầu khi ta có i ký tự đầu tiên trong chuỗi số. Ví dụ đối với chuỗi số s = 25114, thì f[1] = 1 (B), f[2] = 2 (BE, Y), f[3] = 2 (BEA, YA), f[4] = 4 (BEAA, YAA, BEK, YK), f[5] = 6 (theo giải thích test ví dụ 11).
Ta có f[0] = 1 (với chuỗi số rỗng thì ta cũng có một cách là chuỗi chữ cái cũng rỗng).
Khi ta tính f[i], ta có 22 cách để xây dựng thành ii ký tự đầu tiên trong chuỗi số.
Nếu ta quyết định s[i] chính là một ký tự, thì điều kiện là s[i] phải khác 00 (A được mã hóa là 11, không tồn tại ký tự nào mã hóa thành 00). Khi đó, f[i] += f[i - 1]
Nếu ta quyết định s[i - 1] và s[i] ghép lại thành mã hóa của 11 chữ cái, điều kiện để có thể ghép được là i \ge 2i≥2 và chuỗi số mà s[i - 1] và s[i] ghép lại phải nhỏ hơn 2626. Như vậy f[i] += f[i - 2].
Kết quả của bài toán là f[n].
Độ phức tạp: O(T * n)O(T∗n) với nn là độ dài của chuỗi ss trong từng test và TT là số lượng test case.


 */
public class AlphaCode {
}

/*
#include <iostream>
#include <string>

using namespace std;
const int MAX_N = 5005;

long long f[MAX_N];
string s;

int main () {
	while (1) {
		cin >> s;
		if (s == "0") {
			break;
		}

		int n = (int) s.size();
		for (int i = 1; i < MAX_N; i++) {
			f[i] = 0;
		}

		f[0] = 1;
		for (int i = 1; i <= n; i++) {
			if (s[i - 1] != '0') {
				f[i] = f[i - 1];
			}

			if (i > 1 && s[i - 2] != '0') {
				int val = (s[i - 2] - '0') * 10 + s[i - 1] - '0';
				if (val <= 26) {
					f[i] += f[i - 2];
				}
			}
		}

		cout << f[n] << endl;
	}
	return 0;
}
 */

/*
import java.util.Scanner;

public class Solution {
	public static void main(String[] args) {
		int MAX_N = 5005;
		long []f;
		f = new long [MAX_N];
		String s;
		Scanner sc = new Scanner(System.in);

		while (true) {
			s = sc.nextLine();
			if (s.compareTo("0") == 0) {
				break;
			}

			int n = s.length();
			for (int i = 1; i < MAX_N; i++) {
				f[i] = 0;
			}

			f[0] = 1;
			for (int i = 1; i <= n; i++) {
				if (s.charAt(i - 1) != '0') {
					f[i] = f[i - 1];
				}

				if (i > 1 && s.charAt(i - 2) != '0') {
					int val = (s.charAt(i - 2) - '0') * 10 + s.charAt(i - 1) - '0';
					if (val <= 26) {
						f[i] += f[i - 2];
					}
				}
			}

			System.out.printf("%d\n", f[n]);
		}
	}
}
 */