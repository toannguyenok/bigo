package toan.bigo.dynamicprogramming;

/*
Hướng dẫn giải
Giải thích ví dụ:

Harry sẽ đi theo các ô có số lượng đá là: 7 + 1 + 8 + 5 + 4 + 7 = 32.7+1+8+5+4+7=32.

Hướng dẫn giải:

Đối với bài toán này nếu giải theo cách brute force thì từ hàng đầu tiên ứng với mỗi ô ta sẽ chọn hết tất cả các cách có thể đi xuống ô bên dưới đến khi nào đến hàng cuối cùng thì chọn ra cách đi có tổng số đá là nhiều nhất. Nhưng chắc chắn cách này sẽ bị TLE nên ta cần tối ưu hơn, nhận thấy rằng trong lúc duyệt hết các đường đi từ hàng đầu tiên đến hàng cuối cùng thì cùng một ô có thể tham gia vào nhiều cách đi, thế nên nếu ta lưu được lại số đá gom được đến ô đó thì sẽ tránh lãng phí được việc tính toán lại số đá ở ô đó nhiều lần. Như vậy ta hoàn toàn có thể áp dụng Dynamic Programming (Quy hoạch động) vào bài toán này với số đá tại một ô bất kỳ [i,j] = [i,j] + max([i - 1,j - 1],[i- 1,j],[i - 1,j+1])[i,j]=[i,j]+max([i−1,j−1],[i−1,j],[i−1,j+1]).

Bước 1: Đọc hết các dữ liệu cần thiết, tạo một mảng hai chiều dp để lưu các giá trị trung gian số đá tại mỗi ô, giá trị ban đầu của mảng dp sẽ bằng với giá trị mảng hai chiều từ đề bài.
Bước 2: Duyệt từ hàng thứ 22 của mảng dp đến hàng cuối cùng, ứng với mỗi hàng thì giá trị số viên đá mỗi cột trong hàng dp[i, j] = dp[i, j] + max⁡(dp[i - 1, j - 1], dp[i - 1, j], dp[i - 1, j + 1]). Nếu là cột đầu tiên thì không có cột trước đó nên không xét dp[i - 1][j - 1], tương tự nếu là cột cuối cùng thì không có cột sau đó nên không xét dp[i - 1][j + 1].
Bước 3: Duyệt hết các giá trị số đá của mỗi ô ở hàng cuối cùng, giá trị lớn nhất trong hàng này chính là kết quả của bài toán.
Độ phức tạp: O(T*h*w)O(T∗h∗w) với TT là số test case, hh là số hàng và ww là số cột của sàn nhà.


 */
public class PhilosophersStone {
}

/*
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    int T;
    cin >> T;
    while (T-- > 0)
    {
        int h, w;
        cin >> h >> w;
        vector<vector<int>> floor(h, vector<int>(w, 0));
        for (int i = 0; i < h; ++i)
        {
            for (int j = 0; j < w; ++j)
            {
                cin >> floor[i][j];
            }
        }

        vector<vector<int>> dp(floor);
        for (int i = 1; i < h; ++i)
        {
            for (int j = 0; j < w; ++j)
            {
                int a = -1, b = -1, c = -1;
                if (j > 0)
                    a = dp[i - 1][j - 1];
                if (j < w - 1)
                    b = dp[i - 1][j + 1];
                c = dp[i - 1][j];
                dp[i][j] += max(a, max(b, c));
            }
        }

        int ans = 0;
        for (int i = 0; i < w; ++i)
            ans = max(ans, dp[h - 1][i]);
        cout << ans << endl;
    }

    return 0;
}
 */

/*
import java.util.*;
import java.lang.*;
import java.io.*;

public class solution
{
    public static void main (String[] args) throws java.lang.Exception
    {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();

    	while (t-- > 0) {
            int h = in.nextInt();
            int w = in.nextInt();

            int[][] floor = new int[h][w];
            for (int i = 0; i < h; i++) {
                for (int j = 0; j < w; j++) {
                    floor[i][j] = in.nextInt();
                }
            }

            int[][] dp = new int[h][w];
            for (int j = 0; j < w; j++) {
                dp[0][j] = floor[0][j];
            }

            for (int i = 1; i < h; ++i) {
                for (int j = 0; j < w; ++j) {
                    int a = -1, b = -1, c = -1;
                    if (j > 0) {
                        a = dp[i - 1][j - 1];
                    }
                    if (j < w - 1) {
                        b = dp[i - 1][j + 1];
                    }
                    c = dp[i - 1][j];
                    dp[i][j] = floor[i][j] + Math.max(a, Math.max(b, c));
                }
            }

            int ans = 0;
            for (int i = 0; i < w; i++) {
                ans = Math.max(ans, dp[h - 1][i]);
            }
            System.out.println(ans);
        }
    }
}
 */