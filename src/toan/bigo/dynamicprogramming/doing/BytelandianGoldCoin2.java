package toan.bigo.dynamicprogramming.doing;

import java.util.Arrays;
import java.util.Scanner;

public class BytelandianGoldCoin2 {

    private static int MAX = (int) (1e6 + 1);
    private static long[] dp = new long[MAX];
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Arrays.fill(dp,-1);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            long value = Long.parseLong(line);
            System.out.println(bytelandian(value));
        }
    }

    private static long bytelandian(long n) {
        if (n <= 10) {
            return n;
        }
        if(n < MAX) {
            if(dp[(int) n] == -1)
            {
                long a = n / 2;
                long b = n / 3;
                long c = n / 4;
                long total = bytelandian(a) + bytelandian(b) + bytelandian(c);
                long result = Math.max(n,total);
                dp[(int) n] = result;
            }
            return dp[(int) n];
        }
        long a = n / 2;
        long b = n / 3;
        long c = n / 4;
        long total = bytelandian(a) + bytelandian(b) + bytelandian(c);
        return Math.max(n,total);
    }
}
