package toan.bigo.dynamicprogramming.doing;

import java.util.Arrays;
import java.util.Scanner;

public class KBasedNumber {

    public static int MAX = 20;
    public static int[] endWithZero = new int[MAX];
    public static int[] endWithNotZero = new int[MAX];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        Arrays.fill(endWithNotZero, 0);
        Arrays.fill(endWithZero, 0);
        int ans = kBaseNumber(n, k);
        System.out.println(ans);
    }

    public static int kBaseNumber(int n, int k) {
        endWithNotZero[1] = k - 1;
        for (int i = 2; i <= n; i++) {
            endWithZero[i] = endWithNotZero[i - 1];
            endWithNotZero[i] = (endWithZero[i - 1] + endWithNotZero[i - 1]) * (k - 1);
        }
        return endWithZero[n] + endWithNotZero[n];
    }
}
