package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Scanner;

/*
Hướng dẫn giải
Ở bài tập này, ta có thể thấy ta chỉ quan tâm có thể tạo được các tổng là bao nhiêu từ các đồng xu đã cho. Ta có công thức truy hồi gần giống bài Knapsack như sau:

dp[i][j]dp[i][j] là có thể tạo được tổng jj khi xét tới vật ii:
dp[i][j] = \begin{cases} dp[i-1][j] & (j < a[i]) \\ dp[i-1][j-a[i]] \wedge dp[i-1][j] & (j \ne a[i]) \end{cases} , 0 \le i < m, 0 \le j \le \sum{a}dp[i][j]={
​dp[i−1][j]
​dp[i−1][j−a[i]]∧dp[i−1][j]
​​
​(j<a[i])
​(j≠a[i])
​​ ,0≤i<m,0≤j≤∑a

Nhưng ta có thể giảm bớt chiều của bài toán thành: dp[j]dp[j] là có thể tạo được tổng jj hay không?
dp[j] = dp[j] \wedge dp[j-a[i]], , 0 \le i < m, 0 \le j \le \sum{a}dp[j]=dp[j]∧dp[j−a[i]],,0≤i<m,0≤j≤∑a

Với dp[j] = falsedp[j]=false với 1 \le j \le \sum{a}1≤j≤∑a và dp[0] = truedp[0]=true.

Khi trả lời được câu hỏi có thể tạo được một tổng nào đó không, ta sẽ duyệt qua các tổng mà ta có thể tạo được SS và ta tìm SS sao cho |\sum{a} - S - S|∣∑a−S−S∣ là nhỏ nhất.
Bước 1: Khởi tạo mảng dpdp bằng 00, dp[0] = 1dp[0]=1.
Bước 2: Tính tổng mảng aa.
Bước 3:
Lặp ii từ 00 đến m - 1m−1:
Lặp jj từ sumsum về a[i]a[i]:
Tính dp[j] = dp[j]dp[j]=dp[j] oror dp[j - a[i]]dp[j−a[i]] (oror ở đây chính là toán tử OR)
Bước 4: Lặp từ sum / 2sum/2 về 00, tìm vị trí ii đầu tiên thỏa dp[i] = 1dp[i]=1
Kết quả bài toán chính là ii ở bước 44.

Lưu ý:

Ở bước 3, ta phải lặp jj từ sumsum về a[i]a[i], không được lặp jj từ a[i]a[i] lên sumsum, vì khi lặp jj từ a[i]a[i] lên sumsum, \ta có thể vô tình cộng dồn giá trị a[i]a[i]. Lí do ta có thể vô tình cộng dồn là như sau: Ở vòng lặp jj ở trong, ta đang kiểm tra là có thể đạt được tổng jj nếu sử dụng đồng xu với giá trị a[i]a[i] hay không, nếu như ta lặp jj từ a[i]a[i] đến sumsum, khi ta tính dp[j] = dp[j]dp[j]=dp[j] oror dp[j - a[i]]dp[j−a[i]], khi giá trị dp[j - a[i]] = 1dp[j−a[i]]=1 có thể là một giá trị được cập nhật từ một lần lặp trước đó trong vòng lặp này. Khi ấy, để đạt được tổng jj, ta đã vô tình cộng giá trị a[i]a[i] vào tổng nhiều hơn một lần.
Ví dụ trước khi bắt đầu vòng lặp jj ấy, a[i] = 5a[i]=5, sum = 18sum=18 còn dp từ 55 đến 1818 đều bằng falsefalse. Giả sử khi đến j = 8j=8 ta tính được dp[8] = truedp[8]=true. Sau đó, khi đến j = 13j=13, dp[13] = dp[13]dp[13]=dp[13] oror dp[13 - 5] = falsedp[13−5]=false oror dp[8] = falsedp[8]=false oror true = truetrue=true. Khi ấy để đạt được tổng là 1313, ta đã vô tình cho rằng tổng có thể đạt được giá trị 88 ở trước đó vào. Điều này là sai vì để đạt được giá trị 88, ta đã cộng a[i]a[i] vào tổng ban đầu một lần, lúc này để đạt được tổng là 1313, ta lại vô tình cộng a[i]a[i] vào lần thứ hai.
Ở bước 4, ta chỉ là duyệt từ sum / 2sum/2 là được, tìm thấy giá trị ii (tổng có thể tạo thành) đầu tiên, thì đó là cho ra giá trị chênh lệch nhỏ nhất.
Độ phức tạp: O(m * n * (sum - min(a))O(m∗n∗(sum−min(a)) với mm là số lượng đồng xu, nn là số test.
 */
public class DividingCoins {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCases = scanner.nextInt();
        for (int t = 0; t < testCases; t++) {
            int sum = 0;
            ArrayList<Integer> coins = new ArrayList<>();
            int n = scanner.nextInt();
            for(int i = 0;i<n;i++)
            {
                int coin = scanner.nextInt();
                sum += coin;
                coins.add(coin);
            }
            System.out.println(sum - (2*knapSack(coins,sum/2)));
        }
    }

    public static int knapSack(ArrayList<Integer> items, int w) {
        int[][] k = new int[items.size() + 1][w + 1];
        for (int i = 0; i < w + 1; i++) {
            k[0][i] = 0;
        }
        for (int i = 0; i <= items.size(); i++) {
            for (int j = 0; j <= w; j++) {
                if (i == 0 || j == 0) {
                    k[i][j] = 0;
                } else if (items.get(i - 1) > j) {
                    k[i][j] = k[i - 1][j];
                } else {
                    int temp1 = items.get(i - 1) + k[i - 1][j - items.get(i - 1)];
                    int temp2 = k[i - 1][j];
                    k[i][j] = Math.max(temp1, temp2);
                }
            }
        }
        return k[items.size()][w ];
    }
}

/*
import java.util.Scanner;
import java.io.*;

class solution {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t, m, sum;
		int[] a = new int[100];
		int[] dp = new int[50005];
		t = sc.nextInt();
		while (t > 0) {
			--t;
			m = sc.nextInt();
			sum = 0;
			for (int i = 0; i < m; ++i) {
				if (sc.hasNextInt())
					a[i] = sc.nextInt();
				sum += a[i];
			}
			for (int i = 0; i < 50005; ++i) {
				dp[i] = 0;
			}
			dp[0] = 1;
			for (int i = 0; i < m; ++i) {
				for (int j = sum; j >= a[i]; --j) {
					dp[j] = (dp[j - a[i]] == 1) || (dp[j] == 1) ? 1 : 0;
				}
			}
			int diff = -1;
			for (int i = sum / 2; i >= 0; --i) {
				if (dp[i] == 1) {
					diff = Math.abs(sum - 2 * i);
					break;
				}
			}
			System.out.println(diff);
		}
		sc.close();
	}
}
 */

/*
#include <iostream>
#include <math.h>
#include <string.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0);
    int t, m, a[100], dp[50005], sum;
    cin >> t;
    while (t--) {
        cin >> m;
        sum = 0;
        for (int i = 0; i < m; i++) {
            cin >> a[i];
            sum += a[i];
        }
        memset(dp, 0, sizeof(dp));
        dp[0] = 1;
        for (int i = 0; i < m; i++) {
            for (int j = sum; j >= a[i]; j--) {
                dp[j] = dp[j - a[i]] or dp[j];
            }
        }
        int diff = -1;
        for (int i = sum / 2; i >= 0; i--) {
            if (dp[i] == 1) {
                diff = fabs(sum - 2 * i);
                break;
            }
        }
        cout << diff << endl;
    }
    return 0;
}
 */