package toan.bigo.dynamicprogramming;

/*
Hướng dẫn giải
Ta có nhận xét rằng với mỗi loại block ta hoán vị các kích thước với nhau thì có thể tạo thành tối đa 3!=63!=6 block có kích thước khác nhau. Như vậy từ nn loại khối đề cho ban đầu ta sẽ tạo ra danh sách tất cả các block có thể có. Vậy nhiệm vụ còn lại là tìm một dãy block mà sao cho đây là một dãy có kích thước hình chữ nhật đáy giảm và có tổng chiều cao các block trong dãy này là lớn nhất có thể, với ý tưởng như vậy ta có thể hoàn toàn quy về bài toán LIS (Longest Increasing Substring) để giải. Tuy nhiên phải hiện thực thêm hàm dùng để định nghĩa cách so sánh hai block như thế nào là nhỏ hơn, lớn hơn và bằng nhau.

Bước 11: Tạo ra mảng các block từ nn loại block ban đầu bằng cách hoán vị các kích thước.
Bước 22: Sắp xếp mảng block này theo thứ tự giảm dần (hoặc tăng dần nhưng cần chỉnh cách hiện thực hàm LIS tương ứng theo cách sắp xếp này) theo tiêu chí giả sử kích thước của 11 block lần lượt là x, y, zx,y,z thì từ trái sang phải nếu kích thước của block AA lớn hơn kích thước tương ứng của block BB thì AA lớn hơn BB, ngược lại nhỏ hơn thì AA nhỏ hơn BB. Trong trường hợp bằng nhau thì so sánh sánh sang kích thước kế tiếp.
Chạy hàm LIS với mảng heightheight lưu giá trị chiều cao tối đa có thể sắp tới được tương ứng với các block trong mảng khởi tạo ở bước 11, resres lưu kết quả chiều cao cao nhất có thể.
Chạy ii từ 00 tới cuối mảng các block.
Chạy jj từ 00 tới trước ii.
- Nếu như block ii nhỏ hơn block jj thì height_iheight
​i
​​  = maxmax⁡(height_iheight
​i
​​ , height_jheight
​j
​​  + chiều cao block i)i).
So sánh height_iheight
​i
​​  với resres nếu height_iheight
​i
​​  lớn hơn resres thì cập nhật cho resres.
Trả về giá trị của resres.
Độ phức tạp: O(T*N^2)O(T∗N
​2
​​ ) với TT là số test case và NN là số block.
 */
public class TheTowerBabylon {
}
/*
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef struct block {
    int dimension[3];

    block (int x, int y, int z) {
        dimension[0] = x;
        dimension[1] = y;
        dimension[2] = z;
    }

    bool CanStack (const struct block& other) {
        if (dimension[0] < other.dimension[0] && dimension[1] < other.dimension[1]) return true;
        return false;
    }

    bool operator < (const block &x) const {
		for (int i = 0; i < 3; ++i) {
            if (dimension[i] > x.dimension[i])
                return true;
            else if (dimension[i] < x.dimension[i])
                return false;
        }
        return true;
	}
} block;

void GetBlocks(vector<block>& blocks, int a, int b, int c) {
    int arr[] = {a, b, c};
    sort(arr, arr + 3);
    do {
        blocks.push_back(block(arr[0], arr[1], arr[2]));
    }
    while (next_permutation(arr, arr + 3));
}

int LIS(vector<block>& blocks) {
    int res = 0;
    vector<int> height(blocks.size());
    for (int i = 0; i < blocks.size(); ++i) {
        height[i] = blocks[i].dimension[2];
        for (int j = 0; j < i; ++j) {
            if (blocks[i].CanStack(blocks[j]))
                height[i] = max(height[i], height[j] + blocks[i].dimension[2]);
        }
        res = max(res, height[i]);
    }
    return res;
}

int main() {
	int n, a, b, c, cases = 1;
	cin >> n;

	while (n != 0) {
		vector<block> blocks;
		for (int i = 0; i < n; ++i) {
			cin >> a >> b >> c;
			GetBlocks(blocks, a, b, c);
		}
        sort(blocks.begin(), blocks.end());

		cout << "Case " << cases++ << ": maximum height = " << LIS(blocks) << endl;
		cin >> n;
	}

	return 0;
}
 */

/*
import java.util.*;

class Solution {
	static class block implements Comparable<block> {
		public int[] dimension;
		public block(int x, int y, int z) {
			dimension = new int[3];
			dimension[0] = x;
			dimension[1] = y;
			dimension[2] = z;
		}
		public boolean CanStack(block other) {
			if (dimension[0] < other.dimension[0] && dimension[1] < other.dimension[1]) return true;
	        	return false;
		}
		public int compareTo(block x)
	    {
			for(int i = 0 ; i < 3; i++) {
				if(this.dimension[i] != x.dimension[i]) {
					return this.dimension[i] - x.dimension[i];
				}
			}
			return 0;

	    }
	}


	public static ArrayList<block> GetBlocks( int a, int b, int c) {
	    int arr[] = {a, b, c};
	    Arrays.sort(arr);
	    ArrayList<block> list = new ArrayList<>();
	    list.add(new block(arr[0],arr[1],arr[2]));
	    int n = 3;
	    while(true) {
	    	 int k, l;
	         for (k = n-2; k>=0; k--) if (arr[k] < arr[k+1]) break;
	         if (k<0) break;
	         for (l = n-1; l>k; l--) if (arr[k] < arr[l]) break;

	         int temp = arr[k];
	         arr[k] = arr[l];
	         arr[l] = temp;

	         for (int i=k+1, j=n-1; i<j; i++, j--) {
	        	 temp = arr[i];
		         arr[i] = arr[j];
		         arr[j] = temp;
	         }
	         list.add(new block(arr[0],arr[1],arr[2]));
	    }
	    return list;
	}
	public static int LIS(ArrayList<block> blocks) {
	    int res = 0;
	    int[] height = new int[blocks.size()];
	    for (int i = 0; i < blocks.size(); ++i) {
	    	height[i] = blocks.get(i).dimension[2];
	        for (int j = 0; j < i; ++j) {
	            if (blocks.get(i).CanStack(blocks.get(j))) {
	            	height[i] = Math.max(height[i], height[j] + blocks.get(i).dimension[2]);
	            }
	        }
	        res = Math.max(res, height[i]);
	    }
	    return res;
	}
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n,a,b,c;
        int Case = 1;
        while (true)
            try {
                n = sc.nextInt();
                if(n == 0) break;
                ArrayList<block> blocks = new ArrayList<>();
                for(int i = 0; i < n ; i++) {
                	a = sc.nextInt();
                	b = sc.nextInt();
                	c = sc.nextInt();
                	blocks.addAll(GetBlocks(a,b,c));

                }
                Collections.sort(blocks,Collections.reverseOrder());
                System.out.printf("Case %d: maximum height = %d\n", Case++, LIS(blocks) );
            } catch (Exception e) {
                sc.close();
                System.out.println(e.toString());
                System.exit(0);
            }
    }
}
 */