package toan.bigo.dynamicprogramming;

import java.util.Scanner;

/*
Hướng dẫn giải
Giải thích ví dụ:

Với N = 10N=10, có 22 cách trả là 8*1 + 28∗1+2, hoặc 1*101∗10.
Với N = 21N=21, có 33 cách trả là 8*2 + 1*58∗2+1∗5, hoặc 8*1 + 1*138∗1+1∗13, hoặc 1*211∗21.

Hướng dẫn giải:

Bài này có thể được giải bằng Dynamic Programming.

Trước hết nhận xét rằng 21^3 = 926121
​3
​​ =9261 là lũy thừa 33 lớn nhất mà không vượt quá giới hạn đề bài. Như vậy ta chỉ cần quan tâm đến 2121 mệnh giá xu nhỏ nhất.

Ta có thể tưởng tượng rằng ta đang “di chuyển” từ 00 đến NN, mỗi lần đi một bước có độ dài bằng một mệnh giá xu. Hỏi có bao nhiêu cách di chuyển. Nếu ta giải bằng brute force, thì với N = 9999N=9999 sẽ có rất nhiều cách, không thể chạy kịp trong giới hạn thời gian. Ta sẽ cải tiến nó bằng cách áp dụng Dynamic Programming, ghi nhớ lại số cách đến ô thứ ii, với i < Ni<N.

Như vậy, gọi dp[i] là số cách ta có thể trả i đồng, ta có công thức truy hồi:

• dp[i] = tổng(dp[i - W]), trong đó WW là các mệnh giá xu bé hơn hoặc bằng ii.

Base case ở đây là dp[0] = 1, vì ta có đúng 11 cách trả 00 đồng.

Ta có thể tính toán trước dp[i] cho tất cả các ii từ 11 đến 99999999 một lần duy nhất, sau đó đọc và xuất ra kết quả tương ứng cho từng test case.

Độ phức tạp: O(N*M)O(N∗M) với NN là giới hạn của bài toán, MM là số lượng mệnh giá ta có.
 */
public class IngenousCubrency {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] coins = new int[22];
        for (int i = 0; i < 21; i++) {
            coins[i] = (int) Math.pow((i + 1), 3);
        }
        while (scanner.hasNext())
        {
            int n = scanner.nextInt();
            System.out.println(coinChange(n,coins,21));
        }
    }

    public static long coinChange(int total, int[] coins, int n) {
        long[] result = new long[total + 1];
        result[0] = 1;
        for (int i = 0; i < n; i++) {
            for (int j = coins[i]; j <= total; j++) {
                result[j] += result[j - coins[i]];
            }
        }
        return result[total];
    }
}

/*
import java.util.*;
import java.lang.*;
import java.io.*;

public class solution
{
    public static void main (String[] args) throws java.lang.Exception
    {
	    long dp[] = new long[10008];
	    dp[0] = 1L;
	    for (int j = 1; j <= 21; j++) {
	        int W = j*j*j;

	        for (int i = W; i < 10000; i++) {
	            dp[i] += dp[i - W];
	        }
	    }

        Scanner in = new Scanner(System.in);
	    while (in.hasNext()) {
	        System.out.println(dp[in.nextInt()]);
	    }
    }
}
 */

/*
#include <iostream>
using namespace std;

const int MAX = 10008;

long long dp[MAX];

int main() {

    dp[0] = 1;
    for (int j = 1; j <= 21; j++) {
        int W = j*j*j;

        for (int i = W; i < MAX; i++) {
            dp[i] += dp[i - W];
        }
    }

    int n;
    while (cin >> n) {
        cout << dp[n] << '\n';
    }
}
 */