package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/*
Để tìm được số điểm tối đa mà Tom có thể giành được, ta tính số điểm tối đa Tom có thể giành được với mỗi tấm thẻ, rồi lấy giá trị lớn nhất trong các số điểm đó.

Với mỗi hành trình trên một tấm thẻ, số điểm tối đa Tom có thể giành được chính là độ dài chuỗi con chung dài nhất của hành trình trên tấm thẻ và hành trình của Agnes.

Độ phức tạp: O(d * n^2)O(d∗n
​2
​​ ) với dd là số data set và nn là số trạm dừng cần phải ghé qua trên một hành trình.
 */
public class CrossCountry {

    public static int MAX = 2000 + 10;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCase = Integer.parseInt(scanner.nextLine());
        for (int t = 0; t < testCase; t++) {
            boolean flag = true;
            int count = 0;
            ArrayList<int[]> raceMale = new ArrayList<>();
            int[] raceAgnes = null;
            while (flag) {
                String line = scanner.nextLine();
                if (line.equals("0")) {
                    flag = false;
                } else {
                    String[] splits = line.split(" ");
                    if (count == 0) {
                        raceAgnes = new int[splits.length - 1];
                        for (int i = 0; i < splits.length - 1; i++) {
                            raceAgnes[i] = Integer.parseInt(splits[i]);
                        }
                    } else {
                        int[] raceTemp = new int[splits.length - 1];
                        for (int i = 0; i < splits.length - 1; i++) {
                            raceTemp[i] = Integer.parseInt(splits[i]);
                        }
                        raceMale.add(raceTemp);
                    }
                    count++;
                }
            }
            System.out.println(getResult(raceMale, raceAgnes));

        }
    }

    public static int getResult(ArrayList<int[]> raceMan, int[] agnes) {

        int result = -1;
        for (int i = 0; i < raceMan.size(); i++) {
            int[][] dp = new int[MAX][MAX];
            for (int j = 0; j < MAX; j++) {
                Arrays.fill(dp[j], -1);
            }
            result = Math.max(result, crossCountry(dp, agnes, raceMan.get(i), agnes.length, raceMan.get(i).length));
        }
        return result;
    }

    public static int crossCountry(int[][] dp, int[] agnes, int[] raceMan, int m, int n) {
        if (m == 0 || n == 0) {
            return 0;
        }
        if (dp[m - 1][n - 1] != -1) {
            return dp[m - 1][n - 1];
        }
        if (agnes[m - 1] == raceMan[n - 1]) {
            dp[m - 1][n - 1] = 1 + crossCountry(dp, agnes, raceMan, m - 1, n - 1);
        } else {
            dp[m - 1][n - 1] = Math.max(crossCountry(dp, agnes, raceMan, m, n - 1), crossCountry(dp, agnes, raceMan, m - 1, n));
        }
        return dp[m - 1][n - 1];
    }
}

/*
#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int lcs(vector<int>& route, vector<int>& agnesRoute) {
    int n = route.size() - 1;
    int m = agnesRoute.size() - 1;
    vector<vector<int>> dp(n + 1, vector<int>(m + 1, 0));

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (route[i] == agnesRoute[j]) {
                dp[i][j] = dp[i - 1][j - 1] + 1;
            }
            else {
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
            }
        }
    }

    return dp[n][m];
}

void solveDataSet() {
    vector<int> agnesRoute;
    agnesRoute.push_back(0);

    while (true) {
        int point;
        cin >> point;

        if (point == 0) {
            break;
        }

        agnesRoute.push_back(point);
    }

    vector<vector<int>> tomRoutes;

    while (true) {
        vector<int> route;
        route.push_back(0);

        while (true) {
            int point;
            cin >> point;

            if (point == 0) {
                break;
            }
            route.push_back(point);
        }

        if (route.size() == 1) {
            break;
        }
        tomRoutes.push_back(route);
    }

    int ans = 0;

    for (vector<int>& route : tomRoutes) {
        ans = max(ans, lcs(route, agnesRoute));
    }

    cout << ans << '\n';
}

int main() {
    int d;
    cin >> d;

    for (int i = 0; i < d; i++) {
        solveDataSet();
    }

    return 0;
}
 */

/*
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static Scanner in = new Scanner(System.in);
    static PrintWriter out = new PrintWriter(System.out);

    public static void main(String[] args) {
        int d = in.nextInt();

        for (int i = 0; i < d; i++) {
            solveDataSet();
        }

        out.close();
    }

    static void solveDataSet() {
        List<Integer> agnesRoute = new ArrayList<>();
        agnesRoute.add(0);

        while (true) {
            int point = in.nextInt();
            if (point == 0) {
                break;
            }
            agnesRoute.add(point);
        }

        List<List<Integer>> tomRoutes = new ArrayList<>();

        while (true) {
            List<Integer> route = new ArrayList<>();
            route.add(0);

            while (true) {
                int point = in.nextInt();
                if (point == 0) {
                    break;
                }
                route.add(point);
            }

            if (route.size() == 1) {
                break;
            }
            tomRoutes.add(route);
        }

        int ans = 0;

        for (List<Integer> route : tomRoutes) {
            ans = Math.max(ans, lcs(route, agnesRoute));
        }

        out.println(ans);
    }

    static int lcs(List<Integer> route, List<Integer> agnesRoute) {
        int n = route.size() - 1;
        int m = agnesRoute.size() - 1;
        int[][] dp = new int[n + 1][m + 1];

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (route.get(i).equals(agnesRoute.get(j))) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                }
                else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }

        return dp[n][m];
    }
}
 */