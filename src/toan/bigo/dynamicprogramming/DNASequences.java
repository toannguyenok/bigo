package toan.bigo.dynamicprogramming;

/*
Hướng dẫn giải
Ta sẽ áp dụng tư tưởng của thuật toán Longest Increasing Subsequence (LCS) cho bài toán này. Nhắc lại thuật toán LCS:

Nếu S[i] = T[j] \to dp(i, j) = dp(i-1, j-1) + 1S[i]=T[j]→dp(i,j)=dp(i−1,j−1)+1
Ngược lại, dp(i, j) = max(dp(i-1, j), dp(i, j-1))dp(i,j)=max(dp(i−1,j),dp(i,j−1))
Ta sẽ áp dụng tư tưởng tương tự cho bài toán này:

Nếu S[i..i+e] = T[j..j+e] \to dp(i, j) = dp(i-e, j-e) + 1S[i..i+e]=T[j..j+e]→dp(i,j)=dp(i−e,j−e)+1
Ngoài ra, dp(i, j) = max(dp(i-1, j), dp(i, j-1))dp(i,j)=max(dp(i−1,j),dp(i,j−1))
Vấn đề còn lại là kiểm tra S[i..i+e]S[i..i+e] với T[j..j+e]T[j..j+e] có bằng nhau hay không. Ta có thể tính trước tất cả các xâu con có thể bằng cách như sau:

Gọi C(i, j)C(i,j) là độ dài ee lớn nhất sao cho S[i..i+e] = T[j..j+e]S[i..i+e]=T[j..j+e]. Ta có thể lập một thuật toán DP:

Nếu S[i] = T[j] \to C(i, j) = C(i-1, j-1) + 1S[i]=T[j]→C(i,j)=C(i−1,j−1)+1
Ngược lại, C(i, j) = 0C(i,j)=0
Sau đó, để kiểm tra S[i..i+e]S[i..i+e] với T[j..j+e]T[j..j+e] có bằng nhau hay không, ta chỉ cần kiểm tra C(i, j)C(i,j) có lớn hơn hoặc bằng ee hay không.

Ngoài ra ta cũng có thể áp dụng các thuật toán khác để kiểm tra như String Hashing, KMP, ....

Độ phức tạp: O(N*M*KN∗M∗K) – Với N, MN,M là độ dài hai xâu, KK là số được cho trong đề bài.


 */
public class DNASequences {
}
/*
#include <bits/stdc++.h>
using namespace std;
const int N = 1005;

int f[N][N], c[N][N], k, n, m;
char a[N], b[N];
int main() {
    while (scanf("%d", &k), k) {
        scanf("%s%s", a + 1, b + 1);
        n = strlen(a + 1);
        m = strlen(b + 1);
        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j <= m; ++j) {
                if (a[i] == b[j])
                    c[i][j] = c[i - 1][j - 1] + 1;
                else
                    c[i][j] = 0;
                f[i][j] = max(f[i - 1][j], f[i][j - 1]);
                for (int e = k; e <= c[i][j]; ++e)
                    f[i][j] = max(f[i][j], f[i - e][j - e] + e);
            }
        }
        printf("%d\n", f[n][m]);
    }
}
 */

/*

 */