package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Scanner;

/*
Hướng dẫn giải
Ta nhận thấy rằng một thanh vàng chỉ có thể ở trong container mà không rớt ra ngoài khi và chỉ khi 2 *2∗ (độ dài phần nằm trong) \ge≥ độ dài thanh vàng.
Như vậy, nếu ta gấp đôi toàn bộ chiều dài của những thanh vàng, gấp đôi chiều dài của container, thì phần chiều dài của thanh vàng nằm trong container, ta có thể xem như chính là độ dài của thanh vàng ngay từ ban đầu.
Tuy nhiên, vì container chỉ rỗng 2 đầu, như vậy chỉ có tối đa 22 thanh vàng có phần nằm ở ngoài container. Từ đó, ta có thể đặt bài toán con quy hoạch động như sau:
Gọi f[i][j][cnt]f[i][j][cnt] là tổng giá trị lớn nhất khi ta có ii thanh vàng, chiều dài container tối đa là jj và cntcnt là số lượng thanh vàng được phép có phần nằm bên ngoài. (Lưu ý là bài toán con được gọi sau khi ta đã gấp đôi độ dài container và độ dài những thanh vàng lên).
Bài toán cơ sở:
Nếu như không có thanh vàng nào, thì buộc cnt = 0cnt=0, khi đó f[0][j][0] = 0f[0][j][0]=0. Nếu i = 0i=0 và cnt > 0cnt>0, thì điều đó là vô lý, do đó nếu i = 0i=0 và cnt > 0, f[0][j][cnt] = - \inftycnt>0,f[0][j][cnt]=−∞.
Nếu j = 0j=0, hiển nhiên f[i][0][cnt] = 0f[i][0][cnt]=0.
Công thức truy hồi của bài toán:
Khi ta xét đến thanh vàng thứ ii, ta có 3 sự lựa chọn:
Lựa chọn 1: Bỏ qua thanh vàng thứ ii, như vậy f[i][j][cnt] = f[i-1][j][cnt]f[i][j][cnt]=f[i−1][j][cnt].
Lựa chọn 2: Ta lựa chọn bỏ trọn vẹn thanh vàng thứ ii vào trong container, điều kiện này chỉ xảy ra khi j \ge 2 * a_ij≥2∗a
​i
​​ . Khi đó: f[i][j][cnt] = f[i-1][j- 2*a_i][cnt] + v_if[i][j][cnt]=f[i−1][j−2∗a
​i
​​ ][cnt]+v
​i
​​ . (Cộng thêm v_iv
​i
​​  vì khi đó ta thêm một thanh vàng thứ ii vào container rồi).
Lựa chọn 3: Ta bỏ thanh vàng thứ ii vào, nhưng mà ta để lộ một phần của thanh vàng ra khỏi container. Để làm được cần thỏa mãn j \ge a_ij≥a
​i
​​  và cnt > 0cnt>0, khi đó: f[i][j][cnt] = f[i-1][j-a_i][cnt-1] + v_if[i][j][cnt]=f[i−1][j−a
​i
​​ ][cnt−1]+v
​i
​​ .
Kết quả bài toán: Giá trị lớn nhất của f[n][2 * L][cnt]f[n][2∗L][cnt] với cntcnt chạy từ 00 đến 22.
Tuy nhiên, vẫn còn một trường hợp là container chỉ chứa một thanh duy nhất, và thanh này có chiều dài lớn hơn cả LL. Do đó, kết quả cần phải lấy lớn nhất với các v_iv
​i
​​  ban đầu.
Độ phức tạp: O(N * L)O(N∗L) với NN là số lượng thanh vàng và LL là độ dài của container.
 */
public class PickTheSticks {

    public static class Stick {
        int a;
        int v;

        public Stick(int a, int v) {
            this.a = a;
            this.v = v;
        }
    }

    public static class StickInt{
        int half;
        int full;
        public StickInt(int half, int full)
        {
            this.half = half;
            this.full = full;
        }
    }

    private static ArrayList<Stick> input = new ArrayList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCase = scanner.nextInt();
        scanner.nextLine();
        for (int t = 0; t < testCase; t++) {
            scanner.nextLine();
            String line = scanner.nextLine();
            input.clear();
            String[] temp = line.split(" ");
            int n = Integer.valueOf(temp[0]);
            int l = Integer.valueOf(temp[1]);
            for (int i = 0; i < n; i++) {
                String line2 = scanner.nextLine();
                String[] temp2 = line2.split(" ");
                input.add(new Stick(Integer.parseInt(temp2[0]), Integer.valueOf(temp2[1])));
            }
            System.out.println("Case #" + (t + 1) + ": " + knapSack(input, l));
        }
    }

    public static int knapSack(ArrayList<Stick> items, int l) {
        int[][] k = new int[items.size() + 1][l + 1];
        int[][] full = new int[items.size() + 1][l + 1];
        for (int i = 0; i < l + 1; i++) {
            k[0][i] = 0;
            full[0][i]= 0;
        }
        for (int i = 0; i <= items.size(); i++) {
            for (int j = 0; j <= l; j++) {
                if (i == 0 || j == 0) {
                    k[i][j] = 0;
                } else if ((items.get(i - 1).a + 1) / 2 > j) {
                    k[i][j] = k[i - 1][j];
                    k[i][j] = 0;
                } else {
                    int temp1 = items.get(i - 1).v + k[i - 1][j - (items.get(i - 1).a + 1) / 2];
                    int temp2 = k[i - 1][j];
                    k[i][j] = Math.max(temp1, temp2);
                }
                System.out.print(k[i][j] + " ");
            }
            System.out.println();
        }
        return k[items.size()][l];
    }
}
/*
#include <iostream>
#include <algorithm>

using namespace std;
const int MAX_N = 1005;
const int MAX_L = 4005;

struct item {
	int l, v;
};

long long f[MAX_N][MAX_L][3];
int test;
int n, L;
item a[MAX_N];

int main () {
	//freopen("input.txt", "r", stdin);
	//freopen("output2.txt", "w", stdout);

	cin >> test;
	for (int tt = 1; tt <= test; tt++) {
		cout << "Case #" << tt << ": ";
		long long res = -1;
		cin >> n >> L;
		L = L * 2;
		for (int i = 1; i <= n; i++) {
			cin >> a[i].l >> a[i].v;
			res = max(res, 1LL * a[i].v);
		}

		for (int i = 0; i < MAX_N; i++) {
			for (int j = 0; j < MAX_L; j++) {
				for (int c = 0; c <= 2; c++) {
					f[i][j][c] = 0;
				}
			}
		}

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= L; j++) {
				for (int c = 0; c <= 2; c++) {
					f[i][j][c] = f[i - 1][j][c];
					if (j >= 2 * a[i].l) {
						f[i][j][c] = max(f[i][j][c], f[i - 1][j - 2 * a[i].l][c] + 1LL * a[i].v);
					}

					if (j >= a[i].l && c > 0) {
						f[i][j][c] = max(f[i][j][c], f[i - 1][j - a[i].l][c - 1] + 1LL * a[i].v);
					}
				}
			}
		}

		for (int c = 0; c <= 2; c++) {
			res = max(res, f[n][L][c]);
		}

		cout << res << endl;
	}
	return 0;
}
 */

/*
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    static class Item {
        int l, v;

        public Item(int l, int v) {
            this.l = l;
            this.v = v;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int T = in.nextInt();

        for(int iT = 1; iT <= T; iT++) {
            int n = in.nextInt(), L = in.nextInt();
            L *= 2;

            Item[] a = new Item[n+1];
            for(int i = 1; i <= n; i++) {
                int l = in.nextInt(), v = in.nextInt();
                a[i] = new Item(l, v);
            }

            long[][][] dp = new long[n+1][L+1][3];
            for(int i = 0; i <= n; i++)
                for(int j = 0; j <= L; j++)
                    for(int c = 0; c <= 2; c++)
                        dp[i][j][c] = 0;

            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= L; j++) {
                    for (int c = 0; c <= 2; c++) {
                        dp[i][j][c] = dp[i - 1][j][c];
                        if (j >= 2 * a[i].l)
                            dp[i][j][c] = Math.max(dp[i][j][c], dp[i - 1][j - 2 * a[i].l][c] + a[i].v);
                        if (j >= a[i].l && c > 0)
                            dp[i][j][c] = Math.max(dp[i][j][c], dp[i - 1][j - a[i].l][c - 1] + a[i].v);
                    }
                }
            }

            long res = -1;
            for(int i = 1; i <= n; i++)
                res = Math.max(res, a[i].v);
            for(int c = 0; c <= 2; c++)
                res = Math.max(res, dp[n][L][c]);

            out.printf("Case #%d: %d\n", iT, res);
        }

        out.close();
    }
}
 */