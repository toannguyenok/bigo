package toan.bigo.dynamicprogramming;

/*
Hướng dẫn giải
Giải thích ví dụ:

Nếu ta nâng mảng BB lên 11 đơn vị, thì B = [4, 5, 5, 4, 3]B=[4,5,5,4,3] -> xuất hiện 11 lần trong AA.
Nếu ta hạ mảng BB xuống 11 đơn vị, thì B = [2, 3, 3, 2, 1]B=[2,3,3,2,1] -> xuất hiện 11 lần trong AA.
Hướng dẫn giải:

Nhận xét: do ta có thể nâng/hạ mảng BB, nên ta sẽ quan trọng độ lệch giữa hai phần tử kề nhau (khi trừ thì phần cùng cộng/trừ vào sẽ bị mất).
Gọi:
dB_i = B_{i + 1} - B_i (0 \le i < m - 1)dB
​i
​​ =B
​i+1
​​ −B
​i
​​ (0≤i<m−1)
dA_i = A_{i + 1} - A_i (0 \le i < n - 1)dA
​i
​​ =A
​i+1
​​ −A
​i
​​ (0≤i<n−1)
Nhận thấy bây giờ nhiệm vụ bài toán ta sẽ là tìm xem trong dAdA, có bao nhiêu lần xuất hiện dBdB.
Lưu ý trường hợp mảng BB có 11 phần tử thì dBdB sẽ rỗng, nhưng kết quả ta sẽ là |A|∣A∣.
Độ phức tạp: O(|A| + |B|)O(∣A∣+∣B∣) với |A|, |B|∣A∣,∣B∣ là độ dài mảng AA và BB.


 */
public class MUHAndCubeWalls {
}
/*
#include <iostream>
#include <vector>
using namespace std;
void KMPPreprocess(vector<int> p, vector<int> & prefix) {
    int m = p.size();
    prefix.resize(m, 0);
    int j = 0;
    int i = 1;
    while (i < m) {
        if (p[i] == p[j]) {
            j++;
            prefix[i] = j;
            i++;
        }
        else {
            if (j != 0) j = prefix[j - 1];
            else {
                prefix[i] = 0;
                i++;
            }
        }
    }
}
int calc(vector<int> B, vector<int> A) {
    if (B.size() == 0) {
        return A.size() + 1;
    }

    vector<int>prefix;
    KMPPreprocess(B, prefix);

    int cnt = 0;

    int n = A.size();
    int m = B.size();
    int i = 0, j = 0;
    while (i < n) {
        if (B[j] == A[i]) {
            i++;
            j++;
        }
        if (j == m) {
            cnt++;
            j = prefix[j - 1];
        }
        else if (i < n && B[j] != A[i]) {
            if (j != 0) {
                j = prefix[j - 1];
            } else {
                i++;
            }
        }
    }
    return cnt;
}

int main() {
    int n, w, x, y;
    vector<int> A, B;

    cin >> n >> w;

    A.resize(n - 1);
    cin >> x;
    for (int i = 0; i < n - 1; i++) {
        cin >> y;
        A[i] = y - x;
        x = y;
    }

    B.resize(w - 1);
    cin >> x;
    for (int i = 0; i < w - 1; i++) {
        cin >> y;
        B[i] = y - x;
        x = y;
    }

    cout << calc(B, A);

    return 0;

}
 */

/*
import java.io.*;
import java.lang.*;
import java.util.*;

public class Solution_AC {
    public static void KMPPreprocess(int[] p, int[] prefix) {
        int m = p.length;
        prefix[0] = 0;
        int i = 1, j = 0;
        while (i < m) {
            if (p[i] == p[j]) {
                j++;
                prefix[i] = j;
                i++;
            }
            else {
                if (j != 0) {
                    j = prefix[j - 1];
                }
                else {
                    prefix[i] = 0;
                    i++;
                }
            }
        }
    }

    public static int calc(int[] B, int[] A) {
        if (B.length == 0) {
            return A.length + 1;
        }

        int[] prefix;
        prefix = new int[B.length];
        KMPPreprocess(B, prefix);

        int cnt = 0;

        int n = A.length;
        int m = B.length;
        int i = 0, j = 0;
        while (i < n) {
            if (B[j] == A[i]) {
                i++;
                j++;
            }
            if (j == m) {
                cnt++;
                j = prefix[j - 1];
            }
            else if (i < n && B[j] != A[i]) {
                if (j != 0) {
                    j = prefix[j - 1];
                } else {
                    i++;
                }
            }
        }
        return cnt;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n, w, x, y;
        int[] A, B;

        n = sc.nextInt();
        w = sc.nextInt();

        A = new int [n - 1];
        x = sc.nextInt();
        for (int i = 0; i < n - 1; i++) {
            y = sc.nextInt();
            A[i] = y - x;
            x = y;
        }

        B = new int [w - 1];
        x = sc.nextInt();
        for (int i = 0; i < w - 1; i++) {
            y = sc.nextInt();
            B[i] = y - x;
            x = y;
        }

        System.out.print(calc(B, A));
    }
}
 */