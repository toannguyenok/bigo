package toan.bigo.dynamicprogramming;

/*
Hướng dẫn giải
Đầu tiên, ta thấy rằng phần tử B_{ij}B
​ij
​​  mang giá trị 11 khi tồn tại ít nhất một giá trị trên dòng ii hoặc cột jj của AA bằng 11. Ngược lại thì B_{ij}B
​ij
​​  sẽ bằng 00.

Như vậy, ta có thể giải bài toán bằng cách sau:

Giả sử ma trận AA của ta ban đầu đều mang giá trị 11.
Duyệt ma trận BB, nếu tại một vị trí B_{ij}B
​ij
​​  mà nó bằng 00 thì ta sẽ gán toàn bộ dòng ii và cột jj của AA bằng 00.
Kiểm tra lại xem ma trận AA của ta có thể sinh ra được một ma trận giống BB hay không.
Để làm bước 3, gọi CC là một ma trận được sinh ra từ ma trận AA như đề bài đã nói. Sau khi có CC, ta sẽ kiểm tra xem CC có giống BB hay không. Ở đây, ta có thể sử dụng Hash Table để so sánh giá trị hash của từng dòng trong BB và CC có giống nhau hay không. Nếu BB không giống với CC thì ta in ra “NO”. Ngược lại thì ta in ra “YES” và mảng AA.

Độ phức tạp: O(n \cdot m \cdot (n+m))O(n⋅m⋅(n+m)) với nn là số cột và mm là số dòng của ma trận.
 */
public class OrInMatrix {
}

/*
#include <iostream>
using namespace std;

const int N = 109;

int n, m, a[N][N] = {}, b[N][N] = {}, c[N][N] = {};

unsigned int rs_hash(int keys[]) {
    unsigned int a = 63689, b = 378551;
    unsigned int hash_value = 0;
    for (int i = 0; i < m; i++) {
        hash_value = hash_value * a + keys[i];
        a = a * b;
    }
    return hash_value & 0x7FFFFFFF;
}

int main() {
    cin >> n >> m;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> b[i][j];
        }
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            a[i][j] = 1;
        }
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (b[i][j] == 0) {
                for (int k = 0; k < n; k++) {
                    a[k][j] = 0;
                }
                for (int k = 0; k < m; k++) {
                    a[i][k] = 0;
                }
            }
        }
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            for (int k = 0; k < n; k++) {
                if (a[k][j] == 1) {
                    c[i][j] = 1;
                }
            }
            for (int k = 0; k < m; k++) {
                if (a[i][k] == 1) {
                    c[i][j] = 1;
                }
            }
        }
    }

    bool flag = true;
    for (int i = 0; i < n; i++) {
        if (rs_hash(b[i]) != rs_hash(c[i])) {
            flag = false;
            break;
        }
    }

    if (flag) {
        cout << "YES\n";
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                cout << a[i][j] << " ";
            }
            cout << "\n";
        }
    }
    else {
        cout << "NO\n";
    }
    return 0;
}
 */

/*

 */