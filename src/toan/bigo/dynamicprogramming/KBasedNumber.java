package toan.bigo.dynamicprogramming;

import java.util.Scanner;

/*
Hướng dẫn giải
Giải thích ví dụ:

Có 9090 số hợp lệ có 22 chữ số biểu diễn dưới dạng cơ số 1010. Đó là các số từ 1010 đến 9999.

Hướng dẫn giải:

Với N = 1N=1, thì kết quả chắc chắn là KK. Đối với các trường hợp còn lại:

Giả sử, ta có các số biễu diễn dưới dạng cơ số KK với chiều dài ii. Vậy ta có thể tạo được bao nhiêu số cơ số KK chiều dài i + 1i+1?
Chiều dài tăng thêm 11 có nghĩa là chúng ta cần thêm 11 chữ số nữa (các chữ số bao gồm 0,1,2,...,K - 10,1,2,...,K−1) vào các số có độ dài i với cơ số KK thỏa mãn yêu cầu đề bài.
Nếu số kết thúc là 00 thì ta có thể thêm một trong (K - 1)(K−1) số từ 00 đến K - 1K−1, ngược lại thì ta có thể thêm một trong (K - 2)(K−2) số từ 11 đến K - 1K−1.
Lúc này chúng ta cần dùng 22 mảng một chiều gồm các cặp endNot0endNot0 và endWith0endWith0 với:

endNot0[i]endNot0[i] là số lượng chữ số chiều dài ii cơ số KK và kết thúc là một số khác 00.
endWith0[i]endWith0[i] là số lượng chữ số chiều dài ii cơ số KK và kết thúc là 00.
Chúng ta có thể thấy số endWith0[i+1]= endNot0[i]endWith0[i+1]=endNot0[i] vì ta chỉ được thêm 11 số 00 vào sau những số có độ dài ii mà chữ số cuối cùng khác 00. Còn endNot0[i+1] = (endNot0[i] + endWith0[i]) * (K-1)endNot0[i+1]=(endNot0[i]+endWith0[i])∗(K−1) vì chúng ta có thể thêm bất kì chữ số khác 00 nào vào sau số có độ dài ii đã có được.
Khi đó ta sẽ có được công thức:

endWith0[i] = endNot0[i - 1].endWith0[i]=endNot0[i−1].
endNot0[i] = (endWith0[i - 1] + endNot0[i - 1]) * (K- 1)endNot0[i]=(endWith0[i−1]+endNot0[i−1])∗(K−1).
Kết quả cuối cùng thu được là: endNot0[N] + endWith0[N]endNot0[N]+endWith0[N].

Độ phức tạp: O(N)O(N).


 */
public class KBasedNumber {

    private static int MAX = 20 + 5;

    public static int[] countEndWith0 = new int[MAX];
    public static int[] countEndWithOut0 = new int[MAX];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        countEndWith0[0] = 0;
        countEndWithOut0[0] = k - 1;
        for (int i = 1; i < n; i++) {
            countEndWith0[i] = countEndWithOut0[i - 1];
            countEndWithOut0[i] = (countEndWith0[i - 1] + countEndWithOut0[ i- 1]) * (k - 1);
        }
        System.out.println((countEndWith0[n - 1] + countEndWithOut0[n-1]) + "");
    }
}

/*
#include <bits/stdc++.h>
using namespace std;

int main() {
    int N, K;
    cin >> N >> K;

    if (N == 1){
        cout << K << endl;
        return 0;
    }
    vector<int> endWith0(20);
    vector<int> endNot0(20);
    endNot0[1] = K -1;

    for (int i = 2; i <= N; i++) {
        endWith0[i] = endNot0[i - 1];
        endNot0[i]= (endNot0[i - 1] + endWith0[i - 1]) * ( K - 1);
    }
    cout << endNot0[N]+ endWith0[N]<< endl;

    return 0;
}
 */

/*
import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
    public static void main (String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int N, K;
        N = sc.nextInt();
        K = sc.nextInt();

        if (N == 1) {
            System.out.println(K);
            return;
        }
        int[] endWith0 = new int[20];
        int[] endNot0 = new int[20];

        endNot0[1] = K -1;
        for (int i = 2; i <= N; i++) {
            endWith0[i] = endNot0[i-1];
            endNot0[i] = (endNot0[i-1] + endWith0[i-1]) * (K-1);
        }
        System.out.println(endNot0[N] + endWith0[N]);
    }
}
 */