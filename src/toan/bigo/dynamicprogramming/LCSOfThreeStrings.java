package toan.bigo.dynamicprogramming;


/*
Hướng dẫn giải
Đây là một bài toán mở rộng của bài toán tìm dãy con chung dài nhất (LCS) của 2 dãy. Ta sẽ giải bài toán này với ý tưởng quy hoạch động tương tự như bài toán tìm LCS của 2 dãy.

Kí hiệu s[l..r]s[l..r] là xâu con liên tiếp gồm các kí tự từ vị trí ll đến vị trí rr của xâu ss.

Ta gọi dp[i][j][p]dp[i][j][p] là độ dài dãy con chung dài nhất của ba xâu a[1..i]a[1..i], b[1..j]b[1..j] và c[1..p]c[1..p].

Công thức truy hồi để tính giá trị dp[i][j][p]dp[i][j][p] như sau:

Nếu i = 0i=0 hoặc j = 0j=0 hoặc p = 0p=0 thì ít nhất một trong ba xâu là xâu rỗng. Ta có: $dp[i][j][p] = 0

Nếu a_i = b_j = c_pa
​i
​​ =b
​j
​​ =c
​p
​​  thì kết quả sẽ là dãy con chung dài nhất của ba xâu a[1..i-1]a[1..i−1], b[i..j-1]b[i..j−1], c[1..p-1]c[1..p−1], sau đó ghép thêm kí tự a_ia
​i
​​  vào đuôi. Do đó: $dp[i][j][p] = dp[i-1][j-1][p-1] + 1

Nếu cả hai điều kiện trên đều không thỏa, kết quả sẽ là giá trị lớn nhất trong 3 giá trị sau:

Độ dài dãy con chung dài nhất của ba xâu a[1..i-1]a[1..i−1], b[1..j]b[1..j] và c[1..p]c[1..p].
Độ dài dãy con chung dài nhất của ba xâu a[1..i]a[1..i], b[1..j-1]b[1..j−1] và c[1..p]c[1..p].
Độ dài dãy con chung dài nhất của ba xâu a[1..i]a[1..i], b[1..j]b[1..j] và c[1..p-1]c[1..p−1].
Do đó: dp[i][j][p]dp[i][j][p] = min(dp[i-1][j][p], dp[i][j-1][p], dp[i][j][p-1]dp[i−1][j][p],dp[i][j−1][p],dp[i][j][p−1]

Kết quả của bài toán sẽ là dp[n][m][k]dp[n][m][k].

Độ phức tạp: O(nmk) với n, m, k lần lượt là độ dài ba xâu a, b, c.


 */
public class LCSOfThreeStrings {
}

/*
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
    int nTest;
    cin >> nTest;

    for(int iTest = 1; iTest <= nTest; ++iTest) {
        int na, nb, nc;
        string a, b, c;

        cin >> na >> nb >> nc;
        cin >> a >> b >> c;

        vector<vector<vector<int>>> L(na + 1, vector<vector<int>>(nb + 1, vector<int>(nc + 1, 0)));

        for(int i = 0; i <= na; ++i) {
            for(int j = 0; j <= nb; ++j) {
                for(int k = 0; k <= nc; ++k) {
                    if (i == 0 || j == 0 || k == 0)
                        L[i][j][k] = 0;
                    else if (a[i - 1] == b[j - 1] && b[j - 1] == c[k - 1])
                        L[i][j][k] = L[i - 1][j - 1][k - 1] + 1;
                    else
                        L[i][j][k] = max(L[i - 1][j][k], max(L[i][j - 1][k], L[i][j][k - 1]));
                }
            }
        }

        cout << L[na][nb][nc] << endl;
    }

	return 0;
}
 */

/*
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int nTest = in.nextInt();

        for(int iTest = 1; iTest <= nTest; ++iTest) {
            int na = in.nextInt(), nb = in.nextInt(), nc = in.nextInt();
            String a = in.next(), b = in.next(), c = in.next();

            int[][][] L = new int[na + 1][nb + 1][nc + 1];
            for(int i = 0; i <= na; ++i) {
                for(int j = 0; j <= nb; ++j) {
                    for(int k = 0; k <= nc; ++k) {
                        if (i == 0 || j == 0 || k == 0)
                            L[i][j][k] = 0;
                        else if (a.charAt(i-1) == b.charAt(j-1) && b.charAt(j-1) == c.charAt(k-1))
                            L[i][j][k] = L[i - 1][j - 1][k - 1] + 1;
                        else
                            L[i][j][k] = Math.max(L[i - 1][j][k], Math.max(L[i][j - 1][k], L[i][j][k - 1]));
                    }
                }
            }

            out.println(L[na][nb][nc]);
        }

        out.close();
    }
}
 */