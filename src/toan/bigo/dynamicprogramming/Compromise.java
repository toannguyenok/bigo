package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/*
Hướng dẫn giải
Đây là một bài quy hoạch động về dãy con chung dài nhất cơ bản, thay vì ta tìm dãy các ký tự dài nhất thì bây giờ ta xem một từ như một ký tự và ta đi tìm dãy từ chung dài nhất (theo số lượng từ).

Đầu tiên, ta sẽ đưa hai đoạn văn về thành hai mảng với mỗi thành phần trong mảng là một từ. Sau đó ta sẽ áp dụng phương pháp tìm dãy con chung lớn nhất cho hai mảng này, ta sẽ có được kết quả trên bảng quy hoạch động, tạm gọi bảng là dpdp.

Cuối cùng, ta phải truy vết lại để lấy các từ chung trong hai đoạn văn bản dựa vào bảng quy hoạch động dpdp ở trên. Ta cần một mảng để lưu kết quả. Gọi trace(i, j)trace(i,j) là hàm truy vết với ii là vị trí đang xét trên đoạn văn thứ nhất và jj là vị trí đang xét trên đoạn văn thứ hai, cách truy vết có thể được mô tả một cách đệ quy như sau:

Nếu i=0i=0 hoặc j=0j=0, ta sẽ thoát hàm;
Nếu từ thứ ii trong đoạn thứ nhất bằng từ thứ jj trong đoạn thứ hai và dp_{i, j} = dp_{i-1, j-1} + 1dp
​i,j
​​ =dp
​i−1,j−1
​​ +1, ta gọi đệ quy hàm truy vết trace(i-1, j-1)trace(i−1,j−1) rồi sau đó ta đưa từ thứ ii của đoạn thứ nhất (hoặc từ thứ jj của đoạn thứ hai) vào cuối mảng kết quả.
Nếu dp_{i, j} = dp_{i-1, j}dp
​i,j
​​ =dp
​i−1,j
​​  thì ta chỉ cần gọi đệ quy hàm truy vết trace(i-1,j)trace(i−1,j);
Cuối cùng, nếu dp_{i, j} = dp_{i, j-1}dp
​i,j
​​ =dp
​i,j−1
​​  thì ta chỉ cần gọi đệ quy hàm truy vết trace(i,j-1)trace(i,j−1).
Sau khi gọi hàm truy vết trace(n,m)trace(n,m) và hàm truy vết chạy xong, ta chỉ việc in ra các từ trong mảng lưu kết quả.

Độ phức tạp: O(T*n*m)O(T∗n∗m) với TT là số lượng test case, nn là số lượng từ trong đoạn văn thứ nhất và mm là số lượng từ trong đoạn văn thứ hai.
 */
public class Compromise {

    public static int MAX = 100 + 5;

    private static int[][] dp = new int[MAX][MAX];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> string1 = new ArrayList<>();
        ArrayList<String> string2 = new ArrayList<>();
        int count = 0;
        int countResult = 0;
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            if (line.equals("#")) {
                count++;
                countResult++;
                if (countResult == 2 ) {
                    for (int i = 0; i < MAX; i++) {
                        Arrays.fill(dp[i], -1);
                    }
                    int lengthResult = lcs(string1, string2, string1.size(), string2.size());
                    String[] result = printLCS(lengthResult, string1, string2, string1.size(), string2.size());
                    for (int i = 0; i < result.length; i++) {
                        System.out.print(result[i] + " ");
                    }
                    System.out.println();
                    string1.clear();
                    string2.clear();
                    countResult = 0;
                }
            } else {
                String[] temp = line.split(" ");
                for (int i = 0; i < temp.length; i++) {
                    if (count % 2 == 0) {
                        string1.add(temp[i]);
                    } else  {
                        string2.add(temp[i]);
                    }
                }
            }
        }
    }

    public static int lcs(ArrayList<String> string1, ArrayList<String> string2, int m, int n) {
        if (m == 0 || n == 0) {
            return 0;
        }
        if (dp[m][n] != -1) {
            return dp[m][n];
        }
        if (string1.get(m - 1).equals(string2.get(n - 1))) {
            dp[m][n] = 1 + lcs(string1, string2, m - 1, n - 1);
        } else {
            dp[m][n] = Math.max(lcs(string1, string2, m, n - 1), lcs(string1, string2, m - 1, n));
        }
        return dp[m][n];
    }

    public static String[] printLCS(int lengthResult, ArrayList<String> string1, ArrayList<String> string2, int m, int n) {
        int i = m;
        int j = n;
        String[] ans = new String[lengthResult];
        while (i > 0 && j > 0) {
            if (string1.get(i - 1).equals(string2.get(j - 1))) {
                ans[lengthResult - 1] = string1.get(i - 1);
                i--;
                j--;
                lengthResult--;
            } else if (dp[i - 1][j] > dp[i][j - 1]) {
                i--;
            } else {
                j--;
            }
        }
        return ans;
    }
}

/*
#include <bits/stdc++.h>

using namespace std;

const int N = 100;
string text1[N + 1];
string text2[N + 1];
int dp[N + 1][N + 1];
vector<string> answer;

void trace(int i, int j) {
    if (i == 0 || j == 0) {
        return;
    }

    if (text1[i] == text2[j] && dp[i][j] == dp[i - 1][j - 1] + 1) {
        trace(i - 1, j - 1);
        answer.push_back(text1[i]);
    }
    else if (dp[i][j] == dp[i - 1][j]) {
        trace(i - 1, j);
    }
    else {
        trace(i, j - 1);
    }
}

int main() {
    ios_base::sync_with_stdio(false);

    string word;

    while (true) {
        if (!(cin >> word)) {
            break;
        }

        int n = 0;
        int m = 0;

        while (word != "#") {
            ++n;
            text1[n] = word;
            cin >> word;
        }

        cin >> word;
        while (word != "#") {
            ++m;
            text2[m] = word;
            cin >> word;
        }

        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= m; j++) {
                dp[i][j] = 0;
            }
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (text1[i] == text2[j]) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                }
                else {
                    dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }

        answer.clear();

        trace(n, m);

        cout << answer[0];

        for (int i = 1; i < (int) answer.size(); i++) {
            cout << ' ' << answer[i];
        }

        cout << '\n';
    }

    return 0;
}
 */

/*
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static Scanner in = new Scanner(System.in);

    static final int N = 100;
    static String[] text1 = new String[N + 1];
    static String[] text2 = new String[N + 1];
    static int[][] dp = new int[N + 1][N + 1];
    static List<String> answer;

    public static void main(String[] args) {
        while (in.hasNext()) {
            int n = 0;
            int m = 0;

            while (true) {
                String word = in.next();
                if (word.equals("#")) {
                    break;
                }
                ++n;
                text1[n] = word;
            }

            while (true) {
                String word = in.next();
                if (word.equals("#")) {
                    break;
                }
                ++m;
                text2[m] = word;
            }

            for (int i = 0; i <= n; i++) {
                for (int j = 0; j <= m; j++) {
                    dp[i][j] = 0;
                }
            }

            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= m; j++) {
                    if (text1[i].equals(text2[j])) {
                        dp[i][j] = dp[i - 1][j - 1] + 1;
                    }
                    else {
                        dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                    }
                }
            }

            answer = new ArrayList<>();
            trace(n, m);

            System.out.print(answer.get(0));
            for (int i = 1; i < answer.size(); i++) {
                System.out.print(' ' + answer.get(i));
            }
            System.out.println();
        }
    }

    static void trace(int i, int j) {
        if (i == 0 || j == 0) {
            return;
        }

        if (text1[i].equals(text2[j]) && dp[i][j] == dp[i - 1][j - 1] + 1) {
            trace(i - 1, j - 1);
            answer.add(text1[i]);
        }
        else if (dp[i][j] == dp[i - 1][j]) {
            trace(i - 1, j);
        }
        else {
            trace(i, j - 1);
        }
    }
}
 */