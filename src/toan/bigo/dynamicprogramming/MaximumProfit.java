package toan.bigo.dynamicprogramming;

/*
Hướng dẫn giải
Hướng dẫn giải:
Gọi dp[i][j][stat]dp[i][j][stat] là lợi nhuận tối đa có thể đạt được bằng cách thực hiện đúng jj giao dịch trong vòng ii ngày, stat = 0stat=0 nếu không giữ cổ phiếu (đã bán), ngược lại stat = 1stat=1. Ta có công thức liên hệ sau:

Nếu stat = 0stat=0:
dp[i][j][0] = max(dp[i-1][j][0], dp[i-1][j-1][1] + a[i])dp[i][j][0]=max(dp[i−1][j][0],dp[i−1][j−1][1]+a[i])

Nếu stat = 1stat=1:
dp[i][j][1] = max(dp[i-1][j][1], dp[i-1][j][0] - a[i])dp[i][j][1]=max(dp[i−1][j][1],dp[i−1][j][0]−a[i])

Trong đó:

dp[i-1][j][stat]dp[i−1][j][stat] là lợi nhuận tối đa có thể đạt được trong vòng (i-1)(i−1) ngày và thực hiện đúng jj giao dịch, cho rằng ngày thứ ii không thực hiện được bất cứ giao dịch nào có lợi.
dp[i-1][j-1][1] + a[i]dp[i−1][j−1][1]+a[i] (với stat = 0stat=0) và dp[i-1][j][0] - a[i]dp[i−1][j][0]−a[i] (với stat = 1stat=1) là lợi nhuận hiện thời khi thực hiện một bước của giao dịch (mua cổ phiếu hoặc bán cổ phiếu) tại ngày thứ ii.
Độ phức tạp:
O(NK)O(NK) với NN là số ngày và KK là số lượng giao dịch tối đa.
 */
public class MaximumProfit {
}
/*
#pragma GCC optimize("Ofast")

#include <bits/stdc++.h>
using namespace std;

#define endl '\n'

int k, n;
vector<int> a;

void Input() {
	cin >> k >> n;
	a.clear(); a.resize(n);
	for (auto &z: a) cin >> z;
}

void Solve() {
	int ans = 0;
	vector<vector<vector<int>>> dp(n, vector<vector<int>>(k+1, vector<int>(2, INT_MIN)));
	dp[0][0][0] = 0; dp[0][0][1] = -a[0];
	for (int i=0; i<n; i++) {
		for (int j=0; j<=k; j++) {
			for (int stat=0; stat<2; stat++) {
				if (dp[i][j][stat] == INT_MIN) continue;
				if (i == n-1) {
					if (stat == 0) ans = max(ans, dp[i][j][stat]);
					continue;
				}
				dp[i+1][j][stat] = max(dp[i+1][j][stat], dp[i][j][stat]);
				if (j < k) {
					if (stat == 0) dp[i+1][j][1] = max(dp[i+1][j][1], dp[i][j][0] - a[i+1]);
					if (stat == 1) dp[i+1][j+1][0] = max(dp[i+1][j+1][0], dp[i][j][1] + a[i+1]);
				}
			}
		}
	}
	cout << ans << endl;
}

int main(int argc, char* argv[]) {
	ios_base::sync_with_stdio(0); cin.tie(NULL);
	int T; cin >> T; while (T--) {Input(); Solve();} return 0;
}
 */

/*
import java.util.*;

public class Main{
	public static int max(int a, int b) {
		return ((a > b) ? a : b);
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		int INF = 1000000000;

		for (int tc = 0; tc < T; tc++) {
			int k = sc.nextInt(), n = sc.nextInt();
			int[] a = new int[n];
			for (int i=0; i<n; i++) a[i] = sc.nextInt();

			int[][][] dp = new int[n][k+1][2];
			for (int i=0; i<n; i++) {
				for (int j=0; j<=k; j++) {
					for (int stat=0; stat<2; stat++) {
						dp[i][j][stat] = -INF;
					}
				}
			}
			dp[0][0][0] = 0; dp[0][0][1] = -a[0];

			int ans = 0;
			for (int i=0; i<n; i++) {
				for (int j=0; j<=k; j++) {
					for (int stat=0; stat<2; stat++) {
						if (dp[i][j][stat] == -INF) continue;
						if (i == n-1) {
							if (stat == 0) ans = max(ans, dp[i][j][stat]);
							continue;
						}
						dp[i+1][j][stat] = max(dp[i+1][j][stat], dp[i][j][stat]);
						if (j < k) {
							if (stat == 0) dp[i+1][j][1] = max(dp[i+1][j][1], dp[i][j][0] - a[i+1]);
							if (stat == 1) dp[i+1][j+1][0] = max(dp[i+1][j+1][0], dp[i][j][1] + a[i+1]);
						}
					}
				}
			}
			System.out.println(ans);
		}
	}
}
 */