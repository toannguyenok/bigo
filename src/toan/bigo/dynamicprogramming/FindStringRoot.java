package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Scanner;

/*
Hướng dẫn giải
Để dễ hình dung, trước tiên ta sẽ xét ví dụ 11, chuỗi abcabcabcabc, với mỗi kí tự ta sẽ tìm độ dài chuỗi tiền tố dài nhất mà trùng với chuỗi con kết thúc tại kí tự đó. Như vậy với chuỗi trên, ta sẽ có mảng sau:

a	b	c	a	b	c	a	b	c	a	b	c
0	0	0	1	2	3	4	5	6	7	8	9
Dựa vào bảng trên, ta có thể thấy 99 ký tự cuối cùng trùng khớp vói 99 ký tự đầu tiên của dãy:
abc\color{Red} {abcabcabc}abcabcabcabc
\color{Red} {abcabcabc}abcabcabcabcabc

Từ đó ta suy ra 33 ký tự cuối cùng của chuỗi SS sẽ trùng với 33 ký tự trước đó (như ví dụ trên). Ngoài ra, lúc này nếu ta dịch chuỗi sang phải 33 đơn vị thì lúc này bộ 33 ký tự gần cuối sẽ thành bộ 33 ký tự cuối cùng, tương tự các bộ 33 ký tự trước đó sẽ được đẩy lên 11 bộ. Vì các bộ ký tự khớp nhau nên từ đó ta có thể kết luận là tất cả các bộ 33 ký tự như vậy đều giống nhau. Từ đó suy ra, nếu độ dài tiền tố chung dài nhất giữa chuỗi SS và chuỗi con kết thúc tại vị trí cuối cùng (không tính chính nó) là XX thì suy ra |S| -X∣S∣−X ký tự cuối cùng trùng với ký tự liền trước đó. Đồng thời, ta cũng có thể kết luận các bộ |S| -X∣S∣−X kí tự liền tiếp sẽ giống nhau. Như vậy có thể thấy, nếu |S| -X∣S∣−X là ước số của |S|∣S∣ thì ta có thể chia SS thành các bộ gồm |S| -X∣S∣−X ký tự giống nhau, hay nói cách khác chuỗi tiền tố độ dài |S| -X∣S∣−X là một chuỗi gốc của SS (tức KK) và giá trị NN là |S| / \frac{..}{(|S| -X)}∣S∣/
​(∣S∣−X)
​
​..
​​ .Ngoài ra, vì XX là độ dài tiền tố chung dài nhất, nên XX sẽ lớn nhất và |S| -X∣S∣−X sẽ nhỏ nhất. Vì |S| -X∣S∣−X nhỏ nhất nên |S| / \frac{..}{(|S| -X)}∣S∣/
​(∣S∣−X)
​
​..
​​  lớn nhất có thể. Như vậy suy ra nếu ta tìm được XX thì sẽ tính được kết quả dựa trên công thức trên. Mà ta thấy rằng gia trị XX chính là giá trị của quá trình tiền xử lý chuỗi PP trong thuật toán KMPKMP, như vậy có thể sử dụng thuật toán KMPKMP để giải quyết bài toán.

Gọi mm là đồ dài chuỗi SS, kmpkmp là mảng tiền tố, các bước để giải quyết bài toán như sau:

Bước 1: Tạo ra mảng tiền tố cho chuỗi SS.
Bước 2: Dựa vào mảng tiền tố, tính số lần một chuỗi con ngắn nhất được lặp đi lặp lại.
Nếu mm không chia hết cho m-kmp_{m-1}m−kmp
​m−1
​​  thì N=1N=1 vì ta không thể chia mm ký tự thành các nhóm giống nhau (số lượng ký tự không thể chia đủ).
Ngược lại N=m/(m-kmp_{m-1} )N=m/(m−kmp
​m−1
​​ )
Độ phức tạp: O(t*m)O(t∗m) với tt là số chuỗi cần kiểm tra và mm là độ dài của chuỗi.


 */
public class FindStringRoot {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<String> input = new ArrayList<>();
        while (scanner.hasNextLine()) {
            String t = scanner.nextLine();
            if (t.equals("*")) {
                for (int i = 0; i < input.size(); i++) {
                    String s = input.get(i);
                    int[] prefix = new int[s.length()];
                    KMPPreProcesses(s, prefix);
                    int ans = 1;
                    int lastPrefix = prefix[s.length() - 1];
                    int temp = s.length() - lastPrefix;
                    if (s.length() % temp == 0) {
                        ans = s.length() / temp;
                    }
                    System.out.println(ans);
                }
            } else {
                input.add(t);
            }
        }

    }

    public static void KMPPreProcesses(String p, int[] prefix) {
        prefix[0] = 0;
        int m = p.length();
        int len = 0;
        int i = 1;
        while (i < m) {
            if (p.charAt(i) == p.charAt(len)) {
                len++;
                prefix[i] = len;
                i++;
            } else {
                if (len != 0) {
                    len = prefix[len - 1];
                } else {
                    prefix[i] = 0;
                    i++;
                }
            }
        }
    }
}
/*
#include <bits/stdc++.h>

using namespace std;

void KMPPreprocess(string p, vector<int> &kmp) {
    kmp[0] = 0;
    int m = p.length();
    int j = 0;
    int i = 1;
    while (i < m) {
        if (p[i] == p[j]) {
            j++;
            kmp[i] = j;
            i++;
        } else {
            if (j != 0) {
                j = kmp[j - 1];
            } else {
                kmp[i] = 0;
                i++;
            }
        }
    }
}

int solve(string s) {
    vector<int> kmp(s.length());
    KMPPreprocess(s, kmp);
    int m = s.length();
    if (m % (m - kmp[m - 1])) {
        return 1;
    } else {
        return (m / (m - kmp[m - 1]));
    }
}

int main() {
    while (true) {
        string s;
        getline(cin, s);
        if (s == "*") break;
        cout << solve(s) << endl;
    }
    return 0;
}
 */

/*
import java.io.*;
import java.util.Scanner;
import java.util.*;

class Solution_AC {
    static final int MAX_N = 100005;
    public static void main (String[] args) {
        MyScanner sc = new MyScanner();
        int kmp[] = new int [MAX_N];
        while(true){
            String s = sc.nextLine();
            if(s.equals("*"))
                return;
            int res = Solve(s,kmp);
            System.out.println(res);
        }
    }

    static int Solve(String s, int []kmp){
        Build(s,kmp);
        int m = s.length();
        if( (m % (m - kmp[m -1])) != 0){
            return 1;
        }else{
            return (m/(m - kmp[m - 1]));
        }
    }

    static void Build(String s, int[] kmp){
        int m = s.length();
        kmp[0] = 0;
        int i = 1, j = 0;
        while(i < m){
            if(s.charAt(i) == s.charAt(j)){
                j++;
                kmp[i] = j;
                i++;
            }
            else{
                if(j != 0){
                    j = kmp[j-1];
                }
                else{
                    kmp[i] = 0;
                    i++;
                }
            }
        }
    }
    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
 */