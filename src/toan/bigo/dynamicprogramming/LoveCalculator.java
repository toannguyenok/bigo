package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/*
Hướng dẫn giải
Bài này yêu cầu ta 2 câu hỏi liên quan đến nhau: phần đầu yêu cầu tìm được chuỗi ngắn nhất mà ta có thể tìm được các xâu cho trước là xâu con. Chuỗi ngắn nhất thỏa mãn điều kiện trên là chuỗi ghép từ 2 xâu ban đầu và bỏ đi những ký tự trùng nhau vì ta không cần các ký tự thừa và các ký tự trùng nhau theo thứ tự. Vậy để tìm được độ dài chuỗi thỏa mãn ta cần tìm độ dãi chuỗi con chung dài nhất. Sau đó ta có độ dài thỏa mãn sẽ bằng công thức sau:
Độ dài xâu đầu + Độ dài xâu thứ hai – Độ dài chuỗi con chung dài nhất.
Với phần hai của câu hỏi ta cần phải tìm số lượng chuỗi thỏa mãn, yêu cầu này ta cần sử dụng lại kết quả của phần một. Với phần này ta cũng bắt đầu duyệt từ đầu 22 chuỗi nhưng ta cần thêm 11 tham số khác là số lượng ký tự mà ta đã lấy tính tới thời điểm đang xét (ở đây mình gọi là tham số numnum khởi tạo bằng 00). Ta bắt đầu xét với các điều kiện sau trong vòng lặp đệ quy nhưng vì số lượng hàm đệ quy lặp lại quá lớn nên ta đồng thời sử dụng mảng 33 chiều để lưu dữ liệu lại (đặt là scs_num[i][j][num] để lưu số tổ hợp ký tự ghép lại từ ii và jj chữ cái đầu tiên trong xâu AA và BB với num ký tự trong chuỗi kết quả), kết quả của scs_num[ |A| ][ |B| ][ |A ꓵ B| ] là số lượng ta cần tìm:
Nếu đã tính giá trị của scs_num[i][j][num] trước đó rồi thì ta không cần tính nữa mà trả giá trị về.
Nếu 11 trong 22 ii hoặc jj đã đến cuối chuỗi thì ta tiếp tục xét ký tự tiếp theo của chuỗi chưa đến cuối dãy và đồng thời tăng numnum - số lượng ký tự.
Nếu cả hai ii và jj đều ở cuối dãy thì ta cần kiểm tra xem độ dài chuỗi (numnum) có hợp lệ hay không. Ở đây ta so sánh numnum với kết quả có được ở phần 1. Nếu đúng thì trả về 11, sai thì trả về 00 (là ta để ghép các ký tự sai cách).
Nếu a_i = b_ja
​i
​​ =b
​j
​​  thì ta lấy ký tự a_ia
​i
​​  (ta có thể lấy b_jb
​j
​​  nhưng do ký tự giống nhau nên lấy từ xâu nào cũng được) và xét tiếp 22 ký tự tiếp theo trên 22 xâu. Do đó ta gọi scs_num[i][j][num] = SCS(i + 1, j + 1, num + 1).
Nếu 22 ký tự khác nhau thì ta xét đồng thời cả 2 trường hợp xét ký tự tiếp theo của a_ia
​i
​​  hoặc ký tự tiếp theo của b_jb
​j
​​ . Vậy ta sẽ gọi scs_num[i][j][num] = SCS(i + 1, j, num + 1) + SCS(i, j + 1, num + 1).
Vậy kết quả của phần 22 sẽ là giá trị được trả về từ hàm SCS(0, 0, 0).
Độ phức tạp: O(|A|*|B|*|A \cap B|)O(∣A∣∗∣B∣∗∣A∩B∣) - |A|∣A∣ là độ dài của xâu thứ nhất, |B|∣B∣ là độ dài xâu thứ hai, |A \cap B|∣A∩B∣ là độ dài của xâu con chung dài nhất.


 */
public class LoveCalculator {

    public static int MAX = 30 + 5;
    public static int[][] dp = new int[MAX][MAX];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCase = scanner.nextInt();

        for (int t = 0; t < testCase; t++) {
            String string1 = scanner.next();
            String string2 = scanner.next();
            for (int i = 0; i < MAX; i++) {
                Arrays.fill(dp[i], -1);
            }
            int max = lcs(string1, string2, string1.length(), string2.length());
            String[] ans = getLCS(max, string1, string2, string1.length(), string2.length());
            String lcs = "";
            for (int i = 0; i < ans.length; i++) {
                lcs += ans[i];
            }
            ArrayList<String> temp = getResult(lcs, string1, string2, string1.length(), string2.length());

            System.out.println(ans.length + " / " + temp.size());
        }
    }

    public static int lcs(String string1, String string2, int m, int n) {
        if (m == 0 || n == 0) {
            return 0;
        }
        if (dp[m][n] != -1) {
            return dp[m][n];
        }
        if (string1.charAt(m - 1) == string2.charAt(n - 1)) {
            dp[m][n] = 1 + lcs(string1, string2, m - 1, n - 1);
        } else {
            dp[m][n] = Math.max(lcs(string1, string2, m, n - 1), lcs(string1, string2, m - 1, n));
        }
        return dp[m][n];
    }

    public static String[] getLCS(int lengthResult, String string1, String string2, int m, int n) {
        int i = m;
        int j = n;
        String[] ans = new String[lengthResult];
        while (i > 0 && j > 0) {
            if (string1.charAt(i - 1) == string2.charAt(j - 1)) {
                ans[lengthResult - 1] = String.valueOf(string1.charAt(i - 1));
                i--;
                j--;
                lengthResult--;
            } else if (dp[i - 1][j] > dp[i][j - 1]) {
                i--;
            } else {
                j--;
            }
        }
        return ans;
    }

    public static ArrayList<String> getResult(String lcs, String string1, String string2, int m, int n) {
        int l1 = 0;
        int l2 = 0;
        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < lcs.length(); i++) {
            while (l1 < m && string1.charAt(l1) != lcs.charAt(i)) {
                result.add(String.valueOf(string1.charAt(l1)));
                l1++;
            }
            while (l2 < n && string2.charAt(l2) != lcs.charAt(i)) {
                result.add(String.valueOf(string2.charAt(l2)));
                l2++;
            }
            result.add(String.valueOf(lcs.charAt(i)));
            l1++;
            l2++;
        }
        for (int j = l1; j < m; j++) {
            result.add(String.valueOf(string1.charAt(j)));
        }
        for (int j = l2; j < n; j++) {
            result.add(String.valueOf(string2.charAt(j)));
        }
        return result;
    }
}
/*
#include <bits/stdc++.h>

using namespace std;

int t;
string s1, s2;
int m, n;
vector<vector<int>> scs;
vector<vector<long>> numWays;

void solve()
{
    scs.clear();
    numWays.clear();
    scs.resize(m + 1, vector<int>(n + 1));
    numWays.resize(m + 1, vector<long>(n + 1));
    for (int i = 0; i <= m; i++)
        for (int j = 0; j <= n; j++)
        {
            if (i == 0 || j == 0)
            {
                scs[i][j] = i + j;
                numWays[i][j] = 1;
            }
            else if (s1[i - 1] == s2[j - 1])
            {
                scs[i][j] = scs[i - 1][j - 1] + 1;
                numWays[i][j] = numWays[i - 1][j - 1];
            }
            else
            {
                scs[i][j] = min(scs[i - 1][j], scs[i][j - 1]) + 1;
                if (scs[i][j] == scs[i - 1][j] + 1)
                    numWays[i][j] += numWays[i - 1][j];
                if (scs[i][j] == scs[i][j - 1] + 1)
                    numWays[i][j] += numWays[i][j - 1];
            }
        }
}

int main(int argc, char const *argv[])
{
    cin >> t;
    for (int cs = 1; cs <= t; cs++)
    {
        cin >> s1;
        cin >> s2;
        m = s1.length();
        n = s2.length();
        solve();
        printf("Case %d: %d %ld\n", cs, scs[m][n], numWays[m][n]);
    }
    return 0;
}
 */

/*
import java.util.Scanner;

public class Main {
    private static String s1, s2;
    private static int m, n;
    private static int[][] scs; // shortest cover string
    private static long[][] numWays;

    private static void solve() {
        scs = new int[m + 1][n + 1];
        numWays = new long[m + 1][n + 1];
        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0 || j == 0) {
                    scs[i][j] = i + j;
                    numWays[i][j] = 1;
                }
                else if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
                    scs[i][j] = scs[i - 1][j - 1] + 1;
                    numWays[i][j] = numWays[i - 1][j - 1];
                }
                else {
                    scs[i][j] = Math.min(scs[i - 1][j], scs[i][j - 1]) + 1;
                    if (scs[i][j] == scs[i - 1][j] + 1) {
                        numWays[i][j] += numWays[i - 1][j];
                    }
                    if (scs[i][j] == scs[i][j - 1] + 1) {
                        numWays[i][j] += numWays[i][j - 1];
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int cs = 1; cs <= t; cs++) {
            s1 = sc.next();
            s2 = sc.next();
            m = s1.length();
            n = s2.length();
            solve();
            System.out.printf("Case %d: %d %d\n", cs, scs[m][n], numWays[m][n]);
        }
    }
}
 */