package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Scanner;

/*
Hướng dẫn giải
Với bài này ta phải chọn các kho báu mà tổng giá trị cuối cùng là lớn nhất với tổng thời gian các lần lặn xuống lên trong khoảng cho phép. Vậy ta có thể áp dụng quy hoạch động 0/1 Knapsack problem để giải bài này.
Với mỗi lần lặn tốn W * d_iW∗d
​i
​​  thời gian và mỗi lần trở lên tốn $2 * W * d_i thời gian, vậy ta cần tổng cộng 3 * W * d_i3∗W∗d
​i
​​  thời gian để hoàn thành.
Vậy với dữ liệu đầu vào ta đọc theo từng cặp độ sâu và giá trị vàng của mỗi kho báu (ta có thể tạo kiểu dữ liệu mới – TREASURE gồm depth và gold.
Để áp dụng quy hoạch động bài này, ta tạo 1 mảng DPDP có kích thước có N + 1N+1 dòng và T + 1T+1 cột, với dòng thứ ii là biểu thị cho việc chỉ được chọn ii kho báu đầu tiên và cột jj biểu thị cho thời gian giới hạn để thực hiện công việc thu nhập vàng. Vậy với cách sử dụng trên của mảng DPDP, thì kết quả mong muốn cuối cùng của ta sẽ là DP[N][T]DP[N][T].
Vậy các cách khai báo trên ta sẽ có công thức tính mảng DPDP như sau:
DP[0][j] = DP[i][0] = 0DP[0][j]=DP[i][0]=0 do ta không thể có số vàng nếu như không thu thập kho báu nào hoặc không có thời gian đủ để lặn.
Nếu j < treasure[i - 1].depth * w * 3j<treasure[i−1].depth∗w∗3 thì DP[i][j] = DP[i - 1][j]DP[i][j]=DP[i−1][j] do ta không đủ thời gian để thu thập kho báu này.
Nếu j \ge treasure[i - 1].depth * w * 3j≥treasure[i−1].depth∗w∗3 thì DP[i][j]DP[i][j] của ta phải chọn giá trị lớn hơn giữa:
DP[i - 1][j]DP[i−1][j]: là tổng giá trị với i - 1i−1 kho báu trước nhưng với cùng khoảng thời gian.
DP[i - 1][j - treasure[i - 1].depth * w * 3] + treasure[i - 1].goldDP[i−1][j−treasure[i−1].depth∗w∗3]+treasure[i−1].gold: là tổng giá trị vàng của kho báu này và i - 1i−1 kho báu trước.
Sau khi xử lý tất cả các bài toán con, ta sẽ có tổng số vàng lớn nhất có thể thu thập được.
Từ DPDP ta sử dụng lại để tìm số lượng kho báu thu thập được và là những kho báu nào. Ta bắt đầu duyệt từ DP[N][T]DP[N][T] đi lên, nếu DP[i][j]DP[i][j] khác DP[i - 1][j]DP[i−1][j] thì John đã thu nhập kho báu này, vậy ta sẽ lưu giữ idid của kho báu này lại là treasure[i - 1].idtreasure[i−1].id vào một mảng phụ, đồng thời giảm jj bởi treasure[i - 1].depth * w * 3treasure[i−1].depth∗w∗3 do thời gian lặn để thu thập kho báu này.
Từ mảng phụ lưu giữ idid ta có thể có được số lượng kho báu đã lấy theo thứ tự ngược với giá trị nhập vào. Vậy ta chỉ cần duyệt ngược mảng phụ này để có được thứ tự xuất hiện đầu vào và kho báu được chọn, từ giá trị này ta có thể in ra độ sâu và giá trị trong mảng treasure.
Độ phức tạp: O(N * T)O(N∗T) – Với NN là số kho báu, TT là thời gian lặn cho phép.
 */
public class DivingForGold {

    public static class Treasure {
        int d;
        int v;

        public Treasure(int d, int v) {
            this.d = d;
            this.v = v;
        }

        public int getTime(int w) {
            return 3 * d * w;
        }
    }

    private static int[][] k;
    private static ArrayList<Treasure> result = new ArrayList<>();
    private static ArrayList<Treasure> input = new ArrayList<>();
    private static int time;
    private static int w;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (!line.equals("")) {
                String[] temp = line.split(" ");
                time = Integer.valueOf(temp[0]);
                w = Integer.valueOf(temp[1]);
                int n = scanner.nextInt();
                for (int i = 0; i < n; i++) {
                    int d = scanner.nextInt();
                    int v = scanner.nextInt();
                    input.add(new Treasure(d, v));
                }
            } else {
                if(input.size() == 0)
                {
                    System.out.println();
                }else {
                    System.out.println(knapSack(input, time, w));
                    int count = printItems(input, time, w);
                    System.out.println(count);
                    for (int i = result.size() - 1; i >= 0; i--) {
                        System.out.println(result.get(i).d + " " + result.get(i).v);
                    }
                    input.clear();
                    result.clear();
                }
            }
        }
    }

    public static int knapSack(ArrayList<Treasure> items, int time, int w) {
        k = new int[items.size() + 1][time + 1];
        for (int i = 0; i < time + 1; i++) {
            k[0][i] = 0;
        }
        for (int i = 0; i <= items.size(); i++) {
            for (int j = 0; j <= time; j++) {
                if (i == 0 || j == 0) {
                    k[i][j] = 0;
                } else if (items.get(i - 1).getTime(w) > j) {
                    k[i][j] = k[i - 1][j];
                } else {
                    int temp1 = items.get(i - 1).v + k[i - 1][j - items.get(i - 1).getTime(w)];
                    int temp2 = k[i - 1][j];
                    k[i][j] = Math.max(temp1, temp2);
                }
            }
        }
        return k[items.size()][time];
    }

    public static int printItems(ArrayList<Treasure> input, int time, int w) {
        int count = 0;
        for (int i = input.size(); i > 0; i--) {
            if (k[i][time] != k[i - 1][time]) {
                count++;
                result.add(new Treasure(input.get(i - 1).d, input.get(i - 1).v));
                time -= input.get(i - 1).getTime(w);
            }
        }
        return count;
    }

}
/*
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int t, w;
int n;
bool End = 0;

struct TREASURE
{
    int time;
    int gold;
    int id;
};

vector<TREASURE> treasure;
vector<vector<int>> collect;

bool option(const TREASURE &a, const TREASURE &b)
{
    return a.time < b.time;
}

int main()
{
    while (scanf("%d %d", &t, &w) == 2)
    {
        cin >> n;
        treasure.clear();
        for (int i = 0; i < n; i++)
        {
            TREASURE temp;
            cin >> temp.time >> temp.gold;
            temp.id = i;
            treasure.push_back(temp);
        }

        vector<TREASURE> back_up = treasure;
        sort(treasure.begin(), treasure.end(), option);

        // DP
        collect.clear();
        collect.resize(n + 1, vector<int>(t + 1, 0));
        for (int i = 1; i <= n; i++)
            for (int j = 1; j <= t; j++)
                if (j < treasure[i - 1].time * w * 3)
                    collect[i][j] = collect[i - 1][j];
                else
                {
                    int temp1 = collect[i - 1][j];
                    int temp2 = collect[i - 1][j - treasure[i - 1].time * w * 3] + treasure[i - 1].gold;
                    collect[i][j] = max(temp1, temp2);
                }

        // RESULT
        if (End)
            cout << "\n";
        End = 1;

        cout << collect[n][t] << "\n";

        vector<int> pick_list;
        for (int i = collect.size() - 1; i > 0; i--)
            if (collect[i][t] != collect[i - 1][t])
            {
                pick_list.push_back(treasure[i - 1].id);
                t -= treasure[i - 1].time * w * 3;
            }
        cout << pick_list.size() << "\n";
        sort(pick_list.begin(), pick_list.end());
        for (int i = 0; i < pick_list.size(); i++)
        {
            cout << back_up[pick_list[i]].time << " " << back_up[pick_list[i]].gold << "\n";
        }
    }
    return 0;
}
 */

/*
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

class Item {
    public int gold;
    public int depth;
    Item(int depth, int gold) {
        this.gold = gold;
        this.depth = depth;
    }
}

public class Main {
    private static int[][] K;

    private static int Knapsack(Item[] items, int t, int w) {
        K = new int[items.length + 1][t + 1];
        for (int i = 1; i < items.length; i++) {
            for (int j = 0; j <= t; j++) {
                if (items[i].depth * w * 3 > j) {
                    K[i][j] = K[i - 1][j];
                }
                else {
                    int temp1 = items[i].gold + K[i - 1][j - items[i].depth * w * 3];
                    int temp2 = K[i - 1][j];
                    K[i][j] = Math.max(temp1, temp2);
                }
            }
        }
        return K[items.length - 1][t];
    }

    private static ArrayList<Integer> getItems(Item[] items, int t, int w) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = items.length - 1; i > 0; i--) {
            if (K[i][t] != K[i - 1][t]) {
                result.add(i);
                t -= items[i].depth * w * 3;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean newline = false;
        while (sc.hasNext()) {
            int t = sc.nextInt();
            int w = sc.nextInt();
            int n = sc.nextInt();
            Item[] items = new Item[n + 1];
            items[0] = new Item(0, 0);
            for (int gold, depth, i = 1; i <= n; i++) {
                depth = sc.nextInt();
                gold = sc.nextInt();
                items[i] = new Item(depth, gold);
            }
            if (newline) {
                System.out.println();
            }
            newline = true;
            System.out.println(Knapsack(items, t, w));
            ArrayList<Integer> picked = getItems(items, t, w);
            System.out.println(picked.size());
            for (int i = picked.size() - 1; i >= 0; i--) {
                System.out.printf("%d %d\n", items[picked.get(i)].depth, items[picked.get(i)].gold);
            }
        }
    }
}
 */