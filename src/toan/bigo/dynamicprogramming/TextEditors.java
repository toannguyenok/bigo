package toan.bigo.dynamicprogramming;

import java.util.Scanner;

/*
Hướng dẫn giải
Ta thấy rằng giả sử chuỗi ss là tiền tố của chuỗi BB xuất hiện ít nhất nn lần trong chuỗi AA, thì toàn bộ những chuỗi stst là tiền tố của chuỗi ss cũng phải xuất hiện ít nhất nn lần trong chuỗi AA. Ngoài ra, chuỗi ss có độ dài càng lớn thì càng khó xuất hiện ít nhất nn lần và ngược lại.
Từ nhận xét ở trên, ta có thể sử dụng thuật toán Binary Search như sau:
Ta đặt l = 0, r = length(B) - 1l=0,r=length(B)−1 là giới hạn 22 đầu của chuỗi BB.
Ta đặt mid = (l + r) / 2mid=(l+r)/2, gọi s = B(0 .. mid)s=B(0..mid) (ss là chuỗi tiền tố của BB bắt đầu từ vị trí 00 đến midmid).
Sử dụng thuật toán KMPKMP để đếm số vị trí mà ss xuất hiện trong AA, nếu như số lượng vị trí lớn hơn hoặc bằng nn, ta ghi kết quả là ss và tiến hành gán l = mid + 1l=mid+1. Ngược lại, ta gán r = mid - 1r=mid−1.
Độ phức tạp: O(log(|B|) * (|A| + |B|))O(log(∣B∣)∗(∣A∣+∣B∣)) với |A|∣A∣ và |B|∣B∣ lần lượt là độ dài 22 chuỗi A, BA,B.
 */
public class TextEditors {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String t = scanner.nextLine();
        String s = scanner.nextLine();
        int n = Integer.valueOf(scanner.nextLine());
        int[] prefix = new int[s.length()];
        KMPPreProcesses(s, prefix);
        String result = "IMPOSSIBLE";
        String subString = "";
        int l = 0;
        int r = s.length();

        while (l <= r) {
            int mid = (l + r) / 2;
            subString = s.substring(0, mid);
            if (mid == 0) break;
            if (kmpSearch(t, subString, prefix) >= n) {
                result = subString;
                l = mid + 1;
            } else {
                r = mid - 1;
            }
        }

        System.out.println(result);
    }

    public static void KMPPreProcesses(String p, int[] prefix) {
        prefix[0] = 0;
        int m = p.length();
        int len = 0;
        int i = 1;
        while (i < m) {
            if (p.charAt(i) == p.charAt(len)) {
                len++;
                prefix[i] = len;
                i++;
            } else {
                if (len != 0) {
                    len = prefix[len - 1];
                } else {
                    prefix[i] = 0;
                    i++;
                }
            }
        }
    }

    public static int kmpSearch(String t, String p, int[] prefix) {
        int n = t.length();
        int m = p.length();
        int i = 0;
        int j = 0;
        int result = 0;
        while (i < n) {
            if (t.charAt(i) == p.charAt(j)) {
                i++;
                j++;
            }
            if (j == m) {
                result++;
                j = prefix[j - 1];
            } else if (i < n && p.charAt(j) != t.charAt(i)) {
                if (j != 0) {
                    j = prefix[j - 1];
                } else {
                    i = i + 1;
                }
            }
        }
        return result;
    }
}
/*
import java.io.*;
import java.util.Scanner;
import java.util.*;
import java.lang.*;

public class Solution_AC {
    static final int MAX_N = 1000006;
    static int prefix[] = new int [MAX_N];
    static String p,t;
    static int m,n;
    public static void main (String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        //StringBuilder sb = new StringBuilder();
        t = sc.nextLine();
        p = sc.nextLine();
        int k;
        k = sc.nextInt();
        int l = 0;
        int r = (int) p.length() - 1;
        String ans = "IMPOSSIBLE";
        while (l <= r) {
            int mid = (l + r) / 2;
            String tmp = p.substring(0,mid+1);

            KMPPreprocess(tmp);

            int cnt = KMPSearch(t, tmp);

             if (cnt >= k) {
                 ans = tmp;
                 l = mid + 1;
             }
             else {
                 r = mid - 1;
             }
         }

        System.out.println(ans);
    }

    static void KMPPreprocess(String s){
        m = s.length();
        prefix[0] = 0;
        int i = 1, j = 0;
        while(i < m){
            if(s.charAt(i) == s.charAt(j)){
                j++;
                prefix[i] = j;
                i++;
            }
            else{
                if(j != 0){
                    j = prefix[j-1];
                }
                else{
                    prefix[i] = 0;
                    i++;
                }
            }
        }
    }
    static int KMPSearch(String t, String p) {
        n = t.length();
        m = p.length();
        int cnt = 0;
        int i = 0, j = 0;
        while (i < n) {
            if (p.charAt(j)== t.charAt(i)) {
                j++;
                i++;
            }
            if (j == m) {
                cnt++;
                j = prefix[j - 1];
            }
            else {
                if (i < n && p.charAt(j) != t.charAt(i)) {
                    if (j != 0) {
                        j = prefix[j - 1];
                    }
                    else {
                        i = i + 1;
                    }
                }
            }
        }

        return cnt;
    }
}
 */

/*
#include <bits/stdc++.h>

using namespace std;

string t, p;
int k;

void KMPPreprocess(string p, vector<int>& prefix) {
	prefix[0] = 0;
	int m = p.length();
	int j = 0;
	int i = 1;
	while (i < m) {
		if (p[i] == p[j]) {
			j++;
			prefix[i] = j;
			i++;
		}
		else {
			if (j != 0) {
				j = prefix[j - 1];
			}
			else { // if (j == 0)
				prefix[i] = 0;
				i++;
			}
		}
	}
}

int KMPSearch(string t, string p, vector<int> prefix) {
	int n = t.length();
	int m = p.length();
	int cnt = 0;
	int i = 0, j = 0;
	while (i < n) {
		if (p[j] == t[i]) {
			j++;
			i++;
		}
		if (j == m) {
			cnt++;
			j = prefix[j - 1];
		}
		else {
			if (i < n && p[j] != t[i]) {
				if (j != 0) {
					j = prefix[j - 1];
				}
				else {
					i = i + 1;
				}
			}
		}
	}

  return cnt;
}

int main () {

	getline(cin, t);
	getline(cin, p);
	cin >> k;

	int l = 0;
	int r = (int) p.length() - 1;

	string ans = "IMPOSSIBLE";
 	while (l <= r) {
 		int mid = (l + r) / 2;
 		string tmp = "";
 		for (int i = 0; i <= mid; i++) {
 			tmp += p[i];
 		}

 		vector <int> prefix(tmp.length());
 		KMPPreprocess(tmp, prefix);

 	 	int cnt = KMPSearch(t, tmp, prefix);

 		if (cnt >= k) {
 			ans = tmp;
 			l = mid + 1;
 		}
 		else {
 			r = mid - 1;
 		}
 	}

 	cout << ans;
	return 0;
}
 */