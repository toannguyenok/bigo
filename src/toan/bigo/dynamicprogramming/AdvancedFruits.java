package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/*
Hướng dẫn giải
Dễ tưởng tượng rằng tên của của loại trái cây mới là 1 phép trộn tên 2 loại trái cây gốc với nhau, sau đó với các ký tự kề nhau và bằng nhau trong chuỗi mới nhưng không thuộc cùng 1 chuỗi gốc thì ta gộp chúng thành 1.

Ví dụ với chuỗi ananas và banana:

Xét 1 cách trộn như sau:
ananas, banana
anbaannanaas (có nhiều cách trộn, đây chỉ là 1 cách trộn ngẫu nhiên).
anb aa nn an aa s (xét các cặp ký tự kề nhau và bằng nhau nhưng không chung chuỗi gốc).
anb a n an a s (gộp các cặp ký tự trên thành 1).
Như vậy ta được chuỗi anbananas chứa cả 2 chuỗi ananas và banana làm chuỗi con. Tuy nhiên chuỗi này chưa ngắn nhất.
Để chuỗi kết quả ngắn nhất thì số cặp ký tự kề nhau và bằng nhau nhưng không thuộc cùng chuỗi gốc phải là nhiều nhất. Và dễ thấy là đây chính là độ dài chuỗi con chung dài nhất của 2 chuỗi gốc.

Vậy bài toán được đưa về bài toán chuỗi con chung dài nhất của 2 chuỗi tên aa và bb:

Đầu tiên, tính bảng giá trị lcslcs, với lcs[i][j]lcs[i][j] là độ dài chuỗi con chung dài nhất của a[1..i]a[1..i] và b[1..j]b[1..j]:
lcs[i][0] = lcs[0][j] = 0lcs[i][0]=lcs[0][j]=0.
lcs[i][j] = lcs[i - 1][j - 1] + 1lcs[i][j]=lcs[i−1][j−1]+1 nếu a[i] = b[j]a[i]=b[j].
lcs[i][j] = max(lcs[i - 1][j], lcs[i][j - 1])cs[i][j]=max(lcs[i−1][j],lcs[i][j−1]) nếu a[i] \neq b[j]a[i]≠b[j].
Tiếp theo ta dùng bảng giá trị lcs để truy vết chuỗi kết quả:
Ta truy vết chuỗi tên chung của a[1..i]a[1..i] và b[1..j]b[1..j], bắt đầu với i = length(a), j = length(b)i=length(a),j=length(b).
Nếu a[i] = b[j]a[i]=b[j] thì ta biết a[i]a[i] thuộc chuỗi con chung dài nhất, ta thêm ký tự a[i]a[i] vào đầu chuỗi kết quả, rồi di chuyển i, ji,j về i - 1, j - 1i−1,j−1 và tiếp tục truy vết.
Ngược lại:
Nếu lcs[i][j] = lcs[i - 1][j]lcs[i][j]=lcs[i−1][j] thì a[i]a[i] không nằm trong chuỗi con chung dài nhất, ta thêm a[i]a[i] vào đầu chuỗi kết quả rồi tiếp tục truy vết i - 1, ji−1,j.
Nếu lcs[i][j] = lcs[i][j - 1]lcs[i][j]=lcs[i][j−1] thì b[j]b[j] không nằm trong chuỗi con chung dài nhất, ta thêm b[j]b[j] vào đầu chuỗi kết quả rồi tiếp tục truy vết i, j - 1i,j−1.
Cuối cùng in ra chuỗi kết quả.
Độ phức tạp: O(t * n * m)O(t∗n∗m) với n, mn,m là độ dài 2 chuỗi tên gốc, tt là số bộ test.
 */
public class AdvancedFruits {

    public static int MAX = 100 + 5;
    public static int[][] dp = new int[MAX][MAX];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] stringTemp = line.split(" ");
            for (int i = 0; i < MAX; i++) {
                Arrays.fill(dp[i], -1);
            }
            int max = lcs(stringTemp[0], stringTemp[1], stringTemp[0].length(), stringTemp[1].length());
            String[] ans = getLCS(max, stringTemp[0], stringTemp[1], stringTemp[0].length(), stringTemp[1].length());
            String lcs = "";
            for (int i = 0; i < ans.length; i++) {
                lcs += ans[i];
            }
            ArrayList<String> temp = getResult(lcs, stringTemp[0], stringTemp[1], stringTemp[0].length(), stringTemp[1].length());

            String result = "";
            for (int i = 0; i < temp.size(); i++) {
                result += temp.get(i);
            }
            System.out.println(result);
        }
    }

    public static int lcs(String string1, String string2, int m, int n) {
        if (m == 0 || n == 0) {
            return 0;
        }
        if (dp[m][n] != -1) {
            return dp[m][n];
        }
        if (string1.charAt(m - 1) == string2.charAt(n - 1)) {
            dp[m][n] = 1 + lcs(string1, string2, m - 1, n - 1);
        } else {
            dp[m][n] = Math.max(lcs(string1, string2, m, n - 1), lcs(string1, string2, m - 1, n));
        }
        return dp[m][n];
    }

    public static String[] getLCS(int lengthResult, String string1, String string2, int m, int n) {
        int i = m;
        int j = n;
        String[] ans = new String[lengthResult];
        while (i > 0 && j > 0) {
            if (string1.charAt(i - 1) == string2.charAt(j - 1)) {
                ans[lengthResult - 1] = String.valueOf(string1.charAt(i - 1));
                i--;
                j--;
                lengthResult--;
            } else if (dp[i - 1][j] > dp[i][j - 1]) {
                i--;
            } else {
                j--;
            }
        }
        return ans;
    }

    public static ArrayList<String> getResult(String lcs, String string1, String string2, int m, int n) {
        int l1 = 0;
        int l2 = 0;
        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < lcs.length(); i++) {
            while (l1 < m && string1.charAt(l1) != lcs.charAt(i)) {
                result.add(String.valueOf(string1.charAt(l1)));
                l1++;
            }
            while (l2 < n && string2.charAt(l2) != lcs.charAt(i)) {
                result.add(String.valueOf(string2.charAt(l2)));
                l2++;
            }
            result.add(String.valueOf(lcs.charAt(i)));
            l1++;
            l2++;
        }
        for (int j = l1; j < m; j++) {
            result.add(String.valueOf(string1.charAt(j)));
        }
        for (int j = l2; j < n; j++) {
            result.add(String.valueOf(string2.charAt(j)));
        }
        return result;
    }
}

/*
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Solution_AC {
    public static void solve(Scanner sc) {
        String a, b;
        if (!sc.hasNext()) {
            System.exit(0);
        }

        a = sc.next(); b = sc.next();
        int n = a.length(), m = b.length();
        a = '#' + a;
        b = '#' + b;

        ArrayList<ArrayList<Integer>> lcs = new ArrayList<>();
        for (int i = 0; i < n + 1; i++) {
            lcs.add(new ArrayList<>(Collections.nCopies(m + 1, 0)));
        }

        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j <= m; ++j) {
                if (a.charAt(i) == b.charAt(j)) {
                    lcs.get(i).set(j, lcs.get(i - 1).get(j - 1) + 1);
                }
                else {
                    lcs.get(i).set(j, Math.max(lcs.get(i - 1).get(j), lcs.get(i).get(j - 1)));
                }
            }
        }

        String res = "";
        for (int i = n, j = m; i > 0 || j > 0;) {
            if (i == 0) {
                res += b.charAt(j--);
            }
            else if (j == 0) {
                res += a.charAt(i--);
            }
            else {
                if (a.charAt(i) == b.charAt(j)) {
                    res += a.charAt(i--);
                    j--;
                }
                else if (lcs.get(i).get(j).equals(lcs.get(i - 1).get(j))) {
                    res += a.charAt(i--);
                }
                else {
                    res += b.charAt(j--);
                }
            }
        }

        for (int i = res.length() - 1; i >= 0; i--) {
            System.out.print(res.charAt(i));
        }
        System.out.print("\n");
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            solve(sc);
        }
    }
}
 */

/*
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

void solve() {
    string a, b;
    if (!(cin >> a >> b)) {
        exit(0);
    }

    int n = a.length(), m = b.length();
    a = '#' + a;
    b = '#' + b;

    vector<vector<int>> lcs(n + 1, vector<int>(m + 1));
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j) {
            if (a[i] == b[j]) {
                lcs[i][j] = lcs[i - 1][j - 1] + 1;
            }
            else {
                lcs[i][j] = max(lcs[i - 1][j], lcs[i][j - 1]);
            }
        }
    }

    string res;
    for (int i = n, j = m; i || j;) {
        if (i == 0) {
            res += b[j--];
        }
        else if (j == 0) {
            res += a[i--];
        }
        else {
            if (a[i] == b[j]) {
                res += a[i--];
                j--;
            }
            else if (lcs[i][j] == lcs[i - 1][j]) {
                res += a[i--];
            }
            else {
                res += b[j--];
            }
        }
    }

    cout << string(res.rbegin(), res.rend()) << endl;
}

int main() {
    while (1) {
        solve();
    }
}
 */
