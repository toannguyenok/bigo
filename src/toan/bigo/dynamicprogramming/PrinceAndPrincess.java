package toan.bigo.dynamicprogramming;


/*
Hướng dẫn giải
Ta cần tìm một dãy các số a_1,a_2,...,a_la
​1
​​ ,a
​2
​​ ,...,a
​l
​​  với ll là độ dài dãy aa, thỏa mãn:

Các phần tử của aa đều nằm trong xx và thỏa thứ tự xuất hiện trong xx.
Các phần tử của aa đều nằm trong yy và thỏa thứ tự xuất hiện trong yy.
Độ dài của dãy aa là dài nhất.
Bài toán trên có thể giải quyết bằng thuật toán tìm dãy con chung dài nhất của 22 dãy xx và yy. Tuy nhiên, độ phức tạp sẽ là O(p \times q)O(p×q), với p,q \le n^2p,q≤n
​2
​​ , dẫn đến độ phức tạp sẽ là O(n^4)O(n
​4
​​ ), với n \le 250n≤250 sẽ bị Time Limit. Hai dãy xx và yy đều có các giá trị đôi một khác nhau, nên ta có thể áp dụng thuật toán tìm dãy con tăng dài nhất dùng binary search để giải quyết.

Bước 11: Khởi tạo mảng pospos gồm n^2n
​2
​​  phần tử bắt đầu từ 11, với pos_ipos
​i
​​  là thứ tự của ii trong dãy xx. Ban đầu khởi tạo tất cả các giá trị pos_i = -1pos
​i
​​ =−1. Nếu pos[i] == -1pos[i]==−1 nghĩa là ii không xuất hiện trong dãy xx.
Bước 22: Duyệt qua tất cả các phần tử trong xx, lưu lại thứ tự pos_{x_i} = ipos
​x
​i
​​
​​ =i.
Bước 33: Khởi tạo mảng aa để lưu lại các vị trí các số vừa có trong xx và yy, lúc đầu, mảng aa rỗng.
Bước 44: Duyệt qua các phần tử của yy, vì ta chỉ cần tìm phần tử chung nên nếu pos_{y_i} \ge 0pos
​y
​i
​​
​​ ≥0, nghĩa là y_iy
​i
​​  có xuất hiện trong xx, thì thêm pos_{y_i}pos
​y
​i
​​
​​  vô mảng aa, nếu không thì bỏ qua.
Bước 55: Lúc này, mảng aa sẽ lưu vị trí của các số trong dãy xx:
Các số này vừa có trong xx vừa có trong yy.
Bất cứ dãy con nào của aa cũng thỏa thứ tự xuất hiện trong dãy yy.
Các giá trị của aa là thứ tự xuất hiện của các phần tử trong dãy xx, vậy một dãy con tăng của aa sẽ thỏa thứ tự xuất hiện trong xx.
\Rightarrow⇒ Tìm độ dài dãy con tăng dài nhất của mảng aa, chính là kết quả bài toán.
Độ phức tạp: pp là độ dài dãy x, qx,q là độ dài dãy y, ly,l là độ dài của dãy aa, vì dãy aa chỉ lưu các phần tử chung giữa xx và yy nên độ dài lớn nhất của aa là l = min(p,q)l=min(p,q), độ phức tạp để tìm dãy con tăng dài nhất của aa, dùng tìm kiếm nhị phân là O(l \times log(l))O(l×log(l))
➩ Độ phức tạp O(min(p \times log(p),q \times log(q))O(min(p×log(p),q×log(q))
 */
public class PrinceAndPrincess {
}

/*
#include <iostream>
#include <vector>
#include <stdio.h>
#include <algorithm>

using namespace std;

int LIS(const vector<int> &a) {
  vector<int> sub;
  sub.push_back(a[0]);
  for(int i = 0; i < a.size(); i++) {
    if(a[i] < sub[0]) {
      sub[0] = a[i];
    }
    else if(a[i] > sub[sub.size() - 1]){
      sub.push_back(a[i]);
    }
    else {
      int pos = lower_bound(sub.begin(), sub.end(), a[i]) - sub.begin();
      sub[pos] = a[i];
    }
  }
  return sub.size();
}

void solve() {
  int n, p, q;
  cin >> n >> p >> q;
  vector<int> pos(n * n + 1, -1), a;

  for(int i = 0; i <= p; i++) {
    int x;
    cin >> x;
    pos[x] = i;
  }

  for(int i = 0; i <= q; i++) {
    int y;
    cin >> y;
    if(pos[y] >= 0) {
      a.push_back(pos[y]);
    }
  }

  cout << LIS(a) << endl;
}

int main() {
  int t;
  cin >> t;
  for(int i = 1; i <= t; i++) {
    cout << "Case " << i << ": ";
    solve();
  }
  return 0;
}
 */

/*
import java.util.*;

public class solution {
    static private Scanner sc;
    static private int lowerBound(ArrayList<Integer> a, int x) {
        int left = 0, right = a.size(), pos = -1;
        while(left <= right) {
            int mid = (left + right) / 2;
            if(a.get(mid) >= x) {
                pos = mid;
                right = mid - 1;
            }
            else {
                left = mid + 1;
            }
        }
        return pos;
    }

    static int LIS(ArrayList<Integer> a) {
        ArrayList<Integer> sub = new ArrayList<>();
        sub.add(a.get(0));
        for(int i = 0; i < a.size(); i++) {
            if(a.get(i) < sub.get(0)) {
                sub.set(0, a.get(i));
            }
            else if(a.get(i) > sub.get(sub.size() - 1)){
                sub.add(a.get(i));
            }
            else {
                int pos = lowerBound(sub, a.get(i));
                sub.set(pos, a.get(i));
            }
        }
        return sub.size();
    }

    static void solve() {
        int n, p, q;
        n = sc.nextInt();
        p = sc.nextInt();
        q = sc.nextInt();
        int pos[] = new int[n * n + 1];
        ArrayList<Integer> a = new ArrayList<>();
        for (int i = 0; i <= n * n; i++)
            pos[i] = -1;

        for(int i = 0; i <= p; i++) {
            int x = sc.nextInt();
            pos[x] = i;
        }

        for(int i = 0; i <= q; i++) {
            int y = sc.nextInt();
            if(pos[y] >= 0) {
                a.add(pos[y]);
            }
        }

        System.out.println(LIS(a));
    }
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i = 1; i <= t; i++) {
            System.out.printf("Case %d: ", i);
            solve();
        }
    }
}
 */