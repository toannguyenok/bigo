package toan.bigo.dynamicprogramming;

import java.util.Scanner;

/*
Hướng dẫn giải
Bước 1: Tạo ra mảng tiền tố cho chuỗi ss.
Bước 2: Gọi xx là giá trị mảng tiền tố tại vị trí cuối cùng
Nếu xx bằng 00, tức là trong chuỗi ss ta không có chuỗi con nào ở cuối chuỗi trùng với chuỗi con ở đầu chuỗi \Rightarrow⇒ không thỏa điều kiện PrefixPrefix và SuffixSuffix, in ra màn hình Just a legend.
Nếu không thì ta sẽ duyệt mảng tiền tố từ vị trí 11 đến trước vị trí cuối cùng (bỏ qua khả năng tìm lặp lại chuỗi con SuffixSuffix), tại bất kỳ vị trí nào có giá trị trong mảng tiền tố bằng xx ta kết luận tìm được chuỗi con thỏa mãn cả ba điều kiện trên, in chuỗi tìm được ra màn hình.
Nếu không tìm được giá trị nào từ 11 đến trước vị trí cuối cùng bằng xx thì ta sẽ bắt đầu giảm độ dài chuỗi suffixsuffix với hy vọng sẽ tìm được chuỗi nhỏ hơn thỏa mãn. Ta xét giá trị mảng tiền tố tại vị trí x - 1x−1, nếu giá trị này bằng 00 tức là không có chuỗi con nào ngoài prefixprefix bằng với chuỗi suffixsuffix, ta kết luận không tìm thấy chuỗi con tt, in ra màn hình Just a legend. Ngược lại, in chuỗi con tt tìm được ra màn hình.
Ngoại trừ tất cả những trường hợp liệt kê trên thì đều không thể tìm được chuỗi con tt thỏa yêu cầu đề bài, in ra màn hình Just a legend.
Độ phức tạp: O(n)O(n) với nn là độ dài chuỗi ss.
 */
public class Password {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String t = scanner.nextLine();
        int[] prefix = new int[t.length()];
        KMPPreProcesses(t, prefix);
//        for (int i = 0; i < prefix.length; i++) {
//            System.out.print(prefix[i] + " ");
//        }
        int suffixCount = prefix[t.length() - 1];
        int prefixCount = 0;
        if(suffixCount > 0) {
            prefixCount = prefix[suffixCount - 1];
        }

        //check midside
        for (int i = 1; i < t.length() - 1; i++) {
            if (prefix[i] == suffixCount) {
                prefixCount = suffixCount;
            }
        }
        if (prefixCount == 0) {
            System.out.println("Just a legend");
        } else {
            System.out.println(t.substring(0, prefixCount));
        }
    }

    public static void KMPPreProcesses(String p, int[] prefix) {
        prefix[0] = 0;
        int m = p.length();
        int len = 0;
        int i = 1;
        while (i < m) {
            if (p.charAt(i) == p.charAt(len)) {
                len++;
                prefix[i] = len;
                i++;
            } else {
                if (len != 0) {
                    len = prefix[len - 1];
                } else {
                    prefix[i] = 0;
                    i++;
                }
            }
        }
    }
}

/*
#include<iostream>
#include<vector>
#include<string>
using namespace std;

void KMPPreprocess(string& p, vector<int>& kmp) {
	kmp[0] = 0;
	int m = kmp.size();
	int j = 0;
	int i = 1;
	while (i < m) {
		if (p[i] == p[j]) {
			j++;
			kmp[i] = j;
			i++;
		}
		else {
			if (j != 0)
				j = kmp[j - 1];
			else {
				kmp[i] = 0;
				i++;
			}
		}
	}
}

bool findElement(vector<int> &v, int start, int end, int elem){
    for(int i = start; i < end; ++i)
        if(v[i] == elem)
            return true;
    return false;
}

void printString(string& s, int end){
    for(int i = 0; i < end; ++i)
        cout << s[i];
}

int main(){
	string s;
	getline(cin, s);
	vector<int> kmp(s.length());
    KMPPreprocess(s, kmp);
    int x = kmp[s.length() - 1];
    if (x == 0)
        cout << "Just a legend";
    else if (findElement(kmp, 1, s.length() - 1, x))
        printString(s, x);
    else if (kmp[x - 1] == 0)
        cout << "Just a legend";
    else if (kmp[x - 1])
        printString(s, kmp[x -1]);
    else
        cout << "Just a legend";
    return 0;
}
 */
/*
import java.io.*;
import java.util.Scanner;
import java.util.*;
import java.lang.*;

public class Solution_AC {
    static final int MAX_N = 1000006;
    static int kmp[] = new int [MAX_N];
    static String s;
    static int m;
    public static void main (String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        //StringBuilder sb = new StringBuilder();
        s = sc.next();
        m = s.length();
        KMPPreprocess();
        int x = kmp[m-1];
        if (x == 0)
            System.out.println("Just a legend");
        else if (findElement(1, m - 1, x))
            System.out.println(s.substring(0, x));
        else if (kmp[x - 1] == 0)
            System.out.println("Just a legend");
        else if (kmp[x - 1] != 0)
            System.out.println(s.substring(0, kmp[x-1]));
        else
            System.out.println("Just a legend");
    }

    static boolean findElement(int start, int end, int elem){
        for(int i = start; i < end; ++i)
            if(kmp[i] == elem)
                return true;
        return false;
    }

    static void printString(String s, int end){
        System.out.println(s.substring(0, end));
    }
    static void KMPPreprocess(){
        kmp[0] = 0;
        int i = 1, j = 0;
        while(i < m){
            if(s.charAt(i) == s.charAt(j)){
                j++;
                kmp[i] = j;
                i++;
            }
            else{
                if(j != 0){
                    j = kmp[j-1];
                }
                else{
                    kmp[i] = 0;
                    i++;
                }
            }
        }
    }
    public static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public double nextDouble() {
            return Double.parseDouble(next());
        }
    }
}
 */