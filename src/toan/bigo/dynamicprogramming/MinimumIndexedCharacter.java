package toan.bigo.dynamicprogramming;

/*
Hướng dẫn giải
Hướng dẫn giải
Với mỗi kí tự trong strstr, ta lưu lại vị trí xuất hiện nhỏ nhất của kí tự ấy. Do chỉ có 2626 chữ cái nên ta sẽ lưu vị trí này đối với mỗi chữ cái. Do ta biết trước có 2626 kí tự nên ta sẽ dùng một mảng đặt tên là minPosminPos có kích thước là 2626 và dùng một hàm hash gọi là hashCharhashChar – hàm này sẽ nhận vào một kí tự và trả về chỉ số (index) của nó trong mảng minPosminPos như sau: aa sẽ ở vị trí 00, bb ở vị trí 11,…, zz ở vị trí 2525.
Gọi 11 biến là ansans sẽ lưu lại kí tự kết quả và ansIndexansIndex để lưu lại chỉ số nhỏ nhất của kí tự đó trong strstr. Ban đầu đặt ansans là một kí tự nào đó không phải chữ cái (ví dụ là -−) còn ansIndexansIndex là giá trị tượng trưng cho vô cực. Sau đó, với mỗi kí tự trong pattpatt , ta lấy giá trị tương ứng của nó trong minPosminPos, tạm gọi là minPositionminPosition. Nếu như kí tự có tồn tại và minPosition < ansIndexminPosition<ansIndex thì ta cập nhật ansIndexansIndex và ansans.
Nếu như sau khi duyệt xong chuỗi pattpatt mà ansans vẫn là -− thì ta in ra “No character present”. Ngược lại thì ta in ra ansans.
Độ phức tạp
O(T \times (|str| + |patt|))O(T×(∣str∣+∣patt∣))với strstr và pattpatt là hai chuỗi của mỗi trường hợp kiểm tra.


 */
public class MinimumIndexedCharacter {
}

/*
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <vector>
using namespace std;

//=================================================
// DEFINE
#define INF 1e6
#define init_cin cin.tie(NULL), cout.tie(NULL), ios_base::sync_with_stdio(false)

//=================================================
// FUNCTIONS & TEMPLATES
int hashChar(char i)
{
    return i - 'a';
}

void insertKey(int minPos[], char ch, int pos)
{
    int index = hashChar(ch);
    if (minPos[index] == -1) {
        minPos[index] = pos;
    }
}

int getKey(int minPos[], char ch)
{
    int index = hashChar(ch);
    return minPos[index];
}

//=================================================
// MAIN
int main()
{
    // Fast IO
    init_cin;

    int T;
    cin >> T;
    string str, patt;
    int minPos[26];

    while (T--) {
        cin >> str >> patt;

        fill(minPos, minPos + 26, -1);
        for (int i = 0; i < str.size(); ++i) {
            insertKey(minPos, str[i], i);
        }

        char ans = '-';
        int ansIndex = INF;
        for (char ch : patt) {
            int minPosition = getKey(minPos, ch);
            if (minPosition != -1 && minPosition < ansIndex) {
                ans = ch;
                ansIndex = minPosition;
            }
        }

        if (ans == '-') {
            cout << "No character present\n";
        } else {
            cout << ans << endl;
        }
    }
    return 0;
}
 */

/*
import java.util.*;

class Main {
    public static int hashChar(char i)
    {
        return i - 'a';
    }

    public static void insertKey(int minPos[], char ch, int pos)
    {
        int index = hashChar(ch);
        if (minPos[index] == -1) {
            minPos[index] = pos;
        }
    }

    public static int getKey(int minPos[], char ch)
    {
        int index = hashChar(ch);
        return minPos[index];
    }

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int T;
        String str, patt;
        int minPos[] = new int[26];
        int INF = 1000000;

        T = sc.nextInt();
        for (int tc = 0; tc < T; tc++) {
            str = sc.next();
            patt = sc.next();

            Arrays.fill(minPos, - 1);
            for (int i = 0; i < str.length(); ++i) {
                insertKey(minPos, str.charAt(i), i);
            }

            char ans = '-';
            int ansIndex = INF;
            for (int i = 0; i < patt.length(); ++i) {
                int minPosition = getKey(minPos, patt.charAt(i));
                if (minPosition != -1 && minPosition < ansIndex) {
                    ans = patt.charAt(i);
                    ansIndex = minPosition;
                }
            }

            if (ans == '-') {
                System.out.println("No character present");
            } else {
                System.out.println(ans);
            }
        }
    }
}
 */