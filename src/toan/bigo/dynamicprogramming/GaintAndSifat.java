package toan.bigo.dynamicprogramming;

import java.util.Scanner;

/*
Với mỗi test case ta được hai chuỗi SS và ss.

Bước 1: Loại bỏ khoảng trắng trong SS và ss.
Bước 2: Tạo ra mảng tiền tố kmpkmp cho chuỗi ss.
Bước 3: Dựa vào mảng tiền tố kmpkmp, để tìm kiếm chuỗi ss trong SS. Ta dùng một biến countcount để đếm số lần tìm thấy chuỗi ss trong SS.
Độ phức tạp: O(t*(n + m))O(t∗(n+m)) với tt là số test case, nn là độ dài chuỗi SS và mm là độ dài chuỗi ss.
 */
public class GaintAndSifat {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCase = Integer.parseInt(scanner.nextLine());
        for (int t = 0; t < testCase; t++) {
            String s = scanner.nextLine();
            s = s.replaceAll("\\s+", "");
            String p = scanner.nextLine();
            int[] prefix = new int[p.length()];
            KMPPreProcesses(p, prefix);
            int count = kmpSearch(s, p, prefix);
            System.out.println("Case " + (t + 1) + ": "+count);
        }
    }

    public static void KMPPreProcesses(String p, int[] prefix) {
        prefix[0] = 0;
        int m = p.length();
        int len = 0;
        int i = 1;
        while (i < m) {
            if (p.charAt(i) == p.charAt(len)) {
                len++;
                prefix[i] = len;
                i++;
            } else {
                if (len != 0) {
                    len = prefix[len - 1];
                } else {
                    prefix[i] = 0;
                    i++;
                }
            }
        }
    }

    public static int kmpSearch(String t, String p, int[] prefix) {
        int n = t.length();
        int m = p.length();
        int i = 0;
        int j = 0;
        int result = 0;
        while (i < n) {
            if (t.charAt(i) == p.charAt(j)) {
                i++;
                j++;
            }
            if (j == m) {
                result++;
                j = prefix[j - 1];
            } else if (i < n && p.charAt(j) != t.charAt(i)) {
                if (j != 0) {
                    j = prefix[j - 1];
                } else {
                    i = i + 1;
                }
            }
        }
        return result;
    }
}
/*
#include <bits/stdc++.h>

using namespace std;

void KMPPreprocess(string& p, vector<int> &prefix) {
    prefix[0] = 0;
    int m = p.length();
    int j = 0;
    int i = 1;
    while (i < m) {
        if (p[i] == p[j]) {
            j++;
            prefix[i] = j;
            i++;
        } else {
            if (j != 0) {
                j = prefix[j - 1];
            } else {
                prefix[i] = 0;
                i++;
            }
        }
    }
}

int KMPSearch(string& t, string& p, vector<int> &prefix) {
    int n = t.length();
    int m = p.length();
    int i = 0, j = 0, found = 0;
    while (i < n) {
        if (p[j] == t[i]) {
            j++;
            i++;
        }
        if (j == m) {
            found++;
            j = prefix[j - 1];
        } else if (i < n && p[j] != t[i]) {
            if (j != 0) {
                j = prefix[j - 1];
            } else {
                i++;
            }
        }
    }
    return found;
}

int main() {
    int testCase;
    cin >> testCase;
    cin.ignore(256, '\n');
    for (int i = 1; i <= testCase; i++) {
        string t, p;
        getline(cin, t);
        getline(cin, p);
        t.erase(remove_if(t.begin(), t.end(), ::isspace), t.end());
        p.erase(remove_if(p.begin(), p.end(), ::isspace), p.end());
        // t = regex_replace(t, regex("\\s+"), "");
        // p = regex_replace(p, regex("\\s+"), "");
        vector<int> prefix(p.length());
        KMPPreprocess(p, prefix);
        printf("Case %d: %d\n", i, KMPSearch(t, p, prefix));
    }
    return 0;
}
 */

/*
import java.io.*;
import java.util.Scanner;
import java.util.*;

public class Solution_AC {
    static final int MAX_N = 100005;
    public static void main (String[] args) {
        MyScanner sc = new MyScanner();
        int prefix[] = new int [MAX_N];
        int T = sc.nextInt();
        for(int t = 1; t <= T; t++) {
            String test = sc.nextLine().replaceAll("\\s+","");
            String patt = sc.nextLine().replaceAll("\\s+","");
            //System.out.println(test);
            //System.out.println(test.length());
            System.out.printf("Case %d: %d\n",t,Solve(test,patt,prefix));
        }
    }

    static int Solve(String t,String s, int []prefix){ // t : test string
        // s : pattern string
        Build(s,prefix);
        int m = s.length();
        int n = t.length();
        int i = 0, j = 0;
        int res = 0;
        while(i < n){
            if(s.charAt(j) == t.charAt(i)){
                i++; j++;
            }
            if(j == m){
                res++;
                j = prefix[j-1];
            }
            else if(i < n && s.charAt(j) != t.charAt(i)){
                if(j != 0){
                    j = prefix[j-1];
                }
                else{
                    i ++;
                }
            }
        }
        return res;
    }

    static void Build(String s, int[] prefix){
        int m = s.length();
        prefix[0] = 0;
        int i = 1, j = 0;
        while(i < m){
            if(s.charAt(i) == s.charAt(j)){
                j++;
                prefix[i] = j;
                i++;
            }
            else{
                if(j != 0){
                    j = prefix[j-1];
                }
                else{
                    prefix[i] = 0;
                    i++;
                }
            }
        }
    }
    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
 */