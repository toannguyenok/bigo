package toan.bigo.dynamicprogramming;

import java.util.Arrays;
import java.util.Scanner;

/*
Hướng dẫn giải
Giải thích ví dụ:

Trường hợp 1: Ta có thể đổi đồng xu có giá trị 1212 thành 33 đồng xu có gía trị lần lượt là 6, 4, 36,4,3. Tổng 6 + 4 + 3 = 13 > 126+4+3=13>12 nên ta in ra 1313.
Trường hợp 2: Đồng xu có giá trị 22 chỉ có thể đổi thành 1, 0, 01,0,0. Vì vậy, ta để nguyên đồng xu 22 thì sẽ nhận được nhiều hơn.
Hướng dẫn giải:

Bài toán này cho số lượng test cases tối đa là 1010 nên ta sẽ lựa chọn phương án tiếp cận top-down để làm giảm thời gian chạy (do mật độ các trạng thái ta cần biết sẽ rất thưa chứ không dày đặc).

Ở bài này, ta nhận thấy rằng giá trị tối đa nhận được của 11 đồng xu nn sẽ được giải quyết bằng bài toán nhỏ hơn là tổng các giá trị tối đa của các đồng xu có giá trị \frac{n}{2}, \frac{n}{3}
​2
​
​n
​​ ,
​3
​
​n
​​  và \frac{n}{4}
​4
​
​n
​​ . Sau khi tính được tổng ấy, giá trị tối đa mà ta nhận được là số lớn hơn giữa tổng ấy (khi ta quyết định đổi đồng xu nn thành 33 đồng xu \frac{n}{2}, \frac{n}{3}, \frac{n}{4}
​2
​
​n
​​ ,
​3
​
​n
​​ ,
​4
​
​n
​​ ) và nn (khi ta quyết định giữ nguyên đồng xu ấy). Vì vậy công thức quy hoạch động của bài toán này là:

solve(n) = max(solve(n/2) + solve(n/3) + solve(n/4), n)solve(n)=max(solve(n/2)+solve(n/3)+solve(n/4),n)

Ngoài ra, ta còn có được giá trị tối đa của đồng xu có giá trị 11 và 22 lần lượt là 11 và 22. Vì vậy, ta lấy 11 và 22 làm hai giá trị ban đầu để làm gốc.

Cuối cùng, ta sẽ sử dụng một mảng số nguyên để lưu lại các trạng thái giúp cho quá trình tính toán không bị lặp lại (memoization). Ta đặt mảng này là dp và dp dùng để lưu lại kết quả tính được với một giá trị nn cụ thể để không phải tính lại solve(n)solve(n) nhiều hơn 11 lần. Mảng này sẽ có kích thước tối đa là 10^910
​9
​​ . Ban đầu ta có thể cho dp = -1dp=−1. Nếu như khi solve(n)solve(n), ta thấy dp[n] \neq -1dp[n]≠−1 (tức là kết quả cho nn đã được tính trước đó) thì ta không tính tiếp mà trả về dp[n]dp[n] ngay lập tức.

Độ phức tạp: O(N)O(N) với NN là giá trị của đồng xu đề cho.
 */
public class ByteLandianGoldCoins {

    public static int MAX = (int) (10e6 + 10);
    public static long[] dp = new long[MAX];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Arrays.fill(dp, 0);
        while (scanner.hasNext()) {
            long n = scanner.nextLong();
            System.out.println(calculator(n));
        }
    }

    public static long getCoins(long n, long a) {
        return n / a;
    }

    public static long calculator(long n) {
        if (n < 10) {
            return n;
        }
        if (n < MAX) {
            if (dp[(int) n] == 0) {
                long temp = calculator(getCoins(n, 2))
                        + calculator(getCoins(n, 3))
                        + calculator(getCoins(n, 4));
                dp[(int) n] = Math.max(n, temp);
            }
            return dp[(int) n];
        }
        long temp = calculator(getCoins(n, 2))
                + calculator(getCoins(n, 3))
                + calculator(getCoins(n, 4));
        return Math.max(n, temp);

    }
}
/*
#include <bits/stdc++.h>
using namespace std;

const int MAX = 1e6 + 1;

// global variables
long long dp[MAX];

// functions
void init() {
    for (long long i = 0; i < MAX; ++i)
        dp[i] = -1;
}
long long solve(long long n) {
    if (n < MAX && dp[n] != -1)
        return dp[n];
    if (n < 3) {
        return n;
    }
    long long result = max(solve(n / 2) + solve(n / 3) + solve(n / 4), n);
    if (n < MAX)
        dp[n] = result;
    return result;
}

// main function
int main() {
    // freopen("INP.txt", "r", stdin);
    // freopen("OUT.txt", "w", stdout);

    long long n;
    init();
    while (cin >> n) {
        cout << solve(n) << endl;
    }
    return 0;
}
 */
/*
import java.util.*;
import java.lang.*;
import java.io.*;

class solution
{
    public static final int MAX = 10000000 + 1;
    public static long[] dp;

    public static long solve(int n) {
        if (n < MAX && dp[n] != -1)
            return dp[n];
        if (n < 3)
            return n;
        long result = Math.max(solve(n/2)  + solve(n/3) + solve(n/4), n);
        if (n < MAX)
            dp[n] = result;
        return result;
    }

    public static void main (String[] args)
    {
        Scanner sc = new Scanner(System.in);
        dp = new long[MAX];
        for (int i = 0; i < MAX; i++)
            dp[i] = -1;
        while (sc.hasNextInt()) {
            int n = sc.nextInt();
            System.out.println(solve(n));
        }

    }
}
 */