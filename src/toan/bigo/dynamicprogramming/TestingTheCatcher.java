package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/*
Hướng dẫn giải
Ta gọi f_if
​i
​​  là số lượng phần tử trong dãy thỏa mãn kết thúc tại a_ia
​i
​​ .
Đầu tiên, ta xem như mọi phần tử a_ia
​i
​​  như một dãy con chỉ có 11 phần tử, do đó ban đầu ta có thể khởi tạo với mọi phần tử f_i = 1f
​i
​​ =1.
Khi ta đang xét với phần tử a_ia
​i
​​ , nếu tồn tại phần tử jj đứng trước ii thỏa mãn a_j \ge a_ia
​j
​​ ≥a
​i
​​ , ta sẽ cập nhật f_i = max\{f_i, f_{j + 1}\}f
​i
​​ =max{f
​i
​​ ,f
​j+1
​​ }, vì khi a_j \le a_ia
​j
​​ ≤a
​i
​​ , ta có thể chọn dãy con thỏa mãn đề dài nhất mà kết thúc tại a_ja
​j
​​  và đồng thời kết nạp thêm 11 phần tử vào cuối dãy.
Kết quả: Giá trị lớn nhất của các phần tử trong dãy ff.
Độ phức tạp: O(N^2 * T)O(N
​2
​​ ∗T) với NN là số lượng phần tử của dãy aa và TT là số lượng test của bài.


 */
public class TestingTheCatcher {

    private static ArrayList<Integer> result = new ArrayList<>();
    private static int[] path;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> input = new ArrayList<>();
        int testCase = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            int number = Integer.parseInt(line);
            if (number == -1) {
                if (input.size() != 0) {
                    testCase++;
                    Collections.reverse(input);
                    System.out.println("Test #" + testCase + ":");
                    System.out.println("  maximum possible interceptions: " + lis2(input));
                    input.clear();
                    result.clear();
                }
            } else {
                input.add(number);
            }
        }
    }

    public static int lis2(ArrayList<Integer> input) {
        int length = 0;
        for (int i = 0; i < input.size(); i++) {
            result.add(1);
        }
        path = new int[input.size()];
        Arrays.fill(path, -1);
        for (int i = 0; i < input.size(); i++) {
            for (int j = 0; j < i; j++) {
                if (input.get(i) >= input.get(j) && result.get(i) < result.get(j) + 1) {
                    result.set(i, result.get(j) + 1);
                    path[i] = j;
                }
            }
        }
        for (int i = 0; i < result.size(); i++) {
            if (length < result.get(i)) {
                length = result.get(i);
            }
        }
        return length;
    }

    public static int lis(ArrayList<Integer> input) {
        int length = 1;
        result.add(0);
        path = new int[input.size()];
        Arrays.fill(path, -1);
        for (int i = 1; i < input.size(); i++) {
            if (input.get(i) <= input.get(result.get(0))) {
                result.set(0, i);
            } else if (input.get(i) >= input.get(result.get(length - 1))) {
                path[i] = result.get(length - 1);
                result.add(i);
                length++;
            } else {
                int pos = lowerBound(input, result, length, input.get(i));
                path[i] = input.get(pos - 1);
                result.set(pos, i);
            }
        }
        return length;
    }

    public static int lowerBound(ArrayList<Integer> a, ArrayList<Integer> sub, int n, int x) {
        int left = 0;
        int right = n - 1;
        int pos = n;
        while (left < right) {
            int mid = left + (right - left) / 2;
            int index = sub.get(mid);
            if (a.get(index) >= x) {
                pos = mid;
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return pos;
    }

}


/*
#include <iostream>
#include <math.h>
#include <vector>

using namespace std;
vector <int> a;

int main () {
	// freopen("input.txt", "r", stdin);
	// freopen("output.txt", "w", stdout);

	int test = 0;
	while (true) {
		a.clear();
		int x;
		cin >> x;
		if (x == -1) {
			break;
		}

		if (test != 0) {
			cout << endl;
		}

		a.push_back(x);
		while (true) {
			cin >> x;
			if (x == -1) {
				break;
			}

			a.push_back(x);
		}

		int n = (int) a.size();
		vector <int> f(n + 1, 1);
		int ans = 1;
		for (int i = 1; i < n; i++) {
			for (int j = 0; j < i; j++) {
				if (a[j] >= a[i]) {
					f[i] = max(f[i], f[j] + 1);
				}
			}

			ans = max(ans, f[i]);
		}

		test++;
		cout << "Test #" << test << ":" << endl;
		cout << "  maximum possible interceptions: " << ans << endl;
	}
	return 0;
}
 */

/*
import java.util.*;
import java.lang.Math;

class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = 0;
        while (true) {
            ArrayList<Integer> a = new ArrayList<Integer>();
            int x = sc.nextInt();
            if (x == -1)
                break;
            if (test != 0)
                System.out.println();

            a.add(x);
            while (true) {
                x = sc.nextInt();
                if (x == -1)
                    break;
                a.add(x);
            }

            int[] f = new int[a.size() + 1];
            for (int i = 0; i <= a.size(); i++)
                f[i] = 1;

            int ans = 1;
            for (int i = 1; i < a.size(); i++) {
                for (int j = 0; j < i; j++)
                    if (a.get(j) >= a.get(i))
                        f[i] = Math.max(f[i], f[j] + 1);
                ans = Math.max(ans, f[i]);
            }

            test++;
            System.out.printf("Test #%d:\n", test);
            System.out.printf("  maximum possible interceptions: %d\n", ans);
            a.clear();
        }
    }
}
 */