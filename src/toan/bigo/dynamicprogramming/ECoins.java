package toan.bigo.dynamicprogramming;

/*
Hướng dẫn giải
Giải thích ví dụ
Lưu ý: viết (x, y)(x,y) có nghĩa là xx là giá trị conventional value, yy là InfoTechonological value của đồng tiền.

Ở test 1, có các đồng tiền (0, 2)(0,2), (2, 0)(2,0), dễ dàng nhận thấy ta không thể nào tạo được e-modulus bằng 55.

Ở test 2, có các đồng tiền (0, 2)(0,2), (2, 0)(2,0), (2, 1)(2,1), ta có thể sử dụng 1010 đồng (0, 2)(0,2) hoặc 1010 đồng (2, 0)(2,0) đều tạo ra được e-modulus bằng 2020. \sqrt{(2 \cdot 10) ^ 2 + (0 \cdot 10) ^ 2} = \sqrt{(0 \cdot 10) ^ 2 + (2 \cdot 10) ^ 2} = 20√
​(2⋅10)
​2
​​ +(0⋅10)
​2
​​
​
​​ =√
​(0⋅10)
​2
​​ +(2⋅10)
​2
​​
​
​​ =20.

Ở test 3, có các đồng tiền (3, 0)(3,0), (0, 4)(0,4), (5, 5)(5,5), dễ dàng nhận thấy ta để đạt được e-modulus bằng 55, chỉ cần 22 đồng (3, 0)(3,0) và (0, 4)(0,4) là đủ. \sqrt{(3 + 0) ^ 2 + (0 + 4) ^ 2} = 5√
​(3+0)
​2
​​ +(0+4)
​2
​​
​
​​ =5.


Hướng dẫn giải
Bài này là một biến thể của Coin Change Problem.

Ta sẽ gọi dp[i][j]dp[i][j] là số lượng đồng tiền tối thiểu để đạt được tổng conventional value là ii, và tổng InfoTechnological value bằng jj. Ta có công thức truy hồi:

dp[i][j] = \min\limits_{k \in 0..m} (dp[i - conventional[k]][j - infoTechnological[k]]) + 1dp[i][j]=
​k∈0..m
​min
​​ (dp[i−conventional[k]][j−infoTechnological[k]])+1

Với điều kiện: i \geq conventional[k], j \geq infoTechnological[k]i≥conventional[k],j≥infoTechnological[k] với 0 \leq i, j \leq S0≤i,j≤S

và dp[i][j] = 0dp[i][j]=0 khi một trong i, ji,j bằng 00.

Sau khi tính được bảng kết quả quy hoạch động, ta cần tìm dp[i][j]dp[i][j] nhỏ nhất sao cho i \cdot i + j \cdot j = S \cdot Si⋅i+j⋅j=S⋅S (*). Để làm việc này, ta có thể chỉ cần chạy một chỉ số ii và tính chỉ số còn lại bằng tmp= \sqrt{S \cdot S - i \cdot i}tmp=√
​S⋅S−i⋅i
​
​​  (chỉ lấy phần nguyên do i, ji,j là số nguyên) và sau đó thử lại xem tmp \cdot tmp + i \cdot itmp⋅tmp+i⋅i có bằng S \cdot SS⋅S không, ta sẽ có cặp (i, tmp)(i,tmp) thỏa điều kiện (*) trong độ phức tạp O(S)O(S). Lưu ý: chỉ số ii sẽ phải chạy từ 00 đến SS.

Các bước thực hiện cho một bộ test:

Bước 1: Đọc thông tin một bộ test.

Bước 2: Tính bảng kết quả:

Lặp i từ 0 đến S:

  	Lặp j từ 0 đến S:

  		Nếu một trong i và j bằng 0 thì continue

  		Gán dp[i][j] = INF (chưa tính)

  		Lặp k từ 0 đến S:

  			Nếu conventional[k] ≤ i và infoTechnological[k] ≤ j:
  				dp[i][j] = min(dp[i][j], dp[i - conventional[k]][j - infoTechnological[k]]) + 1
Bước 3: Tìm kết quả:

Tạo biến kết quả ans = INFans=INF (chưa tìm được kết quả).

Duyệt ii từ 00 đến SS:

  Tính tmp = phần nguyên của sqrt(S * S - i * i)

  Kiểm tra lại nếu tmp*tmp + i*i = S*S thỏa thì ans = min(ans, dp[i][tmp]).
Bước 4: Nếu ans = INFans=INF, thì không có lời giải in ra “not possible”, ngược lại in ra ansans.

Độ phức tạp: O(n \cdot S^2 \cdot m + S)O(n⋅S
​2
​​ ⋅m+S) với nn là số lượng test, mm là số đồng e-coin, SS là giá trị e-modulus cần đạt được.
 */
public class ECoins {
}

/*
#include <iostream>
#include <math.h>
using namespace std;
#define MAXM 40 + 10
#define MAXS 300 + 10
#define INF 1e9
int main() {
    int t, m, s;
    int conventional[MAXM], infoTechnological[MAXM];
    int dp[MAXS][MAXS];
    cin >> t;
    while (t--) {
        cin >> m >> s;
        for (int i = 0; i < m; i++) {
            cin >> conventional[i] >> infoTechnological[i];
        }
        dp[0][0] = 0;
        for (int i = 0; i <= s; i++) {
            for (int j = 0; j <= s; j++) {
                if (i == 0 && j == 0) continue;
                dp[i][j] = INF;
                for (int k = 0; k < m; k++) {
                    if (i >= conventional[k] && j >= infoTechnological[k]) {
                        dp[i][j] = min(dp[i][j], dp[i - conventional[k]][j - infoTechnological[k]] + 1);
                    }
                }
            }
        }
        int ans = INF;
        for (int i = 0; i <= s; i++) {
            int tmp = sqrt(s * s - i * i);
            if (tmp * tmp == s * s - i * i) {
                ans = min(ans, dp[i][tmp]);
            }
        }
        if (ans == INF) cout << "not possible";
        else cout << ans;
        cout << endl;
    }
}
 */

/*

 */