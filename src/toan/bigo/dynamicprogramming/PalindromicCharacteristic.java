package toan.bigo.dynamicprogramming;


/*
Hướng dẫn giải
Nhận xét: Nếu một chuỗi tt là một chuỗi (k + 1)(k+1)-palinfrome, nó cũng là một chuỗi kk-palindrome.

Ta có thể chứng minh nhận xét này bằng quy nạp:

Với k = 1k=1: tt là một chuỗi 22-palindrome. Như vậy, tt có thể được viết dưới hai dạng: t_1 + t_2t
​1
​​ +t
​2
​​  hoặc t_1 + c + t_2t
​1
​​ +c+t
​2
​​ . Với t_1, t_2t
​1
​​ ,t
​2
​​  lần lượt là nửa bên trái và nửa bên phải của chuỗi tt. Hai nửa t_1, t_2t
​1
​​ ,t
​2
​​  cùng là hai chuỗi 11-palindrome và t_1 = t_2t
​1
​​ =t
​2
​​ . Vì vậy, chuỗi t_1t
​1
​​  cũng bằng chuỗi đảo ngược của t_2t
​2
​​ . Như vậy, tt cũng là một chuỗi 11-palindrome.

Giả sử nhận xét trên đúng với k = ik=i. Ta cần chứng minh nhận xét cũng đúng với k = i + 1k=i+1.

Với k = i + 1k=i+1: tt là một chuỗi i + 2i+2-palindrome. Như vậy, tt có thể được viết dưới hai dạng: t_1 + t_2t
​1
​​ +t
​2
​​  hoặc t_1 + c + t_2t
​1
​​ +c+t
​2
​​ . Với t_1, t_2t
​1
​​ ,t
​2
​​  lần lượt là nửa bên trái và nửa bên phải của chuỗi tt. hai nửa t_1, t_2t
​1
​​ ,t
​2
​​  là hai chuỗi (i + 1)(i+1)-palindrome và t_1 = t_2t
​1
​​ =t
​2
​​ . Vì vậy, chuỗi t_1t
​1
​​  cũng bằng chuỗi đảo ngược của t_2t
​2
​​  nên tt phải là một chuỗi palindrome. Ngoài ra, do mệnh đề đúng với k = ik=i, hai chuỗi t_1, t_2t
​1
​​ ,t
​2
​​  cũng là hai chuỗi ii-palindrome. Như vậy, tt cũng là một chuỗi (i + 1)(i+1)-palindrome. Vậy, nhận xét đúng với k = i + 1k=i+1.

Nói cách khác, nếu tt là một chuỗi kk-palindrome, thì nó cũng là 11-palindrome, 22-palindrome, ..., (k - 1)(k−1)-palindrome.

Nếu một chuỗi tt là một chuỗi kk-palindrome, ta tạm gọi chỉ số palindrome của tt là kk.

Gọi max\_palin(t)max_palin(t) là chỉ số palindrome lớn nhất mà chuỗi tt có thể đạt được. nếu tt hoàn toàn không phải là một chuỗi palindrome, thì max\_palin(t) = 0max_palin(t)=0.

Ta có công thức truy hồi của max\_palin(t)max_palin(t) như sau:



Nếu ta:

Cho tt là một chuỗi con trong ss bắt đầu từ vị trí ll và kết thúc ở vị trí rr,
Đặt độ dài tt là sub\_len = r - l + 1sub_len=r−l+1,
Đặt một hàm dpdp sao cho dp(l, r) = max\_palin(t)dp(l,r)=max_palin(t)
thì hàm dpdp có công thức như sau:



Để kiểm tra t = s[l..r]t=s[l..r] có phải là một chuỗi palindrome hay không, ta có hai cách:

Ta có thể kiểm tra 2 điều kiện đồng thời: s[l + 1 .. r - 1]s[l+1..r−1] là một cuỗi palindrome, và s[l] = s[r]s[l]=s[r]. Để kiểm tra xem s[l + 1 .. r - 1]s[l+1..r−1] có phải là chuỗi palindrome hay không thì ta chỉ cần xét giá trị dp(l + 1, r - 1)dp(l+1,r−1) có lớn hơn 00 hay không.
Sử dụng polynomial hashing. Nếu giá trị hash của chuỗi s[l..r]s[l..r] và chuỗi đảo ngược của nó giống nhau thì s[l..r]s[l..r] là một chuỗi palindrome.
Với công thức truy hồi như trên, ta có thể giải quyết bài toán như sau:

Duyệt tất cả các chuỗi con của chuỗi ss theo thứ tự độ dài tăng dần, chiều từ trái sang phải để tính tất cả các giá trị của hàm dpdp bằng quy hoạch động theo công thức truy hồi đã nêu,
Tạo một mảng count[1..n]count[1..n], với phần tử count[k]count[k] cho biết số chuỗi con kk-palindrome có trong ss. Với mỗi giá trị dp(l, r)dp(l,r) tính được, ta cộng count[dp(l, r)]count[dp(l,r)] lên 11 đơn vị.
Sau bước trên, mảng count[1..n]count[1..n] chỉ mới tính chỉ số palindrome lớn nhất của mỗi chuỗi con. Để countcount tính đầy đủ tất cả các chỉ số palindrome của mỗi chuỗi con, ta cộng dồn mảng countcount từ sau ra trước.
Cuối cùng, ta in mảng countcount và kết thúc.
Độ phức tạp: O(n^2)O(n
​2
​​ ) với nn là độ dài chuỗi ss.


 */
public class PalindromicCharacteristic {
}


/*
#include <iostream>
#include <string>
#include <vector>

using namespace std;

const int P = 311;
// we use two mods to reduce the chance of collision
const int MOD1 = (int) (1e9 + 7);
const int MOD2 = (int) (1e9 + 9);

int BinExp(int64_t a, int64_t x, int mod) {
    int64_t res = 1;

    while (x > 0) {
        if (x & 1) {
            res *= a;
            res %= mod;
        }

        a *= a;
        a %= mod;
        x >>= 1;
    }

    return res;
}

int GetForwardHash(vector<int>& forward_hash, vector<int>& mod_inv, int left, int right, int mod) {
    return 1LL * (forward_hash[right + 1] - forward_hash[left] + mod) * mod_inv[left] % mod;
}

int GetBackwardHash(vector<int>& backward_hash, vector<int>& mod_inv, int n, int left, int right, int mod) {
    int r_left = n - left - 1;
    int r_right = n - right - 1;
    return 1LL * (backward_hash[r_left + 1] - backward_hash[r_right] + mod) * mod_inv[r_right] % mod;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    string s;
    cin >> s;
    int n = s.length();

    // Pre-compute
    vector<int> power_1(n + 1);
    vector<int> power_2(n + 1);
    vector<int> mod_inv_1(n + 1);
    vector<int> mod_inv_2(n + 1);
    power_1[0] = 1;
    power_2[0] = 1;
    mod_inv_1[0] = 1;
    mod_inv_2[0] = 1;

    for (int i = 1; i <= n; i++) {
        power_1[i] = (1LL * power_1[i - 1] * P) % MOD1;
        power_2[i] = (1LL * power_2[i - 1] * P) % MOD2;
        mod_inv_1[i] = BinExp(power_1[i], MOD1 - 2, MOD1);
        mod_inv_2[i] = BinExp(power_2[i], MOD2 - 2, MOD2);
    }

    // Compute hash values
    int64_t hash_1 = 0;
    int64_t hash_2 = 0;
    vector<int> forward_hash_1(n + 1, 0);
    vector<int> forward_hash_2(n + 1, 0);

    for (int i = 1; i <= n; i++) {
        hash_1 += 1LL * s[i - 1] * power_1[i];
        hash_2 += 1LL * s[i - 1] * power_2[i];
        hash_1 %= MOD1;
        hash_2 %= MOD2;
        forward_hash_1[i] = hash_1;
        forward_hash_2[i] = hash_2;
    }

    hash_1 = 0;
    hash_2 = 0;
    vector<int> backward_hash_1(n + 1);
    vector<int> backward_hash_2(n + 1);

    for (int i = 1; i <= n; i++) {
        hash_1 += 1LL * s[n - i] * power_1[i];
        hash_2 += 1LL * s[n - i] * power_2[i];
        hash_1 %= MOD1;
        hash_2 %= MOD2;
        backward_hash_1[i] = hash_1;
        backward_hash_2[i] = hash_2;
    }

    vector<vector<int>> dp(n + 1, vector<int>(n + 1, 0));
    vector<int> count(n + 1, 0);

    for (int sub_len = 1; sub_len <= n; sub_len++) {
        for (int left = 0; left <= n - sub_len; left++) {
            int right = left + sub_len - 1;

            if (sub_len == 1) {
                dp[left][right] = 1;
            }
            else if (sub_len == 2) {
                if (s[left] == s[right] ) {
                    dp[left][right] = 2;
                }
            }
            else {
                int fh1 = GetForwardHash(forward_hash_1, mod_inv_1, left, right, MOD1);
                int fh2 = GetForwardHash(forward_hash_2, mod_inv_2, left, right, MOD2);
                int bh1 = GetBackwardHash(backward_hash_1, mod_inv_1, n, left, right, MOD1);
                int bh2 = GetBackwardHash(backward_hash_2, mod_inv_2, n, left, right, MOD2);
                if (fh1 == bh1 && fh2 == bh2) {
                    dp[left][right] = dp[left][left + sub_len / 2 - 1] + 1;
                }
            }

            count[dp[left][right]]++;
        }
    }

    for (int i = n - 1; i >= 1; i--) {
        count[i] += count[i + 1];
    }

    for (int i = 1; i <= n; i++) {
        cout << count[i] << " ";
    }
    cout << '\n';

    return 0;
}
 */

/*
import java.util.Scanner;

public class Main {
    static final int P = 311;
    // we use two mods to reduce the chance of collision
    static final int MOD1 = (int) (1e9 + 7);
    static final int MOD2 = (int) (1e9 + 9);

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        int n = s.length();

        // Pre-compute
        int[] power1 = new int[n + 1];
        int[] power2 = new int[n + 1];
        int[] modInv1 = new int[n + 1];
        int[] modInv2 = new int[n + 1];
        power1[0] = 1;
        power2[0] = 1;
        modInv1[0] = 1;
        modInv2[0] = 1;

        for (int i = 1; i <= n; i++) {
            power1[i] = (int) (1L * power1[i - 1] * P % MOD1);
            power2[i] = (int) (1L * power2[i - 1] * P % MOD2);
            modInv1[i] = binExp(power1[i], MOD1 - 2, MOD1);
            modInv2[i] = binExp(power2[i], MOD2 - 2, MOD2);
        }

        // Compute hash values
        long hash1 = 0;
        long hash2 = 0;
        int[] forwardHash1 = new int[n + 1];
        int[] forwardHash2 = new int[n + 1];

        for (int i = 1; i <= n; i++) {
            hash1 += 1L * s.charAt(i - 1) * power1[i];
            hash2 += 1L * s.charAt(i - 1) * power2[i];
            hash1 %= MOD1;
            hash2 %= MOD2;
            forwardHash1[i] = (int) hash1;
            forwardHash2[i] = (int) hash2;
        }

        hash1 = 0;
        hash2 = 0;
        int[] backwardHash1 = new int[n + 1];
        int[] backwardHash2 = new int[n + 1];

        for (int i = 1; i <= n; i++) {
            hash1 += 1L * s.charAt(n - i) * power1[i];
            hash2 += 1L * s.charAt(n - i) * power2[i];
            hash1 %= MOD1;
            hash2 %= MOD2;
            backwardHash1[i] = (int) hash1;
            backwardHash2[i] = (int) hash2;
        }

        int[][] dp = new int[n + 1][n + 1];
        int[] count = new int[n + 1];

        for (int subLen = 1; subLen <= n; subLen++) {
            for (int left = 0; left <= n - subLen; left++) {
                int right = left + subLen - 1;

                if (subLen == 1) {
                    dp[left][left] = 1;
                }
                else if (subLen == 2) {
                    if (s.charAt(left) == s.charAt(right)) {
                        dp[left][right] = 2;
                    }
                }
                else {
                    int fh1 = GetForwardHash(forwardHash1, modInv1, left, right, MOD1);
                    int fh2 = GetForwardHash(forwardHash2, modInv2, left, right, MOD2);
                    int bh1 = GetBackwardHash(backwardHash1, modInv1, n, left, right, MOD1);
                    int bh2 = GetBackwardHash(backwardHash2, modInv2, n, left, right, MOD2);
                    if (fh1 == bh1 && fh2 == bh2) {
                        dp[left][right] = dp[left][left + subLen / 2 - 1] + 1;
                    }
                }

                count[dp[left][right]]++;
            }
        }

        for (int i = n - 1; i >= 1; i--) {
            count[i] += count[i + 1];
        }

        for (int i = 1; i <= n; i++) {
            System.out.print(count[i]);
            System.out.print(' ');
        }

        System.out.print('\n');
    }

    static int binExp(long a, long x, int mod) {
        long res = 1;

        while (x > 0) {
            if ((x & 1) != 0) {
                res *= a;
                res %= mod;
            }

            a *= a;
            a %= mod;
            x >>= 1;
        }

        return (int) res;
    }

    static int GetForwardHash(int[] forwardHash, int[] modInv, int left, int right, int mod) {
        return (int) (1L * (forwardHash[right + 1] - forwardHash[left] + mod) * modInv[left] % mod);
    }

    static int GetBackwardHash(int[] backwardHash, int[] modInv, int n, int left, int right, int mod) {
        int rLeft = n - left - 1;
        int rRight = n - right - 1;
        return (int) (1L * (backwardHash[rLeft + 1] - backwardHash[rRight] + mod) * modInv[rRight] % mod);
    }
}
 */