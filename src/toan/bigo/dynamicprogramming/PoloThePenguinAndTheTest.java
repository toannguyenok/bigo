package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Scanner;

/*
Hướng dẫn giải
Nếu ta thay đổi góc nhìn bài này một chút, ta có thể xem T_iT
​i
​​  là “trọng lượng” của câu hỏi ii và C_i \cdot P_iC
​i
​​ ⋅P
​i
​​  là “giá trị” của nó và WW là giới hạn “trọng lượng” của ta. Như vậy, qua cách phát biểu này, ta đã có thể dễ dàng đưa bài này về dạng bài toán 0/1 Knapsack.

Độ phức tạp: O(T*N*W)O(T∗N∗W) với TT là số lượng bộ dữ liệu, NN là số lượng câu hỏi và WW là thời gian học giới hạn.


 */
public class PoloThePenguinAndTheTest {

    public static class Test {
        int numberTest;
        int pointPerTest;
        int time;

        public Test(int numberTest, int pointPerTest, int time) {
            this.numberTest = numberTest;
            this.pointPerTest = pointPerTest;
            this.time = time;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCases = scanner.nextInt();
        for (int t = 0; t < testCases; t++) {
            int n = scanner.nextInt();
            int w = scanner.nextInt();
            ArrayList<Test> input = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                int numberOfTest = scanner.nextInt();
                int pointPerTest = scanner.nextInt();
                int time = scanner.nextInt();
                input.add(new Test(numberOfTest, pointPerTest, time));
            }
            System.out.println(knapSack(input, w));
        }
    }

    public static int knapSack(ArrayList<Test> items, int w) {
        int[][] k = new int[items.size() + 1][w + 1];
        for (int i = 0; i < w + 1; i++) {
            k[0][i] = 0;
        }
        for (int i = 0; i <= items.size(); i++) {
            for (int j = 0; j <= w; j++) {
                if (i == 0 || j == 0) {
                    k[i][j] = 0;
                } else if (items.get(i - 1).time > j) {
                    k[i][j] = k[i - 1][j];
                } else {
                    int temp1 = items.get(i - 1).numberTest * items.get(i - 1).pointPerTest + k[i - 1][j - items.get(i - 1).time];
                    int temp2 = k[i - 1][j];
                    k[i][j] = Math.max(temp1, temp2);
                }
            }
        }
        return k[items.size()][w ];
    }
}
/*
import java.util.Scanner;
import java.io.*;

class solution {
	public static int N = 109;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		int n, w;
		int[] c = new int[N], p = new int[N], t = new int[N];
		while (T-- != 0) {
			n = sc.nextInt();
			w = sc.nextInt();
			for (int i = 1; i <= n; ++i) {
				c[i] = sc.nextInt();
				p[i] = sc.nextInt();
				t[i] = sc.nextInt();
			}
			int[][] dp = new int[N][N];

			for (int i = 1; i <= n; ++i) {
				for(int j = 0; j <= w; ++j) {
					if (t[i] <= j) {
						dp[i][j] = Math.max(dp[i - 1][j], dp[i - 1][j - t[i]] + c[i] * p[i]);
					}
					else {
						dp[i][j] = dp[i - 1][j];
					}
				}
			}

			System.out.println(dp[n][w]);
		}
		sc.close();
	}
}
 */

/*
#include <iostream>     // cin, cout
#include <cstring>      // memset
#include <algorithm>    // max
using namespace std;

const int N = 109;

int n, w, c[N] = {}, p[N] = {}, t[N] = {}, dp[N][N] = {};

int main() {
    int T;
    cin >> T;
    while (T--) {
        cin >> n >> w;
        for (int i = 1; i <= n; i++) {
            cin >> c[i] >> p[i] >> t[i];
        }
        memset(dp, 0, sizeof dp);

        for (int i = 1; i <= n; i++) {
            for (int j = 0; j <= w; j++) {
                if (t[i] <= j) {
                    dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - t[i]] + c[i] * p[i]);
                }
                else {
                    dp[i][j] = dp[i - 1][j];
                }
            }
        }

        cout << dp[n][w] << "\n";
    }
    return 0;
}
 */