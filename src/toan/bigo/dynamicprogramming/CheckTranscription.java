package toan.bigo.dynamicprogramming;


/*
Hướng dẫn giải
Giả sử ta biết được độ dài xâu thứ nhất. Vì các ký tự ‘0’ và ‘1’ là cố định và độ dài của các xâu cũng cố định, nên ta có thể biết được độ dài xâu còn lại.
Đến đây ta chỉ cần duyệt độ dài của xâu thứ nhất, xác định độ dài xâu thứ hai, và kiểm tra xem với độ dài như vậy có thỏa mãn đề bài hay không. Ta có thể sử dụng Hash để kiểm tra nhanh 2 xâu.
Về độ phức tạp: có vẻ như thuật toán trên có độ phức tạp O(|S|*|T|)O(∣S∣∗∣T∣), nhưng thật ra, nó chỉ có độ phức tạp O(|T|)O(∣T∣). Vì lúc duyệt qua độ dài xâu thứ nhất, thuật toán dừng khi tích độ dài và tần suất lớn hơn độ dài xâu TT. Gọi tần suất cao hơn trong 2 ký tự là cc, lúc này đpt là O(|T|/c*|S|)O(∣T∣/c∗∣S∣) với cc \ge≥ (|S|)/2(∣S∣)/2. Do đó, (|T|)/c*|S| \ge 2|T|(∣T∣)/c∗∣S∣≥2∣T∣.
Độ phức tạp: O(N)O(N) với NN là độ dài xâu TT.


 */
public class CheckTranscription {
}

/*
#include <bits/stdc++.h>
using namespace std;

const int P = 1e9 + 7;
const int K = 41;
const int MAX = 1e6 + 5;
int h[MAX], pw[MAX], len[2], cnt[2], g[2], f[2];

int get(int l, int r){
    return (h[r] - 1ll * h[l - 1] * pw[r - l + 1] % P +  P) % P;
}

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    string s, t;
    cin >> s >> t;
    int n = s.size(), m = t.size();
    s = ' ' + s;
    t = ' ' + t;
    for (int i = 1; i <= n; i++)
        cnt[s[i] - '0']++;
    pw[0] = 1;
    for (int i = 1; i <= m; i++){
        pw[i] = (1ll * pw[i - 1] * K) % P;
        h[i] = (1ll * h[i - 1] * K + t[i] - 'a' + 1) % P;
    }
    int ans = 0;
    for (int i = 1; ; i++){
        f[0] = f[1] = 1;
        if (cnt[0] * i >= m)
            break;
        if ((m - cnt[0] * i) % cnt[1])
            continue;
        len[0] = i; len[1] = (m - cnt[0] * i) / cnt[1];
        for (int l = 1, k = 1; k <= n; k++){
            int cur = s[k] - '0';
            if (f[cur])
                g[cur] = get(l, l + len[cur] - 1), f[cur] = 0;
            else if (g[cur] != get(l, l + len[cur] - 1))
                break;
            if (!f[0] && !f[1] && g[0] == g[1])
                break;
            if (k == n)
                ans++;
            l += len[cur];
        }
    }
    cout << ans << '\n';
    return 0;
}
 */

/*

 */