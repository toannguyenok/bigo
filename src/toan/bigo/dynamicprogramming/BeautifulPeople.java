/*
package toan.bigo.dynamicprogramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BeautifulPeople {

    public static class People {
        int s;
        int b;

        public People(int s, int b) {
            this.s = s;
            this.b = b;
        }
    }

//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        int n = scanner.nextInt();
//        ArrayList<Integer> s = new ArrayList<>();
//        ArrayList<Integer> b = new ArrayList<>();
//        for (int i = 0; i < n; i++) {
//            int sTemp = scanner.nextInt();
//            int bTemp = scanner.nextInt();
//            s.add(sTemp);
//            b.add(bTemp);
//        }
//        int[] sResult = lis(s);
//        int[] bResult = lis(b);
//        System.out.println();
//    }
//
//    public static int[] lis(List<People> input) {
//        int length = 1;
//        ArrayList<Integer> result = new ArrayList<>();
//        result.add(0);
//        int[] path = new int[input.size()];
//        int[] lis = new int[input.size()];
//        lis[0] = length;
//        Arrays.fill(path, -1);
//        for (int i = 1; i < input.size(); i++) {
//            if (input.get(i) <= input.get(result.get(0))) {
//                result.set(0, i);
//            } else if (input.get(i) > input.get(result.get(length - 1))) {
//                path[i] = result.get(length - 1);
//                result.add(i);
//                length++;
//            } else {
//                int pos = lowerBound(input, result, length, input.get(i));
//                path[i] = input.get(pos - 1);
//                result.set(pos, i);
//            }
//            lis[i] = length;
//        }
//        return lis;
//    }
//
//    public static int lowerBound(List<Integer> a, ArrayList<Integer> sub, int n, int x) {
//        int left = 0;
//        int right = n;
//        int pos = n;
//        while (left < right) {
//            int mid = left + (right - left) / 2;
//            int index = sub.get(mid);
//            if (a.get(index) >= x) {
//                pos = mid;
//                right = mid;
//            } else {
//                left = mid + 1;
//            }
//        }
//        return pos;
//    }
}

*/
/*
Hướng dẫn giải
Bước 11: Đầu tiên ta sẽ tạo mảng một cấu trúc struct Member gồm 33 thành phần là S, BS,B và idid.
Bước 22: Tạo mảng members kiểu Member và nhập vào các thông tin tương ứng kèm theo idid của người đó theo thứ tự trong input.
Bước 33: Sắp xếp lại mảng members tăng dần theo chỉ số SS. Nếu với 22 thành viên ii và jj có chỉ số S_i = S_jS
​i
​​ =S
​j
​​  thì ta sẽ xét điều kiện B_i > B_jB
​i
​​ >B
​j
​​  để sắp xếp. Lí do: ta nhận thấy rằng 22 thành viên ii và jj chỉ không ghét nhau khi S_i \le S_jS
​i
​​ ≤S
​j
​​  và B_i < B_jB
​i
​​ <B
​j
​​ , hoặc, S_j \le S_iS
​j
​​ ≤S
​i
​​  và B_j < B_iB
​j
​​ <B
​i
​​ . Do đó ta chỉ cần sắp xếp mảng tăng dần theo SS và chỉ gọi hàm LIS để tìm dãy con tăng dài nhất cho dãy thành phần BB trong mảng members vì lúc này do dãy SS trong mảng members đã tăng dần nên dãy con tăng dài nhất trong members mà ta tìm được cũng sẽ có dãy SS tăng dần tương ứng.
Bước 44: Gọi LIS để thực hiện việc tìm dãy con tăng nhất cho thành phần BB trong mảng members.
Độ phức tạp: O(NlogN)O(NlogN) với NN là số thành viên trong câu lạc bộ.
 *//*


*/
/*
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

struct Member {
    int s, b;
    int id;

    bool operator<(const Member & member) const {
        return this->s == member.s ? this->b > member.b : this->s < member.s;
    }
};

int lower_bound(const vector<Member> &a, const vector<int> &sub, int n, int x) {
    int left = 1;
    int right = n;
    int pos = right;
    while (left < right) {
        int mid = (left + right) >> 1;
        int index = sub[mid];
        if (a[index].b >= x) {
            pos = mid;
            right = mid;
        }
        else {
            left = mid + 1;
        }
    }
    return pos;
}

int LIS(const vector<Member> & a, vector<int> & res) {
    vector<int> increasingSub(a.size());
    vector<int> path(a.size(), -1);

    int length = 1;
    increasingSub[1] = 1;

    for (int i = 2; i < a.size(); i++) {
        if (a[i].b > a[increasingSub[length]].b) {
            length++;
            increasingSub[length] = i;
            path[i] = increasingSub[length - 1];
        }
        else {
            int pos = lower_bound(a, increasingSub, length, a[i].b);
            path[i] = increasingSub[pos - 1];
            increasingSub[pos] = i;
        }
    }

    int i = increasingSub[length];
    while (i > 0) {
        res.push_back(a[i].id);
        i = path[i];
    }

    return length;
}

int main() {
    ios::sync_with_stdio(false);
    int n;
    cin >> n;
    vector<Member> members(n + 1);
    for (int i = 1; i <= n; ++i) {
        cin >> members[i].s >> members[i].b;
        members[i].id = i;
    }

    sort(members.begin() + 1, members.end());
    vector<int> res;
    cout << LIS(members, res) << endl;
    for (int i = res.size() - 1; i >= 0; --i) {
        cout << res[i] << " ";
    }
}
 *//*


*/
/*
package main;
import java.util.*;


public class Main {
	static class Member implements Comparable<Member> {
		public int s,b,id;

		public int compareTo(Member x)
	    {
			if(this.s != x.s) {
				return this.s - x.s;
			}
			return - this.b + x.b;

	    }
		public Member(int s, int b, int id) {
			this.s = s;
			this.b = b;
			this.id = id;
		}
	}
	public static int lower_bound(Member[] a, int[] sub, int n, int x) {
	    int left = 1;
	    int right = n;
	    int pos = right;
	    while (left < right) {
	        int mid = (left + right) >> 1;
	        int index = sub[mid];
	        if (a[index].b >= x) {
	            pos = mid;
	            right = mid;
	        }
	        else {
	            left = mid + 1;
	        }
	    }
	    return pos;
	}
	public static int LIS(Member[] a, ArrayList<Integer> res, int n) {
		int[] increasingSub = new int[n];
		int [] path = new int[n];
		Arrays.fill(path, -1);

	    int length = 1;
	    increasingSub[1] = 1;

	    for (int i = 2; i < n; i++) {
	        if (a[i].b > a[increasingSub[length]].b) {
	            length++;
	            increasingSub[length] = i;
	            path[i] = increasingSub[length - 1];
	        }
	        else {
	            int pos = lower_bound(a, increasingSub, length, a[i].b);
	            path[i] = increasingSub[pos - 1];
	            increasingSub[pos] = i;
	        }
	    }

	    int i = increasingSub[length];
	    while (i > 0) {
	        res.add(a[i].id);
	        i = path[i];
	    }

	    return length;
	}

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n,s,b,id;


        //ArrayList<Member> members = new ArrayList<>();
        n = sc.nextInt();
        Member[] members = new Member[n+1];
        for(int i = 1 ; i <= n ; i ++) {
        	s = sc.nextInt();
        	b = sc.nextInt();
        	members[i] = new Member(s,b,i);
        }
        Arrays.sort(members,1,n+1);
        ArrayList<Integer> res = new ArrayList<Integer>();
        System.out.println(LIS(members,res,n+1));
                for (int i = res.size() - 1; i >= 0; --i) {
                System.out.printf("%d ",res.get(i));
                }
                }
                }
 */
