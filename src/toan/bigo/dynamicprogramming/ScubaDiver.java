package toan.bigo.dynamicprogramming;

/*
Hướng dẫn giải
Đây là bài toán tương tự với bài toán Knapsack, khối lượng túi trong bài Knapsnack sẽ tương ứng với lượng oxigen và nitrogen cần thiết và tổng số tiền tương ứng với tổng khối lượng các bình dưỡng khí.

Bước 1: Đọc vào các thông tin của bộ test: lượng oxigen tt và nitrogen aa cần thiết, số lượng bình nn và thông tin của mỗi bình dưỡng khí. Với các mảng chứa thông tin của bình, chúng ta sẽ bắt đầu bằng phần tử thứ 11 và bỏ qua phần tử đầu tiên (đánh số từ 11).
Bước 2: Khởi tạo một mảng 33 chiều dpdp có kích thước 1001*22*801001∗22∗80 với giá trị khởi tạo bằng -1−1. Mảng có kích thước như vậy vì chúng ta có giới hạn tương ứng của n,tn,t và aa.
Bước 3: Viết hàm đệ quy để giải. Hàm này sẽ nhận 33 tham số ii – số bình còn lại, oxiNeedoxiNeed – lượng oxigen cần và nitroNeednitroNeed – lượng nitrogen cần.
Kiểm tra nếu dp[i][oxiNeed][nitroNeed] \neq -1dp[i][oxiNeed][nitroNeed]≠−1 thì chúng ta chỉ cần trả về giá trị này.
Kiểm tra nếu oxiNeed = 0oxiNeed=0 và nitroNeed = 0nitroNeed=0, chúng ta sẽ gán dp[i][oxiNeed][nitroNeed] = 0dp[i][oxiNeed][nitroNeed]=0 và trả về giá trị này.
Kiểm tra nếu i = 0i=0 (chúng ta đã hết bình những vẫn cần lấy thêm), chúng ta trả về một giá trị dương vô cùng kí hiệu trường hợp này không có lời giải.
Chúng ta sẽ có 2 lựa chọn lấy hoặc không lấy bình hiện tại, nếu lấy thì lượng oxiNeedoxiNeed và nitroNeednitroNeed sẽ giảm đi tương ứng với lượng có trong bình và khối lượng sẽ cộng thêm khối lượng của bình hiện tại. Chúng ta cần lưu ý ở bước này sau khi trừ ra, oxiNeedoxiNeed và nitroNeednitroNeed có thể âm, khi đó chúng ta cần đặt lượng tương ứng bằng 00. Giá trị dp[i][oxiNeed][nitroNeed]dp[i][oxiNeed][nitroNeed] sẽ bằng giá trị nhỏ hơn trong 2 giá trị tương ứng lấy và không lấy bình thứ ii.
Bước 4: Gọi TT là lượng oxigen và AA là lượng nitrogen cần thiết. Gọi hàm solve(n, T, A)solve(n,T,A) và in ra giá trị dp[n][T][T]dp[n][T][T].
Độ phức tạp: O(c*N*T*A)O(c∗N∗T∗A) với cc là số lượng test, NN là số bình, TT là giới hạn của lượng oxigen cần thiết, và AA là giới hạn của lượng nitrogen cần thiết.
 */
public class ScubaDiver {
}

/*
#include <bits/stdc++.h>

using namespace std;

const int MAX_OXI = 22;
const int MAX_NITRO  = 80;
const int INF = 100000000;

vector<vector<vector<int>>> dp;
vector<int> oxiCylinder;
vector<int> nitroCylinder;
vector<int> weight;

int solve(int i, int oxiNeed, int nitroNeed) {
    if (dp[i][oxiNeed][nitroNeed] != -1)
        return dp[i][oxiNeed][nitroNeed];
    if (oxiNeed == 0 && nitroNeed == 0) {
        dp[i][oxiNeed][nitroNeed] = 0;
        return dp[i][oxiNeed][nitroNeed];
    }
    if (i == 0) {
        dp[i][oxiNeed][nitroNeed] = INF;
    } else {
        int donTakeThisCylinder = solve(i-1, oxiNeed, nitroNeed);
        int newOxiNeed = max(oxiNeed - oxiCylinder[i], 0);
        int newNitroNeed = max(nitroNeed- nitroCylinder[i], 0);
        int takeThisCylinder = solve(i-1, newOxiNeed, newNitroNeed) + weight[i];
        dp[i][oxiNeed][nitroNeed] = min(donTakeThisCylinder, takeThisCylinder);
    }
    return dp[i][oxiNeed][nitroNeed];
}



int main(int argc, char const* argv[])
{
    int T;
    cin >> T;
    while (T--) {
        int oxi, nitro;
        cin >> oxi >> nitro;

        int n;
        cin >> n;
        oxiCylinder = vector<int>(n+1);
        nitroCylinder = vector<int>(n+1);
        weight = vector<int>(n+1);
        dp = vector<vector<vector<int> > >(n+1, vector<vector<int>>(MAX_OXI, vector<int>(MAX_NITRO, -1)));
        for (int  i = 1; i <= n; i++) {
            cin >> oxiCylinder[i] >> nitroCylinder[i] >> weight[i];
        }
        solve(n, oxi, nitro);
        cout << dp[n][oxi][nitro] << endl;
    }
    return 0;
}
 */

/*
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    private static int[] oxyCylinder;
    private static int[] nitroCylinder;
    private static int[] weight;
    private static int[][][] dp;
    private static final int INF = (int)1e8;

    private static int solve(int n, int oxyNeed, int nitroNeed) {
        if (dp[n][oxyNeed][nitroNeed] != -1) {
            return dp[n][oxyNeed][nitroNeed];
        }
        if (oxyNeed == 0 && nitroNeed == 0) {
            dp[n][oxyNeed][nitroNeed] = 0;
            return dp[n][oxyNeed][nitroNeed];
        }
        if (n == 0) {
            dp[n][oxyNeed][nitroNeed] = INF;
        }
        else {
            int donTakeThisCylinder = solve(n-1, oxyNeed, nitroNeed);
            int newOxiNeed = Math.max(oxyNeed - oxyCylinder[n], 0);
            int newNitroNeed = Math.max(nitroNeed - nitroCylinder[n], 0);
            int takeThisCylinder = solve(n-1, newOxiNeed, newNitroNeed) + weight[n];
            dp[n][oxyNeed][nitroNeed] = Math.min(donTakeThisCylinder, takeThisCylinder);
        }
        return dp[n][oxyNeed][nitroNeed];
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int cs = 1; cs <= t; cs++) {
            int oxy = sc.nextInt();
            int nitro = sc.nextInt();
            int n = sc.nextInt();
            oxyCylinder = new int[n + 1];
            nitroCylinder = new int[n + 1];
            weight = new int[n + 1];
            dp = new int[n+1][22][80];
            for (int[][] matrix : dp) {
                for (int[] row: matrix) {
                    Arrays.fill(row, -1);
                }
            }
            for (int i = 1; i <= n; i++) {
                oxyCylinder[i] = sc.nextInt();
                nitroCylinder[i] = sc.nextInt();
                weight[i] = sc.nextInt();
            }
            System.out.println(solve(n, oxy, nitro));
        }
    }
}
 */