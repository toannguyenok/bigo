package toan.bigo.dynamicprogramming;

import java.util.Arrays;
import java.util.Scanner;

/*
Hướng dẫn giải
Ta có nhận xét quan trọng sau: số ký tự ít nhất cần phải thêm vào ss chính bằng độ dài của ss trừ đi độ dài dãy con palindrome dài nhất trong ss.

(Lưu ý: Dãy con (subsequence) khác với dãy con liên tiếp (substring)).

Như vậy, để giải quyết bài toán, ta chỉ cần tìm ra dãy con palindrome dài nhất của ss. Gọi f(start, end)f(start,end) là độ dài của dãy con palindrome dài nhất từ vị trí startstart đến vị trí endend trong chuỗi ss. Ta có công thức cho hàm ff như sau:



Với công thức trên, ta có thể dễ dàng giải quyết bài toán bằng quy hoạch động:

Đặt nn là độ dài chuỗi ss.
Tạo mảng max\_palin\_length[0..n-1][0..n-1]max_palin_length[0..n−1][0..n−1] lưu giá trị của hàm ff: mỗi phần tử max\_palin\_length[start][end]max_palin_length[start][end] lưu một giá trị f(start, end)f(start,end).
Đầu tiên, cần khởi tạo các giá trị max\_palin\_lengthmax_palin_length cho các dãy con liên tiếp [start, end][start,end] có độ dài là 11 và 22:
Tạo vòng lặp với biến ii, cho ii chạy từ 00 đến n – 1 và gán max\_palin\_length[i][i] = 1max_palin_length[i][i]=1.
Tạo vòng lặp với biến ii, cho ii chạy từ 00 đến n – 2:
Nếu s_i = s_{i + 1}s
​i
​​ =s
​i+1
​​ , gán max\_palin\_length[i][i + 1] = 2max_palin_length[i][i+1]=2.
Nếu s_i \ne s_{i + 1}s
​i
​​ ≠s
​i+1
​​ , gán max\_palin\_length[i][i + 1] = 1max_palin_length[i][i+1]=1.
Sau đó, ta thực hiện tính các giá trị max\_palin\_length[start][end]max_palin_length[start][end] với các dãy con liên tiếp [start, end][start,end] có độ dài từ 33 trở lên bằng công thức của hàm ff đã đề cập phía trên. Với mỗi độ dài lengthlength từ 33 đến nn, ta sẽ cho startstart chạy từ 00 đến n – length (n - lengthn−length là vị trí startstart cuối cùng để dãy con liên tiếp với độ dài lengthlength bắt đầu từ startstart vẫn còn nằm trong ss).
Kết quả bài toán sẽ là hiệu n - max\_palin\_length[0][n - 1]n−max_palin_length[0][n−1].
Độ phức tạp: O(T * n^2)O(T∗n
​2
​​ ) với TT là số test case và nn là độ dài chuỗi .
 */
public class Aibohphobia {

    public static int MAX = 6100 + 10;

    public static int[][] dp = new int[MAX][MAX];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.next();
        for (int i = 0; i < MAX; i++) {
            Arrays.fill(dp[i], -1);
        }
        String string2 = new StringBuffer(string).reverse().toString();
        int max = lcs(string, string2, string.length(), string2.length());
        System.out.println(string.length() - max);
    }


    public static int lcs(String string1, String string2, int m, int n) {
        if (m == 0 || n == 0) {
            return 0;
        }
        if (dp[m][n] != -1) {
            return dp[m][n];
        }
        if (string1.charAt(m - 1) == string2.charAt(n - 1)) {
            dp[m][n] = 1 + lcs(string1, string2, m - 1, n - 1);
        } else {
            dp[m][n] = Math.max(lcs(string1, string2, m, n - 1), lcs(string1, string2, m - 1, n));
        }

        return dp[m][n];
    }

}

/*
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int test_case = 0; test_case < t; test_case++) {
        string s;
        cin >> s;

        int n = s.length();

        vector<vector<int>> max_palin_length(n, vector<int>(n));

        for (int i = 0; i < n; i++) {
            max_palin_length[i][i] = 1;
        }

        if (n > 1) {
            for (int i = 0; i < n - 1; i++) {
                if (s[i] == s[i + 1]) {
                    max_palin_length[i][i + 1] = 2;
                }
                else {
                    max_palin_length[i][i + 1] = 1;
                }
            }
        }

        for (int length = 3; length <= n; length++) {
            for (int start = 0; start < n - length + 1; start++) {
                int end = start + length - 1;

                if (s[start] == s[end]) {
                    max_palin_length[start][end] = max_palin_length[start + 1][end - 1] + 2;
                }
                else {
                    max_palin_length[start][end] = max(max_palin_length[start + 1][end],
                                                       max_palin_length[start][end - 1]);
                }
            }
        }

        int ans = n - max_palin_length[0][n - 1];

        cout << ans << '\n';
    }

    return 0;
}
 */

/*
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    static Scanner in = new Scanner(System.in);
    static PrintWriter out = new PrintWriter(System.out);

    public static void main(String[] args) {
        int t = in.nextInt();

        for (int testCase = 0; testCase < t; testCase++) {
            char[] s = in.next().toCharArray();

            int n = s.length;

            int[][] maxPalinLength = new int[n][n];

            for (int i = 0; i < n; i++) {
                maxPalinLength[i][i] = 1;
            }

            if (n > 1) {
                for (int i = 0; i < n - 1; i++) {
                    if (s[i] == s[i + 1]) {
                        maxPalinLength[i][i + 1] = 2;
                    }
                    else {
                        maxPalinLength[i][i + 1] = 1;
                    }
                }
            }

            for (int length = 3; length <= n; length++) {
                for (int start = 0; start < n - length + 1; start++) {
                    int end = start + length - 1;
                    if (s[start] == s[end]) {
                        maxPalinLength[start][end] = maxPalinLength[start + 1][end - 1] + 2;
                    }
                    else {
                        maxPalinLength[start][end] = Math.max(maxPalinLength[start][end - 1],
                                maxPalinLength[start + 1][end]);
                    }
                }
            }

            out.println(n - maxPalinLength[0][n - 1]);
        }

		out.close();
    }
}
 */