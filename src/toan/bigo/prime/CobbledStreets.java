package toan.bigo.prime;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Đồ thị trong ví dụ được biểu thị như hình bên. Để tìm cây khung nhỏ nhất bạn cần đi qua các cạnh sau:

1 \leftrightarrow 2: 11↔2:1
2 \leftrightarrow 5: 12↔5:1
2 \leftrightarrow 3: 22↔3:2
5 \leftrightarrow 4: 25↔4:2
Tổng chi phí là 6 * 26∗2 (chi phí pp) = 1212.

Bài này bạn chỉ cần cài đặt Prim cơ bản sau đó tính chi phí cây khung nhỏ nhất rồi nhân với pp là ra kết quả.

Độ phức tạp: O(t*mlogn)O(t∗mlogn) với tt là số test case, mm là số cạnh và ee là số đỉnh
 */
public class CobbledStreets {

    private static ArrayList<Long> dist = new ArrayList<>();
    private static ArrayList<Integer> path = new ArrayList<>();
    private static ArrayList<Boolean> visited = new ArrayList<>();

    private static ArrayList<ArrayList<Node>> graph = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        ArrayList<Long> result = new ArrayList<>();
        for (int k = 0; k < t; k++) {
            dist.clear();
            path.clear();
            visited.clear();
            graph.clear();
            int price = sc.nextInt();
            int n = sc.nextInt();
            for (int i = 0; i < n; i++) {
                dist.add(Long.MAX_VALUE);
                path.add(-1);
                visited.add(false);
                graph.add(new ArrayList<>());
            }
            int m = sc.nextInt();
            for (int i = 0; i < m; i++) {
                int u = sc.nextInt() - 1;
                int v = sc.nextInt() - 1;
                long w = sc.nextLong();
                graph.get(u).add(new Node(v, w));
                graph.get(v).add(new Node(u, w));
            }
            prime(0, graph);
            long ans = 0;
            for (int i = 0; i < dist.size(); i++) {
                if (visited.get(i)) {
                    ans += dist.get(i);
                }
            }
            result.add(ans * price);
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println("" + result.get(i));
        }
    }

    private static void prime(int src, ArrayList<ArrayList<Node>> graph) {
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(new Node(src, 0));
        dist.set(src, 0l);

        while (!priorityQueue.isEmpty()) {
            Node currentNode = priorityQueue.remove();
            int u = currentNode.id;
            visited.set(u, true);
            for (int i = 0; i < graph.get(u).size(); i++) {
                Node newNode = graph.get(u).get(i);
                int v = newNode.id;
                long w = newNode.dist;
                if (!visited.get(v) && w < dist.get(v)) {
                    dist.set(v, w);
                    priorityQueue.add(new Node(v, w));
                    path.set(v, u);
                }
            }
        }
    }

    private static class Node implements Comparable<Node> {
        public Integer id;
        public Long dist;

        public Node(int id, long dist) {
            this.id = id;
            this.dist = dist;
        }

        @Override
        public int compareTo(Node o) {
            return this.dist.compareTo(o.dist);
        }
    }
}
/*
import java.util.*;
import java.io.*;


public class Main
{
    static PrintWriter out;
    final static int MAX =  10005;
    static int N;
    static int []path = new int[MAX];
    static int []dist = new int[MAX];
    static int []key = new int[MAX];
    static boolean[] visited = new boolean[MAX];
    static ArrayList<Pair<Integer,Integer> > graph[];

    public static void main(String[] args) {
        MyScanner in = new MyScanner();
        out = new PrintWriter(new BufferedOutputStream(System.out), true);
        int p,n,m,a,b,c,s;
        int T;
        graph = new ArrayList[MAX];
        // initialize
        for(int i = 0 ; i < MAX ; i++){
            graph[i] = new ArrayList<>();
        }
        T = in.nextInt();
        while(T-->0){
            s = 0;
            // initialize variable
            for(int i = 0 ; i < MAX ; i++){
                graph[i].clear();
                path[i] = -1;
                key[i] = Integer.MAX_VALUE;
                visited[i] = false;
            }
            p = in.nextInt();
            n = in.nextInt();
            m = in.nextInt();
            for (int i = 0; i<m; i++)
            {
                a = in.nextInt();
                b = in.nextInt();
                c = in.nextInt();
                graph[a-1].add(new Pair(b-1, c));
                graph[b-1].add(new Pair(a-1, c));
            }
            Prim(s);
            long ans = 0;
            for(int i = 0; i < n;i++){
                s = s + key[i];
            }
            out.println(s*p);
        }
    }


    static void Prim(int source){
        PriorityQueue< Pair<Integer,Integer> > pq = new PriorityQueue<>(new Comparator<Pair<Integer, Integer>>(){
            public int compare(Pair<Integer,Integer> a, Pair<Integer,Integer> b){
                return a.getFirst() - b.getFirst();
            }
        });
        pq.add(new Pair(0,source));
        key[source] = 0;

    	while(!pq.isEmpty())
    	{
            int node = pq.peek().getSecond();
            int d = pq.peek().getFirst();
            pq.remove();
            visited[node]= true;
            for(int i=0; i<graph[node].size(); ++i)
            {
                Pair<Integer,Integer> neighbor = graph[node].get(i);
                if(!visited[neighbor.first] && neighbor.second < key[neighbor.first])
                {
                    key[neighbor.first] = neighbor.second;
                    pq.add(new Pair(key[neighbor.first],neighbor.first));
                    path[neighbor.first] = node;
                }
            }
    	}

    }


    public static class Pair<F,S>{

        public F getFirst() {
            return first;
        }

        public void setFirst(F first) {
            this.first = first;
        }

        public S getSecond() {
            return second;
        }

        public void setSecond(S second) {
            this.second = second;
        }

        public Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }
        public F first;
        public S second;
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                        st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
                return Integer.parseInt(next());
        }
        long nextLong() {
                return Long.parseLong(next());
        }
        double nextDouble() {
                return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
 */