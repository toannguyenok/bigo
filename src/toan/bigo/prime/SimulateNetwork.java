package toan.bigo.prime;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Đầu tiên bạn sẽ tìm cây khung nhỏ nhất của đồ thị đề bài cho, sau đó lần lượt thay các cạnh có độ trễ lớn bằng cách cạnh có độ trễ nhỏ hơn trong QQ sợi cáp được cho.

Sau khi tìm được cây khung nhỏ nhất tạo thành từ các sợi cáp sẵn có, bạn sẽ thay các sợi cáp lớn nhất bằng những sợi cáp mới có độ trễ nhỏ nhất có thể để giảm chi phí xuống. Có thể sử dụng 2 heap (1 heap max cho các sợi cáp trong cây khung để ưu tiên thay những sợi lớn nhất và 1 heap min cho các sợi cáp mới để ưu tiên chọn những sợi cáp nhỏ nhất). Hoặc sắp xếp lại giảm dần theo các sợi cáp trong cây khung và tăng dần theo các sợi cáp mới (tương tự như 2 heap) và sau đó sử dụng kỹ thuật two pointer.

Độ phức tạp: O(MlogN + NlogN + QlogQ)O(MlogN+NlogN+QlogQ) trong đó MlogNMlogN là độ phức tạp Prim, NlogN + QlogQNlogN+QlogQ là độ phức tạp quá trình thay các cáp cũ bằng cáp mới.
 */
public class SimulateNetwork {

    private static class Node implements Comparable<Node> {
        int u;
        int w;

        public Node(int u, int w) {
            this.u = u;
            this.w = w;
        }

        @Override
        public int compareTo(Node o) {
            return this.w - o.w;
        }
    }

    private static int INF = (int) 1e9;
    private static ArrayList<ArrayList<Node>> graph = new ArrayList<>();
    private static ArrayList<Boolean> visited = new ArrayList<>();
    private static ArrayList<Integer> dist = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        for (int i = 0; i < n; i++) {
            visited.add(false);
            dist.add(INF);
            graph.add(new ArrayList<>());
        }

        for (int i = 0; i < m; i++) {
            int u = sc.nextInt() - 1;
            int v = sc.nextInt() - 1;
            int w = sc.nextInt();
            graph.get(u).add(new Node(v, w));
            graph.get(v).add(new Node(u, w));
        }
        int q = sc.nextInt();

        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int i = 0; i < q; i++) {
            minHeap.add(sc.nextInt());
        }
        prime(0);


        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });
        for (int i = 1; i < n; i++) {
            maxHeap.add(dist.get(i));
        }

        long res = 0;
        while (!maxHeap.isEmpty()) {
            int usedCable = maxHeap.remove();
            if (!minHeap.isEmpty() && minHeap.peek() < usedCable) {
                usedCable = minHeap.remove();
            }
            res += usedCable;
        }
        System.out.println(res);
    }

    private static void prime(int s) {
        PriorityQueue<Node> heap = new PriorityQueue<>();
        dist.set(s, 0);
        heap.add(new Node(s, 0));
        while (!heap.isEmpty()) {
            Node current = heap.remove();
            visited.set(current.u, true);
            for (int i = 0; i < graph.get(current.u).size(); i++) {
                Node neighbor = graph.get(current.u).get(i);
                if (!visited.get(neighbor.u) && neighbor.w < dist.get(neighbor.u)) {
                    dist.set(neighbor.u, neighbor.w);
                    heap.add(new Node(neighbor.u, neighbor.w));
                }
            }
        }
    }
}
/*
import java.util.*;

class Main {
    static ArrayList<ArrayList<Node>> graph;
    static int[] dist;
    static boolean[] visited;

    public static void prim(int src) {
        PriorityQueue<Node> pq = new PriorityQueue<Node>();
        pq.add(new Node(src, 0));
        dist[src] = 0;
        while (!pq.isEmpty()) {
            Node top = pq.poll();
            int u = top.id;
            visited[u] = true;
            for (int i = 0; i < graph.get(u).size(); i++) {
                Node neighbor = graph.get(u).get(i);
                int v = neighbor.id, w = neighbor.dist;
                if (!visited[v] && w < dist[v]) {
                    dist[v] = w;
                    pq.add(new Node(v, w));
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner in= new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        graph = new ArrayList<ArrayList<Node>>();
        dist = new int[n + 1];
        visited = new boolean[n + 1];
        Arrays.fill(dist, Integer.MAX_VALUE);
        Arrays.fill(visited, false);
        for (int i = 0; i <= n; i++) {
            graph.add(new ArrayList<Node>());
        }
        for (int i = 0; i < m; i++) {
            int u = in.nextInt();
            int v = in.nextInt();
            int w = in.nextInt();
            graph.get(u).add(new Node(v, w));
            graph.get(v).add(new Node(u, w));
        }
        prim(1);
        PriorityQueue<Integer> mstCable =
            new PriorityQueue<Integer>(new MaxHeapComparator());
        for (int i = 2; i <= n; i++) {
            mstCable.add(dist[i]); // add all edges in mst to max heap
        }
        int q = in.nextInt();
        PriorityQueue<Integer> newCables = new PriorityQueue<Integer>();
        for (int i = 0; i < q; i++) {
            int x = in.nextInt();
            newCables.add(x);
        }
        long res = 0;
        while (!mstCable.isEmpty()) {
            int usedCable = mstCable.poll();
            if (!newCables.isEmpty() && newCables.peek() < usedCable)
                usedCable = newCables.poll(); // replace old cable by new cable
            res += usedCable;
        }
        System.out.print(res);
    }
}

class Node implements Comparable<Node> {
    public Integer id;
    public Integer dist;
    public Node(Integer id, Integer dist) {
        this.id = id;
        this.dist = dist;
    }
    @Override
    public int compareTo(Node other) {
        return this.dist.compareTo(other.dist);
    }
}

class MaxHeapComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer o1, Integer o2) {
        return o2.compareTo(o1);
    }
}
 */
