package toan.bigo.prime;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
ViEng
Đầu tiên ta thực hiện thuật toán Prim để tìm ra được cây khung có tổng trọng số nhỏ nhất. Ta gọi đây là kết quả mini1mini1 và ta lưu lại danh sách các cạnh này. Sau đó, với mỗi cạnh tương ứng, ta thực hiện thao tác xóa cạnh đó ra khỏi đồ thị và thực hiện thuật toán Prim trên đồ thị mà không chứa cạnh đó, rồi ta khôi phục lại cạnh đó. Tổng trọng số nhỏ nhất sau các lần xóa cạnh là tổng trọng số nhỏ nhì của cây khung của đồ thị đã cho.

Độ phức tạp: O(T * N * MlogN)O(T∗N∗MlogN) với TT là số lượng bộ data set cho mỗi test NN là số trường học và MM là số con đường.
 */
public class ACMContestsAndBlackout {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        ArrayList<Result> result = new ArrayList<>();
        ArrayList<Integer> dist = new ArrayList<>();
        ArrayList<Integer> path = new ArrayList<>();
        ArrayList<Boolean> visited = new ArrayList<>();

        ArrayList<ArrayList<Node>> graph = new ArrayList<>();
        for (int k = 0; k < t; k++) {
            dist.clear();
            path.clear();
            visited.clear();
            graph.clear();
            int n = sc.nextInt();
            for (int i = 0; i < n; i++) {
                dist.add(Integer.MAX_VALUE);
                path.add(-1);
                visited.add(false);
                graph.add(new ArrayList<>());
            }
            int m = sc.nextInt();
            for (int i = 0; i < m; i++) {
                int u = sc.nextInt() - 1;
                int v = sc.nextInt() - 1;
                int w = sc.nextInt();
                graph.get(u).add(new Node(v, w));
                graph.get(v).add(new Node(u, w));
            }
            prime(0, graph, dist, path, visited);
            long result1 = 0;
            for (int i = 0; i < dist.size(); i++) {
                if (visited.get(i)) {
                    result1 += dist.get(i);
                }
            }
            long result2 = Long.MAX_VALUE;
            for (int i = 0; i < dist.size(); i++) {
                int u = i;
                if (path.get(u) != -1 && visited.get(u)) {
                    ArrayList<ArrayList<Node>> graphTemp = new ArrayList<>();
                    ArrayList<Integer> distTemp = new ArrayList<>();
                    ArrayList<Integer> pathTemp = new ArrayList<>();
                    ArrayList<Boolean> visitedTemp = new ArrayList<>();
                    for (int b = 0; b < n; b++) {
                        distTemp.add(Integer.MAX_VALUE);
                        pathTemp.add(-1);
                        visitedTemp.add(false);
                        graphTemp.add(new ArrayList<>());
                        for (int v = 0; v < graph.get(b).size(); v++) {
                            graphTemp.get(b).add(new Node(graph.get(b).get(v).id, graph.get(b).get(v).dist));
                        }
                    }
                    int v = path.get(u);
                    graphTemp.get(u).removeIf(node -> node.id == v);
                    graphTemp.get(v).removeIf(node -> node.id == u);
                    prime(0, graphTemp, distTemp, pathTemp, visitedTemp);
                    long tempResult2 = 0;
                    boolean needCompare = true;
                    for (int b = 0; b < distTemp.size(); b++) {
                        if (visitedTemp.get(b)) {
                            tempResult2 += distTemp.get(b);
                        } else {
                            needCompare = false;
                        }
                    }
                    if (needCompare) {
                        result2 = Math.min(result2, tempResult2);
                    }
                }
            }
            result.add(new Result(result1, result2));
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println("" + result.get(i).result1 + " " + result.get(i).result2);
        }
    }

    private static void prime(int src, ArrayList<ArrayList<Node>> graph,
                              ArrayList<Integer> dist,
                              ArrayList<Integer> path,
                              ArrayList<Boolean> visited) {
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(new Node(src, 0));
        dist.set(src, 0);

        while (!priorityQueue.isEmpty()) {
            Node currentNode = priorityQueue.remove();
            int u = currentNode.id;
            visited.set(u, true);
            for (int i = 0; i < graph.get(u).size(); i++) {
                Node newNode = graph.get(u).get(i);
                int v = newNode.id;
                int w = newNode.dist;
                if (!visited.get(v) && w < dist.get(v)) {
                    dist.set(v, w);
                    priorityQueue.add(new Node(v, w));
                    path.set(v, u);
                }
            }
        }
    }

    private static class Node implements Comparable<Node> {
        public Integer id;
        public Integer dist;

        public Node(int id, int dist) {
            this.id = id;
            this.dist = dist;
        }

        @Override
        public int compareTo(Node o) {
            return this.dist.compareTo(o.dist);
        }
    }

    private static class Result {
        long result1;
        long result2;

        public Result(long result1, long result2) {
            this.result1 = result1;
            this.result2 = result2;
        }
    }
}
/*
import java.util.Scanner;
public class Main {

	static Scanner sc  = new Scanner(System.in);
	static final int MAX  =  10001000;
	static final int INF =  1000000000;
	static final int N = 110;
	static int graph[][] = new int[N][N], path[][] = new int[N][N], used[][] = new int[N][N];
	static int pre[]= new int[N], key[] = new int[N], n, res, ans;
	static boolean visited[]=new boolean[N];


	static int minKey(int key[], boolean visited[])
	{
		int min = INF, min_index = 0;

		for (int v = 0; v < n; v++)
			if (visited[v] == false && key[v] < min)
			{
				min = key[v];
				min_index = v;
			}
		return min_index;
	}

	static void prim()
	{
		int i, j;
		for(i=0; i<n; i++)
			key[i] = graph[0][i];

		for(int k=0; k<n-1; k++)
		{

			int u = minKey(key, visited);

			visited[u] = true;
			res += key[u];
			used[u][pre[u]] = used[pre[u]][u] = 1;

			for(j=0; j<n; j++)
			{
				if(!visited[j] && key[j]>graph[u][j])
				{
					key[j] = graph[u][j];
					pre[j] = u;
				}
				else if(visited[j] && j!=u)
				{
					path[u][j] = path[j][u] = Math.max(key[u], path[j][pre[u]]);
				}
			}
		}

		for(i=0; i<n; i++)
		{
			for(j=i+1; j<n; j++)
				if(used[i][j]==0)
				{
					ans = Math.min(ans, res-path[i][j] + graph[i][j]);
				}
		}
	}

	public static void main(String[] args)
	{

		int T, a, b, c, m;
		T = sc.nextInt();
		while(T > 0)
		{
			T--;
			n = sc.nextInt();
			m = sc.nextInt();
			for (int i=0;i<n;i++){
				visited[i]=false;
				pre[i]=0;
				for (int j=0;j<n;j++){
					graph[i][j]=INF;
					path[i][j]=0;
					used[i][j]=0;
				}
			}
			visited[0] = true;
			res = 0;
			ans = INF;
			for(int i=0; i<m; i++)
			{
				a = sc.nextInt();
				b = sc.nextInt();
				c = sc.nextInt();
				graph[a-1][b-1] = c;
				graph[b-1][a-1] = c;
			}
			prim();
			System.out.println(res+" "+ans);
		}

	}

}
 */