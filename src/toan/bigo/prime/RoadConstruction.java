package toan.bigo.prime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Bài này do các tên thành phố là chuỗi nên bạn cần phải lưu tên các thành phố lại vào trong một mapmap. Sau đó, mỗi tên thành phố bạn sẽ lưu idid lại map <string, int> mymap (đối với C++, các ngôn ngữ khác dùng cấu trúc tương tự). Sau khi lưu xong thì bạn lấy idid ra để bỏ vào trong graph và chạy thuật toán Prim.

Ta sẽ không thể tạo được thành một cây khung nếu tồn tại một vị trí ii nào đó mà dist_i = infdist
​i
​​ =inf. Do đó, kết quả xuất ra là Impossible nếu như tồn tại ii mà dist[i] = infdist[i]=inf. Ngược lại, ta xuất ra tổng trọng số của các cạnh trong cây khung nhỏ nhất.

Độ phức tạp: O(t*(mlogn + nlogn))O(t∗(mlogn+nlogn)) với tt là số bộ test, mm là số cạnh và nn là số đỉnh (trong trường hợp xấu nhất thì mỗi cạnh nối giữa hai đỉnh hoàn toàn mới thì n = 2*mn=2∗m). Vì đỉnh của ta là tên nên tốn chi phí tìm kiếm trong map nên phải thêm nlognnlogn.
 */
public class RoadConstruction {

    private static ArrayList<Integer> dist = new ArrayList<>();
    private static ArrayList<Integer> path = new ArrayList<>();
    private static ArrayList<Boolean> visited = new ArrayList<>();

    private static ArrayList<ArrayList<Node>> graph = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        ArrayList<String> result = new ArrayList<>();
        for (int k = 0; k < t; k++) {
            dist.clear();
            path.clear();
            visited.clear();
            graph.clear();

            HashMap<String, Integer> hash = new HashMap<>();

            int m = sc.nextInt();
            int index = 0;
            for (int i = 0; i < m; i++) {
                String city1 = sc.next();
                String city2 = sc.next();
                int w = sc.nextInt();
                int u;
                int v;
                if (!hash.containsKey(city1)) {
                    u = index;
                    hash.put(city1, index++);
                    dist.add(Integer.MAX_VALUE);
                    visited.add(false);
                    graph.add(new ArrayList<>());
                    path.add(-1);
                } else {
                    u = hash.get(city1);
                }
                if (!hash.containsKey(city2)) {
                    v = index;
                    hash.put(city2, index++);
                    dist.add(Integer.MAX_VALUE);
                    visited.add(false);
                    graph.add(new ArrayList<>());
                    path.add(-1);
                } else {
                    v = hash.get(city2);
                }
                int tempIndexV = -1;
                for (int l = 0; l < graph.get(u).size(); l++) {
                    if (graph.get(u).get(l).id == v) {
                        tempIndexV = l;
                    }
                }
                int tempIndexU = -1;
                for (int l = 0; l < graph.get(v).size(); l++) {
                    if (graph.get(v).get(l).id == u) {
                        tempIndexU = l;
                    }
                }

                if (tempIndexV == -1) {
                    graph.get(u).add(new Node(v, w));
                } else {
                    if (graph.get(u).get(tempIndexV).dist > w) {
                        graph.get(u).set(tempIndexV, new Node(v, w));
                    }
                }

                if (tempIndexU == -1) {
                    graph.get(v).add(new Node(u, w));
                } else {
                    if (graph.get(v).get(tempIndexU).dist > w) {
                        graph.get(v).set(tempIndexU, new Node(u, w));
                    }
                }

            }
            prime(0, graph);
            long ans = 0;
            int countVisited = 0;
            for (int i = 0; i < dist.size(); i++) {
                if (visited.get(i)) {
                    countVisited++;
                    ans += dist.get(i);
                }
            }
            if (countVisited == visited.size()) {
                result.add(String.valueOf(ans));
            } else {
                result.add("Impossible");
            }
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println("Case " + (i + 1) + ": " + result.get(i));
        }
    }

    private static void prime(int src, ArrayList<ArrayList<Node>> graph) {
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(new Node(src, 0));
        dist.set(src, 0);

        while (!priorityQueue.isEmpty()) {
            Node currentNode = priorityQueue.remove();
            int u = currentNode.id;
            visited.set(u, true);
            for (int i = 0; i < graph.get(u).size(); i++) {
                Node newNode = graph.get(u).get(i);
                int v = newNode.id;
                int w = newNode.dist;
                if (!visited.get(v) && w < dist.get(v)) {
                    dist.set(v, w);
                    priorityQueue.add(new Node(v, w));
                    path.set(v, u);
                }
            }
        }
    }

    private static class Node implements Comparable<Node> {
        public Integer id;
        public Integer dist;

        public Node(int id, int dist) {
            this.id = id;
            this.dist = dist;
        }

        @Override
        public int compareTo(Node o) {
            return this.dist.compareTo(o.dist);
        }
    }
}
/*
import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.TreeMap;
import java.util.Scanner;

class Node implements Comparable<Node> {
    public Integer id;
    public Integer dist;

    public Node(Integer id, Integer dist) {
        this.id = id;
        this.dist = dist;
    }

    @Override
    public int compareTo(Node other) {
        return this.dist.compareTo(other.dist);
    }
}

public class Main {
    private static int[] dist;
    private static int[] path;
    private static boolean[] visited;

    public static int prim(ArrayList<ArrayList<Node>> graph) {
        PriorityQueue<Node> pq = new PriorityQueue<Node>();
        int n = graph.size();
        dist = new int[n];
        path = new int[n];
        visited = new boolean[n];
        Arrays.fill(dist, Integer.MAX_VALUE);
        Arrays.fill(path, -1);
        Arrays.fill(visited, false);
        pq.add(new Node(1, 0));
        dist[1] = 0;
        while (!pq.isEmpty()) {
            Node top = pq.poll();
            int u = top.id;
            visited[u] = true;
            for (int i = 0; i < graph.get(u).size(); i++) {
                Node neighbor = graph.get(u).get(i);
                int v = neighbor.id, w = neighbor.dist;
                if (!visited[v] && w < dist[v]) {
                    dist[v] = w;
                    pq.add(new Node(v, w));
                    path[v] = u;
                }
            }
        }

        int mst = 0;
        for (int i = 1; i < n; i++) {
            if (dist[i] == Integer.MAX_VALUE) {
                return -1;
            }
            mst += dist[i];
        }
        return mst;
    }

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int cs = 1; cs <= t; cs++) {
            TreeMap<String, Integer> my_map = new TreeMap<String, Integer>();
            ArrayList<ArrayList<Node>> graph = new ArrayList<ArrayList<Node>>();
            int m = sc.nextInt();
            int n = 0;
            int u, v, w;
            String s1, s2;

            // graph index from 1
            // add temporary graph[0]
            graph.add(new ArrayList<Node>());
            for (int i = 0; i < m; i++) {
                s1 = sc.next();
                s2 = sc.next();
                w = sc.nextInt();

                if (my_map.containsKey(s1) == false){
                    my_map.put(s1, ++n);
                    graph.add(new ArrayList<Node>());
                }
                if (my_map.containsKey(s2) == false) {
                    my_map.put(s2, ++n);
                    graph.add(new ArrayList<Node>());
                }
                u = my_map.get(s1);
                v = my_map.get(s2);
                graph.get(u).add(new Node(v, w));
                graph.get(v).add(new Node(u, w));
            }
            int mst = prim(graph);
            System.out.print("Case " + cs + ": ");
            if (mst == -1) {
                System.out.println("Impossible");
            }
            else {
                System.out.println(mst);
            }
        }
        return;
    }
}
 */