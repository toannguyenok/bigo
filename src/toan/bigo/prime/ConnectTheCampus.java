package toan.bigo.prime;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Bạn sẽ tính khoảng cách của tất cả các cặp tòa nhà, trừ những tòa nhà nào đã có kết nối rồi thì bạn đặt chi phí kết nối là 00.

Sau khi đã tính xong thì bạn bỏ toàn bộ các cặp kết nối và chi phí đó vào graph rồi chạy thuật toán Prim.

Cây khung nhỏ nhất là kết quả tìm được.

Độ phức tạp: O(T * N^2 * logN)O(T∗N
​2
​​ ∗logN) với TT là số lượng bộ test và O(N^2 * logN)O(N
​2
​​ ∗logN) là độ phức tạp của thuật toán Prim vì trong bài này ta dùng tất cả cạnh nối giữa các tòa nhà nên số lượng cạnh là N^2N
​2
​​ .
 */
public class ConnectTheCampus {

    private static ArrayList<Float> dist = new ArrayList<>();
    private static ArrayList<Integer> path = new ArrayList<>();
    private static ArrayList<Boolean> visited = new ArrayList<>();

    private static ArrayList<ArrayList<Node>> graph = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Double> result = new ArrayList<>();
        while (sc.hasNext()) {
           int n = sc.nextInt();
            graph.clear();
            dist.clear();
            visited.clear();
            path.clear();
            if (n != 1) {
                ArrayList<Point> points = new ArrayList<>();

                for (int i = 0; i < n; i++) {
                    dist.add(Float.MAX_VALUE);
                    path.add(-1);
                    visited.add(false);
                    graph.add(new ArrayList<>());
                    int x = sc.nextInt();
                    int y = sc.nextInt();
                    points.add(new Point(x, y));
                }

                //create graph
                for (int i = 0; i < graph.size(); i++) {
                    int currentX = points.get(i).x;
                    int currentY = points.get(i).y;
                    for (int j = 0; j < points.size(); j++) {
                        if (i != j) {
                            int newX = points.get(j).x;
                            int newY = points.get(j).y;
                            double x1 = Math.abs(newX - currentX);
                            double x2 = Math.abs(newY - currentY);
                            float w = (float) Math.sqrt(Math.pow(x1, 2) + Math.pow(x2, 2));
                            graph.get(i).add(new Node(j, w));
                        }
                    }
                }
                int m = sc.nextInt();
                for (int i = 0; i < m; i++) {
                    int u = sc.nextInt() - 1;
                    int v = sc.nextInt() - 1;
                    for (int j = 0; j < graph.get(u).size(); j++) {
                        if (graph.get(u).get(j).id == v) {
                            graph.get(u).set(j, new Node(v, 0));
                        }
                    }
                    for (int j = 0; j < graph.get(v).size(); j++) {
                        if (graph.get(v).get(j).id == u) {
                            graph.get(v).set(j, new Node(u, 0));
                        }
                    }
                }

                prime(0, graph);
                double ans = 0;
                for (int i = 0; i < dist.size(); i++) {
                    if (visited.get(i)) {
                        ans += dist.get(i);
                    }
                }
                result.add(ans);
            }
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println("" + String.format("%.2f", result.get(i)));
        }
    }

    private static void prime(int src, ArrayList<ArrayList<Node>> graph) {
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(new Node(src, 0));
        dist.set(src, 0f);

        while (!priorityQueue.isEmpty()) {
            Node currentNode = priorityQueue.remove();
            int u = currentNode.id;
            visited.set(u, true);
            for (int i = 0; i < graph.get(u).size(); i++) {
                Node newNode = graph.get(u).get(i);
                int v = newNode.id;
                float w = newNode.dist;
                if (!visited.get(v) && w < dist.get(v)) {
                    dist.set(v, w);
                    priorityQueue.add(new Node(v, w));
                    path.set(v, u);
                }
            }
        }
    }

    private static class Node implements Comparable<Node> {
        public Integer id;
        public Float dist;

        public Node(int id, float dist) {
            this.id = id;
            this.dist = dist;
        }

        @Override
        public int compareTo(Node o) {
            return this.dist.compareTo(o.dist);
        }
    }

    private static class Point {
        int x;
        int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
/*
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.ArrayList;

class Node implements Comparable<Node> {
	public Integer id;
	public Integer dist;
	public Node(Integer id, Integer dist) {
	this.id = id;
	this.dist = dist;
	}
	public int compareTo(Node other) {
	return this.dist.compareTo(other.dist);
	}
}
public class Main {

	static Scanner sc = new Scanner(System.in);
	static final int MAX = 800;
	static final  int INF = (int) 1e9;
	static ArrayList<ArrayList< Node >> graph = new ArrayList<ArrayList< Node> >();
	static int dist[]= new int[MAX];
	static int path[] = new int[MAX];
	static Boolean visited[] = new Boolean[MAX];
	static int N, M;
	static int x[] = new int[MAX], y[] = new int[MAX];

	static int distance(int i, int j)
	{
		return (x[i] - x[j])*(x[i] - x[j]) + (y[i] - y[j])*(y[i] - y[j]);
	}

	static double result()
	{
		double ans = 0;
		for (int i = 0; i<N; i++)
		{
			ans += Math.sqrt(dist[i]);
			if (ans >= INF)
			{
				return -1;
			}
		}
		return ans;
	}

	static void prims(int src)
	{
		PriorityQueue<Node > pq = new PriorityQueue<Node>();
		pq.add(new Node(src,0));
		dist[src] = 0;
		while (!pq.isEmpty())
		{
			int u = (int) pq.peek().id;
			pq.poll();
			visited[u] = true;
			for (int i = 0; i<graph.get(u).size(); i++)
			{
				int v = graph.get(u).get(i).id;
				int w = (int) graph.get(u).get(i).dist;
				if (!visited[v] && dist[v] > w)
				{
					dist[v] = w;
					pq.add(new Node(v, w));
					path[v] = u;
				}
			}
		}
	}

	static void ResetGraph()
	{
		for (int i = 0; i < N; i++)
		{
			graph.get(i).clear();
			visited[i] = false;
			dist[i] = INF;
			path[i] = -1;
		}
	}

	public static void main(String[] args)
	{
		for (int i=0;i<MAX;i++) graph.add(new ArrayList<Node>());
		N=MAX-1;
		ResetGraph();
		while (sc.hasNext())
		{
			N=sc.nextInt();
			for (int i = 0; i<N; i++)
			{
				int a, b;
				a = sc.nextInt();
				b = sc.nextInt();
				x[i] = a;
				y[i] = b;
			}
			M = sc.nextInt();
			boolean edges[][] = new boolean[MAX][MAX];
			for (int i=0;i<N;i++)
				for (int j=0;j<N;j++) edges[i][j]=false;

			for (int i = 0; i<M; i++)
			{
				int a, b;
				a= sc.nextInt();
				b= sc.nextInt();
				edges[a - 1][b - 1] = true;
				edges[b - 1][a - 1] = true;
			}
			for (int i = 0; i< N - 1; i++)
			{
				for (int j = i + 1; j<N; j++)
				{
					if (edges[i][j] == false)
					{
						int w = distance(i, j);
						graph.get(i).add(new Node(j, w));
						graph.get(j).add(new Node(i, w));
					}
					else
					{
						graph.get(i).add(new Node(j, 0));
						graph.get(j).add(new Node(i, 0));
					}
				}
			}
			prims(0);
			double r = result();
			System.out.printf("%.2f%n", r);
			ResetGraph();
		}
	}
}
 */
