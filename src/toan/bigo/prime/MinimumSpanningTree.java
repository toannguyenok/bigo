package toan.bigo.prime;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Tổng chi phí là 1717.

Bài này bạn chỉ cần cài đặt Prim cơ bản và tính chi phí cây khung nhỏ nhất là ra được kết quả.

Độ phức tạp: O(MlogN)O(MlogN) với MM là số lượng cạnh, NN là số đỉnh.
 */
public class MinimumSpanningTree {

    private static ArrayList<Long> dist = new ArrayList<>();
    private static ArrayList<Integer> path = new ArrayList<>();
    private static ArrayList<Boolean> visited = new ArrayList<>();

    private static ArrayList<ArrayList<Node>> graph = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            dist.add(Long.MAX_VALUE);
            path.add(-1);
            visited.add(false);
            graph.add(new ArrayList<>());
        }
        int m = sc.nextInt();
        for (int i = 0; i < m; i++) {
            int u = sc.nextInt() - 1;
            int v = sc.nextInt() - 1;
            long w = sc.nextLong();
            graph.get(u).add(new Node(v, w));
            graph.get(v).add(new Node(u, w));
        }
        prime(0, graph);
        long ans = 0;
        for (int i = 0; i < dist.size(); i++) {
            if(visited.get(i)) {
                ans += dist.get(i);
            }
        }
        System.out.println("" + ans);
    }

    private static void prime(int src, ArrayList<ArrayList<Node>> graph) {
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(new Node(src, 0));
        dist.set(src, 0l);

        while (!priorityQueue.isEmpty()) {
            Node currentNode = priorityQueue.remove();
            int u = currentNode.id;
            visited.set(u, true);
            for (int i = 0; i < graph.get(u).size(); i++) {
                Node newNode = graph.get(u).get(i);
                int v = newNode.id;
                long w = newNode.dist;
                if (!visited.get(v) && w < dist.get(v)) {
                    dist.set(v, w);
                    priorityQueue.add(new Node(v, w));
                    path.set(v, u);
                }
            }
        }
    }

    private static class Node implements Comparable<Node> {
        public Integer id;
        public Long dist;

        public Node(int id, long dist) {
            this.id = id;
            this.dist = dist;
        }

        @Override
        public int compareTo(Node o) {
            return this.dist.compareTo(o.dist);
        }
    }
}
/*
import java.util.*;
import java.io.*;


public class Main
{
    static PrintWriter out;
    final static int MAX =  10005;
    static int N;
    static int []path = new int[MAX];
    static int []dist = new int[MAX];
    static boolean[] visited = new boolean[MAX];
    static ArrayList<Pair<Integer,Integer> > graph[];

    public static void main(String[] args) {
        MyScanner in = new MyScanner();
        out = new PrintWriter(new BufferedOutputStream(System.out), true);
        int M,u,v,w;

        N = in.nextInt();
        M = in.nextInt();
        graph = new ArrayList[MAX];


        // initialize variable
        for(int i = 0 ; i < MAX ; i++){
            graph[i] = new ArrayList<>();
            path[i] = -1;
            dist[i] = Integer.MAX_VALUE;
            visited[i] = false;
        }
        int s = 0;
        for (int i = 0; i<M; i++)
    	{
            u = in.nextInt();
            v = in.nextInt();
            w = in.nextInt();
            s = u-1;
            graph[u-1].add(new Pair(v-1, w));
            graph[v-1].add(new Pair(u-1, w));
    	}
        Prim(s);
        PrintMST();
    }


    static void Prim(int source){
        PriorityQueue< Pair<Integer,Integer> > pq = new PriorityQueue<>(new Comparator<Pair<Integer, Integer>>(){
            public int compare(Pair<Integer,Integer> a, Pair<Integer,Integer> b){
                return a.getFirst() - b.getFirst();
            }
        });
        pq.add(new Pair(0,source));
        dist[source] = 0;

    	while(!pq.isEmpty())
    	{
            int node = pq.peek().getSecond();
            int d = pq.peek().getFirst();
            pq.remove();
            visited[node]= true;
            for(int i=0; i<graph[node].size(); ++i)
            {
                Pair<Integer,Integer> neighbor = graph[node].get(i);
                if(!visited[neighbor.first] && neighbor.second < dist[neighbor.first])
                {
                    dist[neighbor.first] = neighbor.second;
                    pq.add(new Pair(dist[neighbor.first],neighbor.first));
                    path[neighbor.first] = node;
                }
            }
    	}

    }

    static void PrintMST()
    {
        long ans = 0;
        for (int i = 0; i<N; i++)
        {
            if (path[i] == -1)
                continue;
            ans += dist[i];
            //cout << path[i] << " - " << i << ": " << dist[i]<<endl;
        }
        System.out.print(ans);
        //cout<<"Weight of MST: "<<ans<<endl;
    }

    public static class Pair<F,S>{

        public F getFirst() {
            return first;
        }

        public void setFirst(F first) {
            this.first = first;
        }

        public S getSecond() {
            return second;
        }

        public void setSecond(S second) {
            this.second = second;
        }

        public Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }
        public F first;
        public S second;
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                        st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
                return Integer.parseInt(next());
        }
        long nextLong() {
                return Long.parseLong(next());
        }
        double nextDouble() {
                return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
 */