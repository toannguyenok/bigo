package toan.bigo.finalexam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class TrainSorting {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCase = scanner.nextInt();
        for (int t = 0; t < testCase; t++) {
            int n = scanner.nextInt();
            int arr [] = new int[n];
            for (int i = 0 ; i < n ; ++i) arr[i] = scanner.nextInt();

            int LS = 0;
            int [] LI = new int[n] , LD = new int[n];
            Arrays.fill(LI , 1); Arrays.fill(LD , 1);
            for (int i= n - 1 ; i >= 0 ; --i)
                for (int j = i + 1; j < n; ++j)
                    if (arr[j] > arr[i])
                        LI[i] = Math.max(LI[i], LI[j] + 1);
            for (int i = n - 1 ; i >= 0 ; --i)
                for (int j = i + 1 ; j < n ; ++j)
                    if (arr[i] > arr[j])
                        LD[i] = Math.max(LD[i] , LD[j] + 1);

            for (int i = 0 ; i < n ; ++i)
                LS = Math.max(LS , LI[i] + LD[i] - 1);
            System.out.println(LS);
        }
    }

    public static ArrayList<Integer> lis(int[] input) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < input.length; i++) {
            result.add(1);
        }
        for (int i = 1; i < input.length; i++) {
            for (int j = 0; j < i; j++) {
                if (input[j] < input[i] && result.get(i) < result.get(j) + 1) {
                    result.set(i, result.get(j) + 1);
                }
            }
        }
        return result;
    }

    public static ArrayList<Integer> lds(int[] input) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < input.length; i++) {
            result.add(1);
        }
        for (int i = 1; i < input.length; i++) {
            for (int j = 0; j < i; j++) {
                if (input[j] > input[i] && result.get(i) < result.get(j) + 1) {
                    result.set(i, result.get(j) + 1);
                }
            }
        }
        return result;
    }

}
