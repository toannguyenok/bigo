package toan.bigo.finalexam;

import java.util.Scanner;

public class FileRecoverTesting {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (!line.equals("-1 *")) {
                String[] temp = line.split(" ");
                int k = Integer.valueOf(temp[0]);
                String s = temp[1];
                int[] prefix = new int[s.length()];
                KMPPreProcesses(s, prefix);
                int repeat = prefix[prefix.length - 1];
                int notRepeat = prefix.length - repeat;
                if (k < prefix.length) {
                    System.out.println("0");
                } else {
                    System.out.println((k - repeat) / notRepeat);
                }
            }
        }
    }

    public static void KMPPreProcesses(String p, int[] prefix) {
        prefix[0] = 0;
        int m = p.length();
        int len = 0;
        int i = 1;
        while (i < m) {
            if (p.charAt(i) == p.charAt(len)) {
                len++;
                prefix[i] = len;
                i++;
            } else {
                if (len != 0) {
                    len = prefix[len - 1];
                } else {
                    prefix[i] = 0;
                    i++;
                }
            }
        }
    }
}
