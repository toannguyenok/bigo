package toan.bigo.finalexam;

import java.util.Arrays;
import java.util.Scanner;

public class ManucianAndKOrderedChanged {

    private static final int MAX = 2000 + 5;
    private static final int MAX_K = 5 + 2;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int k = scanner.nextInt();

        int[] s1 = new int[n];
        int[] s2 = new int[m];

        int[][][] dp = new int[MAX][MAX][MAX_K];

        for (int i = 0; i < n; i++) {
            int a = scanner.nextInt();
            s1[i] = a;
        }

        for (int i = 0; i < m; i++) {
            int a = scanner.nextInt();
            s2[i] = a;
        }

        for (int i = 0; i < MAX; i++) {
            for (int j = 0; j < MAX; j++) {
                Arrays.fill(dp[i][j], -1);
            }
        }
        System.out.println(lcsWithK(dp, s1, s2, s1.length, s2.length, k));
    }

    private static int lcsWithK(int[][][] dp, int[] s1, int[] s2, int n, int m, int k) {
        if (k < 0) {
            return -Integer.MAX_VALUE;
        }
        if (n == 0 || m == 0) {
            return 0;
        }
        if (dp[n][m][k] != -1) {
            return dp[n][m][k];
        }

        int ans = Math.max(lcsWithK(dp, s1, s2, n - 1, m, k), lcsWithK(dp, s1, s2, n, m - 1, k));
        if (s1[n - 1] == s2[m - 1]) {
            ans = Math.max(ans, 1 + lcsWithK(dp, s1, s2, n - 1, m - 1, k));
        }
        ans = Math.max(ans, 1 + lcsWithK(dp, s1, s2, n - 1, m - 1, k - 1));
        dp[n][m][k] = ans;
        return ans;
    }
}
