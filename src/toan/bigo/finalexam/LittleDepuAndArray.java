package toan.bigo.finalexam;

import java.util.Scanner;

public class LittleDepuAndArray {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            int temp = scanner.nextInt();
            a[i] = temp;
        }
        int k = scanner.nextInt();
        for (int i = 0; i < k; i++) {
            int temp = scanner.nextInt();
            for (int j = 0; j < a.length; j++) {
                if (a[j] > temp) {
                    a[j] = a[j] - 1;
                }
            }
        }
        for (int j = 0; j < a.length; j++) {
            System.out.print(a[j] + " ");
        }
    }
}
