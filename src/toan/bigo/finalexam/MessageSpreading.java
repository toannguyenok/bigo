package toan.bigo.finalexam;

import java.util.Scanner;

public class MessageSpreading {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCase = scanner.nextInt();
        for (int t = 0; t < testCase; t++) {
            int n = scanner.nextInt();
            System.out.println(2 * n - 2);
        }
    }
}
