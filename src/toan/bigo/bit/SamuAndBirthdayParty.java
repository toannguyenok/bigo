package toan.bigo.bit;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Scanner;

/*
Ý tưởng: Duyệt qua tất cả các cách chọn món ăn có thể có từ KK món ăn được cho và kiểm tra xem cách chọn đó có thỏa yêu cầu mỗi người đều có một món ăn yêu thích hay không. Nếu có thì đó là một cách chọn hợp lệ. Ta chọn cách chọn nào có ít món ăn nhất trong các cách chọn hợp lệ này.

Cho rằng mỗi cách chọn sẽ được biểu diễn bằng một dãy bit 00 và 11. Trong đó, bit 11 tại vị trí xx nghĩa là ta sẽ chọn món ăn thứ xx vào menu. Như vậy, để phát sinh ra tất cả các cách chọn món ăn từ KK món ăn được cho, ta chỉ việc duyệt qua các dãy bit 000...001, 000...010, 000...011, ..., 111...111, tương ứng với các số từ 11 đến 2^K - 12
​K
​​ −1.

Các món ăn yêu thích của một người cũng được biểu diễn bằng một dãy bit 00 và 11. Do đó để kiểm tra xem cách chọn hiện tại có bao gồm ít nhất một món ăn yêu thích của người đó hay không, ta chỉ việc AND hai dãy bit với nhau. Nếu kết quả khác 00 là đúng.

Tìm số lượng bit 11 nhỏ nhất trong số các dãy bit của những cách chọn hợp lệ chính là kết quả của bài toán.

Độ phức tạp: O(T * 2^K * (N + log(K)))O(T∗2
​K
​​ ∗(N+log(K))) với TT là số lượng bộ test, NN là tổng số bạn của Samu và KK là số món ăn có sẵn cần chọn. Trong đó, với mỗi bộ test:

O(N * K)O(N∗K) là chi phí chuyển chuỗi các món ăn yêu thích của NN người bạn thành NN dãy bit.
O(2^K * (N + log(K)))O(2
​K
​​ ∗(N+log(K))) là chi phí tìm ra tất cả các dãy bit tương ứng với các cách chọn phù hợp thỏa yêu cầu và tính số bit 11 trong dãy bit hợp lệ để cho ra dãy bit có ít bit 11 nhất.
 */
public class SamuAndBirthdayParty {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int testCase = Integer.parseInt(sc.nextLine());
        ArrayList<Integer> result = new ArrayList<>();
        for (int t = 0; t < testCase; t++) {
            String[] inputNK = sc.nextLine().split(" ");
            int n = Integer.parseInt(inputNK[0]);
            int k = Integer.parseInt(inputNK[1]);

            ArrayList<Integer> arrayDish = new ArrayList<>();
            int total = 0;
            for (int i = 0; i < k; i++) {
                total = total | (1 << i);
            }
            for (int i = 0; i < n; i++) {
                String input = sc.nextLine();
                int temp = 0;
                for (int j = 0; j < input.length(); j++) {
                    char c = input.charAt(j);
                    if (c == '0') {
                        temp = temp & (~(1 << (k - 1 - j)));
                    } else {
                        temp = temp | (1 << (k - 1 - j));
                    }
                }
                arrayDish.add(temp);
            }
            int ans = Integer.MAX_VALUE;
            for (int i = 0; i < (1 << k); i++) {
                boolean flagValid = true;
                for (int j = 0; j < arrayDish.size(); j++) {
                    if ((i & arrayDish.get(j)) == 0) {
                        flagValid = false;
                        break;
                    }
                }
                if (flagValid) {
                    ans = Math.min(ans, Integer.bitCount(i));
                }
            }
            result.add(ans);
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i));
        }
    }
}

/*
import java.util.*;
import java.lang.*;
import java.io.*;

public class Solution{
    public static int Count1(int x) {
        int cnt = 0;
        while (x != 0) {
            if ((x & 1) != 0) ++cnt;
            x >>= 1;
        }
        return cnt;
    }

    public static void main(String ar[]){
        Scanner sc = new Scanner(System.in);
        int T;
        T = sc.nextInt();
        while (T-- > 0){
            int n, k;
            n = sc.nextInt();
            k = sc.nextInt();
            String[] arr = new String[n];
            sc.nextLine();
            for (int i =0; i < n; i++) {
                arr[i] = sc.nextLine();
            }

            int max_value = 1 << k;
            int ans = n;
            for (int i =1; i < max_value; i++) {
                boolean satisfied_choice = true;
                for (int v = 0; v < n; v++) {
                    boolean flag = false;
                    for (int u = 0; u < k; u++) {
                        if (arr[v].charAt(u) == '1' && ( i & (1 << u) ) != 0) {
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) {
                        satisfied_choice = false;
                        break;
                    }
                }
                if (satisfied_choice) {
                    ans = Math.min(ans, Count1(i));
                }
            }
            System.out.println(ans);
        }

        sc.close();
    }
}
 */

/*
#include<iostream>
#include<cstring>
#include<string>
#include<algorithm>
#include<vector>
using namespace std;

int Count1(int x) {
	int cnt = 0;
	while (x) {
		if (x & 1) ++cnt;
		x >>= 1;
	}
	return cnt;
}

int main() {
	int t;
	cin >> t;
	while (t--) {
		int n, k;
		cin >> n >> k;
		vector<string> arr(n);
		for (int i = 0; i < n; i++) {
			cin >> arr[i];
		}

		int max_value = 1 << k;
		int ans = n;
		for (int i = 1; i < max_value; i++) {
			bool satisfied_choice = true;
			for (int v = 0; v < n; v++) {
				bool flag = false;
				for (int u = 0; u < k; u++) {
					if (arr[v][u] == '1' && (i & (1 << u))) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					satisfied_choice = false;
					break;
				}
			}
			if (satisfied_choice) {
				ans = min(ans, Count1(i));
			}
		}

		cout << ans << endl;
	}
	return 0;
}
 */