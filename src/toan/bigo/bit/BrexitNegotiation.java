package toan.bigo.bit;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/*
Đề bài có thể được trình bày lại một cách ngắn gọn như sau, cho một đồ thị DAG với mỗi đỉnh uu có trọng số là e[u]e[u]. Yêu cầu tìm một thứ tự Topo \piπ sao cho giá trị: max(e[\pi[i]]+i)max(e[π[i]]+i) là nhỏ nhất trong các thứ tự Topo thỏa mãn.

Ta có nhận xét là các cuộc họp được thực hiện sau nên có e[u]e[u] càng nhỏ càng tốt. Do đó ta thực hiện việc sắp xếp Topo từ cuối về đầu. Tại mỗi bước chọn cuộc họp cho thời điểm thứ ii, ta chọn cuộc họp có trọng số nhỏ nhất trong các ứng cử viên có thể lựa chọn. Để làm tốt việc này, ta chỉ cần thay thế queue hoặc stack trong thuật toán sắp xếp Topo thành priority_queue để tại mỗi bước chọn ta có thể lấy ra nhanh chóng đỉnh thỏa mãn và có trọng số nhỏ nhất.

Độ phức tạp tính toán: O((N+M)*log(n))O((N+M)∗log(n)).
 */
public class BrexitNegotiation {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        ArrayList<Integer> minute = new ArrayList<>();
        ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int e = scanner.nextInt();
            minute.add(e);
            int d = scanner.nextInt();
            graph.add(new ArrayList<>());
            for (int j = 0; j < d; j++) {
                int u = scanner.nextInt();
                graph.get(i).add(u);
            }
        }
        int v = graph.size();
        int[] inDegree = new int[v];
        Queue<Integer> zeroQueue = new LinkedList<>();
        for (int i = 0; i < graph.size(); i++) {
            for (int j : graph.get(i)) {
                inDegree[j]++;
            }
        }

        for (int i = 0; i < graph.size(); i++) {
            if (inDegree[i] == 0) {
                zeroQueue.add(i);
            }
        }

        int i = n - 1;
        int ans = -1;

    }
}


/*
#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>
#define ii pair<int,int>
// e[u],u

const int MAXN = 4 * 100000 + 9;
using namespace std;
int n;
int e[MAXN], deg[MAXN];
vector<int> a[MAXN];
int main() {
    cin >> n;
    for (int u = 1; u <= n; ++u)
    {
        int k = 0;
        cin >> e[u] >> k;
        int v = 0;
        for (int i = 1; i <= k; ++i)
        {
            cin >> v;
            a[u].push_back(v);
        }
    }
    priority_queue<ii, vector<ii>, greater<ii>> q;
    for (int u = 1; u <= n; ++u)
    {
        for (auto v : a[u])
            deg[v]++;
    }
    for (int u = 1; u <= n; ++u)
        if (deg[u] == 0) q.push(ii(e[u], u));
    int i = n - 1;
    int ans = -1;
    while (!q.empty())
    {
        int u = q.top().second;
        int cost = q.top().first + i;
        i--;
        ans = max(ans, cost);
        q.pop();

        for (auto v : a[u])
        {
            deg[v]--;
            if (deg[v] == 0)
                q.push(ii(e[v], v));
        }
    }

    cout << ans;
    return 0;
}
 */

