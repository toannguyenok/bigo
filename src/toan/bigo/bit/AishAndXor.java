package toan.bigo.bit;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Scanner;

/*
Ta biết rằng cứ hai bit 11 XOR với nhau sẽ được bit 00 và bit 00 khi XOR với một bit AA bất kỳ đều trả về AA (AA \oplus⊕ 00 = AA). Do đó, ta chỉ cần đếm số lượng bit 11 trong đoạn [L, R][L,R]: nếu số lượng bit 11 là chẵn thì kết quả phép XOR là 00 và ngược lại, kết quả là 11. Số lượng bit 00 lúc này sẽ bằng RR – (L - 1)(L−1) + số lượng bit 11.

Tuy nhiên, thay vì với mỗi truy vấn ta đều phải duyệt từ vị trí LL đến RR để đếm số lượng chữ số 11 trong đoạn, ta có thể sử dụng một mảng tiền tố D để tăng tốc độ tính toán. Trong đó, D[i] là số lượng chữ số 11 tính từ vị trí bắt đầu đến vị trí ii (mảng đánh số từ 11).

Ví dụ: Mảng A = [1, 1, 0, 1, 0, 0] thì ta có mảng tiền tố D tương ứng là D = [1, 2, 2, 3, 3, 3].

Lúc này, số lượng chữ số 1 thuộc trong đoạn [L, R][L,R] chính bằng D[R] - D[L - 1]. Từ đây ta có thể tính được các kết quả của bài toán.
 */
public class AishAndXor {

    private static ArrayList<Integer> prefixSum = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int countZero = 0;
        ArrayList<Integer> input = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int bit = sc.nextInt();
            if (bit == 0) {
                countZero++;
            }
            input.add(bit);
            prefixSum.add(countZero);
        }
        int queries = sc.nextInt();
        ArrayList<Integer> resultXor = new ArrayList<>();
        ArrayList<Integer> resultCountZero = new ArrayList<>();
        for (int q = 0; q < queries; q++) {
            int l = sc.nextInt() - 1;
            int r = sc.nextInt() - 1;
            int length = (r - l) + 1;
            int totalZero = prefixSum.get(r) - prefixSum.get(l);
            if (input.get(l) == 0) {
                totalZero++;
            }
            int totalOne = length - totalZero;
            int resultXorByOne = totalOne % 2;
            if (totalZero == 0) {
                resultXor.add(resultXorByOne);
            } else {
                resultXor.add(resultXorByOne ^ 0);
            }
            resultCountZero.add(totalZero);
        }
        for (int i = 0; i < resultCountZero.size(); i++) {
            System.out.println(resultXor.get(i) + " " + resultCountZero.get(i));
        }
    }
}

/*
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.InputMismatchException;

class Solution_AC {
    public static void main(String args[] ) throws Exception {
        Scan sc = new Scan();
        Print print = new Print();
        int n = sc.nextInt();
        int a[] = new int[n + 1];
        a[0] = 0;
        for(int i = 1; i < n + 1; ++i) {
            a[i] = sc.nextInt();
        }
        for (int i = 1; i < n + 1; ++i) {
            a[i] = a[i] + a[i - 1];
        }

        int q = sc.nextInt();
        int l, r, numOnes, xorValue;
        for (int i = 0; i < q; ++i) {
            l = sc.nextInt();
            r = sc.nextInt();
            numOnes = a[r] - a[l - 1];
            xorValue = numOnes & 1;
            print.print(xorValue);
            print.print(' ');
            print.println(r - l + 1 - numOnes);
        }
        print.close();
    }
}

class Scan
{
    private byte[] buf=new byte[1024];    //Buffer of Bytes
    private int index;
    private InputStream in;
    private int total;
    public Scan()
    {
        in=System.in;
    }
    public int scan()throws IOException
    {
        if(total<0)
        throw new InputMismatchException();
        if(index>=total)
        {
            index=0;
            total=in.read(buf);
            if(total<=0)
            return -1;
        }
        return buf[index++];
    }
    public int nextInt()throws IOException
    {
        int integer=0;
        int n=scan();
        while(isWhiteSpace(n))    //Removing starting whitespaces
        n=scan();
        int neg=1;
        if(n=='-')                //If Negative Sign encounters
        {
            neg=-1;
            n=scan();
        }
        while(!isWhiteSpace(n))
        {
            if(n>='0'&&n<='9')
            {
                integer*=10;
                integer+=n-'0';
                n=scan();
            }
            else throw new InputMismatchException();
        }
        return neg*integer;
    }
    private boolean isWhiteSpace(int n)
    {
        if(n==' '||n=='\n'||n=='\r'||n=='\t'||n==-1)
        return true;
        return false;
    }
}

class Print
{
    private final BufferedWriter bw;
    public Print()
    {
        this.bw=new BufferedWriter(new OutputStreamWriter(System.out));
    }
    public void print(Object object)throws IOException
    {
        bw.append(""+object);
    }
    public void println(Object object)throws IOException
    {
        print(object);
        bw.append("\n");
    }
    public void close()throws IOException
    {
        bw.close();
    }
}
 */


/*
#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, q, l, r;
    cin >> n;
    vector<int> a(n + 1, 0), f(n + 1, 0);

    for (int i = 1; i <= n; i++) cin >> a[i];
    for (int i = 1; i <= n; i++)
        if (a[i] == 0) f[i] = f[i - 1] + 1;
        else f[i] = f[i - 1];
    cin >> q;
    while (q--) {
        cin >> l >> r;
        int nZero = f[r] - f[l - 1];
        int nOne = r - l + 1 - nZero;
        if (nOne % 2 == 0) cout << 0 << " " << nZero << endl;
        else cout << 1 << " " << nZero << endl;
    }
    return 0;
}
 */