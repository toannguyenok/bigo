package toan.bigo.bit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/*
Dễ dàng nhận thấy đây là một biến thể của sắp xếp Topo. Ta xây dựng đồ thị có hướng với cạnh (i, j)(i,j) có ý nghĩa cần hoàn thành môn học jj để có thể học ii. Sau đó ta lần lượt DFS từ kk đỉnh môn học chính kết hợp với thuật toán sắp xếp topo để xuất kết quả. Trong trường hợp đặc biệt nếu phát hiện chu trình thì xuất kết quả là -1−1.


 */
public class OnlineCourseInBSU {

    private static ArrayList<Integer> result = new ArrayList<>();
    private static boolean cycle = false;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        ArrayList<Integer> mainCourse = new ArrayList<>();
        ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < k; i++) {
            int u = scanner.nextInt() - 1;
            mainCourse.add(u);
        }
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
            int t = scanner.nextInt();
            for (int j = 0; j < t; j++) {
                int v = scanner.nextInt() - 1;
                graph.get(i).add(v);
            }
        }
        int[] check = new int[n];
        Arrays.fill(check, 0);
        cycle = false;
        for (int i = 0; i < mainCourse.size(); i++) {
            int u = mainCourse.get(i);
            dfs(u, graph, check);
        }
        if (cycle == true) {
            System.out.println(-1);
        } else {
            System.out.println(result.size());
            for (int i = 0; i < result.size(); i++) {
                System.out.print((result.get(i) + 1) + " ");
            }
        }
    }

    public static void dfs(int u,
                           ArrayList<ArrayList<Integer>> graph,
                           int[] check) {
        if (check[u] == 0) {
            check[u] = 1;
            for (int i = 0; i < graph.get(u).size(); i++) {
                int v = graph.get(u).get(i);
                dfs(v, graph, check);
            }
            check[u] = 2;
            result.add(u);
        } else if (check[u] == 1) {
            cycle = true;
        }
    }
}

/*
#include <iostream>
#include <vector>
#include <queue>
#include <set>
using namespace std;

vector<int> check;
vector< vector<int> > graph;
vector<int> res;
bool cycle;

void dfs(int u){
    if (check[u] == 0){
        check[u] = 1;
        for (int i = 0; i < graph[u].size(); i++){
            int v = graph[u][i];
            dfs(v);
        }
        check[u] = 2;
        res.push_back(u);
    } else if (check[u] == 1){
        cycle = true;
    }
}

int main(){
    vector<int> mainCourses;
    int n, k, x, nums;

    cin >> n >> k;
    for (int i = 0; i < k; i++){
        cin >> x;
        mainCourses.push_back(x - 1);
    }
    graph.resize(n);
    for (int i = 0; i < n; i++){
        cin >> nums;
        for (int j = 0; j < nums; j++){
            cin >> x;
            graph[i].push_back(x - 1);
        }
    }

    check.assign(n, 0);
    cycle = false;
    for (int i = 0; i < k; i++){
        int course = mainCourses[i];
        dfs(course);
    }

    if (cycle == true){
        cout << "-1" << endl;
        return 0;
    }
    cout << res.size() << endl;
    for (int c : res){
        cout << c + 1 << " ";
    }
    return 0;
}
 */