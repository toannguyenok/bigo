package toan.bigo.bit;

import java.util.ArrayList;
import java.util.Scanner;


/*
Ta có 3 \oplus 3 = 03⊕3=0 và 3 \oplus 3 \oplus 3 = 33⊕3⊕3=3. Từ đó ta có nhận xét sau nếu XOR nn lần aa với nhau:

Nếu nn lẻ thì kết quả là aa
Nếu nn chẳn thì kết quả là 00
Giả sử mảng arr = [1, 2, 3]arr=[1,2,3], thực hiện XOR các mảng con liên tiếp ta có biểu thức sau:

AA = 1 \oplus 2 \oplus 3 \oplus (1 \oplus 2) \oplus (2 \oplus 3) \oplus (1 \oplus 2 \oplus 3)1⊕2⊕3⊕(1⊕2)⊕(2⊕3)⊕(1⊕2⊕3)
AA = 1 \oplus 1 \oplus 1 \oplus 2 \oplus 2 \oplus 2 \oplus 2 \oplus 3 \oplus 3 \oplus 31⊕1⊕1⊕2⊕2⊕2⊕2⊕3⊕3⊕3
Ta nhận thấy 11 xuất hiện 33 lần, 22 xuất hiện 44 lần, 33 xuất hiện 33 lần. Nên ta có thể viết lại như sau:

A = 1 \oplus 0 \oplus 3A=1⊕0⊕3
A = 2A=2
Vậy với mảng arr = [a, b, c, d, . . .]arr=[a,b,c,d,...], sau khi XOR thực hiện phép XOR các mảng con liên tiếp ta có biểu thức sau: a \oplus b \oplus c \oplus d \oplus (a \oplus b) \oplus (b \oplus c) \oplus (c \oplus d)a⊕b⊕c⊕d⊕(a⊕b)⊕(b⊕c)⊕(c⊕d) …. Để tính biểu thức này ta cần đếm a, b, ca,b,c xuất hiện lần lược bao nhiêu lần?

Với arr = [a, b, c, d], n = 4arr=[a,b,c,d],n=4 ta liệt kê các mảng con liên tục như sau:

Mảng con	Xét
a	Với i = 0, arr_i = a, ai=0,arr
​i
​​ =a,a xuất hiện 44 lần = (4 - 0) * (0 + 1)(4−0)∗(0+1)
a b	Với i = 1, arr_i = b, bi=1,arr
​i
​​ =b,b xuất hiện 66 lần = (4 - 1) * (1 + 1)(4−1)∗(1+1)
a b c	...
Ta có công thức sau để tính:
Gọi ii là chỉ mục của các số trong mảng arr, narr,n là đồ dài của mảng arrarr. Vậy arr_iarr
​i
​​  xuất hiện (n - i) * (i + 1)(n−i)∗(i+1)

Độ phức tạp: O(T * N)O(T∗N) , Với TT là số lượng trường hợp và NN là độ dài của mảng arrarr.
 */
public class SansaAndXor {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int testCase = sc.nextInt();
        ArrayList<Integer> result = new ArrayList<>();
        for (int t = 0; t < testCase; t++) {
            int n = sc.nextInt();
            int ans = 0;
            for (int i = 0; i < n; i++) {
                int value = sc.nextInt();
                long times = ((long)(i + 1)) * (n - i);
                if (times % 2 == 1) {
                    ans ^= value;
                }
            }
            result.add(ans);
        }

        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i));
        }
    }
}

/*
import java.util.Scanner;

public class solution_AC {
    public static void main(String[] args) {
        int t, n;
        Scanner sc = new Scanner(System.in);
        t = sc.nextInt();
        while (t > 0) {
            t -= 1;
            n = sc.nextInt();
            int tmp, res = 0;
            for (int i = 0; i < n; i++) {
                tmp = sc.nextInt();
                if (((long)n - i) * (i + 1) % 2 == 1)
                    res ^= tmp;
            }
            System.out.println(res);
        }
    }
}
 */

/*
#include <bits/stdc++.h>

using namespace std;

int main() {
    int t, n;
    cin >> t;
    while (t--) {
        cin >> n;
        int tmp, res = 0;
        for (int i = 0; i < n; i++) {
            cin >> tmp;
            if (((long long)n - i) * (i + 1) % 2 == 1) res ^= tmp;
        }
        cout << res << endl;
    }
    return 0;
}
 */