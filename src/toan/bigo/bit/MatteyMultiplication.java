package toan.bigo.bit;

import java.util.ArrayList;
import java.util.Scanner;

/*
Theo yêu cầu của đề bài, ta cần biểu diễn:

N * MN∗M

= (N << p_1) + (N << p_2) + (N << p_3) + ... + (N << p_k)=(N<<p
​1
​​ )+(N<<p
​2
​​ )+(N<<p
​3
​​ )+...+(N<<p
​k
​​ )

= (N * 2^{p_1}) + (N * 2^{p_2}) + (N * 2^{p_3}) + ... (N * 2^{p_k})=(N∗2
​p
​1
​​
​​ )+(N∗2
​p
​2
​​
​​ )+(N∗2
​p
​3
​​
​​ )+...(N∗2
​p
​k
​​
​​ ) (phép dịch trái p_ip
​i
​​  tương đương với phép nhân 2^{p_i}2
​p
​i
​​
​​ )

= N * (2^{p_1} + 2^{p_2} + 2^{p_3}+ ... + 2^{p_k})=N∗(2
​p
​1
​​
​​ +2
​p
​2
​​
​​ +2
​p
​3
​​
​​ +...+2
​p
​k
​​
​​ )

Suy ra M = 2^{p_1} + 2^{p_2} + 2^{p_3}+ ... + 2^{p_k}M=2
​p
​1
​​
​​ +2
​p
​2
​​
​​ +2
​p
​3
​​
​​ +...+2
​p
​k
​​
​​ . Nói cách khác, p_1, p_2, p_3, ..., p_kp
​1
​​ ,p
​2
​​ ,p
​3
​​ ,...,p
​k
​​  chính là những vị trí của bit 11 trong MM.

Từ đây, ta thấy chỉ cần chuyển số MM về hệ nhị phân và lần lượt duyệt qua các bit của MM, bit ở vị trí thứ ii nào bằng 11 sẽ tương ứng với một phép dịch trái p_ip
​i
​​ .

Độ phức tạp: O(T * log(M))O(T∗log(M)) với TT là số lượng bộ test.

O(log(M))O(log(M)) là thời gian tìm vị trí của tất cả các bit 11 trong MM.
 */
public class MatteyMultiplication {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int testCase = sc.nextInt();
        ArrayList<String> result = new ArrayList<>();
        for (int t = 0; t < testCase; t++) {
            long n = sc.nextLong();
            long m = sc.nextLong();
            long val = m;
            ArrayList<Integer> pk = new ArrayList<>();
            for (int i = 63; i >= 0; i--) {
                if (val == 1) {
                    pk.add(0);
                    break;
                }
                if (val >> i != 0) {

                    val = val % (1L << i);
                    pk.add(i);
                }
            }
            String text = "";
            for (int i = 0; i < pk.size(); i++) {
                text = text + "(" + n + "<<" + pk.get(i) + ")";
                if (i != pk.size() - 1) {
                    text = text + " + ";
                }
            }
            result.add(text);
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i));
        }
    }
}

/*
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        long n, m;

        for (int tc = 0; tc < t; tc++) {
            ArrayList<Integer> pos_one = new ArrayList<Integer>();
            n = sc.nextLong(); m = sc.nextLong();
            int i = 0;

            while (m != 0) {
                if ((m & 1L) == 1) {
                    pos_one.add(i);
                }
                m >>= 1;
                i++;
            }

            for (i = pos_one.size() - 1; i >= 0; i--) {
                if (i != pos_one.size() - 1) {
                    System.out.print(" + ");
                }
                System.out.printf("(%d<<%d)", n, pos_one.get(i));
            }
            System.out.println();
        }
    }
}
 */

/*
#include <iostream>
#include <vector>
using namespace std;

int main() {
    int t;
    long long n, m;
    vector<int> pos_one;
    cin >> t;

    for (int tc = 0; tc < t; tc++) {
        cin >> n >> m;

        int i = 0;
        while (m != 0) {
            if (m & 1) {
                pos_one.push_back(i);
            }
            m >>= 1;
            i++;
        }

        for (int i = pos_one.size() - 1; i >= 0; i--) {
            if (i != pos_one.size() - 1) {
                cout << " + ";
            }
            cout << "(" << n << "<<" << pos_one[i] << ")";
        }
        cout << endl;
        pos_one.clear();
    }

    return 0;
}
 */