package toan.bigo.divideAndConquer;


/*
Hướng dẫn giải
Giải thích ví dụ:

(Test 1):
Ta có thể chọn i = 2i=2 và j = 3j=3.
Khi đó, g(2,3) = 0g(2,3)=0 và f(2,3) = (2 - 3)^2 + g(2,3)^2 = 1^2 + 0^2 = 1f(2,3)=(2−3)
​2
​​ +g(2,3)
​2
​​ =1
​2
​​ +0
​2
​​ =1.

(Test 2):
Ta có thể chọn i = 1i=1 và j = 2j=2.
Khi đó, g(1,2) = -1g(1,2)=−1 và f(1,2) = (1 - 2)^2 + g(1,2)^2 = 1^2 + (-1)^2 = 2f(1,2)=(1−2)
​2
​​ +g(1,2)
​2
​​ =1
​2
​​ +(−1)
​2
​​ =2.

Hướng dẫn giải:

Ta nhận xét g(i,j)g(i,j) là tổng các phần tử mảng aa tại các vị trí từ min(i,j) + 1min(i,j)+1 đến max(i,j)max(i,j).
Gọi d[i] = a[1] + a[2] + ... + a[i]d[i]=a[1]+a[2]+...+a[i].
Khi đó g(i,j) = d_{max(i,j)} - d_{min(i,j)}g(i,j)=d
​max(i,j)
​​ −d
​min(i,j)
​​ .
Từ đó, f(i,j) = (i - j)^2 + (d_{max(i,j)} - d_{min(i,j)})^2 = (i - j)^2 + (d[j] - d[i]) ^2f(i,j)=(i−j)
​2
​​ +(d
​max(i,j)
​​ −d
​min(i,j)
​​ )
​2
​​ =(i−j)
​2
​​ +(d[j]−d[i])
​2
​​

Ta nhận xét rằng f(i,j)f(i,j) chính là bình phương của khoảng cách giữa hai điểm (i,d_i)(i,d
​i
​​ ) và (j,d_j)(j,d
​j
​​ ) trên mặt phẳng tọa độ OxyOxy.

Với mỗi chỉ số ii từ 11 đến nn, ta thêm điểm có tọa độ (i,d_i)(i,d
​i
​​ ). Sau đó, ta cần tìm cặp điểm có khoảng cách nhỏ nhất trong tập điểm này. Đây là bài toán kinh điển, có thể giải được bằng phương pháp chia để trị trong O(n log n)O(nlogn) với nn là số lượng điểm trong tập điểm.
Độ phức tạp: O(n log n)O(nlogn) với nn là số phần tử của dãy số aa.
 */
public class TrickyFunction {
}

/*
#include <iostream>
#include <vector>
#include <algorithm>

#define INF 1e9

using namespace std;

struct Point {
    int x, y;
    Point(int x = 0, int y = 0): x(x), y(y) {}
};

bool xCompare(const Point &p1, const Point &p2) {
    return p1.x < p2.x;
}

bool yCompare(const Point &p1, const Point &p2) {
    return p1.y < p2.y;
}

long long sqr(long long num) {
    return num * num;
}

long long distance(Point &p1, Point &p2) {
    int x = p1.x - p2.x;
    int y = p1.y - p2.y;
    return sqr(x) + sqr(y);
}

long long bruteForce(vector<Point> &point_set, int left, int right) {
    long long dist_min = INF;
    for(int i = left; i < right; ++i) {
        for(int j = i + 1; j < right; ++j) {
            dist_min = min(dist_min, distance(point_set[i], point_set[j]));
        }
    }
    return dist_min;
}

long long stripClosest(vector<Point> &point_set, int left, int right, int mid, long long dist_min) {
    Point point_mid = point_set[mid];
    vector<Point> splitted_points;
    for(int i = left; i < right; ++i) {
        if (sqr(point_set[i].x - point_mid.x) <= dist_min) {
            splitted_points.push_back(point_set[i]);
        }
    }

    sort(splitted_points.begin(), splitted_points.end(), yCompare);

    long long smallest = INF;
    int l = splitted_points.size();
    for(int i = 0; i < l; ++i) {
        for(int j = i + 1; j < l && sqr(splitted_points[i].y - splitted_points[j].y) < dist_min; ++j) {
            long long d = distance(splitted_points[i], splitted_points[j]);
            smallest = min(smallest, d);
        }
    }
    return smallest;
}

long long closestUtil(vector<Point> &point_set, int left, int right) {
    if (right - left <= 3)
        return bruteForce(point_set, left, right);

    int mid = (left + right) / 2;
    long long dist_left = closestUtil(point_set, left, mid);
    long long dist_right = closestUtil(point_set, mid + 1, right);
    long long dist_min = min(dist_left, dist_right);

    return min(dist_min, stripClosest(point_set, left, right, mid, dist_min));
}

int main() {
    int n;
    cin >> n;

    vector<int> a(n);
    for(int i = 0; i < n; ++i) {
        cin >> a[i];
    }

    vector<int> pref(1, 0);
    for(int i = 0; i < n; ++i) {
        pref.push_back(pref[i] + a[i]);
    }

    vector<Point> point_set;
    for(int i = 0; i < n; ++i) {
        point_set.push_back(Point(i, pref[i + 1]));
    }

    sort(point_set.begin(), point_set.end(), xCompare);
    long long ans = closestUtil(point_set, 0, n);
    cout << ans << endl;

    return 0;
}
 */

/*
import java.io.PrintWriter;
import java.util.*;

class Point {
    Integer x, y;

    public Point(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }
}

public class Main {
    private static final long INF = (long) 1e9;

    private static long sqr(long num) {
        return num * num;
    }

    private static long distance(Point p1, Point p2) {
        int x = p1.x - p2.x;
        int y = p1.y - p2.y;
        return sqr(x) + sqr(y);
    }

    private static long bruteForce(Point[] point_set, int left, int right) {
        long dist_min = INF;
        for (int i = left; i < right; ++i) {
            for (int j = i + 1; j < right; ++j) {
                dist_min = Math.min(dist_min, distance(point_set[i], point_set[j]));
            }
        }
        return dist_min;
    }

    private static long stripClosest(Point[] point_set, int left, int right, int mid, long dist_min) {
        Point point_mid = point_set[mid];
        List<Point> splitted_points = new ArrayList<Point>();
        for (int i = left; i < right; ++i) {
            if (sqr(point_set[i].x - point_mid.x) <= dist_min) {
                splitted_points.add(point_set[i]);
            }
        }

        Collections.sort(splitted_points, Comparator.comparing(p -> p.y));

        long smallest = INF;
        int l = splitted_points.size();
        for (int i = 0; i < l; ++i) {
            for (int j = i + 1; j < l && sqr(splitted_points.get(j).y - splitted_points.get(i).y) < dist_min; ++j) {
                long d = distance(splitted_points.get(i), splitted_points.get(j));
                smallest = Math.min(smallest, d);
            }
        }
        return smallest;
    }

    private static long closestUtil(Point[] point_set, int left, int right) {
        if (right - left <= 3)
            return bruteForce(point_set, left, right);

        int mid = (left + right) / 2;
        long dist_left = closestUtil(point_set, left, mid);
        long dist_right = closestUtil(point_set, mid + 1, right);
        long dist_min = Math.min(dist_left, dist_right);

        return Math.min(dist_min, stripClosest(point_set, left, right, mid, dist_min));
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int a[] = new int[n];
        for (int i = 0; i < n; ++i) {
            a[i] = in.nextInt();
        }

        int pref[] = new int[n + 1];
        pref[0] = 0;
        for (int i = 0; i < n; ++i) {
            pref[i + 1] = pref[i] + a[i];
        }

        Point[] point_set = new Point[n];
        for (int i = 0; i < n; ++i) {
            point_set[i] = new Point(i, pref[i + 1]);
        }

        Arrays.sort(point_set, Comparator.comparing(p -> p.x));
        long ans = closestUtil(point_set, 0, n);
        out.println(ans);

        out.close();
    }
}
 */