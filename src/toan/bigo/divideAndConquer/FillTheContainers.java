package toan.bigo.divideAndConquer;

import java.util.Scanner;

/*
Giải thích ví dụ:

Ở bộ dữ liệu đầu tiên, có 5 bình sữa cần đổ vào 3 thùng chứa. Bạn có thể phân bố như sau, cho thùng chứa đầu tiên, cho thùng chứa thứ 2, cho thùng chứa thứ 3. Như vậy sức chứa lớn ở đây là 6. Đây là cách làm tối ưu nhất. Với cách làm này, bạn sẽ không thể tìm được một cách chia khác sao cho sức chứa của thùng chứa lớn nhất, nhỏ hơn 6 mà sử dụng 3 thùng chứa để đổ tất cả sữa.

Đối với bộ dữ liệu thứ 2, cách tối ưu nhất là: vào thùng chứa đầu tiên, vào thùng chứa thứ 2. Như vậy giá trị nhỏ nhất có thể là 82. Bạn cũng có thể làm như sau: vào thùng chứa đầu tiên và vào thùng chứa thứ 2. Nhưng cách làm này không tối ưu vì trong trường hợp này sức chứa thùng chứa lớn nhất là 87 > 82.

Hướng dẫn giải:

Bài toán được phát biểu ngắn gọn lại như sau: Cho một dãy số gồm nn phần tử, cần chia nn số thành mm đoạn phân biệt. Tìm cách chia sao cho tổng lớn nhất là nhỏ nhất.

Ý tưởng của bài này là chúng ta sẽ chặt nhị phân trên kết quả. Với kết quả có được, ta sẽ kiểm tra xem với sức chứa đó thì có thể có cách chia sao cho sức chứa đó là lớn nhất không. Nếu có thể chia được, chúng ta tối ưu kết quả bằng cách giới hạn chặn trên, , ngược lại ta sẽ tang chặn dưới lên để tìm được kết quả, .

Giả sử, chúng ta có được giá trị sức chứa x, và muốn kiểm tra xem x có thể là kết quả hay không. Thuật toán kiểm tra như sau, duyệt toàn bộ mảng và cộng dồn các giá trị cho đến khi giá trị lớn hơn x, ta tiến hành tăng biến đếm thùng chứa lên, và gán lại giá trị tổng về 0. Kết thúc vòng lặp, nếu số lượng thùng chứa nhỏ hơn hoặc bằng m thì x là kết quả được chấp nhận.

Ở đây, tư tưởng chia để trị (Divide and Conquer) nằm ở việc chúng ta sẽ chia nhỏ vùng kết quả có thể để tìm ra kết quả phù hợp thông qua thuật toán tìm kiếm nhị phân (Binarry Search).

Độ phức tạp: O(NlogSO(NlogS) với NN là số lượng bình sữa và SS là tổng lượng sữa trong các bình.


 */
public class FillTheContainers {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String line1 = scanner.nextLine();
            String line2 = scanner.nextLine();

            String[] temp1 = line1.split(" ");
            int n = Integer.valueOf(temp1[0]);
            int m = Integer.valueOf(temp1[1]);
            String[] temp2 = line2.split(" ");
            int[] input = new int[n];

            long total = 0;
            for (int i = 0; i < temp2.length; i++) {
                int value = Integer.valueOf(temp2[i]);
                input[i] = value;
                total += value;
            }
            System.out.println(binaryCheck(input, m, total));
        }
    }

    public static long binaryCheck(int[] input, int m, long total) {
        long result = 0;
        long left = 0;
        long right = total;
        while (left <= right) {
            long mid = (left + right) / 2;
            if (checkValid(input, m, mid)) {
                result = mid;
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return result;
    }

    public static boolean checkValid(int[] input, int m, long total) {
        long currentValue = 0;
        int container = 0;
        for (int i = 0; i < input.length; i++) {
            if (input[i] > total) {
                return false;
            }
            if (input[i] + currentValue > total) {
                currentValue = 0;
            }
            if (currentValue == 0) {
                container++;
            }
            if (container > m) {
                return false;
            }
            currentValue += input[i];
        }
        return true;
    }
}
/*
#include <iostream>
#include <vector>

using namespace std;

bool canFill(long long capacity, vector<int> &a, int m){
    long long container = 0;
    int numberOfContainer = 0;
    for (int i = 0; i < a.size(); i++){
        if (a[i] > capacity) return false;
        if (container + a[i] > capacity)
            container = 0;
        if (container == 0) numberOfContainer++;
        if (numberOfContainer > m) return false;

        container += a[i];
    }
    return true;
}

long long binarySearch(vector<int> &a, long long total, int m){
    long long low = 0;
    long long high = total;
    long long res = -1;

    while (low <= high){
        long long mid = (low + high) / 2;
        if (canFill(mid, a, m)){
            res = mid;
            high = mid - 1;
        } else low = mid + 1;
    }

    return res;
}

int main(){
    vector<int> c;
    int n, m;
    long long total;
    while (cin >> n >> m){
        c.resize(n);
        total = 0;
        for (int i = 0; i < n; i++){
            cin >> c[i];
            total += c[i];
        }
        cout << binarySearch(c, total, m) << endl;
    }
    return 0;
}
 */

/*
import java.io.*;
import java.util.*;

public class Main {
  public static boolean canFill(long capacity, ArrayList<Integer> a, int m) {
    long container = 0;
    int numberOfContainer = 0;
    for (int i = 0; i < a.size(); i++) {
      if (a.get(i)> capacity) return false;
      if (container + a.get(i) > capacity)
        container = 0;
      if (container == 0) numberOfContainer++;
      if (numberOfContainer > m) return false;
      container += a.get(i);
    }
    return true;
  }
  public static long binarySearch(ArrayList<Integer> a, long total, int m) {
    long low = 0, high = total, res = -1;

    while (low <= high) {
      long mid = (low + high) / 2;
      if (canFill(mid, a, m)) {
        res = mid;
        high = mid - 1;
      } else {
        low = mid + 1;
      }
    }
    return res;
  }
  public static void main (String[] args) {
    Scanner sc = new Scanner(System.in);
    int n, m;
    ArrayList<Integer> c = new ArrayList<Integer>();

    long total;
    while (sc.hasNextInt()) {
      n = sc.nextInt();
      m = sc.nextInt();
      c.clear();
      total = 0;
      for (int i = 0; i < n; i++) {
        c.add(sc.nextInt());
        total += c.get(i);
      }
      System.out.println(binarySearch(c, total, m));
    }

  }
}
 */
