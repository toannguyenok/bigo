package toan.bigo.divideAndConquer;


import java.util.Scanner;

/*
Hướng dẫn giải
Giải thích ví dụ:

Hai bộ dữ liệu đầu sử dụng chung một cây như hình sau:



Ta dễ dàng thấy rằng cây này có tổng cộng 44 đường đi từ gốc tới lá với tổng như sau:

5 + 4 + 11 + 7 = 275+4+11+7=27
5 + 4 + 11 + 2 = 225+4+11+2=22
5 + 8 + 13 = 265+8+13=26
5 + 8 + 4 + 1 = 185+8+4+1=18
Như vậy, bộ dữ liệu đầu tiên ta sẽ ghi ra yes vì đường đi từ gốc đến lá thứ 22 cho tổng bằng 2222 còn bộ dữ liệu thứ hai ta sẽ ghi ra no vì không tồn tại đường đi từ gốc đến lá nào cho tổng bằng 2020 cả.

Bộ dữ liệu thứ 44 cho ta một cây rỗng. Vì một cây rỗng sẽ không có tổng nên ta sẽ ghi ra no.

Hướng dẫn giải:

Bài này có thể giải bằng phương pháp backtracking. Ta sẽ duyệt toàn bộ đường đi có thể và kiểm tra xem có tồn tại đường đi nào có tổng bằng tổng mong muốn hay không.

Đặc biệt lưu ý phần xử lý dữ liệu đầu vào vì phép biểu diễn cây sẽ bị chia ra thành nhiều dòng và giữa các ký tự trong chuỗi biểu diễn có thể có một hoặc nhiều ký tự trắng. Để xử lý điều này thì ta có thể đọc chuỗi biểu diễn liên tục cho đến khi số lượng ngoặc mở bằng số lượng ngoặc đóng thì ta ngưng.

Độ phức tạp: O(n)O(n) với nn là số lượng nút trên cây.
 */
public class TreeSumming {


    public static StringBuilder exp;

    public static int get_integer(int it) {
        String temp = "";
        while (Character.isDigit(exp.charAt(it)) || exp.charAt(it) == '-') {
            temp += exp.charAt(it);
            it++;
        }
        return Integer.parseInt(temp);
    }

    public static int left_child(int it) {
        while (Character.isDigit(exp.charAt(it)) || exp.charAt(it) == '-') {
            it++;
        }
        if (!(Character.isDigit(exp.charAt(it + 1))) || exp.charAt(it + 1) == '-')
            return -1;
        return it + 1;
    }

    public static int right_child(int it) {
        while (Character.isDigit(exp.charAt(it)) || exp.charAt(it) == '-') {
            it++;
        }
        int brackets = 1;
        it++;
        while (brackets > 0) {
            if (exp.charAt(it) == '(')
                brackets++;
            if (exp.charAt(it) == ')')
                brackets--;
            it++;
        }
        if (!(Character.isDigit(exp.charAt(it + 1))) || exp.charAt(it + 1) == '-')
            return -1;
        return it + 1;
    }

    public static boolean check(int it, int target, int current) {
        if (it >= exp.length())
            return false;

        if (Character.isDigit(exp.charAt(it)) || exp.charAt(it) == '-') {
            current += get_integer(it);
        } else {
            return false;
        }

        int left = left_child(it);
        int right = right_child(it);
        if (left == -1 && right == -1) {
            return target == current;
        }
        boolean left_path = false, right_path = false;
        if (left != -1) {
            left_path = check(left, target, current);
        }
        if (right != -1) {
            right_path = check(right, target, current);
        }
        return left_path || right_path;
    }

    public static void main(String[] args) {
        int sum;
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            sum = scanner.nextInt();
            exp = new StringBuilder();
            int brackets = 0;
            while (scanner.hasNext()) {
                String temp = scanner.next();
                temp.trim();
                exp.append(temp);
                for (int i = 0; i < temp.length(); i++) {
                    if (temp.charAt(i) == '(')
                        brackets++;
                    if (temp.charAt(i) == ')')
                        brackets--;
                }
                if (brackets == 0)
                    break;
            }
            //System.out.println(exp + " " + Integer.toString(sum));
            if (check(1, sum, 0))
                System.out.println("yes");
            else
                System.out.println("no");
        }
    }
}

/*
#include <iostream>
#include <string>
#include <cctype>
using namespace std;

string exp;

int get_integer(int it) {
    string temp = "";
    while (isdigit(exp[it]) || exp[it] == '-') {
        temp += exp[it];
        it++;
    }
    return stoi(temp);
}

int left_child(int it) {
    while (isdigit(exp[it]) || exp[it] == '-') {
        it++;
    }

    if (!(isdigit(exp[it + 1]) || exp[it + 1] == '-')) {
        return -1;
    }

    return it + 1;
}

int right_child(int it) {
    while (isdigit(exp[it]) || exp[it] == '-') {
        it++;
    }

    int brackets = 1;
    it++;
    while (brackets > 0) {
        if (exp[it] == '(') {
            brackets++;
        }
        else if (exp[it] == ')') {
            brackets--;
        }
        it++;
    }

    if (!(isdigit(exp[it + 1]) || exp[it + 1] == '-')) {
        return -1;
    }

    return it + 1;
}

bool check(int it, int target, int current) {
    if (isdigit(exp[it]) || exp[it] == '-') {
        current += get_integer(it);
    }
    else {
        return false;
    }

    int left = left_child(it);
    int right = right_child(it);

    if (left == -1 && right == -1) {
        return target == current;
    }

    bool left_path = false, right_path = false;

    if (left != -1) {
        left_path = check(left, target, current);
    }
    if (right != -1) {
        right_path = check(right, target, current);
    }

    return left_path || right_path;
}

int main() {
    int sum, next_sum;
    while (cin >> sum) {
        exp = "";
        string temp;
        int brackets = 0;
        while (cin >> temp) {
            for (int i = 0; i < temp.length(); i++) {
                if (temp[i] == '(') {
                    brackets++;
                }
                else if (temp[i] == ')') {
                    brackets--;
                }
            }

            exp += temp;
            if (brackets == 0) {
                break;
            }
        }

        cerr << sum << " " << exp << endl;

        if (check(1, sum, 0)) {
            cout << "yes\n";
        }
        else {
            cout << "no\n";
        }
        sum = next_sum;
    }
    return 0;
}
 */

/*
import java.util.Scanner;

public class sol {

    public static StringBuilder exp;

    public static int get_integer(int it){
        String temp = "";
        while (Character.isDigit(exp.charAt(it)) || exp.charAt(it) == '-')
        {
            temp += exp.charAt(it);
            it ++;
        }
        return Integer.parseInt(temp);
    }

    public static int left_child(int it){
        while (Character.isDigit(exp.charAt(it)) || exp.charAt(it) == '-'){
            it++;
        }
        if (!(Character.isDigit(exp.charAt(it + 1))) || exp.charAt(it + 1) == '-')
            return -1;
        return it + 1;
    }

    public static int right_child(int it){
        while (Character.isDigit(exp.charAt(it)) || exp.charAt(it) == '-'){
            it++;
        }
        int brackets = 1;
        it ++;
        while (brackets > 0){
            if (exp.charAt(it) == '(')
                brackets ++;
            if (exp.charAt(it) == ')')
                brackets --;
            it ++;
        }
        if (!(Character.isDigit(exp.charAt(it + 1))) || exp.charAt(it + 1) == '-')
            return -1;
        return it + 1;
    }

    public static boolean check(int it, int target, int current){
        if (it >= exp.length())
            return false;

        if (Character.isDigit(exp.charAt(it)) || exp.charAt(it) == '-'){
            current += get_integer(it);
        }
        else {
            return false;
        }

        int left = left_child(it);
        int right = right_child(it);
        if (left == -1 && right == -1)
        {
            return target == current;
        }
        boolean left_path = false, right_path = false;
        if (left != -1)
        {
            left_path = check(left, target, current);
        }
        if (right != -1)
        {
            right_path = check(right, target, current);
        }
        return left_path || right_path;
    }
    public static void main(String[] args){
        int sum;
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext())
        {
            sum = scanner.nextInt();
            exp = new StringBuilder();
            int brackets = 0;
            while (scanner.hasNext())
            {
                String temp = scanner.next();
                temp.trim();
                exp.append(temp);
                for (int i = 0; i < temp.length(); i++)
                {
                    if (temp.charAt(i) == '(')
                        brackets ++;
                    if (temp.charAt(i) == ')')
                        brackets --;
                }
                if (brackets == 0)
                    break;
            }
            //System.out.println(exp + " " + Integer.toString(sum));
            if (check(1, sum, 0))
                System.out.println("yes");
            else
                System.out.println("no");
        }
    }
}
 */