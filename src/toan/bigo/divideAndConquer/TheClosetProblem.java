package toan.bigo.divideAndConquer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/*
Hướng dẫn giải
Bước 1: Sắp xếp tọa độ điểm theo thứ tự tăng dần của hoành độ OxOx.
Bước 2: Chia mảng ra làm 2 phần (bên trái và bên phải). Tìm khoảng cách ngắn nhất mỗi bên. So sánh bên nào ngắn hơn chọn khoảng cách đó. Gọi là dist\_min.dist_min.
Bước 3: Trường hợp khoảng cách ngắn nhất nằm 2 điểm thuộc nửa này và nửa kia. Dùng dist\_mindist_min để chọn các điểm giữa 2 phần.
Bước 4: Tránh việc so sánh nhiều lần các điểm trong tập vừa tìm được, sắp xếp các điểm theo tung độ (Oy)(Oy). Tạo thành khung so sánh hình chữ nhật. Tính khoảng cách các điểm và cập nhật dist\_mindist_min.
Bước 5: Nếu dist\_min > 10000dist_min>10000 thì xuất INFINITY, ngược lại xuất dist\_mindist_min.
Độ phức tạp: O(N* logN * T)O(N∗logN∗T) với TT là số test set, NN là số lượng điểm có trong một test set.
 */
public class TheClosetProblem {

    private static class Point {
        Double x;
        Double y;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    private static double distance(Point p1, Point p2) {
        double x = p1.x - p2.x;
        double y = p1.y - p2.y;
        return Math.sqrt(x * x + y * y);
    }

    private static double bruteForce(ArrayList<Point> points, int left, int right) {
        double minDist = Double.MAX_VALUE;
        for (int i = left; i < right; i++) {
            for (int j = i + 1; j < right; j++) {
                minDist = Math.min(minDist, distance(points.get(i), points.get(j)));
            }
        }
        return minDist;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        while (flag) {
            int n = scanner.nextInt();
            if (n == 0) {
                flag = false;
            } else {
                ArrayList<Point> points = new ArrayList<>();
                for (int i = 0; i < n; i++) {
                    double x = scanner.nextDouble();
                    double y = scanner.nextDouble();
                    points.add(new Point(x, y));
                }

                Collections.sort(points, new Comparator<Point>() {
                    @Override
                    public int compare(Point o1, Point o2) {
                        return Double.compare(o1.x, o2.x);
                    }
                });
                double result = minimalDistance(points, 0, points.size());
                if (result >= 10000) {
                    System.out.println("INFINITY");
                } else {
                    DecimalFormat df = new DecimalFormat("0.0000");
                    System.out.println(df.format(result));
                }
            }
        }
    }

    private static double stripCloset(ArrayList<Point> points, int left, int right, int mid, double minDist) {
        Point pointMid = points.get(mid);
        ArrayList<Point> splittedPoint = new ArrayList<>();
        for (int i = left; i < right; i++) {
            if (Math.abs(points.get(i).x - pointMid.x) <= minDist) {
                splittedPoint.add(points.get(i));
            }
        }

        Collections.sort(splittedPoint, new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                return Double.compare(o1.y, o2.y);
            }
        });

        double smallest = Double.MAX_VALUE;
        int size = splittedPoint.size();
        for (int i = 0; i < size; i++) {
            for (int j = i + 1; j < size && (splittedPoint.get(j).y - splittedPoint.get(i).y < minDist); j++) {
                double d = distance(splittedPoint.get(i), splittedPoint.get(j));
                smallest = Math.min(d, smallest);
            }
        }
        return smallest;
    }

    private static double minimalDistance(ArrayList<Point> points, int left, int right) {
        if (right - left <= 3) {
            return bruteForce(points, left, right);
        }
        int mid = (left + right) / 2;

        double distLeft = minimalDistance(points, left, mid);
        double distRight = minimalDistance(points, mid + 1, right);
        double distMin = Math.min(distLeft, distRight);
        return Math.min(distMin, stripCloset(points, left, right, mid, distMin));
    }
}

/*
import java.util.*;

public class Main {
    static final double INF = 1E9;

    static class Point {
        double x = 0, y = 0;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    static double distance(Point p1, Point p2) {
        double x = p1.x - p2.x;
        double y = p1.y - p2.y;
        return Math.sqrt(x * x + y * y);
    }

    static double bruteForce(ArrayList<Point> point_set, int left, int right) {
        double min_dist = INF;
        for (int i = left; i < right; ++i) {
            for (int j = i + 1; j < right; ++j) {
                min_dist = Math.min(min_dist, distance(point_set.get(i), point_set.get(j)));
            }
        }
        return min_dist;
    }

    static double stripClosest(ArrayList<Point> point_set, int left, int right, int mid, double dist_min) {
        Point point_mid = point_set.get(mid);
        ArrayList<Point> splitted_points = new ArrayList<>();
        for (int i = left; i < right; i++) {
            if (Math.abs(point_set.get(i).x - point_mid.x) <= dist_min) {
                splitted_points.add(point_set.get(i));
            }
        }

        Collections.sort(splitted_points, (o1, o2) -> Double.compare(o1.y, o2.y));

        double smallest = INF;
        int l = splitted_points.size();
        for (int i = 0; i < l; i++) {
            for (int j = i + 1; j < l && (splitted_points.get(j).y - splitted_points.get(i).y) < dist_min; j++) {
                double d = distance(splitted_points.get(i), splitted_points.get(j));
                smallest = Math.min(smallest, d);
            }
        }
        return smallest;
    }

    static double closestUtil(ArrayList<Point> point_set, int left, int right) {
        if (right - left <= 3)
            return bruteForce(point_set, left, right);

        int mid = (right + left) / 2;
        double dist_left = closestUtil(point_set, left, mid);
        double dist_right = closestUtil(point_set, mid + 1, right);
        double dist_min = Math.min(dist_left, dist_right);

        return Math.min(dist_min, stripClosest(point_set, left, right, mid, dist_min));
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        double x, y;
        while (true) {
            n = sc.nextInt();
            if (n == 0)
                break;
            ArrayList<Point> point_set = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                x = sc.nextDouble();
                y = sc.nextDouble();
                point_set.add(new Point(x, y));
            }

            Collections.sort(point_set, ((o1, o2) -> Double.compare(o1.x, o2.x)));
            double ans = closestUtil(point_set, 0, n);
            if (ans < 10000)
                System.out.format("%.4f\n", ans);
            else
                System.out.println("INFINITY");
        }

        sc.close();
    }
}
 */

/*
#include <stdio.h>
#include <float.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
using namespace std;
#define INF 1e9

struct Point {
    double x, y;
    Point(double x = 0, double y = 0) : x(x), y(y) {}
};

bool xCompare(const Point &p1, const Point &p2) {
    return p1.x < p2.x;
}

bool yCompare(const Point &p1, const Point &p2) {
    return p1.y < p2.y;
}

double distance(Point &p1, Point &p2) {
    double x = p1.x - p2.x;
    double y = p1.y - p2.y;
    return sqrt(x * x + y * y);
}

double bruteForce(vector<Point> &point_set, int left, int right) {
    double min_dist = INF;
    for (int i = left; i < right; ++i) {
        for (int j = i + 1; j < right; ++j) {
            min_dist = min(min_dist, distance(point_set[i], point_set[j]));
        }
    }
    return min_dist;
}

double stripClosest(vector<Point> &point_set, int left, int right, int mid, double dist_min) {
    Point point_mid = point_set[mid];
    vector<Point> splitted_points;
    for (int i = left; i < right; i++) {
        if (abs(point_set[i].x - point_mid.x) <= dist_min) {
            splitted_points.push_back(point_set[i]);
        }
    }

    sort(splitted_points.begin(), splitted_points.end(), yCompare);

    double smallest = INF;
    int l = splitted_points.size();
    for (int i = 0; i < l; i++) {
        for (int j = i + 1; j < l && (splitted_points[j].y - splitted_points[i].y) < dist_min; j++) {
            double d = distance(splitted_points[i], splitted_points[j]);
            smallest = min(smallest, d);
        }
    }
    return smallest;
}

double closestUtil(vector<Point> &point_set, int left, int right) {
    if (right - left <= 3)
        return bruteForce(point_set, left, right);

    int mid = (right + left) / 2;
    double dist_left = closestUtil(point_set, left, mid);
    double dist_right = closestUtil(point_set, mid + 1, right);
    double dist_min = min(dist_left, dist_right);

    return min(dist_min, stripClosest(point_set, left, right, mid, dist_min));
}

int main() {
    int n;
    double x, y;
    while (true)
    {
        cin >> n;
        if (n == 0)
	break;
        vector<Point> point_set;
        for (int i = 0; i < n; i++) {
            cin >> x >> y;
            point_set.push_back(Point(x, y));
        }

        sort(point_set.begin(), point_set.end(), xCompare);
        double ans = closestUtil(point_set, 0, n);
        if (ans < 10000)
            cout << fixed << setprecision(4) << ans << endl;
        else
            cout << "INFINITY" << endl;
    }

    return 0;
}
 */