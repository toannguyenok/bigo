package toan.bigo.backtracking;

import java.util.ArrayList;
import java.util.Scanner;

public class TheHammingDistanceProblem {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCases = scanner.nextInt();
        for (int t = 0; t < testCases; t++) {
            int n = scanner.nextInt();
            int hammer = scanner.nextInt();
            hammerFind("",n,hammer);
        }
    }

    public static void hammerFind(String s, int n, int hammer) {
        if (s.length() == n) {
            if (hammer == 0) {
                System.out.println(s);
            }
            return;

        }
        hammerFind(s + "0", n, hammer);
        hammerFind(s + "1", n, hammer - 1);
    }
}
