package toan.bigo.backtracking;

import java.util.Scanner;

/*
Hướng dẫn giải
Tóm tắt đề:

Dreamoon đang đứng ở vị trí 00 trên một dòng các chữ số. Drazil đang gửi một chuỗi các lệnh thông qua wifi đến điện thoại của Dreamoon, và sau đó Dreamoon theo các chỉ thị nhận được để di chuyển. Mỗi một lệnh có 2 kiểu sau đây:

Đi theo chiều dương của dãy số 1 đơn vị, ký hiệu là ++.
Đi theo chiều âm của dãy số 1 đơn vị, ký hiệu -−.
Do điều kiện Wifi tương đối yếu nên điện thoại của Dreamoon thông báo rằng có một số lệnh không thể nhận diện chính xác và Dreamoon biết được rằng một số lệnh thậm chí còn có thể được nhận dạng sai. Dreamoon quyết định theo sau mỗi chỉ thị mà bạn ấy nhận được, và tung một đồng xu để quyết định xem hướng di chuyển của những chỉ thị mà điện thoại của cậu ấy không nhận diện chính xác (có nghĩa là xác suất mà Dreamoon đi về phía dương một đơn vị và đi về phía âm một đơn vị là như nhau và bằng 0.50.5).

Bạn được cho một danh sách các lệnh được gửi bởi Drazil và một danh sách cách lệnh mà Dreamoon nhận được. Bạn phải xác định xem xác suất để Dreamoon kết thúc chuỗi lệnh nhận được tại vị trí đúng với vị trí mong muốn của chuỗi lệnh được Drazil gửi.

Giải thích ví dụ:

Ở ví dụ đầu tiên, cả 2 chuỗi s_1s
​1
​​  và s_2s
​2
​​  đều dẫn Dreamoon đến cùng một vị trí là +1+1.

Ở ví dụ thứ hai, chuỗi s_1s
​1
​​  sẽ dẫn Dreamoon kết thúc tại vị trí 00. Trong khi đó có tất cả 4 trường hợp có thể có của chuỗi s_2s
​2
​​  (bằng cách thay dấu ‘?’ bởi dấu ‘+” hoặc ‘-‘) : +-++, +-+-,+--+,+--- và sẽ kết thúc tại các vị trí +2, 0, 0, -2+2,0,0,−2. Như vậy có tất cả 22 chuỗi trong 44 chuỗi nêu trên để Dreamoon kết thúc tại vị trí 00. Do đó xác suất sẽ là 0.50.5.

Hướng dẫn giải:

Trường hợp chuỗi s_2s
​2
​​  không chứa dấu ‘?’. Ta sẽ tìm vị trí mà Dreamoon sẽ đến được nếu theo chuỗi lệnh s_1s
​1
​​  và s_2s
​2
​​ , nếu 2 vị trí này bằng nhau, ta xuất ra 1.01.0, ngược lại xuất ra 0.00.0.

Trường hợp chuỗi s_2s
​2
​​  chứa ít nhất một dấu ‘?’: Ta sẽ lần lượt thử thay các ký tự ‘+’ và ‘-‘ tại mỗi vị trí có dấu ‘?’, sau đó tính xem vị trí mà Dreamoon sẽ đến ứng với mỗi trường hợp, đếm xem số trường hợp mà Dreamoon có thể đến đúng ví trị của chuỗi s_1s
​1
​​ , sau đó chi cho tổng các trường hợp, ta sẽ thu được kết quả.

Độ phức tạp: O(2^k \times n + n)O(2
​k
​​ ×n+n) với kk là số lượng dấu ‘?’ có trong chuỗi s_2s
​2
​​ , nn là độ dài lớn nhất của hai chuỗi s_1s
​1
​​  và s_2s
​2
​​ .
 */
public class DreamoonAndWifi {

    private static int expectPosition = 0;
    private static int caseHappen = 0;
    private static int validCount = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s1 = scanner.nextLine();
        String s2 = scanner.nextLine();
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) == '+') {
                expectPosition++;
            } else {
                expectPosition--;
            }
        }
        backTracking(s2, 0, 0, expectPosition);
        System.out.printf("%.10f\n", (double)validCount / caseHappen);
    }

    public static void backTracking(String s, int current, int currentPosition, int expectPosition) {
        if (current == s.length()) {
            if (currentPosition == expectPosition) {
                validCount++;
            }
            caseHappen++;
            return;
        }
        if (s.charAt(current) != '-') {
            backTracking(s, current + 1, currentPosition + 1, expectPosition);
        }
        if (s.charAt(current) != '+') {
            backTracking(s, current + 1, currentPosition - 1, expectPosition);
        }
    }
}
/*
#include <bits/stdc++.h>
using namespace std;

string s1, s2;
int expected = 0;
int cases = 0, valid = 0;

void recursive(int z, int val) {
	if (z == s2.size()) {
		if (val == expected) valid++;
		cases++; return;
	}
	if (s2[z] != '-') recursive(z+1, val+1);
	if (s2[z] != '+') recursive(z+1, val-1);
}

void Input() {
	cin >> s1 >> s2;
	for (int i=0; i<s1.size(); i++) {
		if (s1[i] == '+') expected++;
		else expected--;
	}
}

void Solve() {
	recursive(0, 0);
	cout << fixed << setprecision(10) << (double)valid / cases;
}

int main(int argc, char* argv[]) {
	Input(); Solve();
  return 0;
}
 */

/*
import java.util.*;
import java.lang.*;
import java.io.*;

class Javaki {
	public static String s1, s2;
	public static int expected = 0;
	public static int cases = 0;
	public static int valid = 0;

	public static void recursive(int z, int val) {
		if (z == s2.length()) {
			if (val == expected) valid++;
			cases++; return;
		}
		if (s2.charAt(z) != '-') recursive(z+1, val+1);
		if (s2.charAt(z) != '+') recursive(z+1, val-1);
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		s1 = sc.next(); s2 = sc.next();
		for (int i=0; i<s1.length(); i++) {
			if (s1.charAt(i) == '+') expected++;
			else expected--;
		}

		recursive(0, 0);
		System.out.printf("%.10f\n", (double)valid / cases);
	}
}
 */