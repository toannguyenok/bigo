package toan.bigo.backtracking;

import java.util.ArrayList;
import java.util.Scanner;

/*
Ý tưởng: Dùng kỹ thuật backtracking để phát sinh tất cả các tổ hợp chập 66 của kk phần tử trong mảng ban đầu.

Lợi dụng việc mảng ban đầu đã được sắp xếp tăng dần (không có giá trị trùng lặp), ta sẽ thiết kế hàm backtracking luôn "tiến tới" để đảm bảo tính tăng dần của các tổ hợp được phát sinh.

Gọi aa là mảng đề cho, resres là mảng lưu kết quả của mỗi tổ hợp được phát sinh. Cấu trúc của hàm backtracking, tạm gọi là hàm permutation(i, p, k, len_lotto = 6) với i là vị trí của phần tử thứ ii trong mảng resres, p là vị trí của phần tử thứ pp trong mảng aa (vị trí được đánh số từ 00), k là số lượng phần tử trong mảng ban đầu, len_lotto là độ dài của dãy cần được phát sinh (mặc định bằng 66), được viết dưới dạng mã giả như sau:

func permutation(i, p, k, len_lotto = 6):
    if i == len_lotto:		# Nếu mảng kết quả đã đủ len_lotto = 6 ký tự thì in ra màn hình
            print res[]
            return

    for j: p -> (k - 1):
        res[i] = a[j]		# Lần lượt cố định giá trị res[i] bằng các giá trị từ vị trí p của mảng a trở về sau
        permutation(i + 1, j + 1, k, len_lotto)	# Vì vị trí j của mảng a đã được chọn cho res[i], ta xét tiếp từ vị trí (j + 1) trở về sau
Độ phức tạp: O(T * C_k^6)=O(T * k^6)O(T∗C
​k
​6
​​ )=O(T∗k
​6
​​ ) với TT là số lượng bộ test, kk là số lượng phần tử của tập SS.


 */
public class Lotto {

    private static ArrayList<Integer> input = new ArrayList<>();
    private static int[] temp = new int[100];
    private static boolean[] chosen = new boolean[100];

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        while (flag) {
            int n = sc.nextInt();
            if (n == 0) {
               flag = false;
            } else {
                input.clear();
                for (int i = 0; i < n; i++) {
                    int number = sc.nextInt();
                    input.add(number);
                    chosen[i] = false;
                }
                findSubset(0, 0);
                System.out.println("");
            }
        }
        System.exit(0);
    }

    private static void findSubset(int length, int position) {
        if (length == 6) {
            for (int i = 0; i < 6; i++) {
                System.out.print(temp[i] + " ");
            }
            System.out.println("");
            return;
        }
        for (int i = position; i < input.size(); i++) {
            if (!chosen[i]) {
                temp[length] = input.get(i);
                chosen[i] = true;
                findSubset(length + 1, i + 1);
                chosen[i] = false;
            }
        }
    }
}

/*
#include <iostream>
using namespace std;
const int MAX = 15;
int a[MAX], res[6];

void permutation(int i, int p, int k, int lotto_len) {
    if (i == lotto_len) {
        for (int& ele : res) {
            cout << ele << " ";
        }
        cout << endl;
        return;
    }

    for (int j = p; j < k; j++) {
        res[i] = a[j];
        permutation(i + 1, j + 1, k, lotto_len);
    }
}

int main() {
    int k;
    bool blank_line = false;

    while (true) {
        cin >> k;
        if (k == 0) {
            break;
        }

        for (int i = 0; i < k; i++) {
            cin >> a[i];
        }

        if (blank_line) {
            cout << endl;
        }

        permutation(0, 0, k, 6);
        blank_line = true;
    }
}
 */

/*
import java.util.Scanner;

public class Main {
    static int[] res = new int[6];
    static int[] a = new int[15];

    static void permutation(int i, int p, int k, int lotto_len) {
        if (i == lotto_len) {
            for (int ele : res) {
                System.out.printf("%d ", ele);
            }
            System.out.println();
            return;
        }

        for (int j = p; j < k; j++) {
            res[i] = a[j];
            permutation(i + 1, j + 1, k, lotto_len);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean blank_line = false;

        while (true) {
            int k = sc.nextInt();
            if (k == 0) {
                break;
            }

            for (int i = 0; i < k; i++) {
                a[i] = sc.nextInt();
            }

            if (blank_line) {
                System.out.println();
            }

            permutation(0, 0, k, 6);
            blank_line = true;
        }
    }
}
 */
