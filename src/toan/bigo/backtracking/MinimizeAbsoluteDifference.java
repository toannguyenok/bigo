package toan.bigo.backtracking;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.Predicate;

/*
Vì số lượng phần tử đầu vào rất nhỏ, chỉ có 55 phần tử. Do đó, ta có thể sử dụng backtracking để có thể sinh ra toàn bộ 5!5! trường hợp, mỗi trường hợp ta sẽ tính giá trị của biểu thức và so sánh để tìm ra trường hợp có giá trị nhỏ nhất có thể.

Tuy nhiên, cách làm ở trên không được tốt và đảm bảo đúng 100\%100%, vì khi ta tính các số theo dạng số thực, rất dễ bị sai số và khi so sánh chúng với nhau không khớp 100\%100%. Thay vì như thế, ta có thể so sánh theo kiểu số nguyên bằng cách : Biến đổi giá trị |(a/b) - (c/d)|∣(a/b)−(c/d)∣ thành |(a * d - b * c) / (b * d)| = |(a * d - b * c)| / (b * d)∣(a∗d−b∗c)/(b∗d)∣=∣(a∗d−b∗c)∣/(b∗d). Như vậy, với mỗi giá trị, ta sẽ lưu dưới dạng một cặp gồm tử và mẫu đều là 2 số nguyên dương. Ta tạo một hàm comp(a, b) với aa là phân số thứ nhất và bb là phân số thứ 22. Hàm comp(a, b) này trả ra truetrue hoặc falsefalse tương ứng là aa có nhỏ hơn bb hay không bằng cách so sánh giữa 22 số nguyên. Phân số (a.tu / a.mau) < (b.tu / b.mau) khi và chỉ khi a.tu * b.mau < b.tu * a.mau. Cần lưu ý việc tràn số khi so sánh phân số như thế này, vì giá trị của những phần tử trong mảng xx đầu vào không vượt quá 1000010000.

Để thực hiện hàm backtracking để thực hiện việc liệt kê những hoán vị, ta sẽ tạo hàm backTrack(pos) với ý nghĩa: Ta đang điền vị trí phần tử trong dãy vào vị trí pos hiện tại. Ta lưu các vị trí vào một mảng p[ ]. Nếu pos = 3pos=3 (đủ 44 phần tử) thì ta tiến hành tính giá trị |(a/b) - (c/d)|∣(a/b)−(c/d)∣, ngược lại ta tiếp tục gọi backtracking với pos + 1. Sau khi gọi backTrack(pos + 1), ta hủy vết đã lưu các phần tử trước đó.

Độ phức tạp: O(N! * N)O(N!∗N) với NN là số lượng phần tử trong dãy đầu vào.
 */
public class MinimizeAbsoluteDifference {

    private static ArrayList<Integer> input = new ArrayList<>();
    private static ArrayList<Integer> result = new ArrayList<>();
    private static ArrayList<Integer> dist = new ArrayList<>();
    private static ArrayList<Boolean> visited = new ArrayList<>();
    private static double minValue = Double.MAX_VALUE;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i++) {
            int value = scanner.nextInt();
            input.add(value);
            visited.add(false);
        }
        findMinimum(0);
        for (int i = 0; i < result.size(); i++) {
            System.out.print(result.get(i) + " ");
        }
    }

    private static void findMinimum(int index) {
        if (dist.size() == 4) {
            double newValue = calculatorResult(input.get(dist.get(0)),
                    input.get(dist.get(1)),
                    input.get(dist.get(2)),
                    input.get(dist.get(3)));
            if (newValue < minValue) {
                minValue = newValue;
                result.clear();
                for (int i = 0; i < dist.size(); i++) {
                    result.add(dist.get(i));
                }
            } else if (newValue == minValue) {
                String temp1 = "";
                String temp2 = "";
                for (int i = 0; i < dist.size(); i++) {
                    temp2 += (dist.get(i));
                    temp1 += (result.get(i));
                }
                if (temp1.compareTo(temp2) > 0) {
                    result.clear();
                    for (int i = 0; i < dist.size(); i++) {
                        result.add(dist.get(i));
                    }
                }
            }
            return;
        }
        for (int i = index; i < 4; i++) {
            for (int j = 0; j < input.size(); j++) {
                if (!visited.get(j)) {
                    visited.set(j, true);
                    dist.add(j);
                    findMinimum(i + 1);
                    int finalJ = j;
                    dist.removeIf(new Predicate<Integer>() {
                        @Override
                        public boolean test(Integer integer) {
                            return integer == finalJ;
                        }
                    });
                    visited.set(j, false);
                }
            }
        }
    }

    private static double calculatorResult(int a, int b, int c, int d) {
        return Math.abs(((a * 1.0) * (d * 1.0) - (b * 1.0) * (c * 1.0)) / ((b * 1.0) * (d * 1.0)));
    }
}

/*
#include <iostream>
#include <algorithm>
#include <iomanip>
using namespace std;

bool cmp(long long num, long long denom, long long _num, long long _denom) {
    long long a = num * _denom;
    long long b = _num * denom;
    return (a < b);
}

void check(long long(&c)[5], long long(&a)[5], long long(&minn)[2], long long(&ans)[4]) {
    long long num = abs(c[a[0]] * c[a[3]] - c[a[1]] * c[a[2]]), denom = c[a[1]] * c[a[3]];
    if (cmp(num, denom, minn[0], minn[1])) {
        minn[0] = num;
        minn[1] = denom;
        for (long long i = 0; i < 4; i++) ans[i] = a[i];
    }
}

void permutation(long long(&c)[5], long long(&a)[5], bool(&b)[5], long long j, long long(&minn)[2], long long(&ans)[4]) {
    for (long long i = 0; i < 5; i++) {
        if (b[i]) {
            a[j] = i;
            b[i] = false;
            if (j == 3) {
                check(c, a, minn, ans);
            }
            else permutation(c, a, b, j + 1, minn, ans);
            b[i] = true;
        }
    }
}
int main() {
    long long a[5], c[5], ans[4];
    for (long long i = 0; i < 5; i++) cin >> c[i];
    long long minn[2];
    minn[0] = 10000000007; minn[1] = 1;
    for (int i = 0; i < 5; i++) a[i] = i;
    for (int i = 0; i < 4; i++) ans[i] = 0;

    bool b[5];
    for (long long i = 0; i < 5; i++) b[i] = true;
    permutation(c, a, b, 0, minn, ans);
    cout << ans[0] << " " << ans[1] << " " << ans[2] << " " << ans[3] << endl;

    return 0;
}
 */

/*
import java.util.Scanner;

public class sol{
    public static boolean cmp(long num, long denom, long _num, long _denom) {
        long x = num * _denom;
        long y = _num * denom;
        return (x < y);
    }

    public static void check(long []c, int []a, long []minn, long []ans) {
        long num = Math.abs(c[a[0]] * c[a[3]] - c[a[1]] * c[a[2]]), denom=c[a[1]]*c[a[3]];
        if (cmp(num, denom, minn[0], minn[1])){
            minn[0] = num;
            minn[1] = denom;
            for (int i = 0; i < 4; i++) ans[i] = a[i];
        }
    }

    public static void permutation(long []c, int []a,  boolean[]b, int j, long [] minn, long []ans) {
        for (int i = 0; i < 5; i++) {
            if (b[i]) {
                a[j] = i;
                b[i] = false;
                if (j == 4) {
                    check(c, a, minn, ans);
                }
                else permutation(c, a, b, j + 1, minn, ans);
                b[i] = true;
            }
        }
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        long []c = new long[5];
        for (int i = 0; i < 5; i++)
            c[i] = sc.nextLong();

        long []minn = new long[2];
        minn[0] = 1000000; minn[1] = 1;

        int []a = new int[5];
        for (int i = 0; i < 5; i++) a[i] = i;

        long []ans = new long[4];
        for (int i = 0; i < 4; i++) ans[i] = 0;

        boolean []b = new boolean[5];
        for (int i = 0; i < 5; i++) b[i] = true;

        permutation(c, a, b, 0, minn, ans);

        System.out.print(ans[0] + " " + ans[1] + " " + ans[2] + " " + ans[3]);
    }
}
 */