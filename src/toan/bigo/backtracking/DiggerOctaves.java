package toan.bigo.backtracking;

import java.util.*;


/*
Vì N \le 8N≤8 nên ta chỉ có tối đa 6464 ô, và vì trạng thái của các ô chỉ có thể là X hoặc . nên ta có thể quy thành các số trong dãy bit, với 11 là X và 00 là .

Ta đánh số các ô trên bảng từ 00 đến N^2 - 1N
​2
​​ −1 theo thứ tự từ trên xuống dưới và từ trái sang phải. Như vậy, giả sử để biểu diễn trạng thái {XXX, X.X, XXX}, dãy bit của ta chính là 111101111111101111. Ta thực hiện duyệt DFS trên bảng này, với một ô xuất phát là ô (i,j)(i,j), ta gọi hàm DFS(step, x, y) với ý nghĩa: Hiện tại đang đứng tại ô (x,y)(x,y) là một ô có chứa lục bảo, stepstep là số lượng lục bảo hiện tại. Nếu step = 8step=8, điều này đồng nghĩa với việc ta đã có một hành trình thu đủ 88 lục bảo, ta sẽ đưa trạng thái dãy bit thỏa mãn ở trên vào set nhằm mục đích loại bỏ những trạng thái giống nhau. Ngược lại, ta sẽ thăm tiếp đỉnh kề (nx, ny)(nx,ny) với (x, y)(x,y) với step thêm 11 đơn vị. Sau đó, ta hủy vết đã đánh dấu ở ô (nx, ny)(nx,ny) để tiến hành tìm một đường đi khác.

Kết quả là kích thước của set.

Độ phức tạp: Trong trường hợp xấu nhất, ta có 64 lựa chọn cho đỉnh bắt đầu. Mỗi bước tiếp theo có 4 hướng đi, nhưng phải loại bỏ hướng từ bước trước đó nên còn lại 3 lựa chọn. Vậy số trạng thái tối đa có thể tạo thành là O(64*3^7)O(64∗3
​7
​​ )
 */
public class DiggerOctaves {

    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
    private static ArrayList<ArrayList<Boolean>> visited = new ArrayList<>();

    private static int[] dx = new int[]{0, 0, 1, -1};
    private static int[] dy = new int[]{1, -1, 0, 0};

    private static Set<ArrayList<Point>> setResult = new HashSet<>();

    private static class Point {
        int r;
        int c;

        public Point(int r, int c) {
            this.r = r;
            this.c = c;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return r == point.r &&
                    c == point.c;
        }

        @Override
        public int hashCode() {
            return Objects.hash(r, c);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCase = scanner.nextInt();
        ArrayList<Integer> result = new ArrayList<>();
        for (int t = 0; t < testCase; t++) {
            graph.clear();
            visited.clear();
            setResult.clear();
            int n = scanner.nextInt();
            for (int i = 0; i < n; i++) {
                String input = scanner.next();
                graph.add(new ArrayList<>());
                visited.add(new ArrayList<>());
                for (int j = 0; j < input.length(); j++) {
                    char character = input.charAt(j);
                    if (character == 'X') {
                        graph.get(i).add(1);
                    } else {
                        graph.get(i).add(0);
                    }
                    visited.get(i).add(false);
                }
            }
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    ArrayList<Point> dist = new ArrayList<>();
                    findGraph(n, 1, i, j, dist);
                }
            }
            result.add(setResult.size());
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i));
        }
    }

    private static void findGraph(int n, int counter, int r, int c, ArrayList<Point> dist) {
        if (!checkValid(n, r, c)) {
            return;
        }
        dist.add(new Point(r, c));
        visited.get(r).set(c, true);
        if (counter == 8) {
            Collections.sort(dist, new Comparator<Point>() {
                @Override
                public int compare(Point o1, Point o2) {
                    if (o1.r == o2.r) {
                        return o1.c - o2.c;
                    }
                    return o1.r - o2.r;
                }
            });
            ArrayList<Point> temp = new ArrayList<>(dist);
            setResult.add(temp);
            visited.get(r).set(c, false);
            dist.remove(new Point(r,c));
            return;
        }
        for (int i = 0; i < 4; i++) {
            int newR = r + dy[i];
            int newC = c + dx[i];
            findGraph(n, counter + 1, newR, newC, dist);
        }
        dist.remove(new Point(r,c));
        visited.get(r).set(c, false);
    }

    private static boolean checkValid(int n, int newR, int newC) {
        return (newR >= 0 &&
                newR < n &&
                newC >= 0 &&
                newC < n &&
                !visited.get(newR).get(newC)
                && graph.get(newR).get(newC) == 1);
    }
}
/*
#include <iostream>
#include <set>
using namespace std;
const int MAX = 10;
const int dx[] = {0, 0, 1, -1};
const int dy[] = {1, -1, 0, 0};

char graph[MAX][MAX];
bool visited[MAX][MAX];
int n;
set<long long> octaves;

bool onBoard(int x, int y) {
    return x >= 0 && x < n && y >= 0 && y < n;
}

void dfs(int sx, int sy, int step, long long bits) {
    visited[sx][sy] = true;
    bits |= 1LL << (sx * n + sy);

    if (step == 8) {
        octaves.insert(bits);
    }
    else {
        for (int i = 0; i < 4; i++) {
            int x = sx + dx[i];
            int y = sy + dy[i];

            if (onBoard(x, y) && !visited[x][y] && graph[x][y] == 'X') {
                dfs(x, y, step + 1, bits);
            }
        }
    }

    bits &= ~(1 << (sx * n + sy));
    visited[sx][sy] = false;
}

int main() {
    int t;
    long long bits;
    cin >> t;

    for (int tc = 0; tc < t; tc++) {
        cin >> n;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                cin >> graph[i][j];
                visited[i][j] = false;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (graph[i][j] == 'X') {
                    bits = 0;
                    dfs(i, j, 1, bits);
                }
            }
        }

        cout << octaves.size() << endl;
        octaves.clear();
    }
    return 0;
}
 */

/*
import java.util.Scanner;
import java.util.TreeSet;

public class Main {
    static final int MAX = 10;
    static final int[] dx = {0, 0, 1, -1};
    static final int[] dy = {1, -1, 0, 0};

    static String[] graph = new String[MAX];
    static boolean[][] visited = new boolean[MAX][MAX];
    static TreeSet<Long> octaves;
    static int n;

    static boolean onBoard(int x, int y) {
        return x >= 0 && x < n && y >= 0 && y < n;
    }

    static void dfs(int sx, int sy, int step, long bits) {
        visited[sx][sy] = true;
        bits |= 1L << (sx * n + sy);

        if (step == 8) {
            octaves.add(bits);
        }
        else {
            for (int i = 0; i < 4; i++) {
                int x = sx + dx[i];
                int y = sy + dy[i];

                if (onBoard(x, y) && !visited[x][y] && graph[x].charAt(y) == 'X') {
                    dfs(x, y, step + 1, bits);
                }
            }
        }

        bits &= ~(1 << (sx * n + sy));
        visited[sx][sy] = false;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        long bits;

        for (int tc = 0; tc < t; tc++) {
            n = sc.nextInt();
            octaves = new TreeSet<Long>();

            for (int i = 0; i < n; i++) {
                graph[i] = sc.next();
                for (int j = 0; j < n; j++) {
                    visited[i][j] = false;
                }
            }

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (graph[i].charAt(j) == 'X') {
                        bits = 0;
                        dfs(i, j, 1, bits);
                    }
                }
            }

            System.out.println(octaves.size());
        }
    }
}
 */