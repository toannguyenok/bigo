package toan.bigo.backtracking;

import java.util.*;

/*
Bài này có 22 bước quan trọng là tìm tất cả các từ PigEwu trong 22 bảng Boggle và sau đó lọc ra các từ chung tìm được ở 22 bảng đó.

Tìm các từ PigEwu trong mỗi bảng: chúng ta sử dụng backtracking để tìm ra tất cả các từ 44 ký tự rồi lưu lại những từ có đúng 22 nguyên âm. Ý tưởng backtracking là thử chọn 11 ô làm ký tự đầu tiên, sau đó trong các ô kề của ô đó ta chọn 11 ô làm ký tự thứ 22, rồi chọn 1 ô trong các ô kề của ô thứ 22 mà chưa được chọn để làm ký tự thứ 33, tương tự với ký tự thứ 44.

Gọi find_words(x, y, cur_word) là thủ tục để thử tìm tất cả các cách chọn 44 ô phân biệt kề nhau trên bảng, và đang đứng ở ô (x, y)(x,y), với cur\_wordcur_word là từ đã có được bằng cách nối các ký tự đang được chọn:

Nếu cur\_wordcur_word đã đủ 44 ký tự: nếu nó có 22 nguyên âm thì lưu cur\_wordcur_word lại, ngược lại không làm gì cả. Tới đây thoát hàm vì không cần tìm thêm ký tự thứ 55 nữa.
Đánh dấu ô (x, y)(x,y) là đang được chọn.
Với mọi ô (x', y')(x
​′
​​ ,y
​′
​​ ) kề với ô (x, y)(x,y): nếu ô này nằm trên bảng và chưa được chọn thì gọi thủ tục find_words(x', y', cur_word + ký tự ở ô (x', y')) để thử cho ký tự tiếp theo là (x', y')(x
​′
​​ ,y
​′
​​ ) rồi thử tiếp.
Cuối cùng đánh dấu ô (x, y)(x,y) là hiện đang chưa được chọn và thoát khỏi thủ tục.
Với mỗi ô (x, y)(x,y) trên bảng ta đều gọi thủ tục find_words, tức là thử cho ô (x, y)(x,y) làm ký tự đầu tiên một cách thủ công, còn việc tìm các ký tự còn lại để hàm find_words lo.

Giả sử có tối đa WW từ trên bảng (kể cả từ không phải từ PigEwu). Chi phí để thêm 11 từ là O(log(W))O(log(W)) nếu sử dụng cấu trúc Tree Set, hoặc O(1)O(1) nếu dùng Hash Table. Vậy thao tác tìm và lưu lại tất cả các từ là O(Wlog(W))O(Wlog(W)) hoặc O(W)O(W).

Số ô bắt đầu gọi find_words là 1616 ô của bảng. Với mỗi ô bắt đầu (chọn ký tự đầu tiên), có không quá 88 cách chọn ký tự thứ 22; với mỗi cách chọn ký tự thứ 22 lại có không quá 77 cách chọn ký tự thứ 33; cuối cùng có không quá 77 cách chọn ký tự cuối cùng.
Vậy có nhiều nhất khoảng 16 * 8 * 7 * 7 = 627216∗8∗7∗7=6272 từ.
Sau khi tìm được 22 tập các từ PigEwu của 22 bảng thì ta duyệt mỗi từ trong một tập và kiểm tra xem nó có nằm trong tập kia không. Thao tác này tốn O(Wlog(W))O(Wlog(W)) (nếu dùng Hash Table thì sau khi tìm xong phải sắp xếp lại các từ cho đúng thứ tự từ điển nữa, còn Tree Set thì nó đã đúng thứ tự từ điển rồi nên cả 22 cách có cùng chi phí).

Độ phức tạp: O(T * W * log(W))O(T∗W∗log(W)) với TT là số bộ test và WW là số từ tối đa có trên bảng.
 */
public class TheBoggleGame {

    private static ArrayList<ArrayList<Character>>[] pairBoard = new ArrayList[2];

    private static ArrayList<ArrayList<Boolean>>[] visited = new ArrayList[2];

    private static int[] dx = {0, 0, 1, 1, 1, -1, -1, -1};
    private static int[] dy = {1, -1, 0, 1, -1, 0, 1, -1};

    private static ArrayList<String>[] ans = new ArrayList[2];
    private static Set<String> result = new TreeSet<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        pairBoard[0] = new ArrayList<>();
        pairBoard[1] = new ArrayList<>();

        visited[0] = new ArrayList<>();
        visited[1] = new ArrayList<>();

        ans[0] = new ArrayList<>();
        ans[1] = new ArrayList<>();

        int row = -1;
        while (flag) {
            String line = scanner.nextLine();
            if (line.length() == 1 && line.equals("#")) {
                flag = false;
            } else {
                if (line.equals("")) {
                    for (int i = 0; i < pairBoard[0].size(); i++) {
                        for (int j = 0; j < pairBoard[0].get(i).size(); j++) {
                            findString("", 0, 1, pairBoard[0].size(), i, j);
                            findString("", 1, 1, pairBoard[1].size(), i, j);
                        }
                    }

                    for (String str1 : ans[0]) {
                        for (String str2 : ans[1]) {
                            if (str1.equals(str2)) {
                                result.add(str1);
                            }
                        }
                    }

                    if (result.size() == 0) {
                        System.out.println("There are no common words for this pair of boggle boards.");
                    } else {
                        for (String value : result) {
                            System.out.println(value);
                        }
                    }
                    System.out.println();

                    pairBoard[0].clear();
                    pairBoard[1].clear();
                    visited[0].clear();
                    visited[1].clear();
                    result.clear();
                    ans[0].clear();
                    ans[1].clear();
                    row = -1;
                } else {
                    row++;
                    pairBoard[0].add(new ArrayList<>());
                    pairBoard[1].add(new ArrayList<>());
                    visited[0].add(new ArrayList<>());
                    visited[1].add(new ArrayList<>());

                    String[] pairRow = line.split("  ");
                    String[] input1 = pairRow[0].split(" ");
                    String[] input2 = pairRow[2].split(" ");
                    for (int i = 0; i < input1.length; i++) {
                        pairBoard[0].get(row).add(input1[i].charAt(0));
                        visited[0].get(row).add(false);
                    }
                    for (int i = 0; i < input2.length; i++) {
                        pairBoard[1].get(row).add(input2[i].charAt(0));
                        visited[1].get(row).add(false);
                    }
                }
            }
        }
    }

    private static void findString(String string, int positionBoard, int counter, int n, int r, int c) {
        if (!isValidRowColumn(string, n, r, c, positionBoard)) {
            return;
        }
        String newString = string + pairBoard[positionBoard].get(r).get(c);
        visited[positionBoard].get(r).set(c, true);
        if (counter == 4 && getVowelFromString(newString) == 2) {
            ans[positionBoard].add(newString);
            visited[positionBoard].get(r).set(c, false);
            return;
        }
        for (int i = 0; i < 8; i++) {
            int newR = r + dy[i];
            int newC = c + dx[i];
            findString(newString, positionBoard, counter + 1, n, newR, newC);
        }
        visited[positionBoard].get(r).set(c, false);
    }

    private static boolean isValidRowColumn(String string, int n, int r, int c, int positionBoard) {
        return (r >= 0 &&
                r < n &&
                c >= 0 &&
                c < n &&
                !visited[positionBoard].get(r).get(c) &&
                string.length() < 4 &&
                isValidStringPlus(string, pairBoard[positionBoard].get(r).get(c)));
    }

    public static boolean isValidStringPlus(String string, char newChar) {
        if (getVowelFromString(string) == 2 && isVowel(newChar)) {
            return false;
        }
        return true;
    }

    private static boolean isVowel(char input) {
        return input == 'A' || input == 'E' || input == 'I' || input == 'O' || input == 'U' | input == 'Y';
    }

    public static int getVowelFromString(String string) {
        int countVowel = 0;
        for (int i = 0; i < string.length(); i++) {
            if (isVowel(string.charAt(i))) {
                countVowel++;
            }
        }
        return countVowel;
    }
}

/*
#include <iostream>
#include <string.h>
#include <set>
#include <vector>
using namespace std;

const string VOWELS = "AEOYIU";
const pair<int, int> DIRs[] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};

int count_vowels(const string& word) {
    int res = 0;
    for (char c : word) {
        res += (VOWELS.find(c) != string::npos);
    }
    return res;
}


 * board: current board to find words
 * x, y: current cell
 * cur_word: current cummulative word
 * visited: visiting state for each cell
 * found_words: target location for legal words

void find_words(char board[4][4], int x, int y, const string& cur_word, bool visited[4][4], set<string>& found_words) {
    if (cur_word.length() == 4) {
        if (count_vowels(cur_word) == 2) {
            found_words.insert(cur_word);
        }
        return;
    }

    visited[x][y] = true;
    for (auto d : DIRs) {
        int nx = x + d.first, ny = y + d.second;
        if (nx >= 0 && nx < 4 && ny >= 0 && ny < 4 && !visited[nx][ny]) {
            string new_cur_word = cur_word + board[nx][ny];
            find_words(board, nx, ny, new_cur_word, visited, found_words);
        }
    }
    visited[x][y] = false;
}

    bool start = true; // just for checking whether to print new lines between test cases

    void solve() {
        char board[2][4][4];
        char ch;
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 8; ++j) {
                cin >> ch;
                if (ch == '#') exit(0);
                board[j >> 2][i][j & 3] = ch; // characters 0..3 -> board[0][i][0..3], characters 4..7 -> board[1][i][0..3]
            }
        }

        if (!start) {
            cout << endl;
        }
        start = false;

        // find words for both boards
        bool visited[4][4];
        memset(visited, 0, sizeof(visited));

        set<string> words[2];
        for (int board_id : {0, 1}) {
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j) {
                    string cur_word(1, board[board_id][i][j]);
                    find_words(board[board_id], i, j, cur_word, visited, words[board_id]);
                }
            }
        }

        vector<string> common_words;
        for (const string& word : words[0]) {
            if (words[1].find(word) != words[1].end()) {
                common_words.push_back(word);
            }
        }

        if (common_words.size() == 0) {
            cout << "There are no common words for this pair of boggle boards.\n";
        }
        else {
            for (const string& word : common_words) {
                cout << word << endl;
            }
        }
    }

    int main() {
        while (true) {
            solve();
        }
    }
 */

/*
import java.util.Arrays;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.ArrayList;

public class Main {
    static final private String VOWELS = "AEOYIU";
    static final private int[][] DIRs = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};
    static private boolean start;
    static private Scanner sc;

    static int count_vowels(String word) {
        int res = 0;
        for (int i = 0; i < word.length(); ++i) {
            if (VOWELS.indexOf(word.charAt(i)) > -1) {
                res += 1;
            }
        }
        return res;
    }

     * board: current board to find words
     * x, y: current cell
     * cur_word: current cummulative word
     * visited: visiting state for each cell
     * found_words: target location for legal words

 static void find_words(char[][] board, int x, int y, String cur_word, boolean[][] visited, TreeSet<String> found_words) {
    if (cur_word.length() == 4) {
        if (count_vowels(cur_word) == 2) {
            found_words.add(cur_word);
        }
        return;
    }

    visited[x][y] = true;
    for (int[] d : DIRs) {
        int nx = x + d[0], ny = y + d[1];
        if (nx >= 0 && nx < 4 && ny >= 0 && ny < 4 && !visited[nx][ny]) {
            String new_cur_word = cur_word + board[nx][ny];
            find_words(board, nx, ny, new_cur_word, visited, found_words);
        }
    }
    visited[x][y] = false;
}

    static void solve() {
        char[][][] board = new char[2][4][4];

        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 8; ++j) {
                char ch = sc.next().charAt(0);
                if (ch == '#') {
                    System.exit(0);
                }
                board[j >> 2][i][j & 3] = ch; // characters 0..3 -> board[0][i][0..3], characters 4..7 -> board[1][i][0..3]
            }
        }

        if (!start) {
            System.out.print('\n');
        }
        start = false;

        // find words for both boards
        boolean[][] visited = new boolean[4][4];
        for (boolean[] row : visited) {
            Arrays.fill(row, false);
        }

        TreeSet<String>[] words = new TreeSet[2];
        for (int i = 0; i < words.length; i++) {
            words[i] = new TreeSet<>();
        }

        for (int board_id : new int[] {0, 1}) {
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j) {
                    String cur_word = "";
                    cur_word += board[board_id][i][j];
                    find_words(board[board_id], i, j, cur_word, visited, words[board_id]);
                }
            }
        }

        ArrayList<String> common_words = new ArrayList<>();
        for (String word : words[0]) {
            if (words[1].contains(word)) {
                common_words.add(word);
            }
        }

        if (common_words.size() == 0) {
            System.out.print("There are no common words for this pair of boggle boards.\n");
        }
        else {
            for (String word : common_words) {
                System.out.print(word + "\n");
            }
        }
    }

    public static void main(String[] args) {
        start = true;
        sc = new Scanner(System.in);
        while (true) {
            solve();
        }
    }
}
 */