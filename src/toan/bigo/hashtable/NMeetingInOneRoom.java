package toan.bigo.hashtable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/*
Hướng dẫn giải
Với bài toán này, những việc cần phải làm như sau:

Sắp xếp các cuộc họp theo thời gian kết thúc tăng dần vì chúng ta sẽ thực hiện tham lam chọn các cuộc họp dựa trên thời gian kết thúc.
Ta duyệt qua tất cả các cuộc họp theo thời gian kết thúc, một cuộc họp sẽ được chọn để tổ chức nếu như thời điểm bắt đầu của cuộc họp đó rơi vào lúc căn phòng không được đang sử dụng.
Bước 1: Sắp xếp các cuộc họp theo thời gian kết thúc tăng dần.
Bước 2: Chọn cuộc họp đầu tiên từ danh sách các cuộc họp đã sắp xếp ở trên.
Bước 3: Lần lượt so sánh thời gian kết thúc của cuộc họp trước đó với thời gian bắt đầu của các cuộc họp sau. Nếu phù hợp sẽ chọn.
Bước 4: Nếu còn những cuộc họp phía sau chưa xét thì tiếp tục quay lại bước 3 để thực hiện tiếp. Ngược lại sẽ dừng thuật toán nếu đã hết cuộc họp.
Độ phức tạp: O(nlogn)O(nlogn) với n là số lượng cuộc họp.
 */
public class NMeetingInOneRoom {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCase = scanner.nextInt();
        ArrayList<Meeting> input = new ArrayList<>();
        for (int t = 0; t < testCase; t++) {
            input.clear();
            int n = scanner.nextInt();
            for (int i = 0; i < n; i++) {
                Meeting meeting = new Meeting();
                meeting.start = scanner.nextInt();
                meeting.index = i;
                input.add(meeting);
            }

            for (int i = 0; i < n; i++) {
                input.get(i).end = scanner.nextInt();
            }

            Collections.sort(input, (o1, o2) -> o1.end - o2.end);

            int last = input.get(0).end;
            ArrayList<Integer> result = new ArrayList<>();
            result.add(input.get(0).index);
            for (int i = 1; i < input.size(); i++) {
                if (input.get(i).start > last) {
                    last = input.get(i).end;
                    result.add(input.get(i).index);
                }
            }

            for (int i = 0; i < result.size(); i++) {
                System.out.print((result.get(i) + 1) + " ");
            }
            System.out.println();
        }
    }

    public static class Meeting {
        int start;
        int end;
        int index;

        public Meeting() {

        }
    }
}

/*
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
	int t, n;
	cin >> t;
	while (t--) {
		cin >> n;
		pair<int,int> p[10005];

		for (int i = 0; i < n; i++) {
			cin >> p[i].second;
		}

		for (int i = 0; i < n; i++) {
			cin >> p[i].first;
		}

		vector<pair<pair<int,int>, int>> v;

		for (int i = 0; i < n; i++) {
			v.push_back(make_pair(p[i], i + 1));
		}

		sort(v.begin(), v.end());

		int res = 1, last_finish = v[0].first.first;
		cout << v[0].second << " ";

		for (int i = 1; i < n; i++) {
			if(v[i].first.second >= last_finish) {
			    last_finish = v[i].first.first;
				cout << v[i].second << " ";
			}
		}
		cout << endl;
	}
	return 0;
}
 */

/*

 */