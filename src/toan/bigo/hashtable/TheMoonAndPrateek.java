package toan.bigo.hashtable;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*
Hướng dẫn giải
Bước 1: Đọc vào NN số
Bước 2: Băm NN số với hàm băm f(N) = N_if(N)=N
​i
​​  \oplus⊕ ((Tổng các chữ số của N_i)N
​i
​​ ) rồi lưu vào map hoặc unordered_map với cặp giá trị là <hash\_value<hash_value, tần số xuất hiện của hash\_value>hash_value>. Đồng thời tìm số hash\_valuehash_value có giá trị lớn nhất.
Bước 3: Nếu số lượng phần tử trong map bằng NN (có nghĩa là không xảy ra đụng độ) thì ta in ra số hash\_valuehash_value lớn nhất và số 00. Ngược lại : Ta sẽ tìm số lần đụng độ nhiều nhất trên mỗi số bằng cách duyệt qua map, sau đó duyệt lại một lần nữa để tìm số hash\_valuehash_value nhỏ nhất có số lần đụng độ nhiều nhất. Sau đó in ra cặp giá trị mà đề bài yêu cầu.
Độ phức tạp: O(N * log(N))O(N∗log(N)) khi dùng map với NN là số lượng phần tử trong danh sách, O(N)O(N) khi dùng unordered_map với NN là số lượng phần tử trong danh sách.
 */
public class TheMoonAndPrateek {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int lastKey = -1;
        int ans = -1;
        int totalCollision = 0;
        Map<Integer, Integer> tempSizeHash = new HashMap<>();

        for (int i = 0; i < n; i++) {
            int input = scanner.nextInt();
            int key = r3gz3n(input);
            ans = Math.max(ans, key);
            if (tempSizeHash.containsKey(key)) {
                totalCollision++;
                tempSizeHash.put(key, tempSizeHash.get(key) + 1);
            } else {
                tempSizeHash.put(key, 1);
            }
            if (lastKey == -1) {
                lastKey = key;
            } else {
                if (tempSizeHash.get(key) > tempSizeHash.get(lastKey)) {
                    lastKey = key;
                } else if (tempSizeHash.get(key) == tempSizeHash.get(lastKey)) {
                    lastKey = Math.min(lastKey, key);
                }
            }
        }
        int result = ans;
        if (totalCollision != 0) {
            result = lastKey;
        }
        System.out.print(result + " " + totalCollision);
    }

    private static int r3gz3n(int value) {
        int total = 0;
        String stringValue = String.valueOf(value);
        for (int i = 0; i < stringValue.length(); i++) {
            total = total + (Integer.parseInt(String.valueOf(stringValue.charAt(i))));
        }
        return value ^ total;
    }
}
/*
#include <bits/stdc++.h>
using namespace std;
#define fastIO cin.tie(0);ios_base::sync_with_stdio(0)
map<int,int> mp; // store <hash_value,number_of_frequency>
int SumOfDigit(int n){
    int res = 0;
    while(n > 0){
        res += n%10;
        n = n/10;
    }
    return res;
}
int HashFunc(int n){
    return n^SumOfDigit(n);
}
int main(){
    fastIO;
    int n, num;
    int nCollision = 0;
    int MaxHashValue = -1;
    int MinHashValueCollision = 1e9;
    int MaxFrequency = -1;
    cin >> n;
    for(int i = 0 ; i < n;i++){
        cin >> num;
        int hash_value = HashFunc(num);
        MaxHashValue = max(MaxHashValue,hash_value);
        mp[hash_value]++;

    }
    map<int,int>::iterator it;
    for(it=mp.begin(); it!=mp.end(); it++){
        nCollision += it->second - 1;
        MaxFrequency = max(MaxFrequency, it->second);
    }
    if(mp.size() == n){
        cout << MaxHashValue <<" ";
    }
    else{
        for(it=mp.begin(); it!=mp.end(); it++){
            if(it->second == MaxFrequency ){
                MinHashValueCollision = min(it->first,MinHashValueCollision);
            }
        }
        cout << MinHashValueCollision <<" ";
    }
    cout << nCollision;
    return 0;
}
 */

/*
import java.io.PrintWriter;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class solution_java {

    private static int sumOfDigit(int n) {
        int res = 0;
        while (n > 0) {
            res += n%10;
            n = n/10;
        }

        return res;
    }

    private static int HashFunc(int n) {
        return n ^ sumOfDigit(n);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int nCollision = 0;
        int MaxHashValue = -1;
        int MinHashValueCollision = (int)1e9;
        int MaxFrequency = -1;

        TreeMap<Integer, Integer> mp = new TreeMap<>();

        int n = in.nextInt();
        for(int i = 0; i < n; ++i) {
            int num = in.nextInt();
            int hash_value = HashFunc(num);
            MaxHashValue = Math.max(MaxHashValue, hash_value);
            mp.put(hash_value, mp.getOrDefault(hash_value, 0) + 1);
        }

        for(int v: mp.values()) {
            nCollision += v - 1;
            MaxFrequency = Math.max(MaxFrequency, v);
        }

        if (mp.size() == n)
            out.print(MaxHashValue);
        else {
            for (Map.Entry<Integer, Integer> e : mp.entrySet())
                if (e.getValue() == MaxFrequency)
                    MinHashValueCollision = Math.min(e.getKey(), MinHashValueCollision);

            out.print(MinHashValueCollision);
        }

        out.print(" " + nCollision);

        in.close();
        out.close();
    }
}
 */