package toan.bigo.hashtable;

/*
Hướng dẫn giải
Hướng dẫn giải

Giả sử như xâu ss không thể tạo ra bất kì xâu tt nào. Đáp là sẽ là một hoán vị bất kì của xâu ss.
Nhận xét 1: giả sử xâu kết quả không bắt đầu bằng xâu tt, ta có thể chuyển các kí tự đầu về phía sau, như vậy xâu kết quả bắt đầu bằng xâu tt luôn không thể tệ hơn.
Nhận xét 2: Trong xâu đáp án ta muốn sự xuất hiện của xâu tt tiếp theo là càng gần đầu xâu càng tốt. Hay nói cách khác, ta cần xét xâu con chung dài nhất (độ dài nhỏ hơn độ dài tt) của tiền tố xâu tt và hậu tố xâu tt.
Giả sử: xâu t =t=10101, để xây dựng xâu tt tiếp theo ta chỉ cần thêm 01 vào cuối, hay nói cách khác ta không cần thêm 101 là xâu chung dài nhất của tiền tố tt và hậu tố tt.
Các bước giải:

Bước 1: Khởi tạo xâu tt.
Bước 2: Tìm xâu con chung dài nhất của tiền tố tt và hậu tố tt có độ dài maxPrefixDuplicatemaxPrefixDuplicate.
Bước 3: Thêm vào kết quả đoạn hậu tố có độ dài (|t| - maxPrefixDuplicate + 1), nếu không đủ kí tự 0 hoặc 1 thì các kí tự cuối điền cho đủ số lượng.
Độ phức tạp O(max(|s|, |t|))O(max(∣s∣,∣t∣)) với |s|∣s∣, |t|∣t∣ là độ dài xâu ss, tt.
 */
public class CampSchedule {
}

/*
#include <bits/stdc++.h>

using namespace std;

#define hash fwpqenofqwoipngq

const int BASE = 29;
const int MOD = 1e9 + 7;
const int maxn = 5e5 + 100;
string s, t, sb, st;
int n, m;
int cnt[2], cntNeed[2];
long long power[maxn], hash[maxn];

void CreateHash(string s){
    power[0] = 1;
    for (int i = 0; i < s.size(); i++){
        hash[i + 1] = (hash[i] * BASE + s[i]) % MOD;
        power[i + 1] = (power[i] * BASE) % MOD;
    }
}

long long GetHash(int l, int r){
    return ((hash[r] - (hash[l - 1] * power[r - l + 1]) % MOD + MOD) % MOD);
}

int main(){
    getline(cin,s);
    getline(cin,t);
    n = s.size();
    m = t.size();
    for (int i = 0; i < n; i++){
        cnt[s[i] - '0']++;
    }
    for (int i = 0; i < m; i++){
        cnt[t[i] - '0']--;
    }
    if (cnt[0] < 0 || cnt[1] < 0){
        cout << s;
        return 0;
    }
    CreateHash(t);
    int MaxPrefixDuplicate = 0;
    for (int len = m - 1; len >= 0; len--){
        if (GetHash(1, len) == GetHash(m - len + 1, m)) {
            MaxPrefixDuplicate = len;
            break;
        }
    }

    for (int i = MaxPrefixDuplicate; i < m; i++){
        cntNeed[t[i] - '0']++;
        st.push_back(t[i]);
    }

    int mNeed = m - MaxPrefixDuplicate;
    sb = t;
    while (cnt[0] >= cntNeed[0] && cnt[1] >= cntNeed[1]){
        cnt[0] -= cntNeed[0];
        cnt[1] -= cntNeed[1];
        for (int i = 0; i < mNeed; i++){
            sb.push_back(st[i]);
        }
    }

    for (int i = 0; i < cnt[0]; i++){
        sb.push_back('0');
    }

    for (int i = 0; i < cnt[1]; i++){
        sb.push_back('1');
    }

    cout << sb;
}
 */

/*
import java.util.Scanner;

public class Main {
    private static final int BASE = 29;
    private static final int MOD = (int)1e9 + 7;
    private static long[] hash;
    private static long[] pow;

    private static void createHash(String s) {
        hash = new long[s.length() + 1];
        pow = new long[s.length() + 1];
        pow[0] = 1;
        for (int i = 0; i < s.length(); i++) {
            hash[i + 1] = (hash[i] * BASE + s.charAt(i)) % MOD;
            pow[i + 1] = (pow[i] * BASE) % MOD;
        }
    }

    private static long getHash(int l, int r) {
        return (hash[r] - (hash[l - 1] * pow[r - l + 1]) % MOD + MOD) % MOD;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.next();
        String t = sc.next();
        int n = s.length();
        int m = t.length();

        int[] cnt = new int[2];
        for (int i = 0; i < n; i++) {
            cnt[s.charAt(i) - '0']++;
        }
        for (int i = 0; i < m; i++) {
            cnt[t.charAt(i) - '0']--;
        }

        if (cnt[0] < 0 || cnt[1] < 0) {
            System.out.println(s);
            return;
        }
        createHash(t);
        int maxPrefixDuplicate = 0;
        for (int len = m - 1; len > 0; len--) {
            if (getHash(1, len) == getHash(m - len + 1, m)) {
                maxPrefixDuplicate = len;
                break;
            }
        }
        int[] cntNeed = new int[2];
        for (int i = maxPrefixDuplicate; i < m; i++) {
            cntNeed[t.charAt(i) - '0']++;
        }
        StringBuilder sb = new StringBuilder(t);
        t = t.substring(maxPrefixDuplicate, m);

        while (cnt[0] >= cntNeed[0] && cnt[1] >= cntNeed[1]) {
            sb.append(t);
            cnt[0] -= cntNeed[0];
            cnt[1] -= cntNeed[1];
        }

        for (int i = 0; i < cnt[0]; i++) {
            sb.append('0');
        }
        for (int i = 0; i < cnt[1]; i++) {
            sb.append('1');
        }
        System.out.println(sb.toString());
    }
}
 */