package toan.bigo.hashtable;

import java.io.*;

/*
Hướng dẫn giải
Tính chất của phép xor bit: Nếu a \text{ xor } b = ca xor b=c thì a = c \text{ xor } ba=c xor b.
Ta gọi ansans là vector chứa các bit của BB được đánh số từ 00 đến N - 1N−1 theo thứ tự từ phải sang trái.
Ta gọi cc là bit thứ ii của BB;
Ta xét đến bit thứ ii của BB, nếu i \geq Ki≥K, ta sẽ lấy bit c = c \text{ xor } ans[i - K]c=c xor ans[i−K];
Ta sẽ tiếp tục lấy đưa bit c \text{ xor } S[i]c xor S[i] vào ans, và gán c = S[i]c=S[i];
Kết quả là in toàn bộ các bit trong ansans.
 */
public class Cipher {

        public static void solve(Input in, PrintWriter out) throws IOException {
            int n = in.nextInt();
            int k = in.nextInt();
            char[] c = in.next().toCharArray();
            int x = 0;
            char[] ans = new char[n];
            for (int i = 0; i < n; ++i) {
                ans[i] = (char) (((c[i] - '0') ^ x) + '0');// I understand we are converting the character into integer (c[i] - '0') then xoring it with x which is holding 0 and then again adding '0' to convert back it into character.
                x ^= ans[i] - '0';// But why are we doing this!!. From this line onward things are not clear.
                if (i >= k - 1) {
                    x ^= ans[i-(k-1)] - '0';
                }
            }
            out.println(ans);
        }

        public static void main(String[] args) throws IOException {
            PrintWriter out = new PrintWriter(System.out);
            solve(new Input(new BufferedReader(new InputStreamReader(System.in))), out);
            out.close();
        }

        static class Input {
            BufferedReader in;
            StringBuilder sb = new StringBuilder();

            public Input(BufferedReader in) {
                this.in = in;
            }

            public Input(String s) {
                this.in = new BufferedReader(new StringReader(s));
            }

            public String next() throws IOException {
                sb.setLength(0);
                while (true) {
                    int c = in.read();
                    if (c == -1) {
                        return null;
                    }
                    if (" \n\r\t".indexOf(c) == -1) {
                        sb.append((char)c);
                        break;
                    }
                }
                while (true) {
                    int c = in.read();
                    if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                        break;
                    }
                    sb.append((char)c);
                }
                return sb.toString();
            }

            public int nextInt() throws IOException {
                return Integer.parseInt(next());
            }

            public long nextLong() throws IOException {
                return Long.parseLong(next());
            }

            public double nextDouble() throws IOException {
                return Double.parseDouble(next());
            }
        }
}

/*
#include <bits/stdc++.h>

using namespace std;

string s;
int n, k;
vector <int> ans;

int main () {
	//freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);

	cin >> n >> k;
  cin >> s;


	int c = 0;

	for (int i = 0; i < n; i++) {
		if (i >= k) {
			c ^= ans[i - k];
		}

		int x = s[i] - '0';
		ans.push_back(c ^ x);
		c ^= ans.back();
	}

	for (int i = 0; i < n; i++) {
		cout << ans[i];
	}

	return 0;
}
 */

/*
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main
{
  public static void main(String[] args) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    StringTokenizer st = new StringTokenizer(br.readLine());
    int n = Integer.parseInt(st.nextToken());
    int k = Integer.parseInt(st.nextToken());
    String s = br.readLine();

    int[] a = new int[n];
    int c = 0;
    for(int i = 0; i < n; i++) {
      if(i >= k) {
        c ^= a[i - k];
      }
      a[i] = (s.charAt(i) - '0') ^ c;
      c ^= a[i];
    }

    for(int x : a) {
      System.out.print(x);
    }
  }
}
 */