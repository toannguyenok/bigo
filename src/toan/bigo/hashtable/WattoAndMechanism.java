package toan.bigo.hashtable;

/*
Hướng dẫn giải
Ý tưởng thuật toán: Sử dụng Polynomial Hash. Đầu tiên ta sẽ có 1 bảng băm tính hashValuehashValue cho mỗi chuỗi trong nn chuỗi của máy. Sau đó với mỗi chuỗi tt trong mm chuỗi truy vấn, ta sẽ tìm tất cả các chuỗi có thể thu được bằng cách thay thế 11 kí tự của chuỗi tt bằng 11 kí tự khác (ví dụ chuỗi tt là abc thì sau khi thay thế 11 kí tự sẽ có 66 chuỗi như sau: bbc, cbc, aac, acc, aba, abb).

Tiếp theo ta lấy hashValuehashValue của tất cả chuỗi vừa tạo được so sánh với hashValuehashValue của bảng băm nn chuỗi ở trên, Nếu chuỗi đó xuất hiện trong bảng băm thì in ra YES, ngược lại là NO.

Bước 1: Khai báo các giá trị là hằng số, chọn a = 257a=257, table\_size = 10^9 + 7table_size=10
​9
​​ +7, L = 1000001L=1000001. Trong đó aa là hằng số; table\_sizetable_size là kích thước của bảng băm, kích thước này càng lớn thì độ phân biệt các hashValuehashValue càng cao; LL là độ dài lớn nhất có thể có của chuỗi.
Bước 2: Khởi tạo mảng ff có độ dài LL. Trong công thức tính hashValuehashValue của Polynomial Hash, f_if
​i
​​  tương ứng với a^{l-i-1}a
​l−i−1
​​ .
Bước 3: Tạo hàm polyHash để tính hashValuehashValue của từng chuỗi trong nn chuỗi của máy.
Bước 4: Hàm check để kiểm tra xem giá trị new\_hashValuenew_hashValue của tất cả những chuỗi thu được bằng cách thay thế 11 kí tự của chuỗi tt bằng 11 kí tự khác, nó có bằng với hashValuehashValue của bảng băm nn chuỗi ở trên hay không. Giá trị new\_hashValuenew_hashValue tính bằng công thức:
- new\_hashValue = ((((c - query_i) * f_{len - i - 1}) \% table\_size + table\_size) + h) \% table\_sizenew_hashValue=((((c−query
​i
​​ )∗f
​len−i−1
​​ )%table_size+table_size)+h)%table_size
Trong đó ((c - query_i) * f_{len - i - 1}) \% table\_size + table\_size((c−query
​i
​​ )∗f
​len−i−1
​​ )%table_size+table_size : là độ chênh lệch giữa 22 chuỗi sau khi biến đổi, khi độ chênh lệch âm thì phải cộng với table\_sizetable_size; hh là giá trị hashValuehashValue ban đầu của chuỗi truy vấn.

Tiếp theo, ta dùng hàm find để kiểm tra new\_hashValuenew_hashValue có nằm trong bảng băm hay không. Nếu không thì trả về true, ngược lại là false. Độ phức tạp: O(L*log(n))O(L∗log(n)) với LL là tổng độ dài của tất cả các chuỗi, log(n)log(n) là độ phức tạp khi tìm kiếm trong bảng băm.


 */
public class WattoAndMechanism {
}

/*
#include <iostream>
#include <set>
#include <string>
using namespace std;

typedef long long ll;
const int L = 1000001;
const ll table_size = 1e9 + 7;
const ll a = 257;

int n, m;
string keys, t, tmp;
ll f[L];
set<ll> dic;

void init()
{
    f[0] = 1;
    for (int i = 1; i < L; i++)
        f[i] = (f[i - 1] * a) % table_size;
}

ll polyHash(string keys)
{
    ll hashValue = 0;
    for (int i = 0; i < keys.size(); i++)
        hashValue = (hashValue * a + keys[i]) % table_size;
    return hashValue;
}

bool check(string query)
{
    ll h = polyHash( query);
    ll len = query.length();
    for (int i = 0; i < query.size(); i++)
    {
        for (ll c = 'a'; c <= 'c'; c++)
        {
            if (c == query[i])
                continue;
            ll new_hashValue = ((((c - query[i]) * f[len - i - 1]) % table_size + table_size) + h) % table_size;
            if (dic.find(new_hashValue) != dic.end())
                return true;
        }
    }
    return false;
}

int main()
{
    init();
    cin >> n >> m;
    dic.clear();
    for (int i = 0; i < n; i++)
    {
        cin >> keys;
        dic.insert(polyHash(keys));
    }
    for (int i = 0; i < m; i++)
    {
        cin >> t;
        if (check(t))
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
    }
    return 0;
}
 */

/*
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class Main {
    private static final int MAX = (int)1e6 + 1;
    private static final int BASE = 257;
    private static final long TABLE_SIZE = (long)1e9 + 7;
    private static long[] pow = new long[MAX];
    private static HashSet<Long> dict = new HashSet<>();

    private static void init() {
        pow[0] = 1;
        for (int i = 1; i < MAX; i++)
            pow[i] = (pow[i - 1] * BASE) % TABLE_SIZE;
    }

    private static long polyHash(String keys) {
        long hashValue = 0;
        for (int i = 0; i < keys.length(); i++)
            hashValue = (hashValue * BASE + (keys.charAt(i) - 'a' + 1)) % TABLE_SIZE;
        return hashValue;
    }

    private static boolean check(String s) {
        long hash = polyHash(s);
        int len = s.length();
        for (int i = 0; i < s.length(); i++) {
            for (char c = 'a'; c <= 'c'; c++) {
                if (c == s.charAt(i))
                    continue;
                long newHash = (hash + ((c - s.charAt(i))*pow[len - i - 1]) % TABLE_SIZE + TABLE_SIZE) % TABLE_SIZE;
                if (dict.contains(newHash)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        for (int i = 0; i < n; i++) {
            String s = sc.next();
            dict.add(polyHash(s));
        }
        init();
        for (int i = 0; i < m; i++) {
            String t = sc.next();
            if (check(t)) {
                System.out.println("YES");
            }
            else {
                System.out.println("NO");
            }
        }
    }
}
 */