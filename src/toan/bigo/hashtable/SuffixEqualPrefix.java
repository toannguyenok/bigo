package toan.bigo.hashtable;


/*
Hướng dẫn giải
Ta dùng Polynomial Hashing để giải quyết bài toán này.

Xem các chữ cái từ a đến z trong chuỗi tương ứng là các con số trong hệ cơ số 2626: a là 00, b là 11, ..., z là 2525. Ta xây dựng mảng ff với f_if
​i
​​  là Hash Value ứng với tiền tố có độ dài ii của chuỗi SS trong hệ cơ số 2626. Vì mảng ff có thể nhận 11 giá trị rất lớn, nên ta sẽ chia lấy dư cho một số nào đó. Và để tránh bị trùng ta sẽ chia lấy dư cho một số nguyên tố nào đó, chẳng hạn như MOD = 1000000007MOD=1000000007.

Gọi lenlen là chiều dài tiền tố của chuỗi SS, ta kiểm tra lần lượt từ 11 đến length(S) – 1 xem với độ dài lenlen thì tiền tố đó có phải là hậu tố trong chuỗi SS hay không, ta lấy giá trị của hậu tố có độ dài lenlen bằng công thức:

(f_{length(S)} - (f_{length(S) - len} * 26^{len}) \% MOD) \% MOD(f
​length(S)
​​ −(f
​length(S)−len
​​ ∗26
​len
​​ )%MOD)%MOD
Vì mảng ff đã được chia lấy dư cho MODMOD, nên sẽ có trường hợp f_{length(S)} - (f_{length(S) - len} * 26^{len}) \% MODf
​length(S)
​​ −(f
​length(S)−len
​​ ∗26
​len
​​ )%MOD trả về số âm, khi đó kết quả trả về không chính xác, nên ta sẽ cộng thêm MODMOD trước khi chia lấy dư.

Và để tránh tính lũy thừa của 2626 nhiều lần. Ta nên khởi tạo mảng lưu các giá trị lũy thừa của 2626 trước, để giảm độ phức tạp của bài toán.

Độ phức tạp: O(T * length(S))O(T∗length(S)) với TT là số lượng test case, length(S)length(S) là chiều dài của chuỗi SS.
 */
public class SuffixEqualPrefix {
}

/*
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
const long long MOD = 1e9 + 7;
const int MAXN = 1e6 + 1;
unsigned long long f[MAXN];
unsigned long long mul[MAXN];

void polyHash(string keys) {
    unsigned long long hashValue = 0;
    int n = keys.size();
    for (int i = 0; i < n; i++) {
        hashValue = (keys[i] - 'a' + (26 * hashValue) % MOD) % MOD;
        f[i + 1] = hashValue;
    }
}

int main() {
    //freopen("input.txt", "r", stdin);
    ios::sync_with_stdio(false);
    int test;
    string s;
    mul[0] = 1;
    for (int i = 1; i < MAXN; i++)
        mul[i] = (mul[i - 1] * 26) % MOD;
    cin >> test;
    for (int t = 1; t <= test; t++) {
        cin >> s;
        f[0] = 0;
        int n = s.size();
        polyHash(s);
        int res = 0, len = 1;
        while (len < n) {
            if (f[len] == ((f[n] - (f[n - len] * mul[len]) % MOD) + MOD) % MOD)
                res++;
            len++;
        }
        cout << "Case " << t << ": " << res << "\n";
    }
    return 0;
}
 */

/*
import java.util.Scanner;

public class Main {
    public static long MOD = (long)1e9 + 7;
    public static int MAXN = (int)1e6 + 1;
    public static long []f = new long [MAXN];
    public static long []mul = new long [MAXN];

    public static void polyHash(String keys) {
        long hashValue = 0;
        int n = keys.length();
        for (int i = 0; i < n; i++) {
            hashValue = (keys.charAt(i) - 'a' + (26 * hashValue) % MOD) % MOD;
            f[i + 1] = hashValue;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s, x;
        mul[0] = 1;
        for (int i = 1; i < MAXN; i++)
            mul[i] = (mul[i - 1] * 26) % MOD;

        int test = sc.nextInt(); x = sc.nextLine();
        for (int t = 1; t <= test; t++) {
            s = sc.next();
            f[0] = 0;
            int n = s.length();
            polyHash(s);
            int res = 0, len = 1;
            while (len < n) {
                if (f[len] == ((f[n] - (f[n - len] * mul[len]) % MOD) + MOD) % MOD) {
                    res++;
                }
                len++;
            }
            System.out.println("Case " + t + ": " + res);
        }
    }
}
 */