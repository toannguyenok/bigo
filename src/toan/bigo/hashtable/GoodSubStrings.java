package toan.bigo.hashtable;


/*
Hướng dẫn giải
Ý tưởng: với mỗi đoạn con trong chuỗi ss, nếu nó là chuỗi tốt thì bỏ nó vào một tập hợp, cuối cùng xem trong tập hợp đó có bao nhiêu phần tử khác nhau. Tuy nhiên nếu bỏ nguyên cả chuỗi vào tập hợp thì rất lãng phí vùng nhớ và thời gian, thay vì thế, ta sẽ chỉ bỏ mã hash của chuỗi vào.

Thuật toán:

Duy trì một cấu trúc Set để lưu các mã hash khác nhau.
Với mỗi vị trí ii trong chuỗi ss:
Duyệt các đoạn con kết thúc ở ii, đồng thời tính số ký tự xấu và mã hash của đoạn con đó.
Nếu số ký tự xấu trong chuỗi không quá kk thì cho mã hash hiện tại vào set.
Cuối cùng in ra kích thước của set.
Độ phức tạp: O(n^2)O(n
​2
​​ ) nếu dùng HashSet/unordered set, O(n^2 * log(n^2))O(n
​2
​​ ∗log(n
​2
​​ )) nếu dùng TreeSet/set, với nn là độ dài chuỗi.
 */
public class GoodSubStrings {
}


/*
#include <iostream>
#include <string>
#include <unordered_set>
using namespace std;

int main() {
    string s, b;
    int k;
    cin >> s >> b >> k;

    const int BASE = 29;

    unordered_set<uint64_t> good_strings;
    for (int i = 0; i < s.length(); ++i) {
        uint64_t hash = 0;
        for (int j = i, bad = 0; j >= 0; --j) {
            hash = hash * BASE + s[j] - 'a' + 1;
            bad += (b[s[j] - 'a'] == '0');
            if (bad <= k) {
                good_strings.insert(hash);
            }
            else {
                break;
            }
        }
    }

    cout << good_strings.size();
}
 */

/*
import java.io.*;
import java.util.*;

public class Main {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine(), b = sc.nextLine();
        int k = sc.nextInt();

        Set<Long> good_strings = new HashSet<>();
        final int BASE = 29;
        for (int i = 0; i < s.length(); ++i) {
            long hash = 0;
            for (int j = i, bad = 0; j >= 0; --j) {
                hash = hash * BASE + s.charAt(j) - 'a' + 1;
                bad += (b.charAt(s.charAt(j) - 'a') == '0' ? 1 : 0);
                if (bad <= k) {
                    good_strings.add(hash);
                }
                else {
                    break;
                }
            }
        }

        System.out.print(good_strings.size());
    }
}
 */