package toan.bigo.hashtable;

import java.util.Scanner;

public class SuffixEqualPrefix2 {

    public static long MOD = (long) 1e9 + 7;
    public static int MAXN = (int) 1e6 + 1;
    public static long[] f = new long[MAXN];
    public static long[] mul = new long[MAXN];

    public static void polyHash(String keys) {
        long hashValue = 0;
        int n = keys.length();
        for (int i = 0; i < n; i++) {
            hashValue = (keys.charAt(i) - 'a' + (26 * hashValue) % MOD) % MOD;
            f[i + 1] = hashValue;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s;
        mul[0] = 1;
        for (int i = 1; i < MAXN; i++) {
            mul[i] = (mul[i - 1] * 26) % MOD;
        }

        int test = sc.nextInt();
        for (int t = 1; t <= test; t++) {
            s = sc.next();
            f[0] = 0;
            int n = s.length();
            polyHash(s);
            int res = 0, len = 1;
            while (len < n) {
                if (f[len] == ((f[n] - (f[n - len] * mul[len]) % MOD) + MOD) % MOD) {
                    res++;
                }
                len++;
            }
            System.out.println("Case " + t + ": " + res);
        }
    }
}
