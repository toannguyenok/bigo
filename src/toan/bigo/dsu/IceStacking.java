package toan.bigo.dsu;

import java.util.ArrayList;
import java.util.Scanner;

/*
Giải thích ví dụ:

Ví dụ 1: Có 2 snow drift ở tọa độ (2, 1) và (1, 2). Không thể trượt trực tiếp từ snow drift này tới snow drift kia bằng 4 hướng Đông, Tây, Nam, Bắc. Do đó cần thêm một snow drift phụ, chẳng hạn như (1, 1).
Ví dụ 2: Có 2 snow drift ở tọa độ (2, 1) và (4, 1). Bajtek có thể trượt trực tiếp từ snow drift (2, 1) theo hướng Đông tới (4, 1) hoặc ngược lại. Do đó không cần đắp thêm một snow drift phụ nào.
Hướng dẫn giải:

Ta thấy rằng nếu hai snow drift A và B có cùng tọa độ x hoặc y thì Bajtek có thể trượt trực tiếp mà không cần đắp thêm drift phụ nào. Và nếu từ A đến được B và B đến được C theo cách như trên thì từ A ta cũng có thể đến C mà không cần thêm drift.

Như vậy nếu ta xem mỗi nhóm là tập hợp các snow drift mà ta có thể trượt trực tiếp đến nhau, thì số snow drift phụ mà ta cần đắp chính bằng số nhóm trừ 1. Lúc này ta có thể sử dụng DSU để giải quyết bài toán.

Độ phức tạp: O(N*M)O(N∗M) trong trường hợp sử dụng DSU cơ bản, nếu sử dụng DSU nâng cao (update by rank, path compression, by size) thì sẽ là O(M*logN)O(M∗logN) với NN là số điểm trong dữ liệu đầu vào và MM là số các kết nối tạo được giữa các điểm (tức tối đa M = N*(N - 1)/2M=N∗(N−1)/2).
 */
public class IceStacking {

    private static ArrayList<Integer> parent = new ArrayList<>();
    private static ArrayList<Integer> ranks = new ArrayList<>();
    private static ArrayList<Point> points = new ArrayList<>();

    private static int findSet(int u) {
        if (parent.get(u) != u) {
            parent.set(u, findSet(parent.get(u)));
        }
        return parent.get(u);
    }

    private static void unionSet(int u, int v) {
        int up = findSet(u);
        int vp = findSet(v);

        if (up != vp) {
            if (ranks.get(up) > ranks.get(vp)) {
                parent.set(vp, up);
            } else if (ranks.get(up) < ranks.get(vp)) {
                parent.set(up, vp);
            } else {
                parent.set(up, vp);
                ranks.set(vp, ranks.get(vp) + 1);
            }
        }
    }


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            parent.add(i);
            ranks.add(0);
            int x = sc.nextInt();
            int y = sc.nextInt();
            points.add(new Point(x, y));
        }

        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (points.get(i).x == points.get(j).x ||
                        points.get(i).y == points.get(j).y) {
                    unionSet(i, j);
                }
            }
        }

        int ans = -1;
        for (int i = 0; i < n; i++) {
            if (parent.get(i) == i) {
                ans++;
            }
        }
        System.out.println(ans + "");
    }

    public static class Point {
        int x;
        int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
/*
import java.io.*;
import java.util.*;

public class Main {
    public static class DSU { // chỉ được để 1 class
        Integer[] parent;
        public DSU(int size) {
            parent = new Integer[size];
            for (int i = 0; i < size; i++)
                parent[i] = i;
        }
        public int find(int u) {
            if (u == parent[u])
                return u;
            return parent[u] = find(parent[u]);
        }
        public void union(int u, int v) {
            u = find(u);
            v = find(v);
            parent[v] = u;
        }
        public int count() {
            int res = 0;
            for (int i = 0; i < parent.length; i++)
                if (i == parent[i])
                    res++;
            return res;
        }
    }
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] x = new int[n];
        int[] y = new int[n];
        for (int i = 0; i < n; i++) {
            x[i] = sc.nextInt();
            y[i] = sc.nextInt();
        }
        DSU dsu = new DSU(n);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < i; j++)
                if (x[i] == x[j] || y[i] == y[j])
                    dsu.union(i, j);
        System.out.println(dsu.count() - 1);
    }
}
 */