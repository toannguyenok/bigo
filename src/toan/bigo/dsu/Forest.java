package toan.bigo.dsu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

/*
Giải thích: Ví dụ trên gồm 1 bộ test:

Người 1 và người 3 cùng nghĩ tập hợp cây {2, 3} đã gây ra tiếng ngã. Còn người 2 lại nghĩ là cây {2, 4}. Như vậy ở đây ta có 2 luồng ý kiến.

Hướng dẫn giải: Ta có thể sử dụng DSU để giải bài này như sau:

Với mỗi người, ta sử dụng một mảng lưu những cây mà họ nghĩ đã gây ra tiếng ngã.
Sau khi đọc xong dữ liệu, ta tiến hành thực hiện thao tác union với những cặp người nào có các phần tử trong mảng lưu giống nhau toàn bộ.
Cuối cùng in ra số lượng tập hợp được tạo thành. Biết rằng một đỉnh mà có đỉnh cha bằng với chính nó được xem là một phần tử đại diện của tập hợp.
Độ phức tạp: O(Q * P^2 * (T + P))O(Q∗P
​2
​​ ∗(T+P)) cho DSU cơ bản hoặc O(P * P^2 * (T + log(P)))O(P∗P
​2
​​ ∗(T+log(P))) cho DSU nâng cao với QQ, PP, TT lần lượt là số trường hợp, số người và số cây trong mỗi trường hợp.
 */
public class Forest {

    private static ArrayList<Integer> parent = new ArrayList<>();
    private static ArrayList<Integer> ranks = new ArrayList<>();
    private static ArrayList<ArrayList<Integer>> treesets = new ArrayList<>();

    private static void makeSet(int n) {
        for (int i = 0; i < n; i++) {
            parent.add(i);
            ranks.add(0);
        }
    }

    private static int findSet(int u) {
        if (parent.get(u) != u) {
            parent.set(u, findSet(parent.get(u)));
        }
        return parent.get(u);
    }

    private static void unionSet(int u, int v) {
        int up = findSet(u);
        int vp = findSet(v);

        if (up != vp) {
            if (ranks.get(up) > ranks.get(vp)) {
                parent.set(vp, up);
            } else if (ranks.get(up) < ranks.get(vp)) {
                parent.set(up, vp);
            } else {
                parent.set(up, vp);
                ranks.set(vp, ranks.get(vp) + 1);
            }
        }
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int testCase = Integer.parseInt(sc.nextLine());
        ArrayList<Integer> result = new ArrayList<>();
        sc.nextLine();
        for (int t = 0; t < testCase; t++) {
            int count = 0;
            parent.clear();
            ranks.clear();
            treesets.clear();
            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                int n = 0;
                if (s.equals("")) {
                    break;
                } else {
                    count++;
                    if (count == 1) {
                        n = Integer.parseInt(s.split(" ")[0]);
                        int m = Integer.parseInt(s.split(" ")[1]);
                        for (int i = 0; i < n; i++) {
                            parent.add(i);
                            ranks.add(0);
                            treesets.add(new ArrayList<>());
                        }
                    } else {
                        int p = Integer.parseInt(s.split(" ")[0]) - 1;
                        int tree = Integer.parseInt(s.split(" ")[1]) - 1;
                        treesets.get(p).add(tree);
                    }
                }
            }
            for (int i = 0; i < parent.size(); i++) {
                Collections.sort(treesets.get(i));
            }
            for (int i = 0; i < parent.size(); i++) {
                for(int j = i  + 1;j<parent.size();j++) {
                    if (doCollectionsContainSameElements(treesets.get(i), treesets.get(j))) {
                        unionSet(i, j);
                    }
                }
            }
            int ans = 0;
            for (int i = 0; i < parent.size(); i++) {
                if (parent.get(i) == i) {
                    ans++;
                }
            }
            result.add(ans);
        }
        for (Integer integer : result) {
            System.out.println(integer + "");
            System.out.println();
        }
    }

    public static boolean doCollectionsContainSameElements(
            Collection<Integer> c1, Collection<Integer> c2) {

        if (c1 == null || c2 == null) {
            return false;
        } else if (c1.size() != c2.size()) {
            return false;
        } else {
            return c1.containsAll(c2) && c2.containsAll(c1);
        }
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 105;
    static int[] parent = new int[MAX];
    static int[] ranks = new int[MAX];
    static Set<Integer>[] trees = new TreeSet[MAX];

    private static int findSet(int u) {
        if (u != parent[u]) {
            parent[u] = findSet(parent[u]);
        }
        return parent[u];
    }

    private static void unionSet(int u, int v) {
        int up = findSet(u);
        int vp = findSet(v);

        if (up == vp) {
            return;
        }

        if (ranks[up] > ranks[vp]) {
            parent[vp] = up;
        }
        else if (ranks[up] < ranks[vp]) {
            parent[up] = vp;
        }
        else {
            parent[up] = vp;
            ranks[vp]++;
        }
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int Q = sc.nextInt();

        for (int i = 0; i < MAX; i++) {
            trees[i] = new TreeSet<>();
        }

        while (Q-- > 0) {
            int P = sc.nextInt();
            int T = sc.nextInt();
            sc.nextLine();

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (line.length() == 0) {
                    break;
                }

                String[] arrOfStr = line.split(" ");
                int person = Integer.parseInt(arrOfStr[0]);
                int tree = Integer.parseInt(arrOfStr[1]);
                trees[person].add(tree);
            }

            for (int i = 1; i <= P; i++) {
                parent[i] = i;
                ranks[i] = 0;
            }

            for (int i = 1; i <= P; i++) {
                for (int j = i + 1; j <= P; j++) {
                    if (trees[i].equals(trees[j])) {
                        unionSet(i, j);
                    }
                }
            }

            int nSets = 0;
            for (int i = 1; i <= P; i++) {
                if (i == parent[i]) {
                    nSets++;
                }
                trees[i].clear();
            }

            System.out.println(nSets);
            if (Q > 0) {
                System.out.println();
            }
        }
    }
}
 */