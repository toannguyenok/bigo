package toan.bigo.dsu;

import java.util.ArrayList;
import java.util.Scanner;

/*
Số lượng người tham gia cuộc đàm phán là 10, ta có 2 tập hợp friends = {} và enemies {}.

1 0 1 -> friends = {0, 1}, enemies = {}
1 1 2 -> friends = {0, 1, 2}, enemies = {}
2 0 5 -> friends = {0, 1, 2}, enemies = {0, 5}
3 0 2, lúc này 0 và 2 là bạn nên in 1, friends = {0, 1, 2}, enemies = {0, 5}
3 8 9, lúc này chưa có thông tin gì về 8 và 9 là bạn hay kẻ thù nên in ra 0.
4 1 5, friends = {0, 1, 2}, enemies = {0, 5}. Vì 1~0 và 05 nên 15 -> in 1.
4 1 2, friends = {0, 1, 2}, enemies = {0, 5}. 1~2 -> in 0.
4 8 9, lúc này chưa có thông tin gì về 8 và 9 là bạn hay kẻ thù nên in ra 0.
1 8 9, friends = {(0, 1, 2), (8, 9)}, enemies = {0, 5}
1 5 2, friends = {(0, 1, 2), (8, 9)}, enemies = {0, 5}. Vì 0~2 và 05 nên 25 nên không thể set họ là bạn -> in -1.
3 5 2, friends = {(0, 1, 2), (8, 9)}, enemies = {0, 5}. Vì 0~2 và 05 nên 25 -> in 0.
0 0 0, dừng.
Hướng dẫn giải:

Với bài toán này ta sẽ đưa về một tập hợp để xét và sẽ dùng thuật toán DSU để giải quyết vấn đề này. Với dữ liệu đề bài trong một cuộc đàm phán sẽ có tối đa 10000 người (MAX = 10000MAX=10000), vì vậy ta sẽ quy định trong một tập hợp gồm có 2 khoảng [0, 9999)[0,9999) và [10000, 19999)[10000,19999) nếu cả 2 người cần xét cùng nằm trên 1 khoảng thì sẽ là bạn ngược lại là kẻ thù. Khi đó:

Với hoạt động 1: Đầu tiên sẽ phải kiểm tra xx và y + MAXy+MAXcó cùng nằm trong tập hợp hiện tại hay không?
Nếu có thì in ra -1 và bỏ qua hoạt động setFriendssetFriends vì họ chắc chắn đã là kẻ thù.
Nếu không, thì ta sẽ gom xx, yy vào cùng nhóm tương tự gom x + MAXx+MAX và y + MAXy+MAX vào cùng nhóm.
Với hoạt động 2: Đầu tiên sẽ phải kiểm tra xx và yy có cùng nằm trong tập hợp hiện tại hay không?
Nếu có thì in ra -1−1 và bỏ qua hoạt động setEnemiessetEnemies vì họ chắc chắn đã là bạn.
Nếu không, thì ta sẽ gom xx và y + MAXy+MAX vào cùng nhóm, tương tự gom x + MAXx+MAX và yy vào cùng nhóm.
Với hoạt động 3: Nếu xx và yy thuộc cùng 1 nhóm thì in 1 ngược lại in 0.
Với hoạt động 4: Nếu xx và y + MAXy+MAX thuộc cùng 1 nhóm thì in 1 ngược lại in 0.

 */
public class War {

    private static ArrayList<Integer> parent = new ArrayList<>();
    private static ArrayList<Integer> ranks = new ArrayList<>();


    private static void makeSet(int n) {
        for (int i = 0; i < 2 * n; i++) {
            parent.add(i);
            ranks.add(0);
        }
    }

    private static int findSet(int u) {
        if (parent.get(u) != u) {
            parent.set(u, findSet(parent.get(u)));
        }
        return parent.get(u);
    }

    private static void unionSet(int u, int v) {
        int up = findSet(u);
        int vp = findSet(v);

        if (up != vp) {
            if (ranks.get(up) > ranks.get(vp)) {
                parent.set(vp, up);
            } else if (ranks.get(up) < ranks.get(vp)) {
                parent.set(up, vp);
            } else {
                parent.set(up, vp);
                ranks.set(vp, ranks.get(vp) + 1);
            }
        }
    }

    private static int getEnemyID(int u, int n) {
        return u + n;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine().split(" ")[0]);
        ArrayList<Integer> result = new ArrayList<>();
        makeSet(n);
        boolean flag = true;
        while (flag) {
            String line = sc.nextLine();
            int q = Integer.parseInt(line.split(" ")[0]);
            int u = Integer.parseInt(line.split(" ")[1]);
            int v = Integer.parseInt(line.split(" ")[2]);
            if (q == 0 && u == 0 && v == 0) {
                flag = false;
            } else {
                if (q == 1) {
                    if (findSet(u) == findSet(getEnemyID(v, n)) || findSet(v) == findSet(getEnemyID(u, n))) {
                        result.add(-1);
                    } else {
                        unionSet(u, v);
                        unionSet(getEnemyID(u, n), getEnemyID(v, n));
                    }
                } else if (q == 2) {
                    if (findSet(u) == findSet(v) || findSet(getEnemyID(u, n)) == findSet(getEnemyID(v, n))) {
                        result.add(-1);
                    } else {
                        unionSet(u, getEnemyID(v, n));
                        unionSet(v, getEnemyID(u, n));
                    }
                } else if (q == 3) {
                    if (findSet(u) == findSet(v) || findSet(getEnemyID(u, n)) == findSet(getEnemyID(v, n))) {
                        result.add(1);
                    } else {
                        result.add(0);
                    }
                } else if (q == 4) {
                    if (findSet(u) == findSet(getEnemyID(v, n)) || findSet(v) == findSet(getEnemyID(u, n))) {
                        result.add(1);
                    } else {
                        result.add(0);
                    }
                }
            }
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i) + "");
        }
    }
}
/*
import java.util.Scanner;
import java.util.Map;
import java.util.TreeSet;


class DSU{
  int[] parent;
  int n;
  public DSU(int x){
    n = x;
    parent = new int[x];
    for (int i=0;i< n;i++) {
      parent[i]=i;
    }
  }
  public int find(int u){
    if (parent[u]!=u) parent[u]=find(parent[u]);
    return parent[u];
  }
  public void union(int u ,int v){
    int i = find(u);
    int j = find(v);
    if (i!=j) {
      parent[j]=i;
    }
  }
}
class Main {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int MAX = 10000;
    int n,c,x,y;
    n = sc.nextInt();
    DSU dsu = new DSU(20002);
    while (true) {
      c = sc.nextInt();
      x = sc.nextInt();
      y = sc.nextInt();

      if (c == 0 && x == 0 && y == 0) {
        break;
      }
      if (c == 1) {
        if (dsu.find(x) == dsu.find(y + MAX)) {
          System.out.println(-1);
          continue;
        }

      dsu.union(x, y);
      dsu.union(x + MAX, y + MAX);
      }
      else if (c == 2) {
        if (dsu.find(x) == dsu.find(y)) {
          System.out.println(-1);
          continue;
        }
        dsu.union(x, y + MAX);
        dsu.union(x + MAX, y);
      }
      else if (c == 3) {
        if(dsu.find(x) == dsu.find(y)){
          System.out.println(1);
        }
        else
          System.out.println(0);
      }
      else if (c == 4) {
        if(dsu.find(x) == dsu.find(y+MAX)){
          System.out.println(1);
        }
        else
          System.out.println(0);
      }
    }
  }
}
 */