package toan.bigo.dsu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/*
Ví dụ gồm 2 bộ test:

Bộ 1: Tất cả mọi người đều nằm trong cùng một nhóm bạn. Do đó nhóm bạn có số lượng lớn nhất sẽ là 3.
Bộ 2: Với dữ liệu đề cho, ta thu được 2 nhóm bạn như hình bên: một nhóm có 3 bạn và một nhóm có 7 bạn. Như vậy ta chọn nhóm có nhiều người hơn là 7 bạn.
Hướng dẫn giải:
Ta có thể sử dụng DSU để giải bài toán này như sau:

Với mỗi cặp bạn bè, ta sử dụng thao tác union để đưa họ vào cùng một nhóm bạn.
Tuy nhiên, để dễ dàng biết được trong một nhóm bạn có bao nhiêu người, ta sử dụng thêm một mảng đếm số lượng phần tử ứng với phần tử đại diện. Khi hợp tập hợp của uu và vv, ta cộng dồn số lượng phần tử của tập con vào số lượng phần tử của tập được chọn làm cha.
Cuối cùng in ra số lượng người trong nhóm đông nhất, hay số phần tử trong tập hợp lớn nhất.
Độ phức tạp: O(T*N*M)O(T∗N∗M) trong trường hợp sử dụng DSU cơ bản, nếu sử dụng DSU nâng cao (update by rank, path compression, by size) thì sẽ là O(T*M*log(N))O(T∗M∗log(N)).
 */
public class Friends {

    private static ArrayList<Integer> parent = new ArrayList<>();
    private static ArrayList<Integer> ranks = new ArrayList<>();
    private static ArrayList<Integer> size = new ArrayList<>();

    private static void makeSet(int n) {
        for (int i = 0; i < n; i++) {
            parent.add(i);
            ranks.add(0);
            size.add(1);
        }
    }

    private static int findSet(int u) {
        if (parent.get(u) != u) {
            parent.set(u, findSet(parent.get(u)));
        }
        return parent.get(u);
    }

    private static void unionSet(int u, int v) {
        int up = findSet(u);
        int vp = findSet(v);

        if (up != vp) {
            if (ranks.get(up) > ranks.get(vp)) {
                parent.set(vp, up);
                size.set(up, size.get(up) + size.get(vp));
            } else if (ranks.get(up) < ranks.get(vp)) {
                parent.set(up, vp);
                size.set(vp, size.get(vp) + size.get(up));
            } else {
                parent.set(up, vp);
                ranks.set(vp, ranks.get(vp) + 1);
                size.set(vp, size.get(vp) + size.get(up));
            }

        }
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int testCase = sc.nextInt();
        ArrayList<Integer> result = new ArrayList<>();
        for (int t = 0; t < testCase; t++) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            parent.clear();
            ranks.clear();
            size.clear();
            makeSet(n);
            for (int i = 0; i < m; i++) {
                int u = sc.nextInt() - 1;
                int v = sc.nextInt() - 1;
                unionSet(u, v);
            }
            int ans = -1;
            for (int i = 0; i < n; i++) {
                ans = Math.max(ans, size.get(i));
            }
            result.add(ans);
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i) + "");
        }
    }
}
/*
import java.util.*;

public class Main {
	static final int MAX = 30005;
	static int[] parent = new int[MAX];
	static int[] cnt = new int[MAX];

	private static int findSet(int u) {
		while (u != parent[u]) {
			u = parent[u];
		}
		return u;
	}

	private static void unionSet(int u, int v) {
		int up = findSet(u);
		int vp = findSet(v);
		if (up != vp) {
			parent[up] = vp;
			cnt[vp] += cnt[up];
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();

		while (t-- > 0) {
			int n = sc.nextInt();
			int m = sc.nextInt();

			for (int i = 1; i <= n; i++) {
				parent[i] = i;
				cnt[i] = 1;
			}

			for (int i = 0; i < m; i++) {
				int u = sc.nextInt();
				int v = sc.nextInt();
				unionSet(u, v);
			}

			int res = -1;
			for (int i = 1; i <= n; i++) {
				res = Math.max(res, cnt[i]);
			}

			System.out.println(res);
		}
	}
}
 */