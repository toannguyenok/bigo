package toan.bigo.dsu;

import java.util.ArrayList;
import java.util.Scanner;

/*
Sử dụng DSU, khi gặp các cạnh thì ta thực hiện gộp hai tập hợp chứa hai đỉnh của cạnh lại với nhau thành một thành phần liên thông lớn. Đến cuối thì ta duyệt qua danh sách đỉnh, nếu đỉnh nào có parentparent là chính nó (hoặc -1, tùy giá trị khởi tạo) thì nghĩa là đây là đỉnh đại diện của một tập hợp mới (một thành phần liên thông cực đại). Vậy kết quả của ta chính là số lượng đỉnh như vậy.

Độ phức tạp O(T*N*M)O(T∗N∗M) trong trường hợp sử dụng DSU cơ bản, nếu sử dụng DSU nâng cao (update by rank, path compression, by size) thì sẽ là O(T*M*logN)O(T∗M∗logN) với TT là số bộ test, NN là số đỉnh của đồ thị và MM là số cạnh tối đa của đồ thị.
 */
public class GraphConnectivity {

    private static ArrayList<Integer> parent = new ArrayList<>();
    private static ArrayList<Integer> ranks = new ArrayList<>();

    private static void makeSet(int n) {
        for (int i = 0; i < n; i++) {
            parent.add(i);
            ranks.add(0);
        }
    }

    private static int findSet(int u) {
        if (parent.get(u) != u) {
            parent.set(u, findSet(parent.get(u)));
        }
        return parent.get(u);
    }

    private static void unionSet(int u, int v) {
        int up = findSet(u);
        int vp = findSet(v);

        if (up != vp) {
            if (ranks.get(up) > ranks.get(vp)) {
                parent.set(vp, up);
            } else if (ranks.get(up) < ranks.get(vp)) {
                parent.set(up, vp);
            } else {
                parent.set(up, vp);
                ranks.set(vp, ranks.get(vp) + 1);
            }
        }
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int testCase = Integer.parseInt(sc.nextLine());
        ArrayList<Integer> result = new ArrayList<>();
        sc.nextLine();
        for (int t = 0; t < testCase; t++) {
            int count = 0;
            parent.clear();
            ranks.clear();
            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                if (s.equals("")) {
                    break;
                } else {
                    count++;
                    if (count == 1) {
                        char maxChar = s.charAt(0);
                        makeSet((maxChar - 'A') + 1);
                    } else {
                        for (int i = 1; i < s.length(); i++) {
                            unionSet(s.charAt(i - 1) - 'A', s.charAt(i) - 'A');
                        }
                    }
                }
            }
            int ans = 0;
            for (int i = 0; i < parent.size(); i++) {
                if (parent.get(i) == i) {
                    ans++;
                }
            }
            result.add(ans);
        }
        for (Integer integer : result) {
            System.out.println(integer + "");
            System.out.println();
        }
    }
}
/*
import java.io.*;
import java.util.*;

class DSU {
    Integer[] parent;
    public DSU(int size) {
        parent = new Integer[size];
        for (int i = 0; i < size; i++)
            parent[i] = i;
    }
    public int find(int u) {
        while (u != parent[u])
            u = parent[u];
        return u;
    }
    public void union(int u, int v) {
        u = find(u);
        v = find(v);
        parent[v] = u;
    }
}
class Main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        sc.nextLine();
        while (T-- > 0) {
            int n = sc.nextLine().charAt(0) - 'A' + 1;
            DSU dsu = new DSU(n);
            int ans = n;
            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                if (s.isEmpty())
                    break;
                int u = dsu.find(s.charAt(0) - 'A');
                int v = dsu.find(s.charAt(1) - 'A');
                if (u != v) {
                    ans--;
                    dsu.union(u, v);
                }
            }
            System.out.println(ans);
            if (T > 0)
                System.out.println();
        }
    }
}
 */