package toan.bigo.dsu;

import java.util.ArrayList;
import java.util.Scanner;

/*
Giải thích ví dụ:

Ví dụ 1: Đồ thị có chu trình là (1, 4, 5). Các cây nối từ 3 gốc là (1), (4, 6, 3) và (5, 2).
Ví dụ 2: Đồ thị chỉ là một cây chứ không có chu trình.
Hướng dẫn giải: Ta thấy rằng hình ảnh Cthulhu thực chất một đồ thị liên thông và có đúng một chu trình, vậy thì nếu ta bỏ một cạnh trong chu trình ra thì phần còn lại sẽ tạo thành một cây khung của đồ thị. Như vậy, ta suy ra điều kiện đề đồ thị đã cho là Cthulhu: - Đồ thị có số cạnh bằng số đỉnh (vì đồ thị NN đỉnh sẽ có cây khung N – 1 cạnh, thêm 1 cạnh dư tạo thành chu trình nữa thì đồ thị đó sẽ phải có đúng NN cạnh). - Đồ thị này liên thông với nhau. Như vậy, ta chỉ cần kiểm tra xem NN có bằng MM hay không, nếu thỏa, đọc vào các cạnh và sử dụng DSU để gom các tập đỉnh lại với nhau, lúc này kiểm tra nếu đồ thị liên thông (tức NN đỉnh chỉ thuộc một tập duy nhất) thì có thể kết luận đây là Cthulhu. Lưu ý rằng đề bài có yêu cầu số đỉnh tối thiểu phải là 3. Nhưng vì đồ thị nếu có 1 hoặc 2 đỉnh thì sẽ không thể tạo thành chu trình (vì đồ thị không có cạnh song song hoặc tự vòng), nên ta sẽ không cần phải kiểm tra điều kiện này.

Độ phức tạp: O(N*M)O(N∗M) trong trường hợp sử dụng DSU cơ bản, nếu sử dụng DSU nâng cao (update by rank, path compression, by size) thì sẽ là O(M*logN)O(M∗logN) với NN là số đỉnh và MM là số cạnh.
 */
public class Cthulhu {

    private static ArrayList<Integer> parent = new ArrayList<>();
    private static ArrayList<Integer> ranks = new ArrayList<>();

    private static void makeSet(int n) {
        for (int i = 0; i < n; i++) {
            parent.add(i);
            ranks.add(0);
        }
    }

    private static int findSet(int u) {
        if (parent.get(u) != u) {
            parent.set(u, findSet(parent.get(u)));
        }
        return parent.get(u);
    }

    private static void unionSet(int u, int v) {
        int up = findSet(u);
        int vp = findSet(v);

        if (up != vp) {
            if (ranks.get(up) > ranks.get(vp)) {
                parent.set(vp, up);
            } else if (ranks.get(up) < ranks.get(vp)) {
                parent.set(up, vp);
            } else {
                parent.set(up, vp);
                ranks.set(vp, ranks.get(vp) + 1);
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        makeSet(n);
        for (int i = 0; i < m; i++) {
            int x, y, f1, f2;
            x = sc.nextInt() - 1;
            y = sc.nextInt() - 1;
            f1 = findSet(x);
            f2 = findSet(y);
            unionSet(f1, f2);
        }
        int flag = 1;
        if (m != n)
            flag = 0;
        for (int i = 0; i < n; i++) {
            int temp1 = findSet(i);
            int temp2 = findSet(0);
            if (temp1 != temp2)
                flag = 0;
        }
        if (flag == 1)
            System.out.printf("FHTAGN!");
        else
            System.out.printf("NO");
    }

}

/*
package main;

import java.util.*;
import java.io.*;


public class Main
{
    static PrintWriter out;
    static final int MAX  = 110;
    static int parent[] = new int[MAX];
    static int ranks[] = new int[MAX];
    public static int Findset(int parent[], int i) {
        if(parent[i]!=i){
            return Findset(parent, parent[i]);
        }
        else
            return i;
    }
    public static void Unionset(int parent[], int x, int y)
    {
        int xroot = Findset(parent, x);
        int yroot = Findset(parent, y);

        if (ranks[xroot] < ranks[yroot])
            parent[xroot] = yroot;
        else if (ranks[xroot] > ranks[yroot])
            parent[yroot] = xroot;
        else{
            parent[yroot] = xroot;
            ranks[xroot]++;
        }
    }

    public static void main(String[] args) {
        MyScanner in = new MyScanner();
        out = new PrintWriter(new BufferedOutputStream(System.out), true);
        int i, j, k, n, m;
        n = in.nextInt();
        m = in.nextInt();
        for(i=0; i<=n; i++)
            parent[i] = i;
        for(i=0; i<m; i++)
        {
            int x, y, f1, f2;
            x = in.nextInt();
            y = in.nextInt();
            f1 = Findset(parent, x);
            f2 = Findset(parent, y);
            Unionset(parent, f1, f2);
        }
        int flag = 1;
        if(m!=n)
            flag = 0;
        for(i=1; i<=n; i++)
        {
            int temp1 = Findset(parent, i);
            int temp2 = Findset(parent, 1);
            if(temp1 != temp2)
                    flag = 0;
        }
        if(flag==1)
                out.printf("FHTAGN!");
        else
                out.printf("NO");
        }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                        st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
                return Integer.parseInt(next());
        }
        long nextLong() {
                return Long.parseLong(next());
        }
        double nextDouble() {
                return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
 */