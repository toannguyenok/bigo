package toan.bigo.dfs;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

/*
Lần lượt chạy DFSDFS cho các đỉnh, nếu mỗi lần chạy vẫn còn đỉnh nào có giá trị falsefalse nghĩa là vẫn còn nhóm nào đó khác nhóm trước đã chạy. Bạn sẽ tăng biến đếm lên. Khi chạy hết NN đỉnh thì kết quả cuối cùng là biến đếm.

Độ phức tạp: O(T * (V + E))O(T∗(V+E)) với TT là số lượng bộ dữ liệu, O(V + E)O(V+E) là độ phức tạp của thuật toán DFSDFS với VV là số lượng đỉnh của đồ thị và EE là số lượng cạnh của đồ thị.
 */
public class Prayatna {

    public static void main(String[] args) {

        int testcase;
        Scanner sc = new Scanner(System.in);
        testcase = sc.nextInt();

        ArrayList<Integer> result = new ArrayList<>();

        for (int i = 0; i < testcase; i++) {
            result.add(findResult(sc));
        }

        for (int i = 0; i < testcase; i++) {
            System.out.println(result.get(i) + "");
        }
    }

    private static int findResult(Scanner sc) {
        int n;
        n = sc.nextInt();
        ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
        ArrayList<Boolean> visited = new ArrayList<>();
        ArrayList<Integer> path = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
            visited.add(false);
            path.add(-1);
        }

        int descriptions = sc.nextInt();

        for (int i = 0; i < descriptions; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            graph.get(u).add(v);
            graph.get(v).add(u);
        }

        if (descriptions == 0) {
            return n;
        }

        int result = 0;
        for (int i = 0; i < visited.size(); i++) {
            if (!visited.get(i)) {
                result++;
                dfs(graph, visited, path, i);
            }
        }

        return result;
    }

    public static ArrayList<Integer> dfs(ArrayList<ArrayList<Integer>> graph,
                                         ArrayList<Boolean> visited,
                                         ArrayList<Integer> path,
                                         int firstU) {
        Stack<Integer> stack = new Stack<>();
        stack.add(firstU);
        visited.set(firstU, true);
        while (!stack.isEmpty()) {
            int u = stack.pop();
            for (int i = 0; i < graph.get(u).size(); i++) {
                int nextU = graph.get(u).get(i);
                if (!visited.get(nextU)) {
                    visited.set(nextU, true);
                    path.set(nextU, u);
                    stack.add(nextU);
                }
            }
        }
        return path;
    }
}
/*
import java.util.*;
import java.io.*;

public class Main {
    static final int MAX = 100000 + 5;
    static int V, E;
    static boolean[] visited = new boolean[MAX];
    static ArrayList<Integer> graph[] = new ArrayList[MAX];

    public static void DFS(int scr) {
        Stack<Integer> s = new Stack<>();
        visited[scr] = true;
        s.add(scr);

        while (!s.isEmpty()) {
            int u = s.pop();

            for (int v : graph[u]) {
                if (!visited[v]) {
                    visited[v] = true;
                    s.add(v);
                }
            }
        }
    }

    public static void main(String[] agrs) {
        MyScanner in = new MyScanner();
        PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out), true);
        int Q = in.nextInt();

        while (Q-- > 0) {
            V = in.nextInt();
            E = in.nextInt();

            for (int i = 0; i < V; i++) {
                visited[i] = false;
                graph[i] = new ArrayList<>();
            }

            for (int i = 0; i < E; i++) {
                int u = in.nextInt();
                int v = in.nextInt();
                graph[u].add(v);
                graph[v].add(u);
            }

            int count = 0;

            for (int i = 0; i < V; i++) {
                if (!visited[i]) {
                    count++;
                    DFS(i);
                }
            }

            out.println(count);
        }
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }

        long nextLong() {
            return Long.parseLong(next());
        }

        double nextDouble() {
            return Double.parseDouble(next());
        }

        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
 */
