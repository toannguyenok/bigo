package toan.bigo.dfs;

import java.util.*;

/*
Ta sẽ có một mảng lưu các hồ nước có thể lấp. Mỗi hồ nước sẽ bao gồm các ô chứa trong hồ nước đó.

Sử dụng thuật toán DFSDFS chạy từ các ô là nước mà ta chưa viếng thăm trước đây. Trong thuật DFSDFS, ta đánh dấu với những hồ nào thuộc biển thì không tiến hành đưa vào mảng lưu hồ nước.

Sắp xếp lại mảng lưu hồ nước theo thứ tự tăng dần kích thước và chọn ra các hồ nước có kích thước nhỏ nhất cần lấp để được kk hồ.

Độ phức tạp: O(n * m)O(n∗m) với nn và mm lần lượt là kích thước của bảng.
 */
public class LakesInBerland {

    static ArrayList<ArrayList<Boolean>> visited = new ArrayList<>();
    static ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        getResult(sc);
    }

    private static int[] dx = new int[]{0, 0, 1, -1};
    private static int[] dy = new int[]{1, -1, 0, 0};

    private static void getResult(Scanner sc) {
        int n;
        int m;
        int k;

        n = sc.nextInt();
        m = sc.nextInt();
        k = sc.nextInt();

        ArrayList<Point> lakes = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            matrix.add(new ArrayList<>());
            visited.add(new ArrayList<>());
            String str = sc.next();
            for (int j = 0; j < m; j++) {
                char symbol = str.charAt(j);
                if (symbol == '.') {
                    matrix.get(i).add(0);
                } else {
                    matrix.get(i).add(1);
                }
                visited.get(i).add(false);
            }
        }

        //first, mark all lake near ocean
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if ((i == 0 || i == n - 1 || j == 0 || j == m - 1) &&
                        matrix.get(i).get(j) == 0 &&
                        !visited.get(i).get(j)) {
                    dfsOcean(n, m, i, j);
                }
            }
        }

        //find lakes available
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (matrix.get(i).get(j) == 0 &&
                        !visited.get(i).get(j)) {
                    int counter = dfs(n, m, i, j);
                    Point point = new Point(i, j);
                    point.sum = counter;
                    lakes.add(point);
                }
            }
        }

        //sort lakes
        Collections.sort(lakes);

        int numberChange = 0;
        int totalLakes = lakes.size();
        for (Point lake : lakes) {
            if (totalLakes == k) {
                break;
            }
            dfsChange(n, n, lake.x, lake.y);
            numberChange += lake.sum;
            totalLakes--;
        }
        System.out.println(numberChange + "");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (matrix.get(i).get(j) == 1) {
                    System.out.print("*");
                } else {
                    System.out.print(".");
                }
            }
            System.out.println();
        }
    }

    //1  = land
    //0 = water

    public static void dfsOcean(int n,
                                int m,
                                int x,
                                int y) {
        Stack<Point> stack = new Stack<>();
        stack.add(new Point(x, y));
        visited.get(x).set(y, true);
        while (!stack.isEmpty()) {
            Point u = stack.pop();
            for (int i = 0; i < 4; i++) {
                int newX = dx[i] + u.x;
                int newY = dy[i] + u.y;

                if (newX >= 0 &&
                        newX < n &&
                        newY >= 0 &&
                        newY < m &&
                        !visited.get(newX).get(newY) &&
                        matrix.get(newX).get(newY) == 0) {
                    visited.get(newX).set(newY, true);
                    stack.add(new Point(newX, newY));
                }
            }
        }
    }

    public static int dfs(int n,
                          int m,
                          int x,
                          int y) {
        int counter = 1;
        Stack<Point> stack = new Stack<>();
        stack.add(new Point(x, y));
        visited.get(x).set(y, true);
        while (!stack.isEmpty()) {
            Point u = stack.pop();
            for (int i = 0; i < 4; i++) {
                int newX = dx[i] + u.x;
                int newY = dy[i] + u.y;

                if (newX >= 0 &&
                        newX < n &&
                        newY >= 0 &&
                        newY < m &&
                        !visited.get(newX).get(newY) &&
                        matrix.get(newX).get(newY) == 0) {
                    counter++;
                    visited.get(newX).set(newY, true);
                    stack.add(new Point(newX, newY));
                }
            }
        }
        return counter;
    }

    public static void dfsChange(int n,
                                 int m,
                                 int x,
                                 int y) {
        Stack<Point> stack = new Stack<>();
        stack.add(new Point(x, y));
        matrix.get(x).set(y, 1);
        while (!stack.isEmpty()) {
            Point u = stack.pop();
            for (int i = 0; i < 4; i++) {
                int newX = dx[i] + u.x;
                int newY = dy[i] + u.y;

                if (newX >= 0 &&
                        newX < n &&
                        newY >= 0 &&
                        newY < m &&
                        matrix.get(newX).get(newY) == 0) {
                    matrix.get(newX).set(newY, 1);
                    stack.add(new Point(newX, newY));
                }
            }
        }
    }

    private static class Point implements Comparable<Point> {
        int x;
        int y;
        int sum;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int compareTo(Point o) {
            if (sum == o.sum) {
                if (x == o.x) {
                    return y - o.y;
                } else {
                    return x - o.x;
                }
            }
            return this.sum - o.sum;
        }
    }
}
/*import java.util.*;

public class Main {
    static final int MAX = 51;
    static int[] dr = {0, 0, 1, -1};
    static int[] dc = {1, -1, 0, 0};
    static int n, m, k;
    static boolean[][] visited = new boolean[MAX][MAX];
    static char[][] table = new char[MAX][MAX];

    static class Cell {
        int r, c;

        public Cell(int _r, int _c) {
            this.r = _r;
            this.c = _c;
        }
    };

    static ArrayList<ArrayList<Cell>> lakes = new ArrayList<ArrayList<Cell>>();

    public static boolean isValid(int r, int c) {
        return r >= 0 && c >= 0 && r < n && c < m;
    }

    public static boolean onBorder(int r, int c) {
        return r == 0 || c == 0 || r == n - 1 || c == m - 1;
    }

    public static void DFS(Cell scr) {
        Stack<Cell> s = new Stack<>();
        visited[scr.r][scr.c] = true;
        s.add(scr);

        boolean isOcean = false;
        ArrayList<Cell> temp = new ArrayList<>();

        while (!s.isEmpty()) {
            Cell u = s.pop();
            temp.add(u);

            if (onBorder(u.r, u.c)) {
                isOcean = true;
            }

            for (int i = 0; i < 4; i++) {
                int r = u.r + dr[i];
                int c = u.c + dc[i];

                if (isValid(r, c) && table[r][c] == '.' && !visited[r][c]) {
                    visited[r][c] = true;
                    s.add(new Cell(r, c));
                }
            }
        }

        if (!isOcean) {
            lakes.add(temp);
        }
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        m = sc.nextInt();
        k = sc.nextInt();

        for (int i = 0; i < n; i++) {
            table[i] = sc.next().toCharArray();
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (!visited[i][j] && table[i][j] == '.') {
                    DFS(new Cell(i, j));
                }
            }
        }

        Collections.sort(lakes, new Comparator<ArrayList<Cell>>() {
            public int compare(ArrayList<Cell> a, ArrayList<Cell> b) {
                return a.size() - b.size();
            }
        });

        int sum_cell = 0;

        for (int i = 0; i < lakes.size() - k; i++) {
            sum_cell += lakes.get(i).size();
            for (Cell u : lakes.get(i)) {
                table[u.r][u.c] = '*';
            }
        }

        System.out.println(sum_cell);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }
}

 */