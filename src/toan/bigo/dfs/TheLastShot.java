package toan.bigo.dfs;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

/*
Vì bạn cần lựa chọn để châm ngòi nổ 11 quả bom sao cho số lượng quả bom nổ được là lớn nhất, do đó cách đơn giản nhất là bạn sẽ thử châm ngòi nổ từng quả và đếm xem khi một quả nổ thì sẽ kéo theo có tổng cộng bao nhiêu quả nổ và chọn ra tổng số quả nổ lớn nhất. Để đếm xem có bao nhiêu quả nổ khi quả bom thứ ii được châm ngòi, bạn hãy sử dụng kỹ thuật duyệt DFSDFS với đỉnh xuất phát là đỉnh thứ ii.

Độ phức tạp: O(V * (V + E))O(V∗(V+E)) với VV là số lượng đỉnh, và EE là số lượng cạnh của đồ thị.
 */
public class TheLastShot {
    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        int m;
        n = sc.nextInt();
        m = sc.nextInt();
        graph.clear();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
        }

        for (int i = 0; i < m; i++) {
            int u = sc.nextInt() - 1;
            int v = sc.nextInt() - 1;
            graph.get(u).add(v);
        }
        int max_bombs = 0;

        for (int i = 0; i <n; i++) {
            max_bombs = Math.max(max_bombs, dfs(i,n));
        }

        System.out.println(max_bombs + "");
    }

    public static int  dfs(int node, int n) {
        int bomb = 0;
        ArrayList<Boolean> visited = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            visited.add(false);
        }
        visited.set(node, true);
        Stack<Integer> stack = new Stack<>();
        stack.add(node);
        while (!stack.isEmpty()) {
            int currentU = stack.pop();
            bomb++;
            for (int i = 0; i < graph.get(currentU).size(); i++) {
                int nextU = graph.get(currentU).get(i);
                if (!visited.get(nextU)) {
                   visited.set(nextU,true);
                   stack.add(nextU);
                }
            }
        }
        return bomb;
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 10005;
    static int N, M;
    static ArrayList<Integer> graph[] = new ArrayList[MAX];

    public static int DFS(int src) {
        boolean[] visited = new boolean[N + 1];
        Stack<Integer> s = new Stack<>();
        visited[src] = true;
        s.add(src);

        int nbombs = 0;

        while (!s.isEmpty()) {
            int u = s.pop();
            nbombs++;

            for (int v : graph[u]) {
                if (!visited[v]) {
                    visited[v] = true;
                    s.add(v);
                }
            }
        }

        return nbombs;
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();
        M = sc.nextInt();

        for (int i = 0; i < MAX; i++) {
            graph[i] = new ArrayList<Integer>();
        }

        for (int i = 0; i < M; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            graph[u].add(v);
        }

        int max_bombs = 0;

        for (int i = 1; i <= N; i++) {
            max_bombs = Math.max(max_bombs, DFS(i));
        }

        System.out.print(max_bombs);
    }
}
 */