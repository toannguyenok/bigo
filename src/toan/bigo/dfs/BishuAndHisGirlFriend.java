package toan.bigo.dfs;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

/*
Chạy thuật toán DFSDFS, sau đó lấy lần lượt từng vùng đất của các cô gái rồi tìm đường đi tới vùng đất số 11. Lấy vùng đất có lượng đường đi qua các đỉnh là ít nhất. Để chọn IDID nhỏ thì chỉ khi vùng đất nào thật sự lớn hơn vùng đất hiện tại bạn mới cập nhật lại. Nếu bằng thì cũng không cập nhật.

Bạn cũng có thể cải tiến một chút bằng cách chạy thuật toán từ vùng đất số 11 mà Bishu đang đứng để lấy kết quả đường đi đến các vùng đất mà các cô gái đang đứng. Cách này sẽ đỡ phải mất thời gian chạy đi chạy lại DFSDFS nhiều lần.

Độ phức tạp: O(V + E + Q)O(V+E+Q) với VV là số lượng đỉnh của cây, E = V - 1E=V−1 là số lượng cạnh của cây và QQ là số lượng vùng đất cô gái đang ở.
 */
public class BishuAndHisGirlFriend {

    public static void main(String[] args) {

        int countries;
        Scanner sc = new Scanner(System.in);
        countries = sc.nextInt();

        ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
        ArrayList<Boolean> visited = new ArrayList<>();
        ArrayList<Integer> path = new ArrayList<>();
        for (int i = 0; i < countries; i++) {
            graph.add(new ArrayList<>());
            visited.add(false);
            path.add(0);
        }

        for (int i = 0; i < countries - 1; i++) {
            int u = sc.nextInt() - 1;
            int v = sc.nextInt() - 1;
            graph.get(u).add(v);
            graph.get(v).add(u);
        }
        ArrayList<Integer> pathDfs = dfs(graph, visited, path, 0);

        int q = sc.nextInt();
        int min = Integer.MAX_VALUE;
        int minid = -1;
        for (int i = 0; i < q; i++) {
            int countryGirl = sc.nextInt();
            if ((pathDfs.get(countryGirl - 1) < min) || (pathDfs.get(countryGirl - 1) == min && countryGirl - 1 < minid)) {
                min = pathDfs.get(countryGirl - 1);
                minid = countryGirl - 1;
            }
        }

        System.out.println(minid + 1 + "");
    }

    public static ArrayList<Integer> dfs(ArrayList<ArrayList<Integer>> graph,
                                         ArrayList<Boolean> visited,
                                         ArrayList<Integer> path,
                                         int firstU) {
        Stack<Integer> stack = new Stack<>();
        stack.add(firstU);
        visited.set(firstU, true);
        while (!stack.isEmpty()) {
            int u = stack.pop();
            for (int i = 0; i < graph.get(u).size(); i++) {
                int nextU = graph.get(u).get(i);
                if (!visited.get(nextU)) {
                    visited.set(nextU, true);
                    path.set(nextU, path.get(u) + 1);
                    stack.add(nextU);
                }
            }
        }
        return path;
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 1000 + 5;
    static int V, E;
    static boolean[] visited = new boolean[MAX];
    static int[] dist = new int[MAX];
    static ArrayList<Integer> graph[] = new ArrayList[MAX];

    public static void DFS(int scr) {
        Stack<Integer> s = new Stack<>();
        visited[scr] = true;
        s.add(scr);

        while (!s.isEmpty()) {
            int u = s.pop();

            for (int v : graph[u]) {
                if (!visited[v]) {
                    visited[v] = true;
                    dist[v] = dist[u] + 1;
                    s.add(v);
                }
            }
        }
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);;
        V = sc.nextInt();
        E = V - 1;

        for (int i = 0; i < MAX; i++) {
            graph[i] = new ArrayList<>();
        }

        for (int i = 0; i < E; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            graph[u].add(v);
            graph[v].add(u);
        }

        DFS(1);
        int ans = 0;
        int min_dist = MAX;
        int Q = sc.nextInt();

        for (int i = 0; i < Q; i++) {
            int u = sc.nextInt();

            if (dist[u] < min_dist || (dist[u] == min_dist && u < ans)) {
                min_dist = dist[u];
                ans = u;
            }
        }

        System.out.print(ans);
    }
}
 */