package toan.bigo.dfs;

import java.util.ArrayList;
import java.util.Scanner;

/*
Hướng dẫn giải
Ta có thể nhận thấy rằng việc duyệt A, rồi sau đó duyệt tìm đến B, C, v.v. giống như duyệt theo độ sâu bảng chữ cái dựa trên các chữ cái trong bảng, như vậy ta có thể áp dụng DFS cho bài toán này. Do đường đi cần tìm chỉ có thể bắt đầu từ ‘A’ nên ta sẽ duyệt qua từng phần tử trong bảng, nếu nó là ký tự ‘A’ ta sẽ bắt đầu thực hiện DFS từ đây, trong giải thuật DFS cần chú ý các điểm sau:

Để đi qua các điểm liền kề điểm hiện tại (ngang, dọc, chéo) phải thỏa điều kiện: ký tự mới phải liền sau ký tự hiện tại (ví dụ ký tự hiện tại là C thì ký tự kế tiếp phải là D). Ta có thể kiểm tra bằng việc so sánh mã ASCII ký tự sau = mã ASCII ký tự trước + 1.
Trong lúc duyệt DFS ta phải lồng ghép vào đếm độ sâu của phép duyệt này được bao nhiêu ký tự. Số lượng lớn nhất trong các lần duyệt chính là kết quả bài toán.
Độ phức tạp: O(n * m)O(n∗m) với n, m lần lượt là kích thước của bảng. Khi sử dụng mảng visited chung cho các lần duyệt dfs thì mỗi ô chỉ đc duyệt tối đa 1 lần.
 */
public class ABCPath {

    private static ArrayList<ArrayList<Character>> graph = new ArrayList<>();

    private static ArrayList<ArrayList<Integer>> visited = new ArrayList<>();

    private static int[] dx = {0, 0, 1, 1, 1, -1, -1, -1};
    private static int[] dy = {1, -1, 0, 1, -1, 0, 1, -1};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> result = new ArrayList<>();
        boolean flag = true;
        while (flag) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            graph.clear();
            visited.clear();
            if (n == 0 && m == 0) {
                flag = false;
            } else {
                for (int i = 0; i < n; i++) {
                    graph.add(new ArrayList<>());
                    visited.add(new ArrayList<>());
                    String input = sc.next();
                    for (int j = 0; j < input.length(); j++) {
                        Character character = input.charAt(j);
                        graph.get(i).add(character);
                        visited.get(i).add(0);
                    }
                }
                int ans = 0;
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        if (graph.get(i).get(j) == 'A') {
                            ans = Math.max(ans, dfs(i, j, m, n));
                        }
                    }
                }
                result.add(ans);
            }
        }

        for (int i = 0; i < result.size(); i++) {
            System.out.println("Case " + (i + 1) + ": " + result.get(i));
        }
    }

    private static boolean isValidRowColumn(int newRow, int newColumn, int n, int m) {
        return (newRow >= 0 && newRow < n &&
                newColumn >= 0 && newColumn < m);
    }

    public static int dfs(int row, int column, int n, int m) {
        int count = 0;
        for (int i = 0; i < 8; i++) {
            int newRow = row + dy[i];
            int newColumn = column + dx[i];
            if (isValidRowColumn(newRow, newColumn, n, m) &&
                    visited.get(newRow).get(newColumn) == 0 &&
                    ( graph.get(newRow).get(newColumn) - graph.get(row).get(column)) == 1) {
                dfs(newRow, newColumn, n, m);
                count = Math.max(column, visited.get(newRow).get(newColumn));
            }
        }
        visited.get(row).set(column, count + 1);
        return visited.get(row).get(column);
    }
}
/*
import java.util.Scanner;
public class Main {
	static final int maxn = 1000;
	static int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1};
	static int dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};
	static Scanner sc = new Scanner(System.in);
	static int n, m, ans;
	static int visited[][] = new int[maxn][maxn];
	static String a[] = new String[maxn];

	static int max(int x, int y) {
		if (x > y) return x;
		return y;
	}

	static int dfs(int u, int v) {
		int s = 0;
		for (int i = 0; i < 8; i++) {
			int n_u = u + dx[i];
			int n_v = v + dy[i];
			if (n_u >= 0 && n_u < n && n_v >= 0 && n_v < m)
				if (a[n_u].charAt(n_v) - a[u].charAt(v) == 1) {
					if (visited[n_u][n_v] == 0) dfs(n_u, n_v);
					s = max(s, visited[n_u][n_v]);
				}
		}
		visited[u][v] = s + 1;
		return visited[u][v];
	}

	public static void main(String[] args) {
		int p = 0;
		while (true) {
			ans = 0;
			n = sc.nextInt();
			m = sc.nextInt();
			for (int i = 0; i < n; i++)
				for (int j = 0; j < m; j++)
					visited[i][j] = 0;
			if (n * m == 0) break;
			for (int i = 0; i < n; i++)
				a[i] = sc.next();
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					if (a[i].charAt(j) == 'A') {
						ans = max(ans, dfs(i, j));
					}
				}
			}
			p++;
			System.out.println("Case " + p + ": " + ans);
		}

	}
}
 */