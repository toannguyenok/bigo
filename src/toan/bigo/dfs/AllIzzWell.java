package toan.bigo.dfs;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

/*
Để kiểm tra xem có đường đi nào tạo thành chuỗi ALLIZZWELL hay không, ta sẽ thực hiện chạy DFSDFS từ tất cả các ô chứa ký tự A - ký tự bắt đầu của chuỗi đề cho. Với mỗi bước đi, ta chọn các ô có chứa ký tự tiếp theo trong chuỗi ALLIZZWELL. Nếu đạt tới ký tự cuối cùng trong chuỗi nghĩa là ta đã có một đường đi hợp lệ tạo thành chuỗi đề cho.

Ta lưu ý rằng nếu có bất cứ ô nào thỏa điều kiện là ký tự kế tiếp trong chuỗi, ta chưa thể kết luận là toàn bộ đường đi đó không thể tạo thành chuỗi ALLIZZWELL, hay nói cách khác là ta không thể duyệt qua một lần đỉnh uu và đánh dấu visited_u = truevisited
​u
​​ =true.

Chẳng hạn nếu ta đã tìm được một đường đi A \rightarrow L \rightarrow L \rightarrow I \rightarrow Z \rightarrow Z \rightarrow WA→L→L→I→Z→Z→W nhưng ô tiếp theo ta xét lại là ô có ký tự L - không giống ký tự ta mong đợi là E. Tuy nhiên ta cần hiểu rằng ta không thể loại bỏ ký tự L này được vì rất có thể về sau khi ta đã tìm được một đường đi A \rightarrow L \rightarrow L \rightarrow I \rightarrow Z \rightarrow Z \rightarrow W \rightarrow EA→L→L→I→Z→Z→W→E thì ký tự L đó có thể là một đỉnh hợp lệ trong đường đi của ta.

Do đó, ta sẽ sử dụng kỹ thuật Backtracking (quay lui) như sau:

Với mỗi đỉnh u = s_iu=s
​i
​​ , ta đánh dấu uu là một đỉnh trong đường đi visited_u = truevisited
​u
​​ =true.
Chạy DFSDFS từ ký tự i + 1i+1 về sau.
Đánh dấu lại visited_u = falsevisited
​u
​​ =false để tái sử dụng (nếu có thể) trong những đường đi khác.
Độ phức tạp: O(t*(R^2*C^2))O(t∗(R
​2
​​ ∗C
​2
​​ )) với tt là số lượng bộ test, R, Ctest,R,C là số dòng và số cột của từng ma trận trong mỗi testcasetestcase.
 */
public class AllIzzWell {
    private static ArrayList<ArrayList<Character>> graph = new ArrayList<>();
    private static ArrayList<ArrayList<Boolean>> visited = new ArrayList<>();
    private static String stringResult = "ALLIZZWELL";
    private static boolean found = false;

    private static int[] dx = {0, 0, 1, 1, 1, -1, -1, -1};
    private static int[] dy = {1, -1, 0, 1, -1, 0, 1, -1};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<String> result = new ArrayList<>();
        int testcase = sc.nextInt();
        for (int t = 0; t < testcase; t++) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            graph.clear();
            visited.clear();
            found = false;
            for (int i = 0; i < n; i++) {
                graph.add(new ArrayList<>());
                visited.add(new ArrayList<>());
                String stringInput = sc.next();
                for (int j = 0; j < stringInput.length(); j++) {
                    graph.get(i).add(stringInput.charAt(j));
                    visited.get(i).add(false);
                }
            }
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (graph.get(i).get(j) == stringResult.charAt(0) && !found) {
                        dfs(i, j, 1, n, m);
                    }
                }
            }
            if (found) {
                result.add("YES");
            } else {
                result.add("NO");
            }
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i) + "");
        }
    }

    private static boolean isValidRowColumn(int newRow, int newColumn, int n, int m) {
        return (newRow >= 0 && newRow < n &&
                newColumn >= 0 && newColumn < m);
    }

    public static void dfs(int row, int column, int count, int n, int m) {
        if (count == stringResult.length()) {
            found = true;
            return;
        }
        for (int i = 0; i < 8; i++) {
            int newRow = row + dy[i];
            int newColumn = column + dx[i];
            if (isValidRowColumn(newRow, newColumn, n, m) &&
                    !visited.get(newRow).get(newColumn) &&
                    graph.get(newRow).get(newColumn) == stringResult.charAt(count)) {
                visited.get(newRow).set(newColumn, true);
                dfs(newRow, newColumn, count + 1, n, m);
                visited.get(newRow).set(newColumn, false);
            }
        }
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 101;
    static String term = "ALLIZZWELL";
    static int R, C;
    static boolean found;
    static int[] dr = {0, 0, 1, 1, 1, -1, -1, -1};
    static int[] dc = {1, -1, 0, 1, -1, 0, 1, -1};
    static boolean[][] visited = new boolean[MAX][MAX];
    static char[][] table = new char[MAX][MAX];

    public static boolean isValid(int r, int c) {
        return r >= 0 && c >= 0 && r < R && c < C;
    }

    public static void DFS(int sr, int sc, int count) {
        if (count == term.length()) {
            found = true;
            return;
        }

        for (int i = 0; i < 8; i++) {
            int r = sr + dr[i];
            int c = sc + dc[i];

            if (isValid(r, c) && table[r][c] == term.charAt(count) && !visited[r][c]) {
                visited[r][c] = true;
                DFS(r, c, count + 1);
                visited[r][c] = false;
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();

        while (T-- > 0) {
            R = sc.nextInt();
            C = sc.nextInt();

            for (int i = 0; i < R; i++) {
                table[i] = sc.next().toCharArray();
                for (int j = 0; j < C; j++) {
                    visited[i][j] = false;
                }
            }

            found = false;

            for (int i = 0; i < R; i++) {
                for (int j = 0; j < C; j++) {
                    if (table[i][j] == term.charAt(0) && !found) {
                        DFS(i, j, 1);
                    }
                }
            }

            System.out.println(found ? "YES" : "NO");
        }
    }
}
 */