package toan.bigo.dfs;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

/*
Đồ thị chỉ có thể tồn tại chu trình chỉ khi nào có một cạnh nối từ uu đến một đỉnh vv nào đó được thăm trước đó, đồng thời từ vv phải đến được uu.
Do đó ta cần thêm một mảng để truy vết đường đi và kiểm tra điều kiện trên, tạm gọi là mảng pathpath.
Như vậy, ý tưởng giải cơ bản sẽ là sử dụng DFSDFS để duyệt qua từng đỉnh, với mỗi đỉnh uu đang xét, ta duyệt qua từng đỉnh vv kề với uu:

Nếu vv chưa thăm thì ta duyệt DFS(v).
Nếu vv thăm rồi, lúc này cần kiểm tra trong mảng path xem từ vv có đến được uu hay không, nếu đến được thì chứng tỏ có chu trình.
Độ phức tạp: O(N)O(N) với mỗi đỉnh \rightarrow→ O(N^2)O(N
​2
​​ ) cho toàn đồ thị.

Như vậy ta cần cải tiến để kiểm tra nhanh xem từ vv có đến được uu hay không.
Nhận xét: Nếu từ vv đến được uu thì vv sẽ nằm trên đường đi từ gốc DFSDFS đến uu. Như vậy, nếu mình đánh dấu lại các đỉnh thuộc đường đi từ gốc đến uu, thì có thể kiểm tra nhanh vv có thuộc đường đi đó hay không, đồng nghĩa với việc từ vv có đến được uu hay không.
Do đó thay vì dùng mảng pathpath, ta dùng một mảng là inPathinPath với inPath_i = trueinPath
​i
​​ =true nếu ii nằm trên đường đi từ gốc DFSDFS đến đỉnh uu đang xét, ngược lại inPath_i = falseinPath
​i
​​ =false.
Khi mình duyệt xong DFS(u), thì lúc trở về đỉnh cha của uu, chắc chắn uu không nằm trên đường đi từ gốc đến cha của uu, nên cần gán lại inPath_u = falseinPath
​u
​​ =false trước khi thoát khỏi DFS(u).
Ngoài ra, còn một cách xử lý nữa là sử dụng mảng visitedvisited, nhưng thay vì lúc này chỉ đánh dấu 0/1 (false/true)0/1(false/true) thì lúc này mình đánh dấu 33 giá trị nhằm mục đích sử dụng nó để thực hiện chức năng của cả 22 mảng visitedvisited và pathpath ở cách trên:

visited_u = 0visited
​u
​​ =0 nếu uu chưa được duyệt (tức visited_u = falsevisited
​u
​​ =false và inPath_u = falseinPath
​u
​​ =false theo cách vừa trình bày).
visited_u = 1visited
​u
​​ =1 nếu uu đã được duyệt và ta đang duyệt các đỉnh kề với u (visited_u = trueu(visited
​u
​​ =true và inPath_u = true)inPath
​u
​​ =true).
visited_u = 2visited
​u
​​ =2 nếu uu đã được duyệt và đã duyệt xong các đỉnh kề với u (visited_u = falseu(visited
​u
​​ =false và inPath_u = false)inPath
​u
​​ =false).
Độ phức tạp: O(V + E)O(V+E) với V, EV,E lần lượt là số lượng đỉnh và số lượng cạnh của đồ thị. Tuy nhiên, cách sử dụng mảng visited \ 3visited 3 giá trị sẽ ít tốn bộ nhớ hơn.
 */
public class DuduServiceMarker {
    private static ArrayList<Integer> visited = new ArrayList<>();
    private static boolean loop = false;

    public static void main(String[] args) {

        int testcase;
        Scanner sc = new Scanner(System.in);
        testcase = sc.nextInt();

        ArrayList<String> result = new ArrayList<>();

        for (int i = 0; i < testcase; i++) {
            result.add(findResult(sc));
        }

        for (int i = 0; i < testcase; i++) {
            System.out.println(result.get(i) + "");
        }
    }

    private static String findResult(Scanner sc) {
        int n;
        int m;
        n = sc.nextInt();
        m = sc.nextInt();
        ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
        visited.clear();
        loop = false;
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
            visited.add(0);
        }

        for (int i = 0; i < m; i++) {
            int u = sc.nextInt() - 1;
            int v = sc.nextInt() - 1;
            graph.get(u).add(v);
        }

        for (int i = 0; i < visited.size(); i++) {
            if (visited.get(i) == 0) {
                dfs(graph, i);
                if (loop) {
                    return "YES";
                }
            }
        }

        return "NO";
    }

    public static void dfs(ArrayList<ArrayList<Integer>> graph,
                           int node) {
        visited.set(node, 1);
        if (loop) return;
        for (int i = 0; i < graph.get(node).size(); i++) {
            int nextU = graph.get(node).get(i);
            if (visited.get(nextU) == 0) {
                dfs(graph, nextU);
            } else if (visited.get(nextU) == 1) {
                loop = true;
                return;
            }
        }

        visited.set(node, 2);
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 10005;
    static int N, M;
    static int[] visited = new int[MAX];
    static ArrayList<Integer> graph[] = new ArrayList[MAX];

    public static boolean DFS(int u) {
        visited[u] = 1;

        for (int v : graph[u]) {
            if (visited[v] == 1) {
                return true;
            }
            else if (visited[v] == 0) {
                if (DFS(v)) {
                    return true;
                }
            }
        }

        visited[u] = 2;
        return false;
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();

        while (T-- > 0) {
            N = sc.nextInt();
            M = sc.nextInt();

            for (int i = 1; i <= N; i++) {
                graph[i] = new ArrayList<Integer>();
                visited[i] = 0;
            }

            for (int i = 0; i < M; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                graph[u].add(v);
            }

            boolean isCyclic = false;

            for (int i = 1; i <= N; i++) {
                if (visited[i] == 0) {
                    isCyclic = DFS(i);
                    if (isCyclic) {
                        break;
                    }
                }
            }

            System.out.println(isCyclic ? "YES" : "NO");
        }
    }
}
 */