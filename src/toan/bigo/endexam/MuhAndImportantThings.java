package toan.bigo.endexam;

import java.util.*;

/*
Với bài này, ta thấy rằng các công việc sẽ được ưu tiên thực hiện từ dễ đến khó. Vậy nên ý tưởng ban đầu sẽ là sắp xếp lại các công việc theo độ khó tăng dần. Khi sắp xếp ta cần lưu lại độ khó của mỗi công việc cùng với số thứ tự công việc đó để sau khi sắp xếp, ta biết độ khó đó thuộc về công việc nào.

Nhận xét: Để có ba kế hoạch công việc khác nhau cho ba bạn, đồng nghĩa với việc phải tồn tại ít nhất hai cặp công việc cùng độ khó h để hoán đổi vị trí cho nhau.

Như vậy, công việc cuối cùng của ta chỉ cần làm là tìm hai cặp công việc hoán đổi được cho nhau, sau đó in ra kết quả. Nếu không đủ hai cặp như vậy thì in ra “NO”.

Độ phức tạp: O(nlogn) với n là số lượng công việc.
 */
public class MuhAndImportantThings {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Node> array = new ArrayList<>();
        int[] counterDiff = new int[2001];
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            int value = sc.nextInt();
            array.add(new Node(i + 1, value));
            counterDiff[value]++;
        }

        Collections.sort(array);
        ArrayList<Integer> swapValue = new ArrayList<>();
        for (int i = 0; i < counterDiff.length; i++) {
            if (counterDiff[i] > 1) {
                for (int j = 0; j < counterDiff[i] - 1; j++) {
                    swapValue.add(i);
                }
            }
            if (swapValue.size() >= 2) {
                break;
            }
        }
        if (swapValue.size() >= 2) {
            System.out.println("YES");
            int counterPrint = 0;
            for (int i = 0; i < array.size(); i++) {
                System.out.print(array.get(i).position + " ");
            }

            int currentIndexSwap = -1;
            for (int k = 0; k < swapValue.size(); k++) {
                for (int i = 0; i < array.size(); i++) {
                    if (array.get(i).value == swapValue.get(k) && i > currentIndexSwap) {
                        Node nodetemp = array.get(i);
                        array.set(i, array.get(i + 1));
                        array.set(i + 1, nodetemp);
                        currentIndexSwap = i;
                        break;
                    }
                }
                System.out.println();
                for (int i = 0; i < array.size(); i++) {
                    System.out.print(array.get(i).position + " ");
                }
                counterPrint++;
                if (counterPrint == 2) {
                    break;
                }
            }
        } else {
            System.out.println("NO");
        }
    }

    private static class Node implements Comparable<Node> {
        int position;
        int value;

        public Node(int position, int value) {
            this.position = position;
            this.value = value;
        }

        @Override
        public int compareTo(Node o) {
            return this.value - o.value;
        }
    }
}
