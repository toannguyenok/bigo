package toan.bigo.endexam;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
Giải thích ví dụ:

Ví dụ gồm một bộ test. Trong bộ test đó có 3 nốt tàng nhang -> đồ thị có 3 đỉnh như hình bên.



Khi đó chiều dài vết mực cần thiết để có thể nối các đỉnh lại với nhau sao cho từ một đỉnh có thể đi đến bất kì đỉnh nào là 2 + 1.41 = 3.41

Hướng dẫn giải:

Với bài toán này, để tìm được chiều dài vết mực tối thiểu cần thiết để nối tất cả các tọa độ lại với nhau, thì đầu tiên ta chuyển tất cả về đồ thị với mỗi tọa độ là một đỉnh của đồ thị và trọng số của cạnh nối đỉnh này (x1, y1) với đỉnh kia (x2, y2) tương ứng với khoảng cách giữa chúng, được tính theo công thức \sqrt[2]{(x2-x1)^{2} + (y2-y1)^{2} }
​2
​​ √
​(x2−x1)
​2
​​ +(y2−y1)
​2
​​
​
​​

Sau khi đã đưa về một đồ thị có trọng số với số đỉnh là số lượng nốt tàn nhang từ dữ liệu đề bài đã cho, khi đó nghĩ đến việc nối các đỉnh lại với nhau sao cho từ một đỉnh có thể đi đến bất kì một đỉnh nào khác với tổng chiều dài là ngắn nhất. Và để giải quyết vấn đề này ta sẽ tìm cây khung nhỏ nhất trên đồ thị đó bằng bằng thuật toán tìm cây khung nhỏ nhất (Prim/Kruskal).

In kết quả theo định dạng bài toán yêu cầu.

Độ phức tạp: O(n^2n
​2
​​ log(n)) với n là số lượng nốt tàn nhang.
 */
public class Freckles {

    private static ArrayList<ArrayList<Node>> graph = new ArrayList<>();
    private static ArrayList<Double> dist = new ArrayList<>();
    private static ArrayList<Point> temp = new ArrayList<>();
    private static ArrayList<Boolean> visited = new ArrayList<>();

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int testCase = Integer.parseInt(sc.nextLine());
        ArrayList<Double> result = new ArrayList<>();
        for (int i = 0; i < testCase; i++) {
            sc.nextLine();
            result.add(findResult(sc));
        }
        DecimalFormat df = new DecimalFormat("0.00");
        for (Double integer : result) {
            System.out.println(df.format(integer));
            System.out.println();
        }
    }


    private static double findResult(Scanner sc) {
        int n = sc.nextInt();
        graph.clear();
        dist.clear();
        temp.clear();
        visited.clear();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
            dist.add(Double.MAX_VALUE);
            visited.add(false);
            double x = sc.nextDouble();
            double y = sc.nextDouble();
//            for (int j = 0; j < temp.size(); j++) {
//                double tempWeight = 0.0;
//                if (temp.get(j).x == x) {
//                    tempWeight = Math.abs(temp.get(j).y - y);
//                } else if (temp.get(j).y == y) {
//                    tempWeight = Math.abs(temp.get(j).x - x);
//                } else {
//                    double value1 = Math.pow(temp.get(j).x - x, 2);
//                    double value2 = Math.pow(temp.get(j).y - y, 2);
//                    tempWeight = Math.sqrt(value1 + value2);
//                }
//                graph.get(j).add(new Node(i, tempWeight));
//                graph.get(i).add(new Node(j, tempWeight));
//            }
            temp.add(new Point(x, y));
        }
        //create graph
        for (int i = 0; i < graph.size(); i++) {
            double currentX = temp.get(i).x;
            double currentY = temp.get(i).y;
            for (int j = 0; j < temp.size(); j++) {
                if (i != j) {
                    double newX = temp.get(j).x;
                    double newY = temp.get(j).y;
                    double x1 = Math.abs(newX - currentX);
                    double x2 = Math.abs(newY - currentY);
                    float w = (float) Math.sqrt(Math.pow(x1, 2) + Math.pow(x2, 2));
                    graph.get(i).add(new Node(j, w));
                }
            }
        }
        prime(0);
        double ans =  0;
        for(int i = 0;i<dist.size();i++)
        {
            if(visited.get(i))
            {
                ans += dist.get(i);
            }
        }
        return ans;
    }

    private static void prime(int start) {
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(new Node(start, 0));
        dist.set(start, 0.0);

        while (!priorityQueue.isEmpty()) {
          Node currentNode = priorityQueue.remove();
            int u = currentNode.u;
            visited.set(u, true);
            for (int i = 0; i < graph.get(u).size(); i++) {
              Node newNode = graph.get(u).get(i);
                int v = newNode.u;
                double w = newNode.w;
                if (!visited.get(v) && w < dist.get(v)) {
                    dist.set(v, w);
                    priorityQueue.add(new Node(v, w));
                }
            }
        }
    }

    private static class Node implements Comparable<Node> {
        int u;
        double w;

        public Node(int u, double w) {
            this.u = u;
            this.w = w;
        }

        @Override
        public int compareTo(Node o) {
            if(this.w > o.w)
            {
                return 1;
            }
            return -1;
        }
    }

    private static class Point {
        double x;
        double y;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }
}
