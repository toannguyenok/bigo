package toan.bigo.endexam;

import java.util.ArrayList;
import java.util.Scanner;

public class PhoneList {
    private static class Node {
        public Node[] child;
        public boolean isLeaf;

        public Node() {
            child = new Node[10];
            this.isLeaf = false;
        }
    }

    private static class Trie {
        private Node root;

        public Trie() {
            root = new Node();
        }

        public boolean addWord(String s) {
            Node temp = root;
            boolean flag = false;
            for (int i = 0; i < s.length(); i++) {
                int ch = s.charAt(i) - '0';
                if (temp.child[ch] == null) {
                    flag = true;
                    temp.child[ch] = new Node();
                }
                temp = temp.child[ch];
                if (temp.isLeaf)
                    return false;
            }
            temp.isLeaf = true;
            return flag;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testCase = in.nextInt();
        ArrayList<String> result = new ArrayList<>();
        for(int i  = 0;i<testCase;i++) {
            int n = in.nextInt();
            in.nextLine();
            Trie trie = new Trie();
            boolean nonVulnerable = true;
            while (n-- > 0) {
                String s = in.nextLine();
                nonVulnerable &= trie.addWord(s.trim());
            }
            if (nonVulnerable)
                result.add("YES");
            else
                result.add("NO");
        }
        for(int i = 0;i<result.size();i++)
        {
            System.out.println(result.get(i) + "");
        }
    }

}
