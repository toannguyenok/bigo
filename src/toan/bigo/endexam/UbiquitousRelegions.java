package toan.bigo.endexam;

import java.util.ArrayList;
import java.util.Scanner;

/*
Ta thấy rằng nếu A cùng tôn giáo với B và B cùng tôn giáo với C thì cả A B và C đều thuộc cùng một tôn giáo. Như vậy bài này có thể quy về bài toán tìm số thành phần liên thông trong đồ thị (hay tìm số lượng nhóm trong đồ thị) và ứng dụng DSU cơ bản vào để gom nhóm các sinh viên. Cuối cùng đếm số đỉnh đại diễn cho từng nhóm là sẽ suy ra được số nhóm.

Độ phức tạp: O(T * N * M) nếu sử dụng DSU cơ bản, O(T * M * logN) nếu sử dụng DSU nâng cao.
 */
public class UbiquitousRelegions {

    private static ArrayList<Integer> parent = new ArrayList<>();
    private static ArrayList<Integer> ranks = new ArrayList<>();

    private static void makeSet(int n) {
        for (int i = 0; i < n; i++) {
            parent.add(i);
            ranks.add(0);
        }
    }

    private static int findSet(int u) {
        if (parent.get(u) != u) {
            parent.set(u, findSet(parent.get(u)));
        }
        return parent.get(u);
    }

    private static void unionSet(int u, int v) {
        int up = findSet(u);
        int vp = findSet(v);

        if (up != vp) {
            if (ranks.get(up) > ranks.get(vp)) {
                parent.set(vp, up);
            } else if (ranks.get(up) < ranks.get(vp)) {
                parent.set(up, vp);
            } else {
                parent.set(up, vp);
                ranks.set(vp, ranks.get(vp) + 1);
            }

        }
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> result = new ArrayList<>();
        boolean flag = true;
        while (flag) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            if (n == 0 && m == 0) {
                flag = false;
            } else {
                parent.clear();
                ranks.clear();
                makeSet(n);
                for (int i = 0; i < m; i++) {
                    int u = sc.nextInt() - 1;
                    int v = sc.nextInt() - 1;
                    unionSet(u, v);
                }
                int ans = 0;
                for (int i = 0; i < n; i++) {
                   if(parent.get(i) == i)
                   {
                       ans++;
                   }
                }
                result.add(ans);
            }
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println("Case " + ( i  + 1) + ": "  + result.get(i));
        }
    }
}
