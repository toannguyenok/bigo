package toan.bigo.endexam;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;

/*
Nếu với mỗi câu hỏi mà ta đi lần lượt theo từng giá trị để lấy kết quả thì sẽ không thể thực hiện trong thời gian cho phép.

Quan sát hình, ta có thể xem đây như một mặt phẳng tọa độ với ô số 1 là (1, 1) thì ta có thể thấy rằng các số sẽ nằm theo từng lớp khác nhau (theo từng màu như trong hình ví dụ) và ở lớp thứ k thì:

Nếu k lẻ: các số có giá trị bắt đầu từ (k-1)^{2}(k−1)
​2
​​  + 11, bắt đầu nằm ở vị trí (k, 1) đi lên (tăng dần theo y) đến tọa độ (k, k), sau đó đi sang trái (giảm dần theo x) đến tọa độ (1, k).
Nếu k chẵn: các số bắt đầu từ (k-1)^{2}(k−1)
​2
​​  + 11, bắt đầu nằm ở vị trí (1, k) đi sang phải đến tọa độ (k, k) sau đó đi xuống đến tọa độ (k, 1).
Ngoài ra, với k lớp đầu tiên của ma trận cũng sẽ tạo thành một ma trận vuông k * k. Như vậy, với một số S, ta có thể xác định số này thuộc lớp nào bằng cách lấy căn bậc 2. Sau khi có lớp, ta xác định được là thuộc lớp chẵn hay lẻ, từ đó xác định hướng di chuyển. Từ hướng di chuyển thì sẽ xác định được đang nằm ở đường ngang hay đường dọc của lớp đó và có thể tính nhanh tọa độ của nó.

Độ phức tạp: O(T), T là số lượng test case, với mỗi test case, chỉ cần sử dụng các phép toán cơ bản để xác định số lớp cũng như tọa độ của S.
 */
public class FibsieveFantabulous {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TreeMap<Integer,Point> treeMap = new TreeMap<>();
        treeMap.put(25, new Point(1,5));
        treeMap.put(24, new Point(2,5));
        treeMap.put(23, new Point(3,5));
        treeMap.put(22, new Point(4,5));
        treeMap.put(21, new Point(5,5));
        treeMap.put(20, new Point(5,4));
        treeMap.put(19, new Point(5,3));
        treeMap.put(18, new Point(5,2));
        treeMap.put(17, new Point(5,1));
        treeMap.put(16, new Point(4,1));
        treeMap.put(15, new Point(4,2));
        treeMap.put(14, new Point(4,3));
        treeMap.put(13, new Point(4,4));
        treeMap.put(12, new Point(3,4));
        treeMap.put(11, new Point(2,4));
        treeMap.put(10, new Point(1,4));
        treeMap.put(9, new Point(1,3));
        treeMap.put(8, new Point(2,3));
        treeMap.put(7, new Point(3,3));
        treeMap.put(6, new Point(3,2));
        treeMap.put(5, new Point(3,1));
        treeMap.put(4, new Point(2,1));
        treeMap.put(3, new Point(2,2));
        treeMap.put(2, new Point(1,2));
        treeMap.put(1, new Point(1,1));

        int testCase = sc.nextInt();
        ArrayList<Point> result = new ArrayList<>();
        for(int i = 0;i<testCase;i++)
        {
            int query = sc.nextInt();
            Point ans = treeMap.get(query);
            result.add(ans);
        }

        for(int i = 0;i<result.size();i++)
        {
            System.out.println("Case " + (i + 1) + ": " + result.get(i).column + " " + result.get(i).row);
        }
    }

    private static class Point{
        int column;
        int row;

        public Point(int column, int row)
        {
           this.column = column;
           this.row = row;

        }
    }
}
