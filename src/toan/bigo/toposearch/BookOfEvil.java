package toan.bigo.toposearch;

import java.util.ArrayList;
import java.util.Scanner;


/*
Thuật toán trâu: từ mm đỉnh “đặc biệt” đã cho, tiến hành loang dd lớp đến các đỉnh khác. Những đỉnh thỏa điều kiện có tổng các lần ghé thăm đúng bằng mm. Tuy nhiên, thuật toán này không khả thi vì phải loang mm lần, mỗi lần như thế có độ phức tạp là O(N)O(N), dẫn tới tổng độ phức tạp O(M*N)O(M∗N) và nó quá lớn.

Cải tiến: thay vì phải loang từ mm đỉnh, ta có thể loang từ 2 đỉnh có khoảng cách xa nhất trong đồ thị mà thôi. Ta ký hiệu khoảng cách giữa 2 đỉnh u, vu,v là dist(u,v)dist(u,v). Giả sử 2 đỉnh có khoảng cách xa nhất trong đồ thị là s, rs,r. Một đỉnh tt thỏa yêu cầu bài toán khi dist(s,t) \leq ddist(s,t)≤d và dist(r,t) \leq ddist(r,t)≤d.

Chứng minh: Giả sử đỉnh tt không thỏa yêu cầu bài toán, điều này đồng nghĩa với việc tồn tại một đỉnh qq sao cho dist(q,t) > ddist(q,t)>d. Suy ra, dist(s,r) = dist(s,t) + dist(r,t) < dist(r,t) + dist(q,t) = dist(r,q)dist(s,r)=dist(s,t)+dist(r,t)<dist(r,t)+dist(q,t)=dist(r,q). Mà khoảng cách dist(s,r)dist(s,r) là xa nhất (giả thuyết), điều này trái với giả thuyết dẫn tới điều giả sử sai. Suy ra, đỉnh tt thỏa yêu cầu bài toán.

Ban đầu, vì chưa xác định được 2 đỉnh nào có khoảng cách xa nhất, ta có thể bắt đầu từ 1 đỉnh bất kỳ, loang đến tất cả các đỉnh “đặc biệt”. Dễ thấy, với một đỉnh gốc bất kỳ, thì 1 trong 2 đỉnh có khoảng cách xa nhất sẽ là đỉnh có khoảng cách từ gốc đến nó xa nhất. Đến đây, ta đã xác định được 1 trong 2 đỉnh cần tìm, việc còn lại là loang 1 lần nữa để tìm đỉnh còn lại.

Sau khi tìm được 2 đỉnh xa nhất, từ 2 đỉnh đó ta chỉ cần loang đến tất cả các đỉnh còn lại và tìm kết quả.

Độ phức tạp: O(n)O(n) với nn là số đỉnh của đồ thị.
 */
public class BookOfEvil {

    final static int MAX_N = (int)1e5+5;
    static int n,m,d,p;
    static boolean affected[] = new boolean [MAX_N];
    static ArrayList <Integer> graph[] = new ArrayList[MAX_N];
    static int maxDown[] = new int[MAX_N];
    static int maxUp[] = new int[MAX_N];



    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int u,v;
        n = sc.nextInt();
        m = sc.nextInt();
        d = sc.nextInt();
        for(int i = 0 ; i < n ; i++){
            graph[i] = new ArrayList<>();
        }
        for(int i = 0 ; i < m ; i++){
            p = sc.nextInt();
            affected[--p] = true;
        }
        for(int i = 0 ; i < n-1; i++){
            u = sc.nextInt();
            v = sc.nextInt();
            --u;--v;
            graph[u].add(v);
            graph[v].add(u);
        }
        System.out.println(Solve());
    }
    static int Solve(){
        ComputeMaxDown(0,-1);
        maxUp[0] = affected[0] == true ? 0 : -1;
        ComputeMaxUp(0,-1);
        int ans = 0;
        for(int i = 0 ; i < n ; i++){
            if(maxDown[i] <= d && maxUp[i] <= d)
                ans++;
        }
        return ans;
    }

    static void ComputeMaxUp(int src, int par){
        int max1 = -1, max2 = -1;
        for(int i = 0 ; i < graph[src].size(); i++){
            int v = graph[src].get(i);
            if(v == par) continue;
            if(maxDown[v] > max1){ max2 = max1; max1 = maxDown[v];}
            else if(maxDown[v] > max2) {max2 = maxDown[v];}
        }

        for(int i = 0 ; i < graph[src].size(); i++){
            int v = graph[src].get(i);
            if(v == par) continue;
            maxUp[v] = (maxDown[v] == max1 ? max2 : max1);
            if(maxUp[v] != -1 )
                maxUp[v] += 2;
            if(maxUp[src] != -1)
                maxUp[v] = Math.max(maxUp[v],maxUp[src] +1);
            if(affected[v])
                maxUp[v] = Math.max(maxUp[v],0);
            ComputeMaxUp(v,src);
        }
    }

    static int ComputeMaxDown(int src, int par){
        if(affected[src] == false){
            maxDown[src] = -1;
        }
        else maxDown[src] = 0;
        for(int i = 0 ; i < graph[src].size(); i++){
            int v = graph[src].get(i);
            if(v==par) continue;
            int d = ComputeMaxDown(v,src);
            if(d > -1)
                maxDown[src] = Math.max(maxDown[src],d+1);
        }
        return maxDown[src];
    }
}

/*
import java.io.*;
import java.util.*;


public class Main{
    final static int MAX_N = (int)1e5+5;
    static int n,m,d,p;
    static boolean affected[] = new boolean [MAX_N];
    static ArrayList <Integer> graph[] = new ArrayList [MAX_N];
    static int maxDown[] = new int[MAX_N];
    static int maxUp[] = new int[MAX_N];



    public static void main(String[] args) {
        Read();
        System.out.println(Solve());
    }
    static int Solve(){
        ComputeMaxDown(0,-1);
        maxUp[0] = affected[0] == true ? 0 : -1;
        ComputeMaxUp(0,-1);
        int ans = 0;
        for(int i = 0 ; i < n ; i++){
            if(maxDown[i] <= d && maxUp[i] <= d)
                ans++;
        }
        return ans;
    }

    static void ComputeMaxUp(int src, int par){
        int max1 = -1, max2 = -1;
        for(int i = 0 ; i < graph[src].size(); i++){
            int v = graph[src].get(i);
            if(v == par) continue;
            if(maxDown[v] > max1){ max2 = max1; max1 = maxDown[v];}
            else if(maxDown[v] > max2) {max2 = maxDown[v];}
        }

        for(int i = 0 ; i < graph[src].size(); i++){
            int v = graph[src].get(i);
            if(v == par) continue;
            maxUp[v] = (maxDown[v] == max1 ? max2 : max1);
            if(maxUp[v] != -1 )
                    maxUp[v] += 2;
            if(maxUp[src] != -1)
                    maxUp[v] = Math.max(maxUp[v],maxUp[src] +1);
            if(affected[v])
                    maxUp[v] = Math.max(maxUp[v],0);
            ComputeMaxUp(v,src);
        }
    }

    static int ComputeMaxDown(int src, int par){
        if(affected[src] == false){
            maxDown[src] = -1;
        }
        else maxDown[src] = 0;
        for(int i = 0 ; i < graph[src].size(); i++){
            int v = graph[src].get(i);
            if(v==par) continue;
            int d = ComputeMaxDown(v,src);
            if(d > -1)
            maxDown[src] = Math.max(maxDown[src],d+1);
        }
        return maxDown[src];
    }

    static void Read(){
        Scanner sc = new Scanner(System.in);
        int u,v;
        n = sc.nextInt();
        m = sc.nextInt();
        d = sc.nextInt();
        for(int i = 0 ; i < n ; i++){
            graph[i] = new ArrayList<>();
        }
        for(int i = 0 ; i < m ; i++){
            p = sc.nextInt();
            affected[--p] = true;
        }
        for(int i = 0 ; i < n-1; i++){
            u = sc.nextInt();
            v = sc.nextInt();
            --u;--v;
            graph[u].add(v);
            graph[v].add(u);
        }
    }
}
 */

/*
#include <bits/stdc++.h>
#define eb emplace_back
using namespace std;
const int inf = 1e9;

typedef vector<int> vi;
typedef vector<vi> graph_type;

void dfs(graph_type& adj, vi& dist, const vector<bool>& affected, int u, int d) {
    dist[u] = -inf;
    if (affected[u]) {
        dist[u] = d;
    }

    for (int v : adj[u]) {
        if (dist[v] == -1) {
            dfs(adj, dist, affected, v, d + 1);
        }
    }
}

int main() {
    int n, m, d;
    cin >> n >> m >> d;

    vector<bool> affected(n);
    for (int i = 0, p; i < m; ++i) {
        cin >> p;
        affected[p - 1] = true;
    }

    graph_type adj(n);
    for (int i = 1, x, y; i < n; ++i) {
        cin >> x >> y;
        --x; --y;
        adj[x].eb(y);
        adj[y].eb(x);
    }

    vi dist0(n, -1), distu(n, -1), distv(n, -1);
    dfs(adj, dist0, affected, 0, 0);
    int u = std::max_element(dist0.begin(), dist0.end()) - dist0.begin();
    dfs(adj, distu, affected, u, 0);
    int v = std::max_element(distu.begin(), distu.end()) - distu.begin();

    distu = vi(n, -1);
    dfs(adj, distu, vector<bool>(n, true), u, 0);
    dfs(adj, distv, vector<bool>(n, true), v, 0);

    int res = 0;
    for (int i = 0; i < n; ++i) {
        if (max(distu[i], distv[i]) <= d) {
            res++;
        }
    }

    cout << res;
}
 */