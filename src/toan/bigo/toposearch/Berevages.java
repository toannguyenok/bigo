package toan.bigo.toposearch;

import java.util.*;

/*
Đây là một bài tập về sắp xếp topo. Ràng buộc "Nồng độ cồn của thức uống xx thấp hơn so với thức uống yy"" tương đương với cạnh một chiều từ đỉnh xx sang đỉnh yy trong đồ thị. Tuy nhiên, đề bài yêu cầu ta tìm thứ tự topo có thứ tự từ điển nhỏ nhất nên ta cần một vài chỉnh sửa trong thuật toán sắp xếp topo.

Trước hết, ta cần xử lí dữ liệu vào. Ta sẽ tạo một map (đối với C++) hoặc TreeMap (đối với Java) để lưu thứ tự trong danh sách của từng tên đồ uống. Với mỗi ràng buộc, ta sẽ tìm id_xid
​x
​​  và id_yid
​y
​​  lần lượt là thứ tự trong danh sách của từng xâu xx và yy, và thêm cạnh một chiều từ id_xid
​x
​​  sang id_yid
​y
​​ . Từ đó, ta thu được một đồ thị và thực hiện sắp xếp topo trên đồ thị này.

Ta sẽ áp dụng thuật toán Kahn (đã được trình bày trong slide của BigO) để thực hiện việc sắp xếp topo. Tuy nhiên, ta cần điều chỉnh lại thuật toán này: Với biến zero_indegree, thay vì sử dụng mảng động (vector), ta cần sử dụng hàng đợi ưu tiên (priority_queue). Bởi vì, theo yêu cầu của đề, nếu có nhiều đỉnh cùng có bậc 00, ta cần ưu tiên duyệt đỉnh có chỉ số nhỏ nhất. Do đó, khi lấy ra một đỉnh từ zero_indegree, ta cần lấy ra đỉnh có chỉ số nhỏ nhất.

Cuối cùng, ta in ra tên của các loại thức uống theo thứ tự topo tìm được. Độ phức tạp: \mathcal{O} \left( (N+M) \cdot \log(n) \right)O((N+M)⋅log(n)). với NN là số thức uống, MM là số ràng buộc.
 */
public class Berevages {

    private static class Pair {
        int position;
        int index;

        public Pair(int position, int index) {
            this.position = position;
            this.index = index;
        }
    }

    private static ArrayList<String> name = new ArrayList<>();
    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
    private static HashMap<String, Integer> hashPosition = new HashMap<>();
    private static ArrayList<Integer> result = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<ArrayList<String>> answer = new ArrayList<>();
        while (sc.hasNext()) {
            String space = sc.nextLine();
            if (space.equals("")) {
                name.clear();
                graph.clear();
                hashPosition.clear();
                result.clear();
            } else {
                int n = Integer.parseInt(space);
                for (int i = 0; i < n; i++) {
                    String nameBerevages = sc.next();
                    name.add(nameBerevages);
                    graph.add(new ArrayList<>());
                    hashPosition.put(nameBerevages, i);
                }
                int m = sc.nextInt();
                for (int i = 0; i < m; i++) {
                    String input1 = sc.next();
                    String input2 = sc.next();
                    graph.get(hashPosition.get(input1)).add(hashPosition.get(input2));
                }
                topologicalSorting();
                ArrayList<String> temp = new ArrayList<>();
                for (int i = 0; i < result.size(); i++) {
                    temp.add(name.get(result.get(i)));
                }
                answer.add(temp);
            }
        }
        for (int i = 0; i < answer.size(); i++) {
            String t = "";
            for (int j = 0; j < answer.get(i).size(); j++) {
                if (j == answer.get(i).size() - 1) {
                    t = t + " " + answer.get(i).get(j) + ".";
                } else {
                    t = t + " " + answer.get(i).get(j);
                }
            }
            System.out.println("Case #" + (i + 1) + ": Dilbert should drink beverages in this order:" + t);
            System.out.println("");
        }
    }

    private static void topologicalSorting() {
        int v = graph.size();
        int[] inDegree = new int[v];
        PriorityQueue<Pair> zeroQueue = new PriorityQueue<>(new Comparator<Pair>() {
            @Override
            public int compare(Pair o1, Pair o2) {
                return o1.index - o2.index;
            }
        });
        for (int i = 0; i < graph.size(); i++) {
            for (int j : graph.get(i)) {
                inDegree[j]++;
            }
        }

        for (int i = 0; i < graph.size(); i++) {
            if (inDegree[i] == 0) {
                zeroQueue.add(new Pair(i, hashPosition.get(name.get(i))));
            }
        }

        while (!zeroQueue.isEmpty()) {
            Pair u = zeroQueue.poll();
            result.add(u.position);
            for (int i : graph.get(u.position)) {
                inDegree[i]--;
                if (inDegree[i] == 0) {
                    zeroQueue.add(new Pair(i, hashPosition.get(name.get(i))));
                }
            }
        }
    }
}

/*
import java.util.*;

class Solution_AC {
    static ArrayList<Integer> topologicalSort(ArrayList<ArrayList<Integer>> graph) {
        int V = graph.size();
        ArrayList<Integer> indegree = new ArrayList<>();
        for (int i = 0; i < V; ++i) {
            indegree.add(0);
        }

        ArrayList<Integer> result = new ArrayList<>();
        PriorityQueue<Integer> zero_indegree = new PriorityQueue<>();
        for (int u = 0; u < V; ++u) {
            for (int v: graph.get(u)) {
                indegree.set(v, indegree.get(v) + 1);
            }
        }

        for (int i = 0; i < V; ++i) {
            if (indegree.get(i) == 0) {
                zero_indegree.add(i);
            }
        }

        while (!zero_indegree.isEmpty()) {
            int u = zero_indegree.peek();
            zero_indegree.remove();

            result.add(u);

            for (int v: graph.get(u)) {
                indegree.set(v, indegree.get(v) - 1);
                if (indegree.get(v) == 0) {
                    zero_indegree.add(v);
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int iTest = 0;
        while (sc.hasNext()) {
            int V = sc.nextInt();
            ++iTest;

            ArrayList<String> name = new ArrayList<>();
            for (int u = 0; u < V; ++u)
                name.add(sc.next());

            Map<String, Integer> id = new TreeMap<>();
            for (int u = 0; u < V; ++u)
                id.put(name.get(u), u);

            ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
            for (int i = 0; i < V; ++i) {
                graph.add(new ArrayList<>());
            }

            int E = sc.nextInt();
            for(int i = 0; i < E; ++i) {
                String nameU = sc.next(), nameV = sc.next();
                int u = id.get(nameU), v = id.get(nameV);
                graph.get(u).add(v);
            }

            ArrayList<Integer> topoOrd = topologicalSort(graph);

            System.out.print("Case #" + iTest + ": Dilbert should drink beverages in this order:");
            for (int u: topoOrd)
                System.out.print(" " + name.get(u));
            System.out.println(".");
            System.out.println();
        }
    }
}
 */

/*
#include <iostream>
#include <map>
#include <algorithm>
#include <queue>
#include <vector>
using namespace std;

int V, E;

vector<int> topologicalSort(vector<vector<int>> &graph)
{
    vector<int> indegree(V, 0);
    vector<int> result;
    priority_queue<int, vector<int>, greater<int> > zero_indegree;

    for (int u = 0; u<V; u++)
    {
        vector<int>::iterator it;
        for (it = graph[u].begin(); it != graph[u].end(); it++)
            indegree[*it]++;
    }

    for (int i = 0; i < V; i++) {
        if (indegree[i]==0) {
            zero_indegree.push(i);
        }
    }

    while (!zero_indegree.empty())
    {
        int u = zero_indegree.top();
        zero_indegree.pop();

        result.push_back(u);

        vector<int>::iterator it;
        for (it = graph[u].begin(); it != graph[u].end(); it++)
        {
            indegree[*it]--;
            if (indegree[*it]==0) {
                zero_indegree.push(*it);
            }
        }
    }

    return result;
}

int main() {
    ios::sync_with_stdio(false);

    int iTest = 0;
    while (cin >> V) {
        ++iTest;

        vector<string> name(V);
        for(int u = 0; u < V; ++u)
            cin >> name[u];

        map<string, int> id;
        for(int u = 0; u < V; ++u)
            id[name[u]] = u;

        vector<vector<int>> graph;
        graph.assign(V, vector<int>());

        cin >> E;
        for(int i = 0; i < E; ++i) {
            string nameU, nameV;
            cin >> nameU >> nameV;
            int u = id[nameU], v = id[nameV];
            graph[u].push_back(v);
        }

        vector<int> topoOrd = topologicalSort(graph);

        cout << "Case #" << iTest << ": Dilbert should drink beverages in this order:";
        for(int u: topoOrd)
            cout << " " << name[u];
        cout << "." << endl;
        cout << endl;
    }

    return 0;
}
 */