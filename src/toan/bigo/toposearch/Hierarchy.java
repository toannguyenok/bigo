package toan.bigo.toposearch;

import java.util.*;

/*
Xây dựng một đồ thị có hướng không trọng số tương ứng với các nguyện vọng của KK người xuất sắc nhất: có cạnh nối u \rightarrow vu→v khi người uu muốn làm cấp trên của vv. Đồ thị này không có chu trình nên có thể sắp xếp Topo được. Gọi TT là thứ tự Topo của đồ thị vừa xây dựng. Theo định nghĩa thì nếu có cạnh nối u \rightarrow vu→v thì uu sẽ đứng trước vv trong TT. Điều này có nghĩa là với mỗi người uu, tất cả những người cấp dưới mà uu muốn đều nằm sau uu trong TT.

Cách xây dựng cây phân cấp đơn giản nhất là xây dựng theo thứ tự T:

T_1T
​1
​​  làm sếp tổng \leftrightarrow boss(T_1) = -1↔boss(T
​1
​​ )=−1.
Sếp của T_2T
​2
​​  là T_1T
​1
​​  \leftrightarrow boss(T_2) = T_1↔boss(T
​2
​​ )=T
​1
​​ .
Sếp của T_3T
​3
​​  là T_2T
​2
​​  \leftrightarrow boss(T_3) = T_2↔boss(T
​3
​​ )=T
​2
​​ .
…
Sếp của T_NT
​N
​​  là T_{N-1}T
​N−1
​​  \leftrightarrow boss(T_N) = T_{N-1}↔boss(T
​N
​​ )=T
​N−1
​​ .
Cuối cùng mảng boss chính là kết quả cần xuất ra màn hình.

Độ phức tạp: \mathcal{O} \left( N + sum({W_i}) \right)O(N+sum(W
​i
​​ )). với NN là số người trong công ty và W_iW
​i
​​  là số người cấp dưới mà người i chọn.
 */
public class Hierarchy {

    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
    private static ArrayList<Integer> result = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
        }

        int k = sc.nextInt();
        for (int i = 0; i < k; i++) {
            int u = sc.nextInt();
            for (int j = 0; j < u; j++) {
                int x = sc.nextInt() - 1;
                graph.get(i).add(x);
            }
        }
        topologicalSort(graph, result);
        int position = -1;
        int[] parent = new int[n];
        for (int i = 0; i < result.size(); i++) {
            int student = result.get(i);
            parent[student] = position;
            position = student;
        }

        for (int i = 0; i < n; i++) {
            System.out.println(parent[i] + 1);
        }
    }

//    private static boolean topologicalSortNormal() {
//        int v = graph.size();
//        int[] inDegree = new int[v];
//        Queue<Integer> zeroQueue = new LinkedList<>();
//        for (int i = 0; i < graph.size(); i++) {
//            for (int j : graph.get(i)) {
//                inDegree[j]++;
//            }
//        }
//
//        for (int i = 0; i < graph.size(); i++) {
//            if (inDegree[i] == 0) {
//                zeroQueue.add(i);
//            }
//        }
//
//        while (!zeroQueue.isEmpty()) {
//            int u = zeroQueue.poll();
//            result.add(u);
//            for (int i : graph.get(u)) {
//                inDegree[i]--;
//                if (inDegree[i] == 0) {
//                    zeroQueue.add(i);
//                }
//            }
//        }
//        for (int j  = 0;j < inDegree.length;j++) {
//            if (inDegree[j] != 0) {
//                return false;
//            }
//        }
//        return true;
//    }

    private static void dfs(int u,
                            ArrayList<ArrayList<Integer>> graph,
                            ArrayList<Boolean> visited,
                            ArrayList<Integer> result) {
        visited.set(u, true);
        for (int i = 0; i < graph.get(u).size(); i++) {
            int v = graph.get(u).get(i);
            if (!visited.get(v)) {
                dfs(v, graph, visited, result);
            }
        }
        result.add(u);
    }

    private static void topologicalSort(ArrayList<ArrayList<Integer>> graph,
                                        ArrayList<Integer> result) {
        int v = graph.size();
        ArrayList<Boolean> visited = new ArrayList<>();
        for (int i = 0; i < v; i++) {
            visited.add(false);
        }
        for (int i = 0; i < v; i++) {
            if (!visited.get(i)) {
                dfs(i, graph, visited, result);
            }
        }
        Collections.reverse(result);
    }
}

/*
import java.util.*;

public class Solution_TLE {
    public static int n, k;

    public static void dfs(int u, ArrayList<ArrayList<Integer>> graph, boolean[] visited, ArrayList<Integer> result) {
        visited[u] = true;
        for (int i = 0; i < graph.get(u).size(); i++) {
            int v = graph.get(u).get(i);
            if (!visited[v])
                dfs(v, graph, visited, result);
        }
        result.add(u);
    }

    public static void topologicalSort(ArrayList<ArrayList<Integer>> graph, ArrayList<Integer> result) {
        boolean[] visited = new boolean[n + 1];
        for (int u = 1; u <= n; u++)
            if (!visited[u])
                dfs(u, graph, visited, result);
    }

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        k = sc.nextInt();
        ArrayList<ArrayList<Integer>> graph = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i <= n; i++)
            graph.add(new ArrayList<Integer>());

        for (int u = 1; u <= k; u++) {
            int w = sc.nextInt();
            while (w-- > 0) {
                int v = sc.nextInt();
                graph.get(u).add(v);
            }
        }

        ArrayList<Integer> result = new ArrayList<Integer>();
        topologicalSort(graph, result);
        int[] parent = new int[n + 1];
        for (int i = 0; i <= n; i++)
            parent[i] = 0;

        for (int i = 0; i < n - 1; i++)
            parent[result.get(i)] = result.get(i + 1);

        for (int u = 1; u <= n; u++)
            System.out.println(parent[u]);
        sc.close();
    }
}
 */

/*
#include <bits/stdc++.h>
using namespace std;

void dfs(int u, const vector<vector<int>> &graph, vector<bool> &visited, vector<int> &result) {
    visited[u] = true;
    for (int i = 0; i < graph[u].size(); i++) {
        int v = graph[u][i];
        if (!visited[v]) {
            dfs(v, graph, visited, result);
        }
    }
    result.push_back(u);
}

vector<int> topologicalSort(const vector<vector<int>> &graph) {
    int V = graph.size();
    vector<int> result;
    result.reserve(V);
    vector<bool> visited(V, false);

    for (int i = 0; i < V; i++) {
        if (!visited[i]) {
            dfs(i, graph, visited, result);
        }
    }
    std::reverse(result.begin(), result.end());
    return result;
}

int main() {
    int n, k;
    scanf("%d%d", &n, &k);
    vector<vector<int>> graph(n);
    for (int u = 0; u < k; ++u) {
        int w, v;
        scanf("%d", &w);
        while (w--) {
            scanf("%d", &v);
            graph[u].push_back(v - 1);
        }
    }

    auto topo_order = topologicalSort(graph);
    vector<int> boss(n);
    boss[topo_order[0]] = -1;
    for (int i = 1; i < n; ++i) {
        boss[topo_order[i]] = topo_order[i - 1];
    }

    for (int i = 0; i < n; ++i) {
        printf("%d\n", boss[i] + 1);
    }
}
 */