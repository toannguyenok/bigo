package toan.bigo.toposearch;

import java.util.*;

/*
Ta xem NN công việc là NN đỉnh của đồ thị, còn MM cặp công việc là MM cạnh của đồ thị có hướng. Vì một số công việc sẽ thực hiện trước một vài công việc khác nên ta sẽ dùng Topological Sort để giải quyết bài toán này. Nhận xét :

Sandro sẽ không thực hiện được tất cả công việc nếu xuất hiện chu trình trong đồ thị.
Do yêu cầu đặc biệt nếu có nhiều công việc có cùng thứ tự thực hiện thì ta chọn công việc có số nhỏ nhất. Vậy để luôn chọn được công việc có số thứ tự nhỏ nhất thực hiện lúc đó ta cần cấu trúc dữ liệu thích hợp, ở đây ta chọn thuật toán Kahn và min heap để lưu các đỉnh có thể được chọn tiếp.
Vậy với thuật toán Kahn, ta cần danh sách đỉnh kề, mảng đếm số bậc vào còn lại của mỗi đỉnh và min heap các đỉnh có bậc vào hiện tại bằng 00.

Ta bắt đầu với việc xác định các đỉnh có số bậc vào bằng 00 và cho vào heap.
Khi mà heap vẫn còn các đỉnh chưa được xét, ta chọn đỉnh đầu tiên trong heap sẽ là đỉnh tiếp theo được in ra.
Đồng thời, khi đỉnh này được chọn ta giảm đi số bậc vào của các đỉnh kề từ đỉnh này. Nếu có bất kì đỉnh kề nào có bậc vào bằng 00 thì ta cho đỉnh này vào heap.
Kết quả cuối cùng sau khi kết thúc thuật Kahn sẽ là danh sách các công việc cần thực hiện theo thứ tự thỏa mãn.
Độ phức tạp: \mathcal{O} \left( N \cdot \log(N) + M \right)O(N⋅log(N)+M) với NN là số công việc cần thực hiện, MM là số cặp công việc phải thực hiện theo đúng thứ tự.
 */
public class TopologicalSorting {

    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
    private static ArrayList<Integer> result = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
        }
        for (int i = 0; i < m; i++) {
            int x = sc.nextInt() - 1;
            int y = sc.nextInt() - 1;
            graph.get(x).add(y);
        }
        boolean isValid = topologicalSortWithPriorityQueue();
        if (isValid) {
            for (int i = 0; i < result.size(); i++) {
                System.out.print((result.get(i) + 1) + " ");
            }
        } else {
            System.out.println("Sandro fails.");
        }
    }

    private static boolean topologicalSortWithPriorityQueue() {
        int v = graph.size();
        int[] inDegree = new int[v];
        PriorityQueue<Integer> zeroQueue = new PriorityQueue<>();
        for (int i = 0; i < graph.size(); i++) {
            for (int j : graph.get(i)) {
                inDegree[j]++;
            }
        }

        for (int i = 0; i < graph.size(); i++) {
            if (inDegree[i] == 0) {
                zeroQueue.add(i);
            }
        }

        while (!zeroQueue.isEmpty()) {
            int u = zeroQueue.poll();
            result.add(u);
            for (int i : graph.get(u)) {
                inDegree[i]--;
                if (inDegree[i] == 0) {
                    zeroQueue.add(i);
                }
            }
        }
        for (int j = 0; j < inDegree.length; j++) {
            if (inDegree[j] != 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean topologicalSortNormal() {
        int v = graph.size();
        int[] inDegree = new int[v];
        Queue<Integer> zeroQueue = new LinkedList<>();
        for (int i = 0; i < graph.size(); i++) {
            for (int j : graph.get(i)) {
                inDegree[j]++;
            }
        }

        for (int i = 0; i < graph.size(); i++) {
            if (inDegree[i] == 0) {
                zeroQueue.add(i);
            }
        }

        while (!zeroQueue.isEmpty()) {
            int u = zeroQueue.poll();
            result.add(u);
            for (int i : graph.get(u)) {
                inDegree[i]--;
                if (inDegree[i] == 0) {
                    zeroQueue.add(i);
                }
            }
        }
        for (int j = 0; j < inDegree.length; j++) {
            if (inDegree[j] != 0) {
                return false;
            }
        }
        return true;
    }

    public static void topologicalSorting(ArrayList<ArrayList<Integer>> graph, ArrayList<Integer> result) {
        int v = graph.size();
        boolean[] visited = new boolean[v];
        for (int i = 0; i < graph.size(); i++) {
            if (!visited[i]) {
                dfs(i, graph, visited, result);
            }
        }
    }

    public static void dfs(int u,
                           ArrayList<ArrayList<Integer>> graph,
                           boolean[] visited,
                           ArrayList<Integer> result) {
        visited[u] = true;
        for (int i = 0; i < graph.get(u).size(); i++) {
            int v = graph.get(u).get(i);
            if (!visited[graph.get(u).get(i)]) {
                dfs(v, graph, visited, result);
            }
        }
        result.add(u);
    }
}
/*
import java.util.*;

class Solution_kahn {
    public static int n, m;

    public static ArrayList<Integer> topoSort(ArrayList<ArrayList<Integer>> adj, int[] indegree) {
        PriorityQueue<Integer> zero_indegree = new PriorityQueue<Integer>();
        ArrayList<Integer> topoSorted = new ArrayList<Integer>();

        for (int i = 0; i < n; i++) {
            if (indegree[i] == 0)
                zero_indegree.add(i);
        }

        while (!zero_indegree.isEmpty()) {
            int u = zero_indegree.peek();
            zero_indegree.remove();

            topoSorted.add(u);

            for (int i = 0; i < adj.get(u).size(); i++) {
                int v = adj.get(u).get(i);
                indegree[v]--;
                if (indegree[v] == 0)
                    zero_indegree.add(v);
            }
        }

        return topoSorted;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        m = sc.nextInt();

        ArrayList<ArrayList<Integer>> adj = new ArrayList<ArrayList<Integer>>(n);
        for (int i = 0; i < n; i++)
            adj.add(i, new ArrayList<Integer>());
        int[] indegree = new int[n];

        for (int i = 0; i < m; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            adj.get(u - 1).add(v - 1);
            indegree[v - 1]++;
        }

        ArrayList<Integer> res = topoSort(adj, indegree);
        if (res.size() < n)
        {
            System.out.print("Sandro fails.");
            return;
        }

        for (int i = 0; i < n; i++)
            System.out.printf("%d ", res.get(i) + 1);

        sc.close();
    }
}
 */

/*
#include <iostream>
#include <vector>
#include <queue>
#include <set>

using namespace std;

vector<int> topoSort(vector< vector<int> > &adj, vector<int> &indegree){
    int n = indegree.size();
    priority_queue<int, vector<int>, greater<int> > zero_indegree;
    vector<int> topoSorted;
    // vector<bool> avail(n, true);

    for (int i = 0; i < indegree.size(); i++)
        if (indegree[i] == 0){
            zero_indegree.push(i);
            // avail[i] = false;
        }


    while (!zero_indegree.empty()){
        int u = zero_indegree.top();
        zero_indegree.pop();

        topoSorted.push_back(u);

        for (int i = 0;i < adj[u].size(); i++){
            int v = adj[u][i];
            indegree[v] --;
            if (indegree[v] == 0){
                zero_indegree.push(v);
            }
        }
    }


    return topoSorted;
}

int main(){
    // n: number of vertices
    // m: number of edges
    int n, m, u, v;
    vector< vector<int> > adj;
    vector<int> indegree;
    scanf("%d %d", &n, &m);
    adj.resize(n);
    indegree.assign(n, 0);
    for (int i = 0; i < m; i++){
        scanf("%d %d", &u, &v);
        adj[u - 1].push_back(v - 1);
        indegree[v - 1] ++;
    }

    vector<int> res = topoSort(adj, indegree);
    if (res.size() < n){
        printf("Sandro fails.\n");
        return 0;
    }

    for (int i = 0; i < res.size(); i++)
        printf("%d ", res[i] + 1);
    printf("\n");

    return 0;
}
 */