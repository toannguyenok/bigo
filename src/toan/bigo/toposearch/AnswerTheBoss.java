package toan.bigo.toposearch;

import java.util.*;

/*
Với mỗi bộ test trong bài này, ta có thể xây dựng một đồ thị như sau: mỗi nhân viên được xem là một đỉnh, mỗi quan hệ uu có rank cao hơn vv được xem là một cạnh có hướng từ uu đến vv.

Ta nhận xét rằng đây là một đồ thị có hướng và không có chu trình đơn, vì sẽ không có chuyện người aa cấp bậc cao hơn người bb, người bb cấp bậc cao hơn người cc và người cc lại có cấp bậc cao hơn người aa được.

Trước hết, ta lưu ý về việc tạo đồ thị. Với mỗi cặp số (u, v)(u,v) từ dữ liệu vào, vv có cấp bậc nhỏ hơn uu nên ta cần tạo một cạnh nối từ vv sang uu chứ không phải từ uu sang vv.

Gọi inDeg[u] + 1inDeg[u]+1 là số lượng cạnh nhận uu là đỉnh cuối. Với mỗi cạnh từ vv sang uu, ta tăng inDeg[u] + 1inDeg[u]+1 lên 11 đơn vị.

Ta thực hiện việc sắp xếp Topo trên đồ thị. Sau đó, duyệt theo thứ tự topo từng đỉnh:

Nếu inDeg[u] = 0inDeg[u]=0, tức không có ai có rank cao hơn nhân viên uu, ta gán rank[u] = 1rank[u]=1.
Duyệt các đỉnh vv kề với uu: Nếu rank[v] < rank[u] + 1rank[v]<rank[u]+1 thì ta cập nhật lại rank[v] = rank[u] + 1rank[v]=rank[u]+1, do nếu có một cạnh từ uu đến vv thì rank[v]rank[v] phải ít nhất bằng rank[u] + 1rank[u]+1.
Tạo một mảng động lưu thông tin của các nhân viên: mỗi object tương ứng với một nhân viên lưu 2 thông tin là rank và chỉ số của nhân viên đó.

Thực hiện sắp xếp mảng động theo rank tăng dần, nếu rank bằng nhau thì sắp xếp theo chỉ số tăng dần.

In ra kết quả lưu trong mảng động và kết thúc việc xử lý một bộ test.

Độ phức tạp: \mathcal{O} \left( N + R \right)O(N+R) với NN là số lượng nhân viên - tương ứng với số lượng đỉnh trong đồ thị và RR là số lượng mối quan hệ - tương ứng với số lượng cạnh trong đồ thị.
 */
public class AnswerTheBoss {

    private static class Pair {
        int position;
        int rank;

        public Pair(int position, int rank) {
            this.position = position;
            this.rank = rank;
        }
    }

    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int testCase = sc.nextInt();
        ArrayList<ArrayList<Pair>> answer = new ArrayList<>();
        for (int t = 0; t < testCase; t++) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            graph.clear();
            for (int i = 0; i < n; i++) {
                graph.add(new ArrayList<>());
            }
            for (int i = 0; i < m; i++) {
                int x = sc.nextInt();
                int y = sc.nextInt();
                graph.get(y).add(x);
            }
            ArrayList<Pair> result = topologicalSort();
            answer.add(result);
        }
        for (int i = 0; i < answer.size(); i++) {
            System.out.println("Scenario #" + (i + 1) + ":");
            for (int j = 0; j < answer.get(i).size(); j++) {
                System.out.println((answer.get(i).get(j).rank + 1) + " " + (answer.get(i).get(j).position));
            }
        }
    }

    private static ArrayList<Pair> topologicalSort() {
        ArrayList<Pair> result = new ArrayList<>();
        int v = graph.size();
        int[] inDegree = new int[v];
        PriorityQueue<Pair> zeroQueue = new PriorityQueue<>(new Comparator<Pair>() {
            @Override
            public int compare(Pair o1, Pair o2) {
                if(o1.rank == o2.rank) {
                    return o1.position - o2.position;
                }
                return o1.rank - o2.rank;
            }
        });
        for (int i = 0; i < graph.size(); i++) {
            for (int j : graph.get(i)) {
                inDegree[j]++;
            }
        }

        for (int i = 0; i < graph.size(); i++) {
            if (inDegree[i] == 0) {
                zeroQueue.add(new Pair(i, 0));
            }
        }

        while (!zeroQueue.isEmpty()) {
            Pair u = zeroQueue.poll();
            result.add(u);
            for (int i : graph.get(u.position)) {
                inDegree[i]--;
                if (inDegree[i] == 0) {
                    zeroQueue.add(new Pair(i, u.rank + 1));
                }
            }
        }
        return result;
    }
}

/*
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Main {
    static Scanner in;
    static PrintWriter out;

    public static void main(String[] args) {
        in = new Scanner(System.in);
        out = new PrintWriter(System.out);

        int t = in.nextInt();

        for (int testNumber = 1; testNumber <= t; testNumber++) {
            solveTest(testNumber);
        }

        out.close();
    }

    static void solveTest(int testNumber) {
        int n = in.nextInt();
        int r = in.nextInt();

        List<List<Integer>> adj = new ArrayList<>();
        int[] inDeg = new int[n];

        for (int u = 0; u < n; u++) {
            adj.add(new ArrayList<Integer>());
        }

        for (int i = 0; i < r; i++) {
            int u = in.nextInt();
            int v = in.nextInt();
            adj.get(v).add(u);
            ++inDeg[u];
        }

        ArrayList<Integer> topoOrder = topoSort(adj);
        int[] rank = new int[n];

        for (int u : topoOrder) {
            if (inDeg[u] == 0) {
                rank[u] = 1;
            }
            for (int v : adj.get(u)) {
                rank[v] = Math.max(rank[v], rank[u] + 1);
            }
        }

        ArrayList<Employee> employees = new ArrayList<>();

        for (int u = 0; u < n; u++) {
            employees.add(new Employee(u, rank[u]));
        }

        Collections.sort(employees, new Comparator<Employee>() {
            public int compare(Employee e1, Employee e2) {
                if (e1.rank == e2.rank) {
                    return Integer.compare(e1.index, e2.index);
                }
                return Integer.compare(e1.rank, e2.rank);
            }
        });


        out.println("Scenario #" + testNumber + ":");

        for (Employee e : employees) {
            out.println(e.rank + " " + e.index);
        }
    }

    static ArrayList<Integer> topoSort(List<List<Integer>> adj) {
        ArrayList<Integer> topoOrder = new ArrayList<>();
        int n = adj.size();
        boolean[] visited = new boolean[n];

        for (int u = 0; u < n; u++) {
            if (!visited[u]) {
                dfs(u, adj, visited, topoOrder);
            }
        }

        Collections.reverse(topoOrder);

        return topoOrder;
    }

    static void dfs(int u, List<List<Integer>> adj, boolean[] visited, ArrayList<Integer> topoOrder) {
        visited[u] = true;

        for (int v : adj.get(u)) {
            if (!visited[v]) {
                dfs(v, adj, visited, topoOrder);
            }
        }

        topoOrder.add(u);
    }

    static class Employee {
        int index;
        int rank;

        Employee(int index, int rank) {
            this.index = index;
            this.rank = rank;
        }
    }
}
 */

/*
#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

struct Employee {
    int index;
    int rank;

    Employee(int index, int rank) : index(index), rank(rank) {}
};

void dfs(int u, vector<vector<int>>& adj, vector<int>& visited, vector<int>& topoOrder) {
    visited[u] = 1;

    for (int v : adj[u]) {
        if (!visited[v]) {
            dfs(v, adj, visited, topoOrder);
        }
    }

    topoOrder.push_back(u);
}

vector<int> topoSort(vector<vector<int>>& adj) {
    vector<int> topoOrder;
    int n = adj.size();
    vector<int> visited(n, 0);

    for (int u = 0; u < n; u++) {
        if (!visited[u]) {
            dfs(u, adj, visited, topoOrder);
        }
    }

    reverse(topoOrder.begin(), topoOrder.end());

    return topoOrder;
}

void solveTest(int testNumber) {
    int n, r;
    cin >> n >> r;

    vector<vector<int>> adj(n, vector<int>());
    vector<int> inDeg(n, 0);

    for (int i = 0; i < r; i++) {
        int u, v;
        cin >> u >> v;
        adj[v].push_back(u);
        ++inDeg[u];
    }

    vector<int> topoOrder = topoSort(adj);
    vector<int> rank(n);

    for (int u  : topoOrder) {
        if (inDeg[u] == 0) {
            rank[u] = 1;
        }
        for (int v : adj[u]) {
            rank[v] = max(rank[v], rank[u] + 1);
        }
    }

    vector<Employee> employees;

    for (int u = 0; u < n; u++) {
        employees.push_back(Employee(u, rank[u]));
    }

    sort(employees.begin(), employees.end(), [](const Employee& e1, const Employee& e2) {
        if (e1.rank == e2.rank) {
            return e1.index < e2.index;
        }
        return e1.rank < e2.rank;
    });

    cout << "Scenario #" << testNumber << ":\n";

    for (Employee& e : employees) {
        cout << e.rank << ' ' << e.index << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int testNumber = 1; testNumber <= t; testNumber++) {
        solveTest(testNumber);
    }

    return 0;
}
 */