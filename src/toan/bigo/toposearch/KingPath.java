package toan.bigo.toposearch;

import java.util.*;

public class KingPath {

    private static int MAX = (int) (10e5 + 5);
    private static int[] dx = {0, 0, 1, 1, 1, -1, -1, -1};
    private static int[] dy = {1, -1, 0, 1, -1, 0, 1, -1};

    private static class Point {
        int r;
        int c;

        public Point(int r, int c) {
            this.r = r;
            this.c = c;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return r == point.r &&
                    c == point.c;
        }

        @Override
        public int hashCode() {
            return Objects.hash(r, c);
        }
    }

    private static HashMap<Point, Integer> graphCanMove = new HashMap<>();
    private static HashMap<Point, Integer> dist = new HashMap<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int startR = scanner.nextInt();
        int startC = scanner.nextInt();
        graphCanMove.put(new Point(startR, startC), 1);

        int endR = scanner.nextInt();
        int endC = scanner.nextInt();
        graphCanMove.put(new Point(endR, endC), 1);

        int lines = scanner.nextInt();
        for (int i = 0; i < lines; i++) {
            int r = scanner.nextInt();
            int cStart = scanner.nextInt();
            int cEnd = scanner.nextInt();
            for (int j = cStart; j <= cEnd; j++) {
                graphCanMove.put(new Point(r, j), 1);
                dist.put(new Point(r, j), MAX);
            }
        }
        int ans = findResult(startR, startC, endR, endC);
        System.out.println(ans + "");
    }

    private static int findResult(int startX, int startY, int endX, int endY) {
        Queue<Point> queue = new LinkedList<>();
        queue.add(new Point(startX, startY));
        dist.put(new Point(startX, startY), 0);
        int ans = MAX;
        while (!queue.isEmpty()) {
            Point current = queue.remove();
            for (int i = 0; i < 8; i++) {
                int newR = current.r + dy[i];
                int newC = current.c + dx[i];
                if (graphCanMove.containsKey(new Point(newR,newC)) &&
                        graphCanMove.get(new Point(newR, newC)) == 1 &&
                        dist.get(new Point(newR, newC)) > dist.get(new Point(current.r, current.c)) + 1) {
                    dist.put(new Point(newR, newC), dist.get(new Point(current.r, current.c)) + 1);
                    queue.add(new Point(newR, newC));
                    if (newR == endX && newC == endY) {
                        ans = Math.min(ans, dist.get(new Point(newR,newC)));
                        return ans;
                    }
                }
            }
        }
        return -1;
    }
}
