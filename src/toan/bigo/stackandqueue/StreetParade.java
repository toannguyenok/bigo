package toan.bigo.stackandqueue;

import java.util.Scanner;
import java.util.Stack;

/*
Nhận xét:

Ta có thể mô phỏng hoạt động của con hẻm bằng CTDL stack với nguyên lý xe nào vào hẻm sau thì có thể đi ra trước.
Các chiếc xe được trình diễn theo số thứ tự từ nhỏ đến lớn, tức từ xe có số thứ tự 1 đến số thứ tự n. Do đó, ta hoàn toàn có thể biết được chiếc xe hiện tại đang cần đưa ra trình diễn có số thứ tự là bao nhiêu bằng cách sử dụng một biến đếm.
Như vậy, với mỗi lần cần đưa một chiếc xe ra trình diễn, ta đối chiếu số thứ tự của xe được yêu cầu với xe ở bãi đỗ và xe ở hẻm để quyết định đưa xe nào ra trình diễn. Trong trường hợp không có xe nào được đem ra, ta cần đưa xe đang đỗ trong bãi vào hẻm để xét xe kế tiếp nằm trong bãi.
Ta có các bước giải bài này như sau:

Bước 1: Đọc thông tin các xe hiện có trong bãi vào một mảng.
Bước 2: Khởi tạo một stack dùng để mô phỏng con hẻm. Ban đầu stack rỗng.
Bước 3: Sử dụng một biến đếm lưu số thứ tự xe cần để đem ra trình diễn với giá trị khởi tạo bằng 11.
Bước 4: Duyệt qua từng chiếc xe trong bãi:
Nếu số thứ tự đang cần đúng bằng số thứ tự xe trong bãi đỗ xe thì đưa xe này ra trình diễn và tăng thứ tự trình diễn.
Ngược lại, xét xem xe trong hẻm có phù hợp không:
Nếu có, đem xe trong hẻm ra trình diễn và tăng thứ tự trình diễn.
Nếu không, nghĩa là cả xe đang nằm trong bãi và xe đầu hẻm đều không đúng thứ tự, ta đưa xe hiện tại trong bãi vào hẻm để xét xe kế tiếp.
Bước 5: Nếu hết xe trong bãi đỗ, ta cần kiểm tra các xe còn lại trong hẻm có thỏa thứ tự còn lại hay không và đưa ra trình diễn nếu đúng.
Bước 6: Kiểm tra biến lưu thứ tự trình diễn. Nếu biến đạt tới thứ tự (N + 1)(N+1) nghĩa là ta đã đem đủ n chiếc xe ra trình diễn, in “yes”. Ngược lại, in “no”.
Bước 7: Reset các biến cần thiết, chuẩn bị cho bộ test tiếp theo.
Độ phức tạp: \mathcal {O} \left(T * N \right)O(T∗N) với TT là số lượng bộ test và NN là số lượng xe hiện có trong bãi đỗ.
 */
public class StreetParade {

    public static void main(String[] args) {
        // write your code here
        int n = 1;

        Scanner sc = new Scanner(System.in);
        while(n != 0) {
            n = sc.nextInt();
            if(n != 0) {
                int[] mobiles = new int[n];
                Stack<Integer> stack = new Stack<>();

                for (int i = 0; i < n; i++) {
                    mobiles[i] = sc.nextInt();
                }
                int start = 1;
                boolean valid = true;
                for (int i = 0; i < n; i++) {
                    while (stack.size() > 0 && stack.peek() == start) {
                        stack.pop();
                        start++;
                    }
                    if (mobiles[i] == start) {
                        start++;
                    } else if (stack.size() > 0 && stack.peek() < mobiles[i]) {
                        valid = false;
                        break;
                    } else {
                        stack.add(mobiles[i]);
                    }
                }
                System.out.println(valid ? "yes" : "no");
            }
        }
    }
}
/*
import java.util.Scanner;
import java.util.Stack;

public class Main {
    static final int MAX = 1005;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] trucks = new int[MAX];
        Stack<Integer> side_trucks = new Stack<>();

        while (true) {
            int n = sc.nextInt();
            if (n == 0) {
                break;
            }

            for (int i = 0; i < n; i++) {
                trucks[i] = sc.nextInt();
            }

            int ordering = 1;
            int i = 0;

            while (i < n) {
                if (trucks[i] == ordering) {
                    ordering++;
                    i++;
                }
                else if (!side_trucks.empty() && side_trucks.peek().equals(ordering)) {
                    ordering++;
                    side_trucks.pop();
                }
                else {
                    side_trucks.push(trucks[i]);
                    i++;
                }
            }

            while (!side_trucks.empty() && side_trucks.peek().equals(ordering)) {
                ordering++;
                side_trucks.pop();
            }

            System.out.println(ordering == n + 1 ? "yes" : "no");
            side_trucks.clear();
        }
    }
}
 */