package toan.bigo.stackandqueue;

import java.util.Scanner;
import java.util.Stack;

/*
Nhận xét:

Xét trên phạm vi một biểu thức lớn có thể có nhiều ngoặc lồng nhau, ta thấy rằng dấu mở ngoặc chính là báo hiệu bắt đầu của một biểu thức con mới, còn dấu đóng ngoặc chính là báo hiệu kết thúc của một biểu thức con liền kề trước nó. \to→ Gần với nguyên tắc hoạt động LIFO của stack.
Hơn nữa, khác với biểu thức thông thường có phép toán được đặt giữa hai toán hạng thì trong cách biểu diễn theo ký pháp Ba Lan ngược, phép toán được thực hiện trên hai toán hạng liền trước nó. Như vậy, với một biểu thức a + b, ta có thể chuyển qua dạng ab++ dễ dàng bằng cách in ra các toán hạng trước, sau đó sẽ in ra phép toán ++.
Từ những nhận định trên, ta nảy ra ý tưởng chuyển từ biểu thức có dạng ((a ++ b)) sang ab++ bằng cách: Duyệt qua chuỗi ký tự, nếu gặp dấu ngoặc “((“ thì ta bỏ qua, gặp ký tự chữ cái thì in thẳng ra màn hình, còn gặp phép toán thì bỏ nó vào stack. Như vậy, khi gặp được dấu “))” tiếp theo, ta chắc chắn phép toán trên đầu stack thuộc về biểu thức đang được in ra trên màn hình, ta in phép toán này ra và xóa nó ra khỏi stack.
Trường hợp các dấu ngoặc lồng nhau chỉ là một trường hợp đặc biệt có thể giải quyết bằng ý tưởng trên.
Như vậy, ta có cách giải của bài này như sau:

Bước 1: Đọc vào biểu thức cần chuẩn hóa.
Bước 2: Duyệt qua từng ký tự từ đầu tới cuối chuỗi, nếu ký tự đó là:
Ký tự chữ cái: in ra màn hình.
Dấu đóng ngoặc “))”: in phép toán đầu tiên trong stack ra màn hình.
Phép toán hợp lệ: đưa vào stack.
Bước 3: Reset các biến, chuẩn bị cho bộ test tiếp theo.
Độ phức tạp: \mathcal {O}\left(T * len(expression)\right)O(T∗len(expression)) với TT là số lượng bộ test và len(expression)len(expression) là độ dài biểu thức.
 */
public class TransformTheExpression {
    public static void main(String[] args) {
        // write your code here
        int n;

        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            String expression = sc.next();
            Stack<Character> stack = new Stack<>();
            for (int j = 0; j < expression.length(); j++) {
                char symbol = expression.charAt(j);
                if (Character.isLetter(symbol)) {
                    System.out.print(symbol);
                } else if (symbol == ')') {
                    System.out.print(stack.pop());
                } else if (symbol != '(') {
                    stack.push(symbol);
                }
            }
            System.out.println();
        }
    }
}
/*
import java.util.Scanner;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        Stack<Character> s = new Stack<>();

        for (int i = 0; i < t; i++) {
            char[] expression = sc.next().toCharArray();
            for (Character symbol : expression) {
                if (Character.isLetter(symbol)) {
                    System.out.print(symbol);
                }
                else if (symbol.equals(')')) {
                    System.out.print(s.pop());
                }
                else if (!symbol.equals('(')) {
                    s.add(symbol);
                }
            }
            System.out.println();
        }
    }
}
 */