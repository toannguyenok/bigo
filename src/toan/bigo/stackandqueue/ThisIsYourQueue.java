package toan.bigo.stackandqueue;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/*
Nhận xét:

Hàng đợi khám bệnh có thể dễ dàng được mô phỏng bằng cấu trúc queue với quy tắc FIFO.

Ban đầu, các bệnh nhân xếp hàng theo thứ tự từ nhỏ đến lớn, tương đương với việc ta đưa vào hàng đợi lần lượt các phần tử có giá trị từ 11 đến PP. Như vậy đối với truy vấn NN, ta chỉ việc lấy phần tử ở đầu hàng đợi ra và đưa lại vào cuối hàng.

Đối với truy vấn EE xx, ta cần đưa phần tử có giá trị đúng bằng xx lên đầu hàng đợi. Yêu cầu này có thể được thực hiện thông qua hai bước như sau:

Đưa giá trị xx vào cuối hàng.

Lần lượt lấy từng giá trị (khác xx) ở đầu hàng và đưa vào sau xx.

Cụ thể, giả sử hàng đợi lúc này của ta có dạng {1, 2, 3, x, 4, 51,2,3,x,4,5}. Đầu tiên, ta sẽ đưa xx vào cuối hàng đợi, thu được {1, 2, 3, x, 4, 5, x1,2,3,x,4,5,x}. Lúc này, để xx giữ vai trò là phần tử đầu tiên của hàng đợi mới, ta lần lượt đưa các phần tử còn lại (khác xx) vào sau xx. Hàng đợi trở thành {x, 1, 2, 3, 4, 5x,1,2,3,4,5}.

Tuy nhiên, ta thấy rằng nếu chỉ thực hiện như các bước trên thì độ phức tạp sẽ rất lớn do hàng đợi lúc này có kích thước đúng bằng PP với PP tối đa là 1.000.000.0001.000.000.000 – chi phí lấy ra và đưa ngược lại toàn bộ các phần tử hiện có trong hàng đợi khi xử lý truy vấn E xEx là vô cùng tốn kém.

Mặt khác, ta cũng nhận thấy rằng trong trường hợp CC truy vấn đều thuộc NN và giá trị của C < PC<P, chẳng hạn ta có một hàng đợi 1010 người {1, 2, 3, 4, 5, 6, 7, 8, 9, 101,2,3,4,5,6,7,8,9,10} và chỉ thực hiện 33 truy vấn NN thì tối đa ta chỉ thao tác trên 33 phần tử {1, 2, 31,2,3} mà thôi. Nói cách khác, việc khởi tạo một hàng đợi 1010 người là không cần thiết. Do đó, để tối ưu ta chỉ cần khởi tạo hàng đợi ban đầu với các giá trị từ 11 đến min(PP, CC) (với CC tối đa chỉ bằng 1.0001.000). (Lưu ý rằng với cách khởi tạo này, khi xuất hiện các truy vấn E xEx không làm mất tính chính xác của giải thuật vì nếu xx không tồn tại trong các giá trị được khởi tạo ban đầu của hàng đợi thì cũng tự động được thêm vào.)

Tóm lại, ta có các bước giải này như sau:

Bước 1: Khởi tạo hàng đợi khám bệnh. Đưa vào hàng đợi lần lượt các giá trị từ 11 đến min(PP, CC).
Bước 2: Duyệt qua từng truy vấn:
Truy vấn NN: In ra phần tử đầu tiên ở đầu hàng đợi, xóa phần tử đó và đưa nó lại vào cuối hàng.
Truy vấn E x:
Gán nn là kích thước hiện tại của hàng đợi.
Đưa xx vào cuối hàng đợi.
Lặp nn lần, đưa lần lượt các phần tử khác xx vào cuối hàng.
Bước 3: Reset các biến, chuẩn bị cho bộ test tiếp theo.
Độ phức tạp: \mathcal {O} \left (T*C*(min(P, C) + C) \right )O(T∗C∗(min(P,C)+C)) với TT là số lượng bộ test, PP là số lượng người đến khám bệnh và CC là số lượng truy vấn cần xử lý.
 */
public class ThisIsYourQueue {

    public static void main(String[] args) {
        // write your code here
        int p = 1;
        int c = 1;
        int countCase = 0;

        Scanner sc = new Scanner(System.in);
        while (p != 0 && c != 0) {
            countCase++;
            p = sc.nextInt();
            c = sc.nextInt();
            if (p != 0 && c != 0) {
                LinkedList<Integer> queue = new LinkedList<>();
                ArrayList<Integer> array = new ArrayList<>();
                int count = Math.min(1000,p);
                for (int i = 0; i < count; i++) {
                    queue.add(i + 1);
                }

                for (int i = 0; i < c; i++) {
                    String command = sc.next();
                    if (command.equals("N")) {
                        int process = queue.remove();
                        array.add(process);
                        queue.add(process);
                    } else if (command.equals("E")) {
                        int index = sc.nextInt();
                        queue.removeIf(integer -> integer == index);
                        queue.offerFirst(index);
                    }
                }
                System.out.print("Case " + countCase + ": ");
                System.out.println();
                for (int i = 0; i < array.size(); i++) {
                    System.out.println("" + array.get(i));
                }
            }
        }
    }
}
/*
import java.util.Scanner;
import java.util.Queue;
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int P, C, n, x, temp, tc = 1;
        char cmd;
        Queue<Integer> q = new LinkedList<Integer>();

        while (true) {
            P = sc.nextInt();
            C = sc.nextInt();

            if (P == 0 && C == 0) {
                break;
            }

            for (int i = 1; i <= Math.min(P, C); i++) {
                q.add(i);
            }

            System.out.format("Case %d:\n", tc++);

            for (int i = 0; i < C; i++) {
                cmd = sc.next().charAt(0);

                if (cmd == 'N') {
                    System.out.println(q.peek());
                    q.add(q.poll());
                }
                else {
                    x = sc.nextInt();
                    n = q.size();
                    q.add(x);

                    for (int j = 0; j < n; j++) {
                        temp = q.poll();
                        if (temp != x) {
                            q.add(temp);
                        }
                    }
                }
            }

            while (!q.isEmpty()) {
                q.remove();
            }
        }
    }
}
 */
