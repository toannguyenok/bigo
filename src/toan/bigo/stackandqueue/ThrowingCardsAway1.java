package toan.bigo.stackandqueue;

import java.util.*;

/*
Nhận xét:

Có thể mô phỏng thao tác rút lá bài ra tương tự như thao tác lấy dữ liệu từ queue và đưa lá bài vào giống như thêm dữ liệu vào queue (lá bài được thêm vào sẽ nằm ở cuối).

Ta có các bước thực hiện để giải bài toán như sau:

Bước 1: Đưa hết các lá bài theo thứ tự từ 11 đến NN vào CTDL queue.
Bước 2: Khi số lượng lá bài trong queue còn ít nhất hai lá:
Lấy lá bài trên đầu queue ra.
Lấy lá bài mới trên đầu queue ra và đưa lại vào cuối queue.
Bước 3: In ra kết quả theo định dạng trong các ví dụ.
Bước 4: Reset các biến cần thiết, chuẩn bị cho bộ test tiếp theo.
Lưu ý: Cần cẩn thận trong cách trình bày kết quả. Với trường hợp không rút được lá bài nào ra khỏi chồng bài (N = 1)(N=1), ta chỉ in ra "Discarded cards:" nhưng không có khoảng trắng ở cuối.

Độ phức tạp: \mathcal {O} \left(T * N \right)O(T∗N) với TT là số lượng bộ test và NN là số lượng lá bài.
 */
public class ThrowingCardsAway1 {

    public static void main(String[] args) {
        // write your code here
        int n = 1;

        Scanner sc = new Scanner(System.in);
        while (n != 0) {
            n = sc.nextInt();
            if (n != 0) {
                int lasCard = 0;
                Queue<Integer> queue = new LinkedList();
                ArrayList<Integer> discarded = new ArrayList<>();
                for (int i = 0; i < n; i++) {
                    queue.add(i);
                }
                while (queue.size() > 0) {
                    if (queue.size() == 1) {
                        lasCard = queue.remove() + 1;
                    } else {
                        int discard = queue.remove();
                        discarded.add(discard + 1);
                        int temp = queue.remove();
                        queue.add(temp);
                    }
                }
                System.out.print("Discarded cards:");
                for (int i = 0; i < discarded.size(); i++) {
                    if (i == discarded.size() - 1) {
                        System.out.print(" " + discarded.get(i));
                    } else {
                        System.out.print(" " +discarded.get(i) + ",");
                    }
                }
                System.out.println();
                System.out.println("Remaining card: " + lasCard + "");
            }
        }
    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        Queue<Integer> deck = new LinkedList<>();
        ArrayList<Integer> discarded_cards = new ArrayList<>();

        while (true) {
            int n = sc.nextInt();
            if (n == 0) {
                break;
            }

            for (int i = 1; i <= n; i++) {
                deck.add(i);
            }

            while (deck.size() >= 2) {
                discarded_cards.add(deck.poll());
                deck.add(deck.poll());
            }

            System.out.print("Discarded cards:");
            for (int i = 0; i < n - 1; i++) {
                if (i != 0) {
                    System.out.print(",");
                }
                System.out.format(" %d", discarded_cards.get(i));
            }
            System.out.println();
            System.out.format("Remaining card: %d\n", deck.poll());

            discarded_cards.clear();
        }
    }
}
 */
