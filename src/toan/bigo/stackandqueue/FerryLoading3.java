package toan.bigo.stackandqueue;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/*
Nhận xét:

Ở mỗi bên bờ sông, quá trình xếp hàng chờ đợi của những chiếc xe có thể được mô phỏng bằng cấu trúc dữ liệu queue. Ta có thể sử dụng 22 queue để mô phỏng quá trình này, với mỗi queue tương ứng với một bên bờ sông.
Các bước giải quyết bài toán như sau:

Bước 1: Khởi tạo 22 queue để lưu trữ tình trạng xếp hàng ở mỗi bên bờ sông.
Bước 2: Đọc và xử lí dữ liệu vào, lần lượt tương ứng với từng xe. Với xe thứ ii, ta cần đọc các dữ liệu vào:
Đọc vào thời gian đến nơi của xe thứ ii, tạm gọi là arriveTime[i]arriveTime[i]
Đọc vào vị trí bờ sông của xe thứ ii. Nếu bờ sông của xe thứ ii là left, ta push dữ liệu của xe thứ ii vào queue tương ứng của bờ sông bên trái. Ngược lại, nếu là right, ta push dữ liệu đó vào queue của bờ sông bên phải.
Bước 3: khởi tạo một số biến như sau:
curTimecurTime: cho biết thời gian hiện tại của quá trình mô phỏng. Giá trị ban đầu bằng 00.
curSidecurSide: nhận giá trị 00 và 11, cho biết con tàu hiện tại đang nằm bên bờ sông nào.
ans[]ans[]: mảng có MM phần tử (MM là số lượng xe), với ans[i]ans[i] cho biết thời gian cập bến của xe thứ ii
Bước 4: mô phỏng quá trình xe đang vận chuyển. Ta sẽ thực hiện liên tục như sau:
Nếu như cả 22 queue rỗng, tức là tất cả các xe đều đã được vận chuyển, ta kết thúc thuật toán.
Ngược lại, nếu ít nhất một queue còn xe, nghĩa là quá trình vận chuyển chưa kết thúc, ta xem xét xe tiếp theo cần được vận chuyển là xe nào. Ta sẽ xét arriveTimearriveTime đầu tiên của 22 queue (nếu queue đó vẫn còn xe), và lấy thời gian đến sớm hơn. Gọi thời gian đó là nextTimenextTime:
Nếu curTime < nextTimecurTime<nextTime, có nghĩa là xe tiếp theo chưa đến để xếp hàng. Ta sẽ phải đặt curTime = nextTimecurTime=nextTime để tiếp tục quá trình vận chuyển.
Xét queue ở bờ sông curSidecurSide, mà con thuyền đang neo đậu:
Chừng nào xe đầu tiên của queue bên curSidecurSide đã đến nơi (tức là arriveTime[i]arriveTime[i] của xe tiếp theo \le curTime≤curTime), ta đặt ans[i] = curTime + tans[i]=curTime+t (xe thứ ii đã được vận chuyển xong), và pop xe đó ra khỏi queue.
Lưu ý rằng tải trọng tối đa của thuyền là NN xe. Vì vậy ta cần đảm bảo rằng ta không pop một lần nhiều hơn NN xe ra khỏi queue. Việc này có thể được thực hiện bằng một biến đếm.
Đặt curTime = curTime + tcurTime=curTime+t, và curSide = 1 - curSidecurSide=1−curSide, có nghĩa là thuyền đã di chuyển sang bên kia sông, mất tt thời gian.
Bước 5: in ra mảng đáp án ans[]ans[].
Độ phức tạp: \mathcal {O} \left(T*M \right )O(T∗M) với MM là số lượng xe qua sông, TT là số lượng test
 */
public class FerryLoading3 {

    private static class Car {
        int i;
        int arrivedTime;

        public Car(int i, int arrivedTime) {
            this.i = i;
            this.arrivedTime = arrivedTime;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        int testCase = sc.nextInt();
        for (int tc = 0; tc < testCase; tc++) {
            int n = sc.nextInt();
            int t = sc.nextInt();
            int m = sc.nextInt();

            int currentTime = 0;
            int nextTime = 0;
            int currentSide = 0; //current side = 0 mean in left

            ArrayList<LinkedList<Car>> queues = new ArrayList<>();
            queues.add(new LinkedList<>());
            queues.add(new LinkedList<>());
            ArrayList<Integer> ans = new ArrayList<>();

            for (int i = 0; i < m; i++) {
                int arrivedTime = sc.nextInt();
                String side = sc.next();
                ans.add(0);
                if (side.equals("left")) {
                    queues.get(0).add(new Car(i, arrivedTime));
                } else {
                    queues.get(1).add(new Car(i, arrivedTime));
                }
            }

            int watingTime = (!queues.get(0).isEmpty() ? 1 : 0) + (!queues.get(1).isEmpty() ? 1 : 0);
            while (watingTime > 0) {
                if (watingTime == 1) {
                    nextTime = queues.get(0).isEmpty() ? queues.get(1).peek().arrivedTime : queues.get(0).peek().arrivedTime;
                } else {
                    nextTime = Math.min(queues.get(0).peek().arrivedTime, queues.get(1).peek().arrivedTime);
                }

                currentTime = Math.max(currentTime, nextTime);
                int carried = 0;
                while (!queues.get(currentSide).isEmpty()) {
                    Car car = queues.get(currentSide).peek();
                    if (car.arrivedTime <= currentTime && carried < n) {
                        queues.get(currentSide).remove();
                        ans.set(car.i, currentTime + t);
                        carried++;
                    } else {
                        break;
                    }
                }
                currentTime = currentTime + t;
                currentSide = 1 - currentSide;
                watingTime = (!queues.get(0).isEmpty() ? 1 : 0) + (!queues.get(1).isEmpty() ? 1 : 0);
            }
            result.add(ans);
        }

        for (int i = 0; i < result.size(); i++) {
            for (int j = 0; j < result.get(i).size(); j++) {
                System.out.println(result.get(i).get(j));
            }
            if(i != result.size() - 1) {
                System.out.println();
            }
        }
    }
}
/*
import java.util.Scanner;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
    static final int MAX = 10000 + 5;
    static class Car {
        int id, arriveTime;

        public Car(int _id, int _arriveTime) {
            this.id = _id;
            this.arriveTime = _arriveTime;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int Q = sc.nextInt();
        int res[] = new int[MAX];

        while (Q-- > 0) {
            Queue<Car> qSide[] = new Queue[2];
            qSide[0] = new LinkedList<>();
            qSide[1] = new LinkedList<>();

            int n = sc.nextInt();
            int t = sc.nextInt();
            int m = sc.nextInt();

            for (int i = 1; i <= m; i++) {
                int arrived = sc.nextInt();
                String atBank = sc.next();

                if (atBank.equals("left")) {
                    qSide[0].add(new Car(i, arrived));
                }
                else {
                    qSide[1].add(new Car(i, arrived));
                }
            }

            int curSide = 0, curTime = 0, nextTime;
            int waiting = (!qSide[0].isEmpty() ? 1 : 0) + (!qSide[1].isEmpty() ? 1 : 0);

            while (waiting > 0) {
                if (waiting == 1) {
                    nextTime = (qSide[0].isEmpty() ? qSide[1].peek().arriveTime : qSide[0].peek().arriveTime);
                }
                else {
                    nextTime = Math.min(qSide[0].peek().arriveTime, qSide[1].peek().arriveTime);
                }

                curTime = Math.max(curTime, nextTime);
                int carried = 0;

                while (!qSide[curSide].isEmpty()) {
                    Car car = qSide[curSide].peek();
                    if (car.arriveTime <= curTime && carried < n) {
                        res[car.id] = curTime + t;
                        carried++;
                        qSide[curSide].remove();
                    }
                    else {
                        break;
                    }
                }

                curTime += t;
                curSide = 1 - curSide;
                waiting = (!qSide[0].isEmpty() ? 1 : 0) + (!qSide[1].isEmpty() ? 1 : 0);
            }

            for (int i = 1; i <= m; i++) {
                System.out.println(res[i]);
            }

            if (Q > 0) {
                System.out.println();
            }
        }
    }
}
 */
