package toan.bigo.stackandqueue;

import java.util.Scanner;
import java.util.Stack;

/*
Duyệt từng ký tự trong chuỗi hóa chất:

Nếu ký tự đó là chữ ‘C’, ‘H’, hoặc ‘O’ thì bỏ lần lượt số 1212, 11, 1616 vào stack.

Nếu ký tự là số thì lấy phần tử đầu stack ra và nhân với số này, rồi bỏ lại vào trong stack.

Nếu ký tự đó là mở ngoặc ‘((‘ thì bỏ vào -1−1 stack giống ‘C’, ‘H’, ‘O’.

Nếu ký tự đó là đóng ngoặc ‘))’ thì lấy các số trong stack ra và cộng lại với nhau cho đến khi nào gặp -1−1 (nghĩa là dấu mở ngoặc) thì ngưng.

Khi duyệt xong toàn bộ chuỗi hóa chất thì lấy toàn bộ số trong stack ra cộng lại và đó là kết quả của bài toán cần tìm.

Độ phức tạp: \mathcal {O} \left ( N \right )O(N) với NN là độ dài của chuỗi hóa chất.
 */
public class MassOFMolecule {

    public static void main(String[] args) {
        // write your code here
        String input;

        Scanner sc = new Scanner(System.in);
        input = sc.next();

        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < input.length(); i++) {
            char symbol = input.charAt(i);
            if (symbol == 'C') {
                stack.add(12);
            } else if (symbol == 'H') {
                stack.add(1);
            } else if (symbol == 'O') {
                stack.add(16);
            } else if (Character.isDigit(symbol)) {
                int last = stack.pop();
                last = last * (symbol - '0');
                stack.add(last);
            } else if (symbol == '(') {
                stack.add(-1);
            } else if (symbol == ')') {
                int sumLast = 0;
                while (stack.peek() != -1) {
                    sumLast = sumLast + stack.pop();
                }
                stack.pop();
                stack.add(sumLast);
            }
        }

        int sum = 0;
        while (!stack.isEmpty()) {
            sum += stack.pop();
        }
        System.out.print(sum);
    }
}
/*
import java.util.Scanner;
import java.util.Stack;
import java.lang.Character;

public class Main {
    public static int mass(char c) {
        return c == 'H' ? 1 : (c == 'C' ? 12 : 16);
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        String s;
        s = sc.next();

        Stack<Integer> atom = new Stack<>();
        int w, mol;

        for (char c : s.toCharArray()) {
            if (Character.isLetter(c)) {
                atom.add(mass(c));
            }
            else if (Character.isDigit(c)) {
                mol = atom.peek() * (c - '0');
                atom.pop();
                atom.add(mol);
            }
            else if (c == '(') {
                atom.add(-1);
            }
            else if (c == ')') {
                w = 0;
                while (atom.peek() != -1) {
                    w += atom.peek();
                    atom.pop();
                }
                atom.pop();
                atom.add(w);
            }
        }

        int sum = 0;
        while (!atom.isEmpty()) {
            sum += atom.peek();
            atom.pop();
        }

        System.out.print(sum);
    }
}
 */
