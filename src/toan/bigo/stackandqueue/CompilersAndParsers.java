package toan.bigo.stackandqueue;

import java.util.Scanner;
import java.util.Stack;

/*
Với mỗi biểu thức, ta bắt đầu duyệt lần lượt từng kí tự trong chuỗi: Gặp kí tự ‘<<’ ta bỏ vào stack, ngược lại:

Xét xem stack có rỗng không: nếu rỗng nghĩa là có dấu ‘>>’ nhưng không có dấu ‘<<’ nào trước nó nên ta ngưng duyệt chuỗi.
Ngược lại, ta lấy phần đầu của stack ra và một lần nữa kiểm tra xem stack có rỗng không, nếu có ta cập nhật kết quả là i + 1i+1.
Độ phức tạp: O(T * N)O(T∗N) với TT là số lượng test case và NN là số lượng biểu thức.
 */
public class CompilersAndParsers {

    public static void main(String[] args) {
        // write your code here
        String input;

        int testCase;

        Scanner sc = new Scanner(System.in);
        testCase = sc.nextInt();
        for (int k = 0; k < testCase; k++) {
            input = sc.next();

            Stack<Character> stack = new Stack<>();
            int count = 0;
            for (int i = 0; i < input.length(); i++) {
                char symbol = input.charAt(i);
                if (symbol == '<') {
                    stack.add(symbol);
                } else if (stack.isEmpty()) {
                    break;
                } else {
                    stack.pop();
                    count = stack.isEmpty() ? i + 1 : count;
                }
            }

            System.out.println(count);
        }
    }
}
/*
import java.util.Scanner;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();

        for (int k = 0; k < T; k++) {
            String s = sc.next();
            Stack<Character> expr = new Stack<>();
            int ans = 0;

            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == '<') {
                    expr.push(s.charAt(i));
                }
                else if (expr.empty()) {
                    break;
                }
                else {
                    expr.pop();
                    ans = (expr.empty() ? i + 1 : ans);
                }
            }

            System.out.println(ans);
        }
    }
}
 */