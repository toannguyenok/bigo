package toan.bigo.numbertheory;

import java.util.Scanner;

/*
Hướng dẫn giải
Bài này ta sẽ đơn giản duyệt qua các ngày để xem các cặp bạn nam và bạn nữ để kiểm tra xem có ai trong hai người vui không. Ta có số lượng người không vui ban đầu (m + n - b - g)(m+n−b−g), mỗi lần duyệt một cặp nam – nữ nếu chỉ một người vui thì ta giảm lượng này đi 11. Nếu qua số ngày ta quy định mà lượng này vẫn chưa bằng 00 thì đáp án là "No" ngược lại là "Yes".

Nhưng câu hỏi đặt ra là số ngày cần phải duyệt qua là bao nhiêu?

Vào ngày thứ 00 thì bạn nam thứ 00 và bạn nữ thứ 00 được mời, ngày gần nhất tiếp theo mà 22 người này cùng nhau được mời là LCM(N, M)LCM(N,M) – bội chung nhỏ nhất của n, mn,m. Do: \left\{\begin{matrix} LCM(n, m) \% n = 0\\ LCM(n, m) \% m = 0 \end{matrix}\right.{
​LCM(n,m)%n=0
​LCM(n,m)%m=0
​​

Vậy số ngày tối đa bạn cần phải duyệt là 2 * LCM(m, n)2∗LCM(m,n) (để bạn nam thứ N - 1N−1 và nữ thứ M - 1M−1 có thể gặp nhau lần 22) với LCM(m, n) = \frac{m * n}{GCD(m, n)}LCM(m,n)=
​GCD(m,n)
​
​m∗n
​​ .

Độ phức tạp: O(LCM(n, m))O(LCM(n,m)) với n, mn,m là số bạn nam và nữ.
 */
public class DrazilAndHisFriend {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        boolean[] boys = new boolean[n];
        boolean[] girls = new boolean[m];
        int b = scanner.nextInt();
        for (int i = 0; i < b; i++) {
            int index = scanner.nextInt();
            boys[index] = true;
        }
        int g = scanner.nextInt();
        for (int i = 0; i < g; i++) {
            int index = scanner.nextInt();
            girls[index] = true;
        }

        for (int i = 0; i < 1000; i++) {
            int boyIndex = i % n;
            int girlIndex = i % m;
            if (boys[boyIndex] || girls[girlIndex]) {
                boys[boyIndex] = true;
                girls[girlIndex] = true;
            }
        }

        boolean flag = true;
        for (int i = 0; i < n; i++) {
            if (!boys[i]) {
                flag = false;
                break;
            }

        }

        for (int i = 0; i < m; i++) {
            if (!girls[i]) {
                flag = false;
                break;
            }
        }

        if (flag) {
            System.out.println("YES");
        }else
        {
            System.out.println("NO");
        }
    }
}
/*
#include <iostream>
using namespace std;
const int MAX = 105;

int gcd(int a, int b) {
   int r;
   while (b != 0) {
      r = a % b;
      a = b;
      b = r;
   }
   return a;
}

int main() {
   int n, m, b, g, x;
   bool happy_boy[MAX] = {}, happy_girl[MAX] = {};
   cin >> n >> m;

   cin >> b;
   for (int i = 0; i < b; i++) {
      cin >> x;
      happy_boy[x] = true;
   }

   cin >> g;
   for (int i = 0; i < g; i++) {
      cin >> x;
      happy_girl[x] = true;
   }

   int res = n + m - b - g;
   int lcm = n * m / gcd(n, m);

   for (int i = 0; i < 2 * lcm; i++) {
      if (happy_boy[i % n] + happy_girl[i % m] == 1) {
         happy_boy[i % n] = happy_girl[i % m] = true;
         res--;
      }
   }

   cout << (res == 0 ? "Yes" : "No");
   return 0;
}
 */

/*
import java.util.Scanner;

public class Main {
   private static int gcd(int a, int b) {
      int r;
      while (b != 0) {
         r = a % b;
         a = b;
         b = r;
      }
      return a;
   }

   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt(), m = sc.nextInt();
      boolean[] happy_boys = new boolean[n], happy_girls = new boolean[m];

      int b = sc.nextInt();
      for (int i = 0; i < b; i++) {
         happy_boys[sc.nextInt()] = true;
      }

      int g = sc.nextInt();
      for (int i = 0; i < g; i++) {
         happy_girls[sc.nextInt()] = true;
      }

      int res = n + m - b - g;
      int lcm = n * m / gcd(n, m);

      for (int i = 0; i < 2 * lcm; i++) {
         int state = (happy_boys[i % n] ? 1 : 0) + (happy_girls[i % m] ? 1 : 0);
         if (state == 1) {
            happy_boys[i % n] = happy_girls[i % m] = true;
            res--;
         }
      }

      System.out.print(res == 0 ? "Yes" : "No");
   }
}
 */