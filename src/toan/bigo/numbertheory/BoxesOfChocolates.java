package toan.bigo.numbertheory;

import java.util.Scanner;

/*
Hướng dẫn giải
Giải thích ví dụ:

Hình ảnh ví dụ minh họa hộp quà thứ nhất:

Số bạn của Pippy là N = 5N=5, số hộp quà mà Pippy có được là B = 2B=2.

Hộp quà thứ nhất có 22 hộp quà nhỏ bên trong. 22 hộp quà nhỏ bên trong, mỗi hộp có 33 hộp nhỏ bên trong nữa. Mỗi hộp nhỏ này có 44 viên kẹo.
\to→ Hộp quà thứ nhất có 2 \times 3 \times 4 = 242×3×4=24 viên kẹo
Tương tự hộp quà nhỏ thứ hai có 5 \times 2 \times 3 \times 1 = 305×2×3×1=30 viên kẹo. Vậy Pippy có tổng cộng 24 + 30 = 5424+30=54 viên kẹo. Mang chia đều cho 55 bạn, mỗi bạn sẽ được 5 / 45 = 105/45=10, dư 44.
Hướng dẫn giải:

Để tìm được số kẹo còn dư, trước tiên ta phải tìm được tổng số viên kẹo ở tất cả các hộp, sau đó thực hiện phép modulo cho NN - số lượng bạn bè.

Trong đó, tổng số viên kẹo trong một hộp chính là tích của dãy số a_1, a_2, ...,a_ka
​1
​​ ,a
​2
​​ ,...,a
​k
​​ . Tuy nhiên với trường hợp K = 100K=100 và a_i = 10000a
​i
​​ =10000 với mọi (0 < i \leq K)(0<i≤K) thì tổng số lượng kẹo thu được ở mỗi hộp là 10000^{100}10000
​100
​​  - là một số rất lớn, dẫn đến tràn số. Để giải vấn đề này, ta áp dụng hai tính chất của phép modulo:

(a \times b) \% m = ((a \% m) * (b \% m)) \% m(a×b)%m=((a%m)∗(b%m))%m
(a + b) \% m = ((a \% m) + (b \% m)) \% m(a+b)%m=((a%m)+(b%m))%m
Độ phức tạp: O(T * B * K)O(T∗B∗K) Với TT là số lượng test, BB là số hộp quà PippyPippy nhận được, và KK là số hộp quà mà hộp quà thứ ii chứa bên trong.


 */
public class BoxesOfChocolates {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCase = scanner.nextInt();
        for (int t = 0; t < testCase; t++) {
            int friends = scanner.nextInt();
            int boxes = scanner.nextInt();
            int totalKitty = 0;
            for (int i = 0; i < boxes; i++) {
                int first = scanner.nextInt();
                int temp = 1;
                for (int j = 0; j < first; j++) {
                    int smallBox = scanner.nextInt();
                    temp = (temp * smallBox) % friends;

                }
                totalKitty = (totalKitty + temp) % friends;
            }
            System.out.println(totalKitty);

        }
    }
}

/*
#include <iostream>

using namespace std;

void solve() {
    int N, B, K, sum;
    sum = 0;
    cin >> N >> B;
    while(B--) {
        int count = 1;
        cin >> K;
        for(int i = 1; i <= K; i++) {
            int x;
            cin >> x;
            count = (count * x) % N;
        }
        sum = (sum + count) % N;
    }
    cout << sum << endl;
}

int main()
{
    int t;
    cin >> t;
    while(t--) {
        solve();
    }
    return 0;
}
 */
/*
import java.util.*;
import java.lang.*;
import java.io.*;

class solution
{
	public static void main (String[] args)
	{
		Scanner inputStream = new Scanner(System.in);
        ArrayList<Integer> results = new ArrayList<>();

        int t = inputStream.nextInt();
        for (int i = 0; i < t; ++i) {
            int n = inputStream.nextInt();
            int b = inputStream.nextInt();
            int res = 0;

            for (int j = 0; j < b; ++j) {
                int k = inputStream.nextInt();
                int cnt = 1;

                for (int u = 0; u < k; ++u) {
                    int x = inputStream.nextInt();
                    cnt = (cnt * x) % n;
                }
                res = (res + cnt) % n;
            }
            results.add(res);
        }

        for (Integer res : results) {
            System.out.println(res);
        }
	}
}
 */