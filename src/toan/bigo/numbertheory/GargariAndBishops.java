package toan.bigo.numbertheory;

/*
Hướng dẫn giải
Ta nhận thấy rằng các ô nằm trên cùng một đường chéo song song với đường chéo chính sẽ có tổng tọa độ bằng nhau, và các ô nằm trên cùng một đường chéo song song với đường chéo phụ sẽ có hiệu tọa độ bằng nhau.

Như vậy để nhanh chóng tính toán tổng các phần tử nằm trên đường chéo đó, ta chỉ việc hash tọa độ của ô (x, y)(x,y) sang một con số K_1K
​1
​​  và K_2K
​2
​​  – những ô nằm trên cùng đường chéo song song với đường chéo chính sẽ có cùng mã hash K_1K
​1
​​  và tương tự với đường chéo phụ được mã hash K_2K
​2
​​ .

Ta có: K_1 = x + yK
​1
​​ =x+y và K_2 = x - y + nK
​2
​​ =x−y+n (cộng thêm nn để không bị tràn ra khỏi mảng).

Hơn nữa để thõa yêu cầu một ô không thể bị cả hai con tượng tấn công cùng một lúc, ta phải đặt hai con tượng ở hai ô sao cho tổng tọa độ của chúng không cùng là số chẵn hoặc không cùng là số lẻ. Nói cách khác nếu ta đặt con tượng thứ nhất ở ô có tổng tọa độ là số chẵn thì con tượng tiếp theo phải được đặt ở ô có tổng tọa độ là số lẻ và ngược lại.

Bằng cách này, việc giải bài toán của ta rút gọn trong các bước sau:

Bước 1: Tính tổng các phần tử trên đường chéo.

Bước 2: Duyệt qua từng ô (x, y) trên bàn cờ:

Nếu tổng tọa độ x + y là số chẵn: So sánh tổng phần tử trên hai đường chéo của nó với biến lưu kết quả của con tượng đầu tiên và cập nhật.

Làm tương tự nếu x + y là số lẻ, cập nhật vào biến kết quả của con tượng còn lại.

Độ phức tạp: O(n^2n
​2
​​ ) với nn là số dòng/cột của bàn cờ.


 */
public class GargariAndBishops {
}
/*
import java.util.*;
import java.io.*;

public class Main {
    public static void main(String[] agrs) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());

        int a[][] = new int[n + 1][n + 1];
        int x[] = new int[2], y[] = new int[2];
        long d1[] = new long[2 * (n + 1)], d2[] = new long[2 * (n + 1)];
        long res[] = new long[2];

        for (int i = 1; i <= n; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            for (int j = 1; j <= n; j++) {
                a[i][j] = Integer.parseInt(st.nextToken());
                d1[i + j] += a[i][j];
                d2[i - j + n] += a[i][j];
            }
        }

        res[0] = res[1] = -1;

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                int pos = (i + j) & 1;
                long sum = d1[i + j] + d2[i - j + n] - a[i][j];
                if (sum > res[pos]) {
                    res[pos] = sum;
                    x[pos] = i;
                    y[pos] = j;
                }
            }
        }

        System.out.println(res[0] + res[1]);
        System.out.println(x[0] + " " + y[0] + " " + x[1] + " " + y[1]);
    }
}
 */

/*
#include <bits/stdc++.h>
using namespace std;
const int MAX = 2005;

int a[MAX][MAX];
int x[2], y[2];
long long d1[2 * MAX], d2[2 * MAX];
long long res[2];

int main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            cin >> a[i][j];
            d1[i + j] += a[i][j];
            d2[i - j + n] += a[i][j];
        }
    }

    res[0] = res[1] = -1;

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            int pos = (i + j) & 1;
            long long sum = d1[i + j] + d2[i - j + n] - a[i][j];
            if (sum > res[pos]) {
                res[pos] = sum;
                x[pos] = i;
                y[pos] = j;
            }
        }
    }

    cout << res[0] + res[1] << endl;
    cout << x[0] << " " << y[0] << " " << x[1] << " " << y[1];
    return 0;
}
 */