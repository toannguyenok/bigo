package toan.bigo.numbertheory;

/*
Hướng dẫn giải
Giải thích ví dụ
Test case 1: Ta chỉ cần để 2 tàu khởi hành từ A vì 1 tàu khởi hành từ B đi chuyến 9:30 - 10:309:30−10:30 từ B đến A có thể đi tiếp chuyến 11:00 - 12:3011:00−12:30 từ A đến B. Vì thế số tàu ban đầu ở A và B đều là 2.

Test case 2: Ta cần 2 tàu riêng biệt cho 2 chuyến đi từ A đến B nên kết quả là 2 0.

Hướng dẫn giải
Khởi tạo biến result với giá trị NA. Gọi tripA, tripB lần lượt là danh sách các chuyến tàu khởi hành từ A và B.
Sắp xếp tripA theo chiều tăng dần của thời gian khởi hành. Sắp xếp tripB theo chiều tăng dần của thời gian đến đích. Sử dụng kỹ thuật two pointers đối với 2 danh sách các chuyến tàu trên, bất cứ khi nào thời gian khởi hành của 1 chuyến tàu từ A đến B lớn hơn thời gian đến đích của một chuyến tàu từ B đến A cộng với thời gian quay đầu thì ta giảm biến result đi 1. Kết quả cuối cùng của result sẽ là số chuyến tàu cần xuất phát từ A.
Lặp lại tương tự các bước trên với NB và hoán đổi vai trò của tripA và tripB trong các bước ấy, ta sẽ có được số chuyến tàu cần xuất phát từ B.
 */
public class TrainTimeTable {
}

/*
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <vector>
using namespace std;

// classes
struct byArrival {
    bool operator()(const pair<int, int>& a, const pair<int, int>& b) {
        return a.second < b.second;
    }
};

void print(vector<pair<int,int>> v) {
    for (int i = 0; i < v.size(); i++) {
        fprintf(stderr, "(%d - %d) ", v[i].first, v[i].second);
    }
    fprintf(stderr, "\n");
}

// main function
int main() {
    // freopen("INP.txt", "r", stdin);
    // freopen("OUT.txt", "w", stdout);

    cin.tie(NULL);
    cin.sync_with_stdio(false);
    int T;
    cin >> T;
    for (int p = 0; p < T; ++p) {
        int turn_time, n[2], result[2];
        string hour, minute;
        vector<pair<int, int>> trip[2];

        cin >> turn_time >> n[0] >> n[1];
        cin.get();
        cout << "Case #" << p + 1 << ':';
        for (int j = 0; j < 2; ++j) {
            for (int i = 0; i < n[j]; ++i) {
                getline(cin, hour, ':');
                getline(cin, minute, ' ');
                int start_time = stoi(hour) * 60 + stoi(minute);
                getline(cin, hour, ':');
                getline(cin, minute);
                int end_time = stoi(hour) * 60 + stoi(minute);
                end_time += turn_time;
                trip[j].push_back({start_time, end_time});
            }
        }
        result[0] = n[0];
        result[1] = n[1];
        for (int j = 0; j < 2; ++j) {
            int waiting = j, travelling = (j + 1) % 2;
            sort(trip[waiting].begin(), trip[waiting].end());
            sort(trip[travelling].begin(), trip[travelling].end(), byArrival());
            int i1 = 0, i2 = 0;
            while (i1 < n[waiting] && i2 < n[travelling]) {
                if (trip[waiting][i1].first >= trip[travelling][i2].second) {
                    result[waiting]--;
                    i2++;
                }
                i1++;
            }
        }

        cout << ' ' << result[0] << ' ' << result[1] << '\n';
    }
    return 0;
}
 */

/*
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.lang.StringBuilder;

class Main {
    private static class IntegerPair {
        Integer first;
        Integer second;

        public IntegerPair(Integer f, Integer s) {
            first = f;
            second = s;
        }

        public int first() {
            return first;
        }

        public int second() {
            return second;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        for (int p = 0; p < T; ++p) {
            int turn_time;
            int n[] = new int[2];
            int result[] = new int[2];

            ArrayList<ArrayList<IntegerPair>> trip = new ArrayList<ArrayList<IntegerPair>>();

            for (int i = 0; i < 2; ++i) {
                trip.add(new ArrayList<IntegerPair>());
            }

            turn_time = sc.nextInt();
            n[0] = sc.nextInt();
            n[1] = sc.nextInt();

            String hour;
            String minute;
            StringBuilder token;
            int sep;
            for (int j = 0; j < 2; ++j) {
                for (int i = 0; i < n[j]; ++i) {
                    token = new StringBuilder(sc.next());
                    sep = token.indexOf(":");
                    hour = token.substring(0, sep);
                    minute = token.substring(sep + 1, token.length());
                    // System.err.println(hour + " " + minute);
                    int start_time = Integer.parseInt(hour) * 60 + Integer.parseInt(minute);

                    token = new StringBuilder(sc.next());
                    sep = token.indexOf(":");
                    hour = token.substring(0, sep);
                    minute = token.substring(sep + 1, token.length());
                    // System.err.println(hour + " " + minute);
                    int end_time = Integer.parseInt(hour) * 60 + Integer.parseInt(minute);
                    end_time += turn_time;
                    trip.get(j).add(new IntegerPair(start_time, end_time));
                }
            }

            result[0] = n[0];
            result[1] = n[1];
            for (int j = 0; j < 2; ++j) {
                int waiting = j, travelling = (j + 1) % 2;

                Collections.sort(trip.get(waiting), new Comparator<IntegerPair>() {
                    @Override
                    public int compare(IntegerPair o1, IntegerPair o2) {
                        return o1.first() - o2.first();
                    }
                });

                Collections.sort(trip.get(travelling), new Comparator<IntegerPair>() {
                    @Override
                    public int compare(IntegerPair o1, IntegerPair o2) {
                        return o1.second() - o2.second();
                    }
                });

                int i1 = 0, i2 = 0;
                while (i1 < n[waiting] && i2 < n[travelling]) {
                    if (trip.get(waiting).get(i1).first() >= trip.get(travelling).get(i2).second()) {
                        result[waiting]--;
                        i2++;
                    }
                    i1++;
                }
            }

            System.out.printf("Case #%d: %d %d\n", p + 1, result[0], result[1]);
        }
        sc.close();
    }
}
 */