package toan.bigo.bst;

import java.util.*;

public class IsenBaevNumber {

    private static void bfs(int start, ArrayList<ArrayList<Integer>> graph, ArrayList<Integer> dist, ArrayList<Boolean> visited) {
        Queue<Integer> q = new LinkedList<>();
        q.add(start);
        visited.set(start, true);
        dist.set(start, 0);
        while (!q.isEmpty()) {
            int current = q.remove();
            for (int i = 0; i < graph.get(current).size(); i++) {
                int next = graph.get(current).get(i);
                if (!visited.get(next)) {
                    visited.set(next, true);
                    q.add(next);
                    dist.set(next, dist.get(current) + 1);
                }
            }
        }
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        TreeMap<String, Integer> bst = new TreeMap<>();
        ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
        ArrayList<Integer> dist = new ArrayList<>();
        ArrayList<Boolean> visited = new ArrayList<>();
        for(int i = 0 ;i<301;i++)
        {
            graph.add(new ArrayList<>());
            dist.add(0);
            visited.add(false);
        }
        int p = 1;
        for (int i = 0; i < n; i++) {

            String name1 = sc.next();
            String name2 = sc.next();
            String name3 = sc.next();
            if (!bst.containsKey(name1)) {
                bst.put(name1, p++);
            }
            if (!bst.containsKey(name2)) {
                bst.put(name2, p++);
            }
            if (!bst.containsKey(name3)) {
                bst.put(name3, p++);
            }

            graph.get(bst.get(name1)).add(bst.get(name2));
            graph.get(bst.get(name2)).add(bst.get(name1));

            graph.get(bst.get(name1)).add(bst.get(name3));
            graph.get(bst.get(name3)).add(bst.get(name1));

            graph.get(bst.get(name2)).add(bst.get(name3));
            graph.get(bst.get(name3)).add(bst.get(name2));

        }

       bfs(bst.get("Isenbaev"),graph,dist,visited);
        for(String key : bst.keySet())
        {
            if(bst.get(key) != 0)
            {
                if (!visited.get(bst.get(key)))
                    System.out.println(key + " undefined");
                else
                    System.out.println(key + " " + dist.get(bst.get(key)));
            }
        }
    }
}
