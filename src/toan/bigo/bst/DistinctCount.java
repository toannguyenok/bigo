package toan.bigo.bst;

import java.util.*;

public class DistinctCount {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        ArrayList<String> ans = new ArrayList<>();
        int testCase = sc.nextInt();
        for (int k = 0; k < testCase; k++) {
            int n = sc.nextInt();
            int x = sc.nextInt();
            TreeSet<Integer> bst = new TreeSet<>();
            for (int i = 0; i < n; i++) {
                int value = sc.nextInt();
                bst.add(value);
            }
            if (bst.size() == x) {
                ans.add("Good");
            } else if (bst.size() < x) {
                ans.add("Bad");
            } else {
                ans.add("Average");
            }
        }
        for (int i = 0; i < ans.size(); i++) {
            System.out.println(ans.get(i));
        }
    }
}
