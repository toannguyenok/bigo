package toan.bigo.bfs;

import java.util.*;

/*
Với bài này, ta phải thử đi hết các đường đi từ nút gốc (nhà Kefa) đến tất cả nút lá (nhà hàng) để xem có bao nhiêu nút lá có thể đến được. Vậy ta sẽ sử dụng BFS để giải quyết bài này.

Số đỉnh đánh dấu từ 11 nên khi lưu trữ dữ liệu và chạy BFS, ta cũng bắt đầu từ 11. Dùng một mảng chứa số lượng mèo xuất hiện khi đi từ đỉnh 11 đến đỉnh ii, đồng thời dùng một biến để đếm số nút lá (nhà hàng) có thể đến được.

Đầu tiên ta phải xét liệu đỉnh 11 có chứa mèo, nếu có thì tăng số mèo ở đỉnh này lên 11.

Kefa chỉ đi đến nhà hàng khi đường đó không xuất hiện quá MM con mèo, nên ta chỉ cần xét những đỉnh mà số lượng mèo xuất hiện nhỏ hơn hoặc bằng MM, lớn hơn MM không cần xét nữa.

Vậy khi chạy BFS, thực hiện xét các đỉnh kề với đỉnh đang xét uu, nếu đỉnh kề vv chưa thăm thì đánh dấu đã thăm. Đồng thời, nếu đỉnh đó chứa mèo thì cập nhật lại cat[v] = cat[u] + 1cat[v]=cat[u]+1, và nếu cat[v]cat[v] nhỏ hơn hoặc bằng MM thì kiểm tra nếu số đỉnh kề với vv là 11 (đồng nghĩa với việc chỉ có 11 đỉnh đi đến đỉnh vv hay vv là nút lá) thì tăng biến đếm lên, ngược lại ta đẩy đỉnh vv vào hàng đợi.

Độ phức tạp: \mathcal {O} \left (V + E \right )O(V+E) với VV và EE lần lượt là số lượng đỉnh và cạnh của đồ thị.
 */
public class KefaAndPark {
    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
    private static ArrayList<Boolean> visited = new ArrayList<>();
    private static ArrayList<Integer> cated = new ArrayList<>();

    private static int bfs(int m) {
        LinkedList<Integer> queue = new LinkedList<>();
        queue.add(0);
        visited.set(0, true);
        int nrestaurants = 0;
        while (!queue.isEmpty()) {
            int current = queue.remove();
            for (int i = 0; i < graph.get(current).size(); i++) {
                int u = graph.get(current).get(i);
                if (!visited.get(u)) {
                    visited.set(u, true);
                    if (cated.get(u) == 1) {
                        cated.set(u, cated.get(current) + 1);
                    }
                    if (cated.get(u) <= m) {
                        if (graph.get(u).size() == 1) {
                            nrestaurants++;
                        } else {
                            queue.add(u);
                        }
                    }
                }
            }
        }
        return nrestaurants;
    }


    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        for (int i = 0; i < n; i++) {
            visited.add(false);
            int hasCat = sc.nextInt();
            if (hasCat == 1) {
                cated.add(1);
            } else {
                cated.add(0);
            }
            graph.add(new ArrayList<>());
        }

        for (int i = 0; i < n - 1; i++) {
            int u = sc.nextInt() - 1;
            int v = sc.nextInt() - 1;
            graph.get(u).add(v);
            graph.get(v).add(u);
        }
        System.out.print(bfs(m));
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 100000 + 5;
    static int n, m;
    static int[] a = new int[MAX];
    static int[] cat = new int[MAX];
    static boolean[] visited = new boolean[MAX];
    static ArrayList<Integer> graph[] = new ArrayList[MAX];

    public static int BFS(int s) {
        int nrestaurants = 0;
        Queue<Integer> q = new LinkedList<>();
        visited[s] = true;
        q.add(s);

        cat[s] = (a[s] == 1 ? 1 : 0);

        while (!q.isEmpty()) {
            int u = q.poll();

            for (int v : graph[u]) {
                if (!visited[v]) {
                    visited[v] = true;

                    if (a[v] == 1) {
                        cat[v] = cat[u] + 1;
                    }

                    if (cat[v] <= m) {
                        if (graph[v].size() == 1) {
                            nrestaurants++;
                        }
                        else {
                            q.add(v);
                        }
                    }
                }
            }
        }

        return nrestaurants;
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        m = sc.nextInt();

        for (int i = 1; i <= n; i++) {
            a[i] = sc.nextInt();
            graph[i] = new ArrayList<>();
        }

        for (int i = 1; i < n; i++) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            graph[u].add(v);
            graph[v].add(u);
        }

        System.out.print(BFS(1));
    }
}
 */