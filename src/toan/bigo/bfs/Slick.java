package toan.bigo.bfs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

/*
Ta có thể thấy rằng hai ô (x, y)(x,y) và ô (i, j)(i,j) sẽ đi được với nhau nếu chúng có chung cạnh. Do đó, nếu có thể xem mỗi ô trên ma trận là một đỉnh của một đồ thị, thì hai đỉnh trên đồ thị sẽ có một cạnh nối với nhau nếu chúng thỏa mãn ba tính chất sau đây:

Hai ô được biểu diễn bởi hai đỉnh của đồ thị phải nằm trong phạm vi ma trận NNxMM.
Hai ô, không ô nào được phép biểu diễn bởi số 00 (cả hai ô đều là số 11).
Hai ô phải chung cạnh với nhau.
Vì số lượng ô tối đa của một vũng dầu là NNxMM, nên để sắp xếp kích thước các vũng dầu theo thứ tự tăng dần và số vũng dầu của kích thước đó, ta chỉ cần tạo một mảng đếm NNxMM phần tử, sau đó chỉ cần duyệt từ 11 đến NNxMM và in số lượng vũng dầu có kích thước tương ứng.

Như vậy, sau khi ta xây dựng được một đồ thị vô hướng, ý tưởng của ta sẽ như sau:

Xác định vị trí của ô số 11, giả sử là ô (sx, sy)(sx,sy).
Với mỗi ô (i, j)(i,j) trong ma trận mà được biểu diễn bằng số 11, ta sẽ sử dụng kỹ thuật duyệt DFS hoặc BFS từ ô (i, j)(i,j) để xem có thể đến được ô (sx, sy)(sx,sy) hay không, nếu có thì ta sẽ cập nhật kích thước của vũng dầu đó lên 11. Sau khi duyệt hết tất cả ô liền kề có số 11, ta cập nhật số lượng vũng dầu có kích thước tương ứng lên 11.
Lưu ý: với Python để tăng tốc độ xử lý, ta tự cài đặt queue sử dụng trong thuật toán BFS bằng list với hai con trỏ leftleft để lấy phần tử đầu tiên và rightright để thêm phần tử vào cuối queue.

Độ phức tạp:

Time Complexity: \mathcal {O} \left (N * M \right )O(N∗M) với NN và MM lần lượt là độ dài của 22 cạnh của bảng.
Space Complexity: \mathcal {O} \left (N * M \right )O(N∗M).
 */
public class Slick {
    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();

    private static int[] dx = {0, 0, 1, -1};
    private static int[] dy = {1, -1, 0, 0};

    private static void bfs(Cell s, int n, int m, ArrayList<Integer> slickCount) {
        LinkedList<Cell> queue = new LinkedList<>();
        queue.add(s);
        graph.get(s.r).set(s.c, 0);
        int count = 1;
        while (!queue.isEmpty()) {
            Cell current = queue.remove();
            for (int i = 0; i < 4; i++) {
                int newRow = current.r + dx[i];
                int newColumn = current.c + dy[i];

                if (newRow >= 0 && newRow < n && newColumn >= 0 && newColumn < m
                        && graph.get(newRow).get(newColumn) == 1) {
                    queue.add(new Cell(newRow, newColumn));
                    graph.get(newRow).set(newColumn, 0);
                    count++;
                }
            }
        }
        slickCount.set(count, slickCount.get(count) + 1);
    }


    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        ArrayList<Result> result = new ArrayList<>();
        while (flag) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            if (n == 0 && m == 0) {
                flag = false;
            } else {
                graph.clear();
                ArrayList<Integer> slickCount = new ArrayList<>();
                for (int i = 0; i < n; i++) {
                    graph.add(new ArrayList<>());
                    for (int j = 0; j < m; j++) {
                        slickCount.add(0);
                        if(j == m - 1)
                        {
                            slickCount.add(0);
                        }
                        graph.get(i).add(sc.nextInt());
                    }
                }
                int nSlick = 0;
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        if (graph.get(i).get(j) == 1) {
                            nSlick++;
                            bfs(new Cell(i, j), n, m, slickCount);
                        }
                    }
                }
                result.add(new Result(nSlick, slickCount));
            }
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(0).nSlick);
            for (int j = 0; j < result.get(i).slickCount.size(); j++) {
                if (result.get(i).slickCount.get(j) != 0) {
                    System.out.println((j + " " + result.get(i).slickCount.get(j)));
                }
            }
        }
    }

    private static class Cell {
        int r;
        int c;

        public Cell(int r, int c) {
            this.r = r;
            this.c = c;
        }
    }

    private static class Result {
        int nSlick;
        ArrayList<Integer> slickCount;

        public Result(int nSlick, ArrayList<Integer> slickCount) {
            this.nSlick = nSlick;
            this.slickCount = slickCount;
        }
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 251;
    static int[] dr = {0, 0, 1, -1};
    static int[] dc = {1, -1, 0, 0};
    static int N, M;
    static int[][] table = new int[MAX][MAX];
    static int[] slick = new int[MAX * MAX];

    static class Cell {
        int r, c;

        public Cell(int _r, int _c) {
            this.r = _r;
            this.c = _c;
        }
    };

    public static boolean isValid(int r, int c) {
        return r >= 0 && c >= 0 && r < N && c < M;
    }

    public static void BFS(Cell s) {
        Queue<Cell> q = new LinkedList<>();
        q.add(s);
        table[s.r][s.c] = 0;
        int count = 1;

        while (!q.isEmpty()) {
            Cell u = q.poll();

            for (int i = 0; i < 4; i++) {
                int r = u.r + dr[i];
                int c = u.c + dc[i];

                if (isValid(r, c) && table[r][c] == 1) {
                    table[r][c] = 0;
                    q.add(new Cell(r, c));
                    count++;
                }
            }
        }

        slick[count]++;
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            N = sc.nextInt();
            M = sc.nextInt();

            if (N == 0 && M == 0) {
                break;
            }

            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    table[i][j] = sc.nextInt();
                    slick[i * M + j + 1] = 0;
                }
            }

            int nslicks = 0;

            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    if (table[i][j] == 1) {
                        nslicks++;
                        BFS(new Cell(i, j));
                    }
                }
            }

            System.out.println(nslicks);

            for (int i = 1; i <= N * M; i++) {
                if (slick[i] != 0) {
                    System.out.println(i + " " + slick[i]);
                }
            }
        }
    }
}
 */