package toan.bigo.bfs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

/*
Bạn bắt đầu đi từ ô bị nứt (X) sau đó bạn phải đi được tới đích bằng những đường đấu “.” (ô nguyên) • Nếu ô đích của bạn là ô X (ô nứt) thì bạn chỉ cần 1 đường đi tới đó. Vì nhiệm vụ của bạn là thoát khỏi Level này nên cần gặp ô nứt để qua Level khác. • Nếu ô đích của bạn là “.” thì phải đi đến được đó và xung quanh của ô đó phải có ít nhất 1 ô “.” Để sau khi bạn nhảy xong 1 lần nó trở thành ô nứt rồi bạn nhảy qua ô khác rồi nhảy lại để qua được Level đó.

Độ phức tạp: O(n * m)O(n∗m) với nn và mm lần lượt là kích thước của bảng.
 */
public class IceCave {
    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();

    private static int LEVEL_2 = 2;
    private static int LEVEL_1 = 1;

    private static int[] dx = {0, 0, 1, -1};
    private static int[] dy = {1, -1, 0, 0};

    private static boolean bfs(Cell s, Cell d, int n, int m) {
        LinkedList<Cell> queue = new LinkedList<>();
        queue.add(s);
        graph.get(s.r).set(s.c, LEVEL_1);
        while (!queue.isEmpty()) {
            Cell current = queue.remove();
            for (int i = 0; i < 4; i++) {
                int newRow = current.r + dx[i];
                int newColumn = current.c + dy[i];

                if (newRow == d.r && newColumn == d.c && graph.get(newRow).get(newColumn) == LEVEL_1) {
                    return true;
                }
                if (newRow >= 0 && newRow < n && newColumn >= 0 && newColumn < m
                        && graph.get(newRow).get(newColumn) == LEVEL_2) {
                    queue.add(new Cell(newRow, newColumn));
                    graph.get(newRow).set(newColumn, LEVEL_1);
                }
            }
        }
        return false;
    }


    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
            String input = sc.next();
            for (int j = 0; j < input.length(); j++) {
                char character = input.charAt(j);
                if (character == 'X') {
                    graph.get(i).add(LEVEL_1);
                } else {
                    graph.get(i).add(LEVEL_2);
                }
            }
        }
        int sRow = sc.nextInt() - 1;
        int sColumn = sc.nextInt() - 1;
        int dRow = sc.nextInt() - 1;
        int dColumn = sc.nextInt() - 1;
        boolean result = bfs(new Cell(sRow, sColumn), new Cell(dRow, dColumn), n, m);
        if (result) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }

    private static class Cell {
        int r;
        int c;

        public Cell(int r, int c) {
            this.r = r;
            this.c = c;
        }
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 500 + 1;
    static int[] dr = {0, 0, 1, -1};
    static int[] dc = {1, -1, 0, 0};
    static int n, m;
    static char[][] level = new char[MAX][MAX];

    static class Cell {
        int r, c;

        public Cell(int _r, int _c) {
            this.r = _r;
            this.c = _c;
        }
    };

    public static boolean isValid(int r, int c) {
        return r >= 0 && c >= 0 && r < n && c < m;
    }

    public static boolean BFS(Cell s, Cell f) {
        Queue<Cell> q = new LinkedList<>();
        level[s.r][s.c] = 'X';
        q.add(s);

        while (!q.isEmpty()) {
            Cell u = q.poll();

            for (int i = 0; i < 4; i++) {
                int r = u.r + dr[i];
                int c = u.c + dc[i];

                if (r == f.r && c == f.c && level[r][c] == 'X') {
                    return true;
                }

                if (isValid(r, c) && level[r][c] == '.') {
                    level[r][c] = 'X';
                    q.add(new Cell(r, c));
                }
            }
        }

        return false;
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        m = sc.nextInt();

        for (int i = 0; i < n; i++) {
            level[i] = sc.next().toCharArray();
        }

        Cell s = new Cell(sc.nextInt() - 1, sc.nextInt() - 1);
        Cell f = new Cell(sc.nextInt() - 1, sc.nextInt() - 1);

        System.out.print(BFS(s, f) ? "YES" : "NO");
    }
}
 */