package toan.bigo.bfs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/*
Bài này áp dụng BFS cơ bản, đọc vào số lượng đỉnh của đồ thị và danh sách cạnh. Sau đó chạy BFS bắt đầu từ điểm SS. Viết một hàm đếm các cạnh đi qua từ SS đến các đỉnh khác. Lấy kết quả đếm nhân 66 để ra kết quả cần tìm.

Tuy nhiên ta có thể biến tấu thuật toán BFS đôi chút để có được kết quả ngay lúc duyệt đồ thị.

Gọi dist[v]dist[v] lưu khoảng cách ngắn nhất từ đỉnh SS đến đỉnh vv. Với đỉnh vv có được thông qua duyệt các đỉnh kề của đỉnh uu. Như vậy dễ dàng nhận thấy dist[v] = dist[u] + 1dist[v]=dist[u]+1, nghĩa là từ SS đến vv ta mất một quãng đường bằng khoảng cách từ SS đến uu và từ uu đến vv (chưa tính trọng số 66 của mỗi cạnh).

Cuối cùng cho vòng lặp ii duyệt lại toàn bộ các đỉnh trong đồ thị. Nếu đỉnh đó chưa được viếng thăm thì in -1−1, ngược lại in dist[i] * 6dist[i]∗6 với trọng số của mỗi cạnh là 66.

Độ phức tạp: \mathcal {O} \left (T * (V + E) \right )O(T∗(V+E)) với TT là số lượng test, VV là số lượng đỉnh trong đồ thị và EE là số lượng cạnh trong đồ thị.
 */
public class ShortestReach {

    public static void main(String[] args) {
        // write your code here
        int queries;

        Scanner sc = new Scanner(System.in);
        queries = sc.nextInt();
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        for (int i = 0; i < queries; i++) {
            result.add(findShortestReach(sc));
        }
        for (int i = 0; i < result.size(); i++) {
            for (int j = 0; j < result.get(i).size(); j++) {
                if (result.get(i).get(j) != 0) {
                    System.out.print(result.get(i).get(j) + " ");
                }
            }
            if (i != result.size() - 1) {
                System.out.println();
            }
        }
    }

    public static ArrayList<Integer> findShortestReach(Scanner sc) {
        int node = sc.nextInt();
        int edges = sc.nextInt();
        ArrayList<ArrayList<Integer>> nodeEdges = new ArrayList<>();
        ArrayList<Integer> path = new ArrayList<>();
        for (int i = 0; i < node; i++) {
            nodeEdges.add(new ArrayList());
            path.add(-1);
        }
        //read the edges
        for (int i = 0; i < edges; i++) {
            int first = sc.nextInt();
            int second = sc.nextInt();
            nodeEdges.get(first - 1).add(second - 1);
            nodeEdges.get(second - 1).add(first - 1);
        }
        int start = sc.nextInt();

        Queue<Integer> queue = new LinkedList<>();

        //init
        queue.add(start - 1);
        path.set(start - 1, 0);

        while (!queue.isEmpty()) {
            int currentNode = queue.remove();
            for (int i = 0; i < nodeEdges.get(currentNode).size(); i++) {
                int currentEdges = nodeEdges.get(currentNode).get(i);
                if (path.get(currentEdges) == -1) {
                    path.set(currentEdges, path.get(currentNode) + 6);
                    queue.add(currentEdges);
                }
            }
        }

        return path;
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 1000 + 5;
    static int V, E;
    static boolean[] visited = new boolean[MAX];
    static int[] dist = new int[MAX];
    static ArrayList<Integer> graph[] = new ArrayList[MAX];

    public static void BFS(int s) {
        Queue<Integer> q = new LinkedList<>();
        visited[s] = true;
        q.add(s);

        while (!q.isEmpty()) {
            int u = q.poll();

            for (int v : graph[u]) {
                if (!visited[v]) {
                    visited[v] = true;
                    dist[v] = dist[u] + 1;
                    q.add(v);
                }
            }
        }
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int Q = sc.nextInt();

        for (int i = 0; i < MAX; i++) {
            graph[i] = new ArrayList<>();
        }

        while (Q-- > 0) {
            V = sc.nextInt();
            E = sc.nextInt();

            for (int i = 0; i < MAX; i++) {
                graph[i].clear();
                visited[i] = false;
                dist[i] = 0;
            }

            for (int i = 0; i < E; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                graph[u].add(v);
                graph[v].add(u);
            }

            int s = sc.nextInt();
            BFS(s);

            for (int i = 1; i <= V; i++) {
                if (i == s) {
                    continue;
                }

                System.out.print((visited[i] ? dist[i] * 6 : -1) + " ");
            }

            System.out.println();
        }
    }
}
 */