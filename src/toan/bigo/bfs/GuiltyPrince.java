package toan.bigo.bfs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

/*
Bạn có thể thấy rằng hai ô (x, y)(x,y) và ô (i, j)(i,j) sẽ đi được với nhau nếu chúng có chung cạnh. Do đó, nếu bạn có thể xem mỗi ô trên bảng là một đỉnh của một đồ thị, thì hai đỉnh trên đồ thị sẽ có một cạnh nối với nhau nếu chúng thỏa mãn ba tính chất sau đây:

Hai ô được biểu diễn bởi hai đỉnh của đồ thị phải nằm trong bảng đầu vào.

Hai ô, không ô nào được phép biểu diễn bởi ký tự ‘#’.

Hai ô phải chung cạnh với nhau.

Như vậy, sau khi ta xây dựng được một đồ thị vô hướng, cách đơn giản để giải quyết bài này là:

Ta đi xác định vị trí của ô ‘@’, giả sử là ô (sx, sy)(sx,sy).

Sau đó, với mỗi ô (i, j)(i,j) trong bảng được biểu diễn bằng dấu ‘..’, ta sẽ sử dụng kỹ thuật duyệt BFS/DFS từ ô (i, j)(i,j) để xác định xem có thể đến được ô (sx, sy)(sx,sy) hay không. Nếu có, ta tăng biến kết quả ansans lên 11.

Kết quả là ans + 1ans+1 (Tính cả ô (sx, sy)(sx,sy)).

Phải xét mỗi ô (i, j)(i,j) có đến được ô (sx, sy)(sx,sy) hay không như vậy khá tốn thời gian, ta có một cách khác để cải tiến thuật toán ở trên:

Dựa vào một nhận xét tự nhiên, ta thấy mọi ô giả sử có đường đi đến ô (sx, sy)(sx,sy) thì chúng đều sẽ tập trung lại tại ô (sx , sy)(sx,sy). Do đó, nếu như ta nhìn nhận bài toán ở một khía cạnh khác, chúng ta sẽ phát hiện rằng: thay vì với mỗi ô (i , j)(i,j), ta kiểm tra xem có đường đi tới (sx, sy)(sx,sy), thì bây giờ ta sẽ xuất phát từ ô (sx , sy)(sx,sy), đi loang ra các đỉnh khác, mỗi lần ta tiếp cận được với một đỉnh mà chưa được xét thì ta sẽ tăng biến ansans lên 11 đơn vị. Kết quả là ansans.

Độ phức tạp:

Cách chưa cải tiến:

Time Complexity: \mathcal {O} \left (T * W * W * H * H \right )O(T∗W∗W∗H∗H)

Space Complexity: \mathcal {O} \left (W * H \right )O(W∗H)

WW và HH không quá 2020 nên độ phức tạp \mathcal {O} \left (T * W * W * H * H \right )O(T∗W∗W∗H∗H) vẫn có thể chấp nhận được.

Cách cải tiến:

Time Complexity: \mathcal {O} \left (T * W * H \right )O(T∗W∗H) với WW và HH lần lượt là chiều rộng và chiều cao của bảng, còn TT là số lượng bộ test đầu vào.

Space Complexity: \mathcal {O} \left (W * H \right )O(W∗H)
 */
public class GuiltyPrince {

    private static ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();
    private static ArrayList<ArrayList<Boolean>> visited = new ArrayList<>();
    private static int LAND = 1;
    private static int WATER = 0;

    private static int[] dx = {0, 0, 1, -1};
    private static int[] dy = {1, -1, 0, 0};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int testCase = sc.nextInt();
        ArrayList<Integer> result = new ArrayList<>();
        for (int tc = 0; tc < testCase; tc++) {

            matrix.clear();
            visited.clear();

            int n = sc.nextInt();
            int m = sc.nextInt();
            Cell initPosition = null;
            for (int i = 0; i < m; i++) {
                matrix.add(new ArrayList());
                visited.add(new ArrayList<>());

                String input = sc.next();
                for (int j = 0; j < input.length(); j++) {
                    visited.get(i).add(false);
                    char character = input.charAt(j);
                    if (character == '.') {
                        matrix.get(i).add(LAND);
                    } else if (character == '#') {
                        matrix.get(i).add(WATER);
                    } else {
                        matrix.get(i).add(LAND);
                        initPosition = new Cell(i, j);
                    }
                }
            }
            result.add(bfs(initPosition, m, n));
        }

        for (int i = 0; i < result.size(); i++) {
            System.out.println("Case " + (i + 1) + ": " + result.get(i));
        }
    }

    private static int bfs(Cell init, int row, int column) {
        int count = 1;
        LinkedList<Cell> queue = new LinkedList<>();
        queue.add(init);
        visited.get(init.row).set(init.column, true);
        while (!queue.isEmpty()) {
            Cell current = queue.remove();
            for (int i = 0; i < 4; i++) {
                int newRow = current.row + dy[i];
                int newColumn = current.column + dx[i];
                if (newColumn >= 0 && newColumn < column
                        && newRow >= 0 && newRow < row && !visited.get(newRow).get(newColumn)
                        && matrix.get(newRow).get(newColumn) == LAND) {
                    count++;
                    visited.get(newRow).set(newColumn, true);
                    queue.add(new Cell(newRow, newColumn));
                }
            }
        }
        return count;
    }

    private static class Cell {
        int row;
        int column;

        public Cell(int row, int column) {
            this.row = row;
            this.column = column;
        }
    }

}
/*
import java.util.*;

public class Main {
    static final int MAX = 21;
    static int dr[] = {0, 0, 1, -1};
    static int dc[] = {1, -1, 0, 0};
    static int W, H;
    static boolean visited[][] = new boolean[MAX][MAX];
    static String maze[] = new String[MAX];

    static class Cell {
        int r, c;

        public Cell(int _r, int _c) {
            this.r = _r;
            this.c = _c;
        }
    };

    public static boolean isValid(int r, int c) {
        return r >= 0 && c >= 0 && r < H && c < W;
    }

    public static int BFS(Cell s) {
        Queue<Cell> q = new LinkedList<>();
        q.add(s);
        visited[s.r][s.c] = true;
        int count = 1;

        while (!q.isEmpty()) {
            Cell u = q.poll();

            for (int i = 0; i < 4; i++) {
                int r = u.r + dr[i];
                int c = u.c + dc[i];

                if (isValid(r, c) && maze[r].charAt(c) == '.' && !visited[r][c]) {
                    visited[r][c] = true;
                    q.add(new Cell(r, c));
                    count++;
                }
            }
        }

        return count;
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int Q = sc.nextInt();
        Cell s = new Cell(0, 0);

        for (int k = 1; k <= Q; k++) {
            W = sc.nextInt();
            H = sc.nextInt();

            for (int i = 0; i < H; i++) {
                maze[i] = sc.next();

                for (int j = 0; j < W; j++) {
                    if (maze[i].charAt(j) == '@') {
                        s = new Cell(i, j);
                    }

                    visited[i][j] = false;
                }
            }

            System.out.println("Case " + k + ": " + BFS(s));
        }
    }
}
 */