package toan.bigo.bfs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/*
Bài toán này bạn sẽ có hai cách giải:

Cách 1:

Bạn sẽ chuyển toàn bộ đồ thị đã cho về ma trận kề hoặc danh sách kề, bằng cách duyệt qua toàn bộ ma trận, hai đỉnh được nối với nhau khi và chỉ khi có thể đi từ “.” này đến “.” kia.

Bạn có thể đặt đỉnh ở tọa độ (0, 0)(0,0) là đỉnh 00, (0, 1)(0,1) là đỉnh 11, … lần lượt như vậy. Nếu đồ thị có 44x44 thì bạn sẽ có 1616 đỉnh ((từ 00 đến 1515)).

Bước tiếp theo bạn sẽ xem đỉnh nào ở rìa ma trận thì sẽ đặt đỉnh đó là đỉnh vào hoặc đỉnh ra, bạn cần tìm đủ 22 đỉnh như vậy rồi chạy BFS. Nếu có đường đi thì bạn sẽ in ra “validvalid” ngược lại bạn sẽ in ra “invalidinvalid”.

\to→ Nhận xét: Với cách giải này bạn sẽ tốn thời gian chuẩn bị lại đồ thị cho đúng định dạng.

Cách 2:

Bạn sẽ chạy BFS trên mê cung đã cho mà không cần phải chuyển lại thành dạng ma trận kề hay danh sách kề. Cách này bạn phải thêm và chỉnh lại một số dòng code để chạy phù hợp.

Ban đầu, bạn cũng sẽ tìm hai điểm đầu vào và đầu ra. Từ điểm đầu ra bạn sẽ xác định 44 hướng đi (lên, xuống, trái, phải). Nếu có đường đi, nghĩa là gặp dấu “.” và nằm trong giới hạn của mê cung thì bạn sẽ dịch chuyển bước đi của mình xuống điểm mới. Lần lượt đi đến khi nào gặp đỉnh đầu ra thì dừng. Lúc này sẽ in ra là “valid”, ngược lại nếu đi mà không thấy đường ra sẽ in ra “invalid”.

Độ phức tạp: \mathcal {O} \left (T * R * C \right )O(T∗R∗C) với T, R, CT,R,C lần lượt là số lượng test, số dòng và số cột của ma trận.
 */
public class ValidateTheMaze {

    public static void main(String[] args) {
        // write your code here
        int testCase;

        Scanner sc = new Scanner(System.in);
        testCase = sc.nextInt();

        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < testCase; i++) {
            result.add(getResult(sc));
        }

        for (String s : result) {
            System.out.println(s);
        }
    }

    private static int[] dx = new int[]{0, 0, 1, -1};
    private static int[] dy = new int[]{1, -1, 0, 0};

    private static String getResult(Scanner sc) {
        int n;
        int m;

        m = sc.nextInt();
        n = sc.nextInt();
        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();

        ArrayList<Integer> path = new ArrayList<>();
        ArrayList<ArrayList<Integer>> visited = new ArrayList<>();
        boolean flagValid = true;
        Point pointStart = new Point(-1, -1);
        Point pointEnd = new Point(-1, -1);

        int countExitEnter = 0;
        for (int i = 0; i < m; i++) {
            String maze = sc.next();
            matrix.add(new ArrayList<>());
            visited.add(new ArrayList<>());
            for (int j = 0; j < n; j++) {
                visited.get(i).add(-1);
                path.add(-1);
                char symbol = maze.charAt(j);
                if (symbol == '.') {
                    matrix.get(i).add(1);
                    if (i == 0 || i == m - 1 || j == 0 || j == n - 1) {
                        if (countExitEnter == 0) {
                            pointStart = new Point(i, j);
                        }
                        if (countExitEnter == 1) {
                            pointEnd = new Point(i, j);
                        }
                        countExitEnter++;
                    }
                } else {
                    matrix.get(i).add(0);
                }
            }
        }
        if (countExitEnter != 2) {
            flagValid = false;
        }
        if (!flagValid) {
            return "invalid";
        } else {
            path.set(pointStart.transformToInt(m, n), -1);
            Queue<Point> queue = new LinkedList<>();
            queue.add(pointStart);
            visited.get(pointStart.x).set(pointStart.y, 1);
            while (!queue.isEmpty()) {
                Point node = queue.remove();
                for (int i = 0; i < 4; i++) {
                    int nextX = dx[i] + node.x;
                    int nextY = dy[i] + node.y;
                    if(nextX >= 0 && nextX < m &&
                            nextY >= 0 && nextY <n &&
                            matrix.get(nextX).get(nextY) == 1) {
                        Point nextPoint = new Point(nextX, nextY);
                        if (visited.get(nextPoint.x).get(nextPoint.y) == -1) {
                            visited.get(nextPoint.x).set(nextPoint.y, 1);
                            path.set(nextPoint.transformToInt(m, n), node.transformToInt(m, n));
                            queue.add(nextPoint);
                        }
                    }
                }
            }

            Point pointResult = findStartNode(pointEnd.transformToInt(m, n), path, m, n);
            if (pointResult.x == pointStart.x && pointResult.y == pointStart.y) {
                return "valid";
            } else {
                return "invalid";
            }
        }
    }

    private static Point findStartNode(int index, ArrayList<Integer> path, int m, int n) {
        if (path.get(index) == -1) {
            return transformToPoint(index, m, n);
        }
        return findStartNode(path.get(index), path, m, n);
    }


    private static class Point {
        int x;
        int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int transformToInt(int m, int n) {
            return transform(x, y, m, n);
        }
    }

    private static int transform(int x, int y, int m, int n) {
        return x * n + y;
    }

    private static Point transformToPoint(int index, int m, int n) {
        return new Point(index / n, index % n);
    }
}
/*
import java.util.*;
import java.io.*;

public class Main {
    static final int MAX = 21;
    static int dr[] = {0, 0, 1, -1};
    static int dc[] = {1, -1, 0, 0};
    static int n, m;
    static boolean visited[][] = new boolean[MAX][MAX];
    static String maze[] = new String[MAX];

    static class Cell {
        int r, c;

        public Cell(int _r, int _c) {
            r = _r;
            c = _c;
        }
    };

    public static boolean isValid(int r, int c) {
        return r >= 0 && r < n && c >= 0 && c < m;
    }

    public static boolean BFS(Cell s, Cell f) {
        Queue<Cell> q = new LinkedList<>();
        visited[s.r][s.c] = true;
        q.add(s);

        while (!q.isEmpty()) {
            Cell u = q.poll();

            if (u.r == f.r && u.c == f.c) {
                return true;
            }

            for (int i = 0; i < 4; i++) {
                int r = u.r + dr[i];
                int c = u.c + dc[i];

                if (isValid(r, c) && maze[r].charAt(c) == '.' && !visited[r][c]) {
                    visited[r][c] = true;
                    q.add(new Cell(r, c));
                }
            }
        }

        return false;
    }

    public static void main(String[] agrs) {
        MyScanner in = new MyScanner();
        PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out), true);
        int Q = in.nextInt();

        while (Q-- > 0) {
            n = in.nextInt();
            m = in.nextInt();

            for (int i = 0; i < n; i++) {
                maze[i] = in.nextLine();
            }

            ArrayList<Cell> entrance = new ArrayList<>();

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    visited[i][j] = false;
                    if (maze[i].charAt(j) == '.' && (i == 0 || j == 0 || i == n - 1 || j == m - 1)) {
                        entrance.add(new Cell(i, j));
                    }
                }
            }

            if (entrance.size() != 2) {
                out.println("invalid");
            }
            else {
                Cell s = entrance.get(0);
                Cell f = entrance.get(1);
                out.println(BFS(s, f) ? "valid" : "invalid");
            }
        }
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }

        long nextLong() {
            return Long.parseLong(next());
        }

        double nextDouble() {
            return Double.parseDouble(next());
        }

        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
 */