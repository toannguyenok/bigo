package toan.bigo.algorithmcomplexity;

import java.util.*;

/*
Ý tưởng giải dựa trên kỹ thuật Two Pointers với hai biến chạy ii và jj:

Bước 1: Sử dụng biến chạy ii để chạy tìm đoạn đầu tiên chứa đủ KK số phân biệt.

Bước 2: Sử dụng biến chạy jj chạy từ dưới lên nhằm rút ngắn đoạn tìm được, đảm bảo trong [j, i][j,i] không còn bất cứ đoạn con nào cũng thỏa yêu cầu.

Bước 3: In ra kết quả bài toán nằm trong hai biến jj và ii.

Trong đó, để kiểm soát số phần tử phân biệt đã tìm được, ta dùng thêm một mảng đếm phân phối fre[]fre[] với fre[X]fre[X] là số lần XX xuất hiện trong khoảng [j, i][j,i]. Với mỗi lần fre[X]fre[X] thay đổi từ 00 lên 11, tức là ta vừa nhận được thêm một số mới không trùng với những số trước đó, ta tăng biến đếm số lượng phần tử phân biệt lên 11.

Độ phức tạp: \mathcal {O} \left( N \right)O(N) với NN số lượng phần tử trong dãy.
 */
public class Array {

    public static void main(String[] args) {
        // write your code here

        int n;
        int k;

        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        k = sc.nextInt();

        int countUnique = 0;
        int pow = 10 * 10 * 10 * 10 * 10 * 10;
        int[] freq = new int[pow + 1];
        int[] arrayA = new int[n];
        for (int i = 0; i < n; i++) {
            int number = sc.nextInt();
            arrayA[i] = number;
            if (freq[number] == 0) {
                countUnique++;
            }
            freq[number]++;
        }

        int left = 0;
        int right = n - 1;

        if (countUnique < k) {
            System.out.println("-1 -1");
        } else {
            while (right >= 0 && countUnique >= k) {
                freq[arrayA[right]] = freq[arrayA[right]] - 1;
                if (freq[arrayA[right]] == 0) {
                    countUnique = countUnique - 1;
                }
                right = right - 1;
            }

            right += 1;
            countUnique += 1;

            while (left <= right && countUnique >= k) {
                freq[arrayA[left]] = freq[arrayA[left]] - 1;
                if (freq[arrayA[left]] == 0) {
                    countUnique = countUnique - 1;
                }
                left = left + 1;
            }
            left = left - 1;

            System.out.println((left + 1) + " " + (right + 1));
        }

    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    static final int MAX = (int)1e5 + 5;
    static int fre[] = new int[MAX];

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        ArrayList<Integer> a = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            a.add(sc.nextInt());
        }

        int unique = 0;
        int j = 0;

        for (int i = 0; i < n; i++) {
            if (fre[a.get(i)] == 0) {
                unique++;
            }

            fre[a.get(i)]++;

            while (unique == k) {
                fre[a.get(j)]--;

                if (fre[a.get(j)] == 0) {
                    System.out.printf("%d %d", j + 1, i + 1);
                    return;
                }

                j++;
            }
        }

        System.out.print("-1 -1");
    }
}
 */