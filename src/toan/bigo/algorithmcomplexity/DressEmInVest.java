package toan.bigo.algorithmcomplexity;

import java.util.ArrayList;
import java.util.Scanner;

/*
Nhận xét:

Để tối ưu số người lính có thể có áo, với mỗi chiếc áo từ bé đến lớn ta sẽ lần lượt dành cho những người lính có kích thước nhỏ nhất mang vừa chiếc áo đó. Bằng cách này, những áo có kích cỡ lớn hơn sẽ có cơ hội vừa với những người lính khác có kích thước lớn hơn.

Hơn nữa, nếu người lính i được chọn mang bộ vest j, thì người lính (i + 1) trở về sau chắc chắn chỉ có thể mang áo từ (j + 1) trở về sau. Bởi nếu những chiếc áo từ j trở về trước mà có thể dùng được thì người lính i đã dùng rồi. Như vậy nghĩa là các bộ vest từ 1 đến j hoặc đã được dùng, hoặc không thể dùng, nên ta không cần kiểm tra lại.

Từ đó, ta hình thành các bước giải quyết bài toán như sau:

Bước 1: Đưa thông tin về kích thước của mỗi người lính và kích cỡ của các áo hiện có vào 2 mảng.

Bước 2: Sử dụng hai biến chạy i và j, trong đó biến i sẽ chạy trên mảng chứa kích thước các người lính, còn biến j sẽ chạy trên mảng còn lại. Đặt i = j = 0 (tức xét người lính đầu tiên và chiếc áo đầu tiên).

Bước 3: Sử dụng vòng lặp, kết thúc khi không còn lính hoặc không còn chiếc áo nào có thể phát:

Nếu chiếc áo nằm trong khoảng kích thước có thể chấp nhận được của người lính, ta nhận được một cặp (người lính - áo) hợp lệ và đưa vào mảng kết quả, đồng thời ta chuyển sang chiếc áo và người lính tiếp theo, tức tăng biến i và j lên 1.

Nếu kính cỡ tối đa mà người lính có thể mang nhỏ hơn kích cỡ của chiếc áo, ta cần tìm một người lính có kích thước lớn hơn có khả năng mang vừa chiếc áo đó, tức tăng i lên 1.

Nếu kính cỡ tối thiểu mà người lính có thể mang vượt quá kích cỡ của chiếc áo, ta cần tìm một chiếc áo có kích thước lớn hơn có khả năng mang vừa chiếc áo đó, tức tăng j lên 1

Bước 4: In đáp án dựa trên mảng kết quả

Độ phức tạp: O(max(n, m)) với n là số lượng người lính và m là số lượng áo.
 */
public class DressEmInVest {
    public static void main(String[] args) {

        int n;
        int m;
        int x;
        int y;

        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        m = sc.nextInt();
        x = sc.nextInt();
        y = sc.nextInt();

        int[] soldiers = new int[n];
        int[] vests = new int[m];
        for (int i = 0; i < n; i++) {
            soldiers[i] = sc.nextInt();
        }

        for (int i = 0; i < m; i++) {
            vests[i] = sc.nextInt();
        }

        ArrayList<Pair> results = new ArrayList<>();
        int i = 0;
        int j = 0;
        while (i < n && j < m) {
            if (soldiers[i] - x <= vests[j] && vests[j] <= soldiers[i] + y) {
                results.add(new Pair(i + 1, j + 1));
                i++;
                j++;
            } else if (vests[j] < soldiers[i] - x) {
                j++;
            } else if (vests[j] > soldiers[i] + y) {
                i++;
            }
        }

        System.out.println(results.size() + "");
        for (Pair result : results) {
            System.out.println(result.u + " " + result.v);
        }
    }

    static class Pair {
        int u;
        int v;

        Pair(int u, int v) {
            this.u = u;
            this.v = v;
        }
    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
   static class Vest {
      int u, v;

      public Vest(int x , int y) {
         u = x;
         v = y;
      }
   }

   public static void main (String[] args) {
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int m = sc.nextInt();
      int x = sc.nextInt();
      int y = sc.nextInt();

      ArrayList<Integer> a = new ArrayList<>(), b = new ArrayList<>();

      for (int i = 0; i < n; i++) {
         a.add(sc.nextInt());
      }

      for (int i = 0; i < m; i++) {
         b.add(sc.nextInt());
      }

      ArrayList<Vest> v = new ArrayList<>();

      int i = 0, j = 0;
      while (i < n && j < m) {
         if (a.get(i) - x <= b.get(j) && b.get(j) <= a.get(i) + y) {
            v.add(new Vest(i + 1, j + 1));
            i++;
            j++;
         }
         else if (a.get(i) + y < b.get(j)) {
            i++;
         }
         else if (a.get(i) - x > b.get(j)) {
            j++;
         }
      }

      System.out.println(v.size());

      for (Vest vest : v) {
         System.out.println(vest.u + " " + vest.v);
      }
   }
}
 */
