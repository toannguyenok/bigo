package toan.bigo.algorithmcomplexity;

import java.util.Scanner;

/*
Sử dụng kỹ thuật Two Pointers với hai biến chạy i và j đại diện cho vị trí của hai thẻ bài ngoài cùng mà người chơi hiện tại có thể chọn.

Bước 1: Đưa thông tin số điểm ghi trên thẻ bài vào một mảng.
Bước 2: Gọi i là vị trí của thẻ bài ngoài cùng bên trái (tức i = 0), j là vị trí của thẻ bài ngoài cùng bên phải (tức j = n - 1).
Bước 3: Sử dụng thêm một biến đánh dấu lượt chơi hiện tại là của Sereja hay Dima.
Bước 4: Lần lượt so sánh số điểm ghi trên thẻ bài i và thẻ bài j:
Nếu i và j chưa gặp nhau, tức còn thẻ bài có thể được lật, ta lấy thẻ bài lớn hơn và cộng dồn vào biến kết quả của người chơi thích hợp.
Tăng biến i nếu thẻ bài vừa lật nằm ở ngoài cùng bên trái, ngược lại giảm biến j.
Bước 5: In ra kết quả.
Độ phức tạp: O(n) với n là số lượng thẻ bài.
 */
public class SerajaAndDima {

    public static void main(String[] args) {
        // write your code here

        int n;

        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();

        int[] cards = new int[n];
        for (int i = 0; i < n; i++) {
            int number = sc.nextInt();
            cards[i] = number;
        }

        int left = 0;
        int right = n - 1;

        int[] countTotal = new int[2];
        int turnPlayer = 0;

        while (left <= right) {
            if (cards[left] < cards[right]) {
                countTotal[turnPlayer] = countTotal[turnPlayer] + cards[right];
                right--;
            } else {
                countTotal[turnPlayer] = countTotal[turnPlayer] + cards[left];
                left++;
            }
            turnPlayer = 1 - turnPlayer;
        }

        System.out.println(countTotal[0] + " " + countTotal[1]);
    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> cards = new ArrayList<>();
        int n = sc.nextInt();

        for (int i = 0; i < n; i++) {
            cards.add(sc.nextInt());
        }

        int[] res = {0, 0};
        int player = 0;
        int i = 0, j = n - 1;

        while (i <= j) {
            if (cards.get(i) > cards.get(j)) {
                res[player] += cards.get(i);
                i++;
            }
            else {
                res[player] += cards.get(j);
                j--;
            }

            player = 1 - player;
        }

        System.out.print(res[0] + " " + res[1]);
    }
}
 */