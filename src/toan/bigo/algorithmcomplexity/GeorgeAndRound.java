package toan.bigo.algorithmcomplexity;

import java.util.Scanner;

/*
Ta có các nhận xét sau:

Giả sử chúng ta đang chọn bài tập cho đề thứ ii, nhưng trong số bài tập đã chuẩn bị có nhiều hơn 22 bài có thể dùng cho đề bài đó. Thì ta sẽ chọn bài tập có độ khó nhỏ nhất cho đề thứ ii, vì những bài tập có độ khó cao hơn sẽ dành cho những đề bài có độ khó cao hơn trong kì thi.
Nếu chúng ta đang chọn bài tập cho đề bài thứ ii của kì thi, nhưng đã không chọn được bài tập nào thỏa mãn rồi. Thì đương nhiên là đề bài thứ i + 1i+1 với độ khó lớn hơn đề bài thứ ii cũng sẽ không chọn được bài tập nào thỏa mãn cho nó. Như vậy trong quá trình chọn bài tập, xét đến đề bài thứ ii đã không chọn được bài tập nào thỏa mãn thì sẽ dừng không chọn nữa.
Giả sử chúng ta đã chọn bài tập thứ jj cho đề bài ii của kì thi, lúc này nếu xét chọn bài tập cho đề bài thứ i + 1i+1, thì tất cả những bài tập trước bài thứ jj sẽ không thể chọn được (vì nó đã không được chọn cho đề bài thứ ii thì đề bài thứ i + 1i+1 với độ khó lớn hơn cũng sẽ không chọn được).
Với các nhận xét trên, chúng ta có thể sử dụng kĩ thuật Two-Pointer để giải quyết bài này như sau:

Bước 1: Bỏ độ khó được quy định trong kỳ thi vào mảng AA, độ khó của các bài đã được George chuẩn bị vào mảng BB.

Bước 2: Sử dụng hai biến ii, jj chạy song song trên hai mảng, biến countcount để đếm số lượng đề bài đã chuẩn bị được cho kì thi.

Biến ii chạy trên mảng AA, biến jj chạy trên mảng BB.
Nếu A_i \le B_jA
​i
​​ ≤B
​j
​​  nghĩa là bài đã chuẩn bị thứ jj có thể sử dụng làm đề bài thứ ii trong kỳ thi, ta tăng biến ii, jj, countcount lên 11 đơn vị để tìm bài tập cho đề bài i + 1i+1.
Ngược lại, nếu A_i > B_jA
​i
​​ >B
​j
​​  ta tăng biến jj lên 11 đơn vị để tiếp tục tìm kiếm bài có độ khó \le A_i≤A
​i
​​  trong khoảng (j + 1)(j+1) trở về sau.
Bước 3: In ra đáp án của bài toán: n - countn−count (với countcount là số đề bài đã chuẩn bị được cho kì thi).

Độ phức tạp: \mathcal {O} \left (max(N,M) \right )O(max(N,M)) với NN, MM lần lượt là số bài kỳ thi cần và số bài hiện có.
 */
public class GeorgeAndRound {
    public static void main(String[] args) {
        // write your code here

        int n;
        int m;

        int[] nCount = new int[26];

        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        m = sc.nextInt();
        int[] arrayN = new int[n];
        int[] arrayM = new int[m];
        for (int i = 0; i < n; i++) {
            int number = sc.nextInt();
            arrayN[i] = number;
        }
        for (int i = 0; i < m; i++) {
            int number = sc.nextInt();
            arrayM[i] = number;
        }
        int ans = Math.min(n, m);
        for (int i = ans; i >= 0; --i) {
            System.out.println("khi i= " + i);
            boolean works = true;
            for (int j = 0; j < i; ++j) {
                System.out.println("so sanh" + " " + arrayN[j] + "  voi array M o vi tri " +   (m - i + j) + "  +  arrayM[m - i + j]");
                works = works & arrayN[j] <= arrayM[m - i + j];
            }
            if (works) {
                ans = n - i;
                System.out.println("works " + ans);
                break;
            }
        }
        System.out.println(ans + "");

    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        ArrayList<Integer> a = new ArrayList<>(), b = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            a.add(sc.nextInt());
        }
        for (int i = 0; i < m; i++) {
            b.add(sc.nextInt());
        }

        int count = 0;
        for (int i = 0, j = 0; i < n && j < m; j++) {
            if (b.get(j) >= a.get(i)) {
                count++;
                i++;
            }
        }

        System.out.print(n - count);
    }
}
 */
