package toan.bigo.algorithmcomplexity;

import java.util.Scanner;

/*
Nhận thấy rằng nếu ta có thể duyệt qua từng đoạn liên tiếp có tổng thời gian còn nằm trong T thời gian cho phép thì việc còn lại trở nên vô cùng đơn giản: chọn ra đoạn có chiều dài lớn nhất, tương ứng với số lượng sách đọc được là nhiều nhất.

Mỗi đoạn thỏa tính chất như trên đều có thể được biểu diễn dưới dạng một đoạn dài nhất có tổng thời không lớn hơn T tính từ quyển sách thứ i trở về trước.

Lúc này, ta có thể áp dụng kỹ thuật Two Pointers với hai biến chạy i và j như sau:

Bước 1: Đưa toàn bộ thời gian cần để đọc các quyển sách hiện có vào mảng.
Bước 2: Duyệt qua lần lượt từng quyển sách thứ i trong mảng, kiểm tra xem thời gian còn lại có đủ để ta đọc hết quyển sách thứ i này không:
Nếu không, đồng nghĩa với việc đoạn ta đang chọn đọc bắt đầu từ quyển sách thứ j đã tới giới hạn (đã là đoạn lớn nhất có tổng thời gian không quá T tính từ quyển i - 1 trở về trước). Ta di chuyển đến đoạn tiếp theo, tức đoạn bắt đầu tại vị trí (j + 1) và xét tiếp cho đến khi nào ta nhận được một vị trí bắt đầu mới mà tại đó ta vẫn có thể đọc được quyển sách thứ i đang xét.
Ngược lại, nghĩa là ta còn có thể đọc thêm quyển sách thứ i này vào đoạn hiện tại. Ta tiến hành cập nhật lại các thông số tương ứng về thời gian đọc sách còn lại và số lượng sách đã đọc được trong đoạn.
So sánh số sách hiện tại đọc được với số sách tối đa hiện có và cập nhật nếu cần.
Bước 3: In ra kết quả.
Độ phức tạp: O(N) với N là số lượng quyển sách.
 */
public class Books {
    public static void main(String[] args) {
        // write your code here

        int books;
        int freeTime;

        Scanner sc = new Scanner(System.in);
        books = sc.nextInt();
        freeTime = sc.nextInt();

        int[] arrayBook = new int[books];
        for (int i = 0; i < books; i++) {
            int number = sc.nextInt();
            arrayBook[i] = number;
        }

        int start = 0;
        int finish = 0;
        int currentTime = 0;
        int total = 0;
        while(finish < books)
        {
            currentTime = currentTime + arrayBook[finish];
            finish++;
            while (currentTime > freeTime)
            {
                currentTime = currentTime - arrayBook[start];
                start++;
            }
            if(total < finish - start)
            {
                total = finish - start;
            }
        }
        System.out.println(total + "");
    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int t = sc.nextInt();
        ArrayList<Integer> a = new ArrayList<>();

        int j = 0, max_books = 0, read_books = 0;

        for (int i = 0; i < n; i++) {
            a.add(sc.nextInt());

            while (t < a.get(i)) {
                t += a.get(j);
                j++;
                read_books--;
            }

            t -= a.get(i);
            read_books++;
            max_books = Math.max(max_books, read_books);
        }

        System.out.print(max_books);
    }
}
 */
