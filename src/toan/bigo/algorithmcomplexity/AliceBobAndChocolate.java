package toan.bigo.algorithmcomplexity;

import java.util.Scanner;

/*
Sử dụng kỹ thuật Two Pointers với hai biến chạy ii và jj tương ứng với vị trí mà Alice và Bob đang ăn. Đồng thời dùng thêm hai biến lưu thời điểm ăn của cả hai.

Bước 1: Đưa thông tin về thời gian ăn các thanh chocolate vào một mảng.
Bước 2: Khởi tạo thời điểm ăn của cả hai là 0.
Bước 3: Sử dụng hai biến chạy ii đầu mảng (Alice) và jj cuối mảng (Bob).
So sánh thời điểm ăn của Alice và Bob:
Nếu thời điểm Alice bắt đầu ăn thanh chocolate tại vị trí ii nhỏ hơn hoặc bằng thời điểm Bob ăn thanh chocolate thứ jj thì Alice sẽ ăn thanh chocolate tại vị trí ii.
Ngược lại Bob sẽ ăn thanh chocolate tại vị trí jj.
Cập nhật thời điểm ăn của từng người và hai biến chạy ii, jj.
Bước 4: Số thanh chocolate mà Alice đã ăn được chính bằng giá trị của ii và của Bob là (n - i)(n−i).
Độ phức tạp O(n)O(n) với nn là số lượng thanh chocolate.
 */
public class AliceBobAndChocolate {
    public static void main(String[] args) {
        // write your code here
        int n;

        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();

        int[] chocolate = new int[n];
        for (int i = 0; i < n; i++) {
            int number = sc.nextInt();
            chocolate[i] = number;
        }

        int left = 0;
        int right = n - 1;
        int totalTimeAlice = 0;
        int totalTimeBob = 0;
        while (left <= right) {
            if (totalTimeAlice + chocolate[left] <= totalTimeBob + chocolate[right]) {
                totalTimeAlice += chocolate[left];
                left = left + 1;
            } else {
                totalTimeBob += chocolate[right];
                right = right - 1;
            }
        }
        System.out.println(left + " " + (n - left));
    }
}
/*
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> chocolate = new ArrayList<>();
        int n = sc.nextInt();

        for (int i = 0; i < n; i++) {
            chocolate.add(sc.nextInt());
        }

        int t_alice = 0, t_bob = 0;
        int i = 0, j = n - 1;

        while (i <= j) {
            if (t_alice + chocolate.get(i) <= t_bob + chocolate.get(j)) {
                t_alice += chocolate.get(i);
                i++;
            }
            else {
                t_bob += chocolate.get(j);
                j--;
            }
        }

        System.out.print(i + " " + (n - i));
    }
}
 */