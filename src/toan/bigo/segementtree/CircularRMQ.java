package toan.bigo.segementtree;

/*
Hướng dẫn giải
Trước hết, giả sử mỗi truy vấn đều là truy vấn đoạn thẳng (có l \le rl≤r). Khi đó, ta có thể sử dụng cây phân đoạn (Segment Tree) với kĩ thuật Lazy Update để giải bài toán này.

Cụ thể, mỗi nút trên cây phân đoạn sẽ lưu trữ hai thông tin:

min_a: giá trị nhỏ nhất của các phần tử trong đoạn mà nút này quản lí
lazy: tổng giá trị cần cộng thêm cho các phần tử trong đoạn mà nút này quản lí, nhưng hiện tại chưa được cập nhật (theo cơ chế Lazy Update)
Chi tiết về cây phần đoạn, cơ chế cập nhật và cách cài đặt cây phân đoạn để xử lí hai loại truy vấn trên đã được trình bày trong lecture của BigO.

Với các truy vấn vòng (có l>rl>r), ta sẽ xử lí chúng như sau:

Truy vấn loại inc(l,r,v)inc(l,r,v): ta sẽ tách truy vấn này thành hai truy vấn inc(l,n-1,v)inc(l,n−1,v) và inc(0,r,v)inc(0,r,v)
Truy vấn loại rmq(l,r)rmq(l,r): kết quả sẽ là giá trị nhỏ nhất của hai truy vấn rmq(l,n-1)rmq(l,n−1) và rmq(0,r)rmq(0,r)
Độ phức tạp: O(N + Qlog(N))O(N+Qlog(N)) với NN là số phần tử của mảng a, Qa,Q là số truy vấn cần xử lí.

 */
public class CircularRMQ {

}

/*
#include <stdio.h>
#include <vector>
#include <math.h>

using namespace std;

const long long INF = 1e18;

struct Node
{
    long long mina;
    long long lazy;

    Node(long long mina, long long lazy) {
        this->mina = mina;
        this->lazy = lazy;
    }
};

void buildTree(vector<int> &a, vector<Node> &segtree, int left, int right, int index)
{
    if (left == right)
    {
        segtree[index].mina = a[left];
        return;
    }
    int mid = (left + right) / 2;

    buildTree(a, segtree, left, mid, 2 * index + 1);
    buildTree(a, segtree, mid + 1, right, 2 * index + 2);

    segtree[index].mina = min(segtree[2 * index + 1].mina, segtree[2 * index + 2].mina);
}

void updateQuery_minRangeLazy(vector<Node> &segtree, int left, int right, const int &from, const int &to, const int &delta, int index)
{
    if (left > right)
        return;

	//make sure all propagation is done at index. If not update tree
	//at index and mark its children for lazy propagation.
    if (segtree[index].lazy != 0)
    {
        segtree[index].mina += segtree[index].lazy;
        if (left != right)
        { // not a leaf node
            segtree[2 * index + 1].lazy += segtree[index].lazy;
            segtree[2 * index + 2].lazy += segtree[index].lazy;
        }
        segtree[index].lazy = 0;
    }

	//no overlap condition
    if (from > right || to < left)
        return;

	//total overlap condition
    if (from <= left && to >= right)
    {
        segtree[index].mina += delta;
        if (left != right)
        {
            segtree[2 * index + 1].lazy += delta;
            segtree[2 * index + 2].lazy += delta;
        }
        return;
    }

    //otherwise partial overlap so look both left and right.
    int mid = (left + right) / 2;
    updateQuery_minRangeLazy(segtree, left, mid, from, to, delta, 2 * index + 1);
    updateQuery_minRangeLazy(segtree, mid + 1, right, from, to, delta, 2 * index + 2);

    segtree[index].mina = min(segtree[2 * index + 1].mina, segtree[2 * index + 2].mina);
}

long long minRangeLazy(vector<Node> &segtree, int left, int right, const int &from, const int &to, int index)
{
    if (left > right)
        return INF;

	//make sure all propagation is done at index. If not update tree
	//at index and mark its children for lazy propagation.
    if (segtree[index].lazy != 0)
    {
        segtree[index].mina += segtree[index].lazy;
        if (left != right)
        { // not a leaf node
            segtree[2 * index + 1].lazy += segtree[index].lazy;
            segtree[2 * index + 2].lazy += segtree[index].lazy;
        }
        segtree[index].lazy = 0;
    }

	//no overlap condition
    if (from > right || to < left)
        return INF;

	//total overlap condition
	if (from <= left && to >= right)
        return segtree[index].mina;

    //partial overlap
    int mid = (left + right) / 2;
    return min(minRangeLazy(segtree, left, mid, from, to, 2 * index + 1),
               minRangeLazy(segtree, mid + 1, right, from, to, 2 * index + 2));
}

void circularUpdate(const int &n, vector<Node> &segtree, const int &from, const int &to, const int &delta) {
    if (from <= to)
    {
        updateQuery_minRangeLazy(segtree, 0, n - 1, from, to, delta, 0);
        return;
    }

    updateQuery_minRangeLazy(segtree, 0, n - 1, from, n - 1, delta, 0);
    updateQuery_minRangeLazy(segtree, 0, n - 1, 0, to, delta, 0);
}

long long circularQuery(const int &n, vector<Node> &segtree, const int &from, const int &to) {
    if (from <= to)
        return minRangeLazy(segtree, 0, n - 1, from, to, 0);

    return min(minRangeLazy(segtree, 0, n - 1, from, n - 1, 0),
               minRangeLazy(segtree, 0, n - 1, 0, to, 0));
}

int main() {
    int n;
    scanf("%d", &n);

    vector<int> a(n);
    for(int i = 0; i < n; ++i)
        scanf("%d", &a[i]);

    //Height of segment tree
    int h = (int)ceil(log2(n));

    //Maximum size of segment tree
    int sizetree = 2 * (int)pow(2, h) - 1;

    vector<Node> segtree(sizetree, Node(0, 0));
    buildTree(a, segtree, 0, n - 1, 0);

    int q;
    scanf("%d", &q);

    char line[50];
    gets(line);

    for(int i = 0; i < q; ++i)
    {
        gets(line);

        int from, to, delta;
        if (sscanf(line, "%d %d %d", &from, &to, &delta) == 3)
            circularUpdate(n, segtree, from, to, delta);
        else
            printf("%lld\n", circularQuery(n, segtree, from, to));
    }

    return 0;
}
 */

/*
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {
    private static final long INF = (long)1e18;

    private static class Node {
        long mina;
        long lazy;

        public Node(long mina, long lazy) {
            this.mina = mina;
            this.lazy = lazy;
        }
    }

    private static void buildTree(ArrayList<Integer> a, ArrayList<Node> segtree, int left, int right, int index)
    {
        if (left == right)
        {
            segtree.get(index).mina = a.get(left);
            return;
        }
        int mid = (left + right) / 2;

        buildTree(a, segtree, left, mid, 2 * index + 1);
        buildTree(a, segtree, mid + 1, right, 2 * index + 2);

        segtree.get(index).mina = Math.min(segtree.get(2 * index + 1).mina, segtree.get(2 * index + 2).mina);
    }


    private static void updateQuery_minRangeLazy(ArrayList<Node> segtree, int left, int right, int from, int to, int delta, int index)
    {
        if (left > right)
            return;

        //make sure all propagation is done at index. If not update tree
        //at index and mark its children for lazy propagation.
        if (segtree.get(index).lazy != 0)
        {
            segtree.get(index).mina += segtree.get(index).lazy;
            if (left != right)
            { // not a leaf node
                segtree.get(2 * index + 1).lazy += segtree.get(index).lazy;
                segtree.get(2 * index + 2).lazy += segtree.get(index).lazy;
            }
            segtree.get(index).lazy = 0;
        }

        //no overlap condition
        if (from > right || to < left)
            return;

        //total overlap condition
        if (from <= left && to >= right)
        {
            segtree.get(index).mina += delta;
            if (left != right)
            {
                segtree.get(2 * index + 1).lazy += delta;
                segtree.get(2 * index + 2).lazy += delta;
            }
            return;
        }

        //otherwise partial overlap so look both left and right.
        int mid = (left + right) / 2;
        updateQuery_minRangeLazy(segtree, left, mid, from, to, delta, 2 * index + 1);
        updateQuery_minRangeLazy(segtree, mid + 1, right, from, to, delta, 2 * index + 2);

        segtree.get(index).mina = Math.min(segtree.get(2 * index + 1).mina, segtree.get(2 * index + 2).mina);
    }

    private static long minRangeLazy(ArrayList<Node> segtree, int left, int right, int from, int to, int index)
    {
        if (left > right)
            return INF;

        //make sure all propagation is done at index. If not update tree
        //at index and mark its children for lazy propagation.
        if (segtree.get(index).lazy != 0)
        {
            segtree.get(index).mina += segtree.get(index).lazy;
            if (left != right)
            { // not a leaf node
                segtree.get(2 * index + 1).lazy += segtree.get(index).lazy;
                segtree.get(2 * index + 2).lazy += segtree.get(index).lazy;
            }
            segtree.get(index).lazy = 0;
        }

        //no overlap condition
        if (from > right || to < left)
            return INF;

        //total overlap condition
        if (from <= left && to >= right)
            return segtree.get(index).mina;

        //partial overlap
        int mid = (left + right) / 2;
        return Math.min(minRangeLazy(segtree, left, mid, from, to, 2 * index + 1),
                minRangeLazy(segtree, mid + 1, right, from, to, 2 * index + 2));
    }

    private static void circularUpdate(int n, ArrayList<Node> segtree, int from, int to, int delta) {
        if (from <= to)
        {
            updateQuery_minRangeLazy(segtree, 0, n - 1, from, to, delta, 0);
            return;
        }

        updateQuery_minRangeLazy(segtree, 0, n - 1, from, n - 1, delta, 0);
        updateQuery_minRangeLazy(segtree, 0, n - 1, 0, to, delta, 0);
    }

    private static long circularQuery(int n, ArrayList<Node> segtree, int from, int to) {
        if (from <= to)
            return minRangeLazy(segtree, 0, n - 1, from, to, 0);

        return Math.min(minRangeLazy(segtree, 0, n - 1, from, n - 1, 0),
                minRangeLazy(segtree, 0, n - 1, 0, to, 0));
    }

    private static double logb( double a, double b )
    {
        return Math.log(a) / Math.log(b);
    }

    private static double log2( double a )
    {
        return logb(a,2);
    }

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();

        ArrayList<Integer> a = new ArrayList<Integer>();
        for(int i = 0; i < n; ++i)
            a.add(in.nextInt());

        //Height of segment tree
        int h = (int)Math.ceil(log2(n));

        //Maximum size of segment tree
        int sizetree = 2 * (int)Math.pow(2, h) - 1;

        ArrayList<Node> segtree = new ArrayList<>();
        for(int i = 0; i < sizetree; ++i)
            segtree.add(new Node(0, 0));

        buildTree(a, segtree, 0, n - 1, 0);

        int q = in.nextInt();
        String line = in.nextLine();

        for(int i = 0; i < q; ++i)
        {
            line = in.nextLine();
            StringTokenizer st = new StringTokenizer(line);

            int from = Integer.parseInt(st.nextToken());
            int to = Integer.parseInt(st.nextToken());

            if (st.hasMoreTokens()) {
                int delta = Integer.parseInt(st.nextToken());
                circularUpdate(n, segtree, from, to, delta);
            } else
                out.println(circularQuery(n, segtree, from, to));
        }

        out.close();
    }
}
 */