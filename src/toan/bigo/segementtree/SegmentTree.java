package toan.bigo.segementtree;

import java.util.Arrays;

public class SegmentTree {

    public static void main(String[] args) {

        int[] a = {5, -7, 9, 0, -2, 8, 3, 6, 4, 1};
        int n = a.length;

        int h = (int) Math.ceil(log2(n));

        int sizeTree = 2 * (int) Math.pow(2, h) - 1;
        int[] segTree = new int[sizeTree];
        Arrays.fill(segTree, INF);
        buildTree(a, segTree, 0, n - 1, 0);

        int fromRange = 2;
        int toRange = 7;
        int min = minRange(segTree, 0, n - 1, fromRange, toRange, 0);
    }

    // minimum query
    private static void buildTree(int[] a, int[] segTree, int left, int right, int index) {
        if (left == right) {
            segTree[index] = a[left];
            return;
        }
        int mid = (left + right) / 2;
        buildTree(a, segTree, left, mid, index * 2 + 1);
        buildTree(a, segTree, mid + 1, right, index * 2 + 2);
        segTree[index] = Math.max(segTree[index * 2 + 1], segTree[index * 2 + 2]);
    }

    private static final int INF = (int) 1e9;

    private static double log2(int number) {
        return Math.log(number) / Math.log(2);
    }

    private static int minRange(int[] segtree, int left, int right, int from,
                                int to, int index) {
        if (from <= left && to >= right) {
            return segtree[index];
        }

        if (from > right || to < left) {
            return INF;
        }
        int mid = (left + right)/2;
        int a = minRange(segtree, left, mid, from, to, 2 * index + 1);
        int b = minRange(segtree, mid + 1, right, from, to, 2 * index + 2);
        return Math.min(a, b);
    }

    private static void updateQuery(int[] segTree, int[] a, int left, int right, int index, int pos, int value) {
        if (pos < left || pos > right) {
            return;
        }
        if (left == right) {
            a[pos] = value;
            segTree[index] = value;
            return;
        }

        int mid = (left + right) / 2;
        if (pos <= mid) {
            updateQuery(segTree, a, left, mid, 2 * index + 1, pos, value);
        } else {
            updateQuery(segTree, a, mid + 1, right, 2 * index + 2, pos, value);
        }
        segTree[index] = Math.min(segTree[2 * index + 1], segTree[2 * index + 2]);
    }

    private static void updateQuery_minRangeLazy(int[] segTree, int[] lazy, int left,
                                                 int right, int from, int to, int delta, int index) {
        if (left > right) {
            return;
        }

        if (lazy[index] != 0) {
            segTree[index] += lazy[index];
            if (left != right) {
                lazy[2 * index + 1] += lazy[index];
                lazy[2 * index + 2] += lazy[index];
            }
            lazy[index] = 0;
        }

        //no overlap condition
        if (from > right || to < left) {
            return;
        }

        //total overlap condition
        if (from <= left && to >= right) {
            segTree[index] += delta;
            if (left != right) {
                lazy[2 * index + 1] += delta;
                lazy[2 * index + 2] += delta;
            }
            return;
        }

        int mid = (left + right) / 2;
        updateQuery_minRangeLazy(segTree, lazy, left, mid, from, to, delta, 2 * index + 1);
        updateQuery_minRangeLazy(segTree, lazy, mid + 1, right, from, to, delta, 2 * index + 2);
        segTree[index] = Math.min(segTree[2 * index + 1], segTree[2 * index + 2]);
    }

    private static int minRangeLazy(int[] segTree, int[] lazy, int left, int right,
                                    int from, int to, int index) {
        if (left > right) {
            return INF;
        }

        if (lazy[index] != 0) {
            segTree[index] += lazy[index];
            if (left != right) {
                lazy[2 * index + 1] += lazy[index];
                lazy[2 * index + 2] += lazy[index];
            }
            lazy[index] = 0;
        }

        //no overlap condition
        if (from > right || to < left) {
            return INF;
        }

        //total overlap condition
        if (from <= left && to >= right) {
            return segTree[index];
        }

        int mid = (left + right) / 2;
        int a = minRangeLazy(segTree, lazy, left, mid, from, to, 2 * index + 1);
        int b = minRangeLazy(segTree, lazy, mid + 1, right, from, to, 2 * index + 2);
        return Math.min(a, b);
    }

}
