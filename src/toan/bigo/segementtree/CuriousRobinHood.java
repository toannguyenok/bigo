package toan.bigo.segementtree;

import java.util.Scanner;

/*
Trong bài này ta sẽ sử dụng cây Segment Tree để giải quyết các truy vấn.

Đầu tiên, ta lưu giá trị của các bao tiền vào một mảng aa. Sau đó, ta xây dựng Segment Tree dựa trên mảng aa: mỗi node lưu tổng của mảng con mà node đó quản lý.

Với truy vấn 1 i: Ta in ra giá trị a_ia
​i
​​ . Sau đó, ta gọi hàm cập nhật cây: tại vị trí thứ ii trừ đi a_ia
​i
​​ . Cuối cùng ta phải gán a_i = 0a
​i
​​ =0.
Với truy vấn 2 i v: Ta gọi hàm cập nhật cây: tại vị trí ii cộng thêm vv. Cuối cùng, ta cũng phải cộng a_ia
​i
​​  cho vv.
Với truy vấn 3 i j: Ta gọi hàm tính tổng từ vị trí ii đến jj.
Độ phức tạp: O(T * (NlogN + QlogN))O(T∗(NlogN+QlogN)) với TT là số lượng test, NN là số bao tiền, QQ là số truy vấn.
 */
public class CuriousRobinHood {

    private static final int INF = (int) 1e9;

    private static double log2(int number) {
        return Math.log(number) / Math.log(2);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int testCase = scanner.nextInt();
        for (int t = 0; t < testCase; t++) {
            int n = scanner.nextInt();
            int q = scanner.nextInt();
            int[] a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = scanner.nextInt();
            }

            int h = (int) Math.ceil(log2(n));

            int sizeTree = 2 * (int) Math.pow(2, h) - 1;
            int[] segTree = new int[sizeTree];
            buildTree(a, segTree, 0, n - 1, 0);
            System.out.println("Case " + (t + 1) + ":");
            for (int i = 0; i < q; i++) {
                int query = scanner.nextInt();
                if (query == 1) {
                    int index = scanner.nextInt();
                    int v = a[index];
                    updateQuery(segTree, a, 0, n - 1, 0, index, -v );
                    System.out.println(v);
                } else if (query == 2) {
                    int index = scanner.nextInt();
                    int v = scanner.nextInt();
                    updateQuery(segTree, a, 0, n - 1, 0, index, v);
                } else if (query == 3) {
                    int from = scanner.nextInt();
                    int to = scanner.nextInt();
                    int sumRange = sumRange(segTree, 0, n - 1, from, to, 0);
                    System.out.println(sumRange);
                }
            }
        }
    }

    private static void buildTree(int[] a, int[] segTree, int left, int right, int index) {
        if (left == right) {
            segTree[index] = a[left];
            return;
        }
        int mid = (left + right) / 2;
        buildTree(a, segTree, left, mid, index * 2 + 1);
        buildTree(a, segTree, mid + 1, right, index * 2 + 2);
        segTree[index] = segTree[index * 2 + 1] + segTree[index * 2 + 2];
    }

    private static void updateQuery(int[] segTree, int[] a, int left, int right, int index, int pos, int value) {
        if (pos < left || pos > right) {
            return;
        }
        if (left == right) {
            a[pos] += value;
            segTree[index] += value;
            return;
        }

        int mid = (left + right) / 2;
        if (pos <= mid) {
            updateQuery(segTree, a, left, mid, 2 * index + 1, pos, value);
        } else {
            updateQuery(segTree, a, mid + 1, right, 2 * index + 2, pos, value);
        }
        segTree[index] = segTree[2 * index + 1] +  segTree[2 * index + 2];
    }

    private static int sumRange(int[] segtree, int left, int right, int from,
                                int to, int index) {
        if (from <= left && to >= right) {
            return segtree[index];
        }

        if (from > right || to < left) {
            return 0;
        }
        int mid = (left + right)/2;
        int a = sumRange(segtree, left, mid, from, to, 2 * index + 1);
        int b = sumRange(segtree, mid + 1, right, from, to, 2 * index + 2);
        return a + b;
    }
}

/*
#include <bits/stdc++.h>

using namespace std;

void buildTree(const vector<int> &a, vector<int> &segTree, int index, int left, int right) {
    if (left == right) {
        segTree[index] = a[left];
        return;
    }

    int mid = (left + right) / 2;

    buildTree(a, segTree, 2 * index + 1, left,    mid);
    buildTree(a, segTree, 2 * index + 2, mid + 1, right);

    segTree[index] = segTree[2 * index + 1] + segTree[2 * index + 2];
}

void updateTree(vector<int> &segTree, int index, int left, int right, int pos, int val) {
    if (pos < left || pos > right) return;

    if (left == right) {
        if (left == pos) {
            segTree[index] += val;
        }
        return;
    }

    int mid = (left + right) / 2;

    updateTree(segTree, 2 * index + 1, left,    mid,   pos, val);
    updateTree(segTree, 2 * index + 2, mid + 1, right, pos, val);

    segTree[index] = segTree[2 * index + 1] + segTree[2 * index + 2];
}

int sumRange(const vector<int> &segTree, int index, int left, int right, int from, int to) {
    if (left > to || right < from) {
        return 0;
    }

    if (from <= left && right <= to) {
        return segTree[index];
    }

    int mid = (left + right) / 2;

    return sumRange(segTree, 2 * index + 1, left,    mid,   from, to)
         + sumRange(segTree, 2 * index + 2, mid + 1, right, from, to);
}

int main() {
    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int case_number = 1; case_number <= t; case_number++) {
        cout << "Case " << case_number << ":\n";

        int n, q;
        cin >> n >> q;

        vector<int> a(n);

        for (int i = 0; i < n; i++) {
            cin >> a[i];
        }

        int h = (int) ceil(log2(n));
        int treeSize = 2 * (int) pow(2, h) - 1;
        vector<int> segTree(treeSize);

        buildTree(a, segTree, 0, 0, n - 1);

        for (int qi = 0; qi < q; qi++) {
            int query;
            cin >> query;

            if (query == 1) {
                int i;
                cin >> i;
                cout << a[i] << '\n';
                updateTree(segTree, 0, 0, n - 1, i, -a[i]);
                a[i] = 0;
            }
            else if (query == 2) {
                int i, v;
                cin >> i >> v;
                updateTree(segTree, 0, 0, n - 1, i, v);
                a[i] += v;
            }
            else {
                int i, j;
                cin >> i >> j;
                cout << sumRange(segTree, 0, 0, n - 1, i, j) << '\n';
            }
        }
    }

    return 0;
}
 */

/*
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int t = in.nextInt();

        for (int caseNumber = 1; caseNumber <= t; caseNumber++) {
            System.out.println("Case " + caseNumber + ":");

            int n = in.nextInt();
            int q = in.nextInt();

            int[] a = new int[n];

            for (int i = 0; i < n; i++) {
                a[i] = in.nextInt();
            }

            int h = (int) Math.ceil(Math.log(n) / Math.log((2)));
            int treeSize = 2 * (int) Math.pow(2, h) - 1;

            int[] segTree = new int[treeSize];

            buildTree(a, segTree, 0, 0, n - 1);

            for (int qi = 0; qi < q; qi++) {
                int query = in.nextInt();

                if (query == 1) {
                    int i = in.nextInt();
                    System.out.println(a[i]);
                    updateTree(segTree, 0, 0, n - 1, i, -a[i]);
                    a[i] = 0;
                }
                else if (query == 2) {
                    int i = in.nextInt();
                    int v = in.nextInt();
                    updateTree(segTree, 0, 0, n - 1, i, v);
                    a[i] += v;
                }
                else {
                    int i = in.nextInt();
                    int j = in.nextInt();
                    System.out.println(sumRange(segTree, 0, 0, n - 1, i, j));
                }
            }
        }
    }

    private static int sumRange(int[] segTree, int index, int left, int right, int from, int to) {
        if (left > to || right < from) {
            return 0;
        }

        if (from <= left && right <= to) {
            return segTree[index];
        }

        int mid = (left + right) / 2;

        return sumRange(segTree, 2 * index + 1, left, mid, from, to)
             + sumRange(segTree, 2 * index + 2, mid + 1, right, from, to);
    }

    private static void updateTree(int[] segTree, int index, int left, int right, int pos, int val) {
        if (pos < left || pos > right) return;

        if (left == right) {
            if (left == pos) {
                segTree[index] += val;
            }
            return;
        }

        int mid = (left + right) / 2;

        updateTree(segTree, 2 * index + 1, left, mid, pos, val);
        updateTree(segTree, 2 * index + 2, mid + 1, right, pos, val);

        segTree[index] = segTree[2 * index + 1] + segTree[2 * index + 2];
    }

    private static void buildTree(int[] a, int[] segTree, int index, int left, int right) {
        if (left == right) {
            segTree[index] = a[left];
            return;
        }

        int mid = (left + right) / 2;

        buildTree(a, segTree, 2 * index + 1, left, mid);
        buildTree(a, segTree, 2 * index + 2, mid + 1, right);

        segTree[index] = segTree[2 * index + 1] + segTree[2 * index + 2];
    }
}
 */