package toan.bigo.segementtree;

/*
Hướng dẫn giải
Một nút trên cây phân đoạn cần chứa đủ thông tin để nút cha (nếu có) của nó có thể tính toán được kết quả của nó từ hai hút con. Với bài toán này, nhìn vào ta thấy số lượng ngoặc mở và số lượng ngoặc đóng là 2 dữ liệu quan trọng cần phải quản lý. Tuy nhiên, ta cần phải quản lý thêm số lượng cặp ngoặc đúng trong đoạn đó nữa.

Lấy ví dụ dãy ngoặc )(()))( có số lượng ngoặc mở là 33, số lượng ngoặc đóng là 44 và số cặp ngoặc đúng là 22, nó là những cặp được in đậm như sau )(()))(.

Xét dãy ngoặc ))((, ta có số lượng ngoặc mở và số lượng ngoặc đóng đồng thời bằng 22, nếu ta chỉ nghĩ đơn giản là lấy số lượng ngoặc mở trừ số lượng ngoặc đóng (hoặc ngược lại) để tính số lượng ngoặc sai thì sẽ không đúng vì với trường hợp này, kết quả của phép tính sẽ ra là 00 (tức dãy ngoặc này là đúng) nhưng thực chất nó là sai. Lúc này, số lượng cặp ngoặc đúng sẽ giúp giải quyết vấn đề này.

Trong quá trình xây dựng / cập nhật cây, giả sử các giá trị của hai nút con trái phải đã được tính, quá trình xây dựng nút cha sẽ diễn ra như sau:

Số lượng ngoặc mở sẽ bằng tổng số lượng ngoặc mở của hai nút con;
Số lượng ngoặc đóng sẽ bằng tổng số lượng ngoặc đóng của hai nút con;
Số lượng cặp ngoặc đúng sẽ bằng tổng số cặp ngoặc đúng của hai nút con cộng thêm cho giá trị nhỏ nhất của số lượng ngoặc mở còn thừa của nút con trái và số lượng ngoặc đóng còn thừa của nút con phải.
Số lượng ngoặc còn thừa của một loại được tính bằng cách lấy số ngoặc loại đó trừ đi cho số cặp ngoặc đúng.
Ví dụ dãy ngoặc )(()))( có số lượng ngoặc mở thừa là 3-2=13−2=1 và số lượng ngoặc đóng thừa là 4-2=24−2=2.

Khi ta xây dựng đến nút lá, công việc khá là đơn giản là ta chỉ cần tăng số lượng loại ngoặc tương ứng với vị trí trong dãy lên 11 đơn vị. Khi cập nhật ta cũng chỉ cần đảo giá trị của hai loại ngoặc cho nhau và đệ quy lên lại tương tự như lúc xây dựng cây.

Với thao tác kiểm tra, ta chỉ cần kiểm tra xem ba giá trị số ngoặc mở, số ngoặc đóng và số cặp ngoặc đúng của cả dãy có bằng nhau hay không. Nếu cả 33 đều bằng nhau thì đấy là một dãy ngoặc đúng, còn không thì đấy không là một dãy ngoặc đúng.

Độ phức tạp: O(T*m*log(N))O(T∗m∗log(N)) với TT là số lượng bộ dữ liệu, nn là độ dài dãy ngoặc và mm là số lượng thao tác thực hiện.
 */
public class Brackets {
}

/*
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

const int N = 30009;

struct segment_tree_node {
    int open;
    int close;
    int match;

    segment_tree_node() : open(0), close(0), match(0) {}
    segment_tree_node(int a, int b) : open(a), close(b), match(0) {}
};

int n, m;
string st;
segment_tree_node segment_tree[4 * N] = {};

segment_tree_node calculate(segment_tree_node left, segment_tree_node right) {
    segment_tree_node result;
    result.match = min(left.open - left.match, right.close - right.match) + left.match + right.match;
    result.open = left.open + right.open;
    result.close = left.close + right.close;
    return result;
}

void build_tree(int left, int right, int index) {
    if (left == right) {
        if (st[left] == ')') {
            segment_tree[index] = segment_tree_node(0, 1);
        }
        else {
            segment_tree[index] = segment_tree_node(1, 0);
        }
        return;
    }

    int mid = (left + right) / 2;

    build_tree(left, mid, 2 * index + 1);
    build_tree(mid + 1, right, 2 * index + 2);

    segment_tree[index] = calculate(segment_tree[2 * index + 1], segment_tree[2 * index + 2]);
}

void update_tree(int left, int right, int pos, int index) {
    if (pos < left || right < pos) {
        return;
    }

    if (left == right) {
        if (segment_tree[index].close == 1) {
            segment_tree[index] = segment_tree_node(1, 0);
        }
        else {
            segment_tree[index] = segment_tree_node(0, 1);
        }
        return;
    }

    int mid = (left + right) / 2;

    update_tree(left, mid, pos, 2 * index + 1);
    update_tree(mid + 1, right, pos, 2 * index + 2);

    segment_tree[index] = calculate(segment_tree[2 * index + 1], segment_tree[2 * index + 2]);
}

int main() {
    int test = 0;
    while (cin >> n >> st >> m) {
        cout << "Test " << ++test << ":\n";
        build_tree(0, n - 1, 0);

        for (int i = 0; i < m; i++) {
            int p;
            cin >> p;
            if (p == 0) {
                if (segment_tree[0].match == n / 2 && n % 2 == 0) {
                    cout << "YES\n";
                }
                else {
                    cout << "NO\n";
                }
            }
            else {
                update_tree(0, n - 1, p - 1, 0);
            }
        }
    }
    return 0;
}
 */

/*
import java.io.*;
import java.util.StringTokenizer;

public class Main
{
    static class FastReader
    {
        BufferedReader br;
        StringTokenizer st;

        public FastReader(InputStream in)
        {
            br = new BufferedReader(new InputStreamReader(in));
        }

        String next()
        {
            while (st == null || !st.hasMoreElements())
            {
                try
                {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt()
        {
            return Integer.parseInt(next());
        }

        long nextLong()
        {
            return Long.parseLong(next());
        }

        double nextDouble()
        {
            return Double.parseDouble(next());
        }

        String nextLine()
        {
            String str = "";
            try
            {
                str = br.readLine();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            return str;
        }
    }

    static final int N = 30009;
    static int n, m;
    static segment_tree_node[] segment_tree = new segment_tree_node[4 * N];
    static String st;

    static class segment_tree_node
    {
        int open, close, match;

        public segment_tree_node()
        {
            this.open = 0;
            this.close = 0;
            this.match = 0;
        }

        public segment_tree_node(int a, int b)
        {
            this.open = a;
            this.close = b;
            this.match = 0;
        }
    }

    static segment_tree_node calculate(segment_tree_node left, segment_tree_node right)
    {
        segment_tree_node result = new segment_tree_node();
        result.match = Math.min(left.open - left.match, right.close - right.match) + left.match + right.match;
        result.open = left.open + right.open;
        result.close = left.close + right.close;
        return result;
    }

    static void build_tree(int left, int right, int index)
    {
        if (left == right)
        {
            if (st.charAt(left) == ')')
            {
                segment_tree[index] = new segment_tree_node(0, 1);
            }
            else
            {
                segment_tree[index] = new segment_tree_node(1, 0);
            }
            return;
        }

        int mid = (left + right) / 2;

        build_tree(left, mid, 2 * index + 1);
        build_tree(mid + 1, right, 2 * index + 2);

        segment_tree[index] = calculate(segment_tree[2 * index + 1], segment_tree[2 * index + 2]);
    }

    static void update_tree(int left, int right, int pos, int index)
    {
        if (pos < left || right < pos)
        {
            return;
        }

        if (left == right) {
            if (segment_tree[index].close == 1)
            {
                segment_tree[index] = new segment_tree_node(1, 0);
            }
            else
                {
                segment_tree[index] = new segment_tree_node(0, 1);
            }
            return;
        }

        int mid = (left + right) / 2;

        update_tree(left, mid, pos, 2 * index + 1);
        update_tree(mid + 1, right, pos, 2 * index + 2);

        segment_tree[index] = calculate(segment_tree[2 * index + 1], segment_tree[2 * index + 2]);
    }

    public static void main(String[] args) throws IOException
    {
        FastReader sc = new FastReader(System.in);
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(System.out));

        for (int t = 0; t < 10; ++t)
        {
            n = sc.nextInt();
            st = sc.next();
            m = sc.nextInt();

            out.write("Test " + (t + 1) + ":\n");
            build_tree(0, n - 1, 0);

            for (int i = 0; i < m; i++)
            {
                int p = sc.nextInt();
                if (p == 0)
                {
                    if (segment_tree[0].match == n / 2 && n % 2 == 0)
                    {
                        out.write("YES\n");
                    }
                    else
                    {
                        out.write("NO\n");
                    }
                }
                else
                {
                    update_tree(0, n - 1, p - 1, 0);
                }
            }
        }

        out.close();
    }
}
 */