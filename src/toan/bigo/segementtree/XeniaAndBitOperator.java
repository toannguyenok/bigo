package toan.bigo.segementtree;

import java.util.Scanner;

/*
Hướng dẫn giải
Ta dùng Segment tree gồm 2^{n+1}-12
​n+1
​​ −1 nút để biểu diễn tất cả các bước biến đổi. Cụ thể, 2^n2
​n
​​  nút lá của cây chính là các phần tử a_1, a_2,...a_{2^n}a
​1
​​ ,a
​2
​​ ,...a
​2
​n
​​
​​ , mỗi tầng của cây là kết quả biến đổi của tầng dưới nó:

T[i]=a[i-2^{n+1}]T[i]=a[i−2
​n+1
​​ ] nếu i =i= 2^n2
​n
​​ ...2^{n+1}-12
​n+1
​​ −1.
T[i]=T[i*2]T[i]=T[i∗2] oror T[i*2+1]T[i∗2+1] nếu ii ở tầng 2, 4, 6,2,4,6,… đếm từ dưới lên.
T[i]=T[i*2]T[i]=T[i∗2] xorxor T[i*2+1]T[i∗2+1] nếu ii ở tầng 3, 5, 7,3,5,7,… đếm từ dưới lên.
Giá trị vv chính là T[1]T[1].

Như vậy, với mỗi thao tác p b, sau khi cập nhật a[p] = ba[p]=b, ta thấy là chỉ có đúng nn nút trong cây bị thay đổi, đó là các nút i=p+ {2^n-1}, i/2, i/4, i/8, ..., i/2^{n - 1}i=p+2
​n
​​ −1,i/2,i/4,i/8,...,i/2
​n−1
​​  . Do đó ta chỉ cần tính lại giá trị của nn nút này và xuất ra giá trị T[1]T[1]. Việc này có thể được thực hiện bằng cách cập nhật dần từ ii lên gốc và duy trì 11 biến trạng thái OR/XOROR/XOR để biết thao tác đang cần thực hiện là OROR hay XORXOR.

Độ phức tạp: O(n * (m + 2n))O(n∗(m+2n)).


 */
public class XeniaAndBitOperator {

    private static double log2(int number) {
        return Math.log(number) / Math.log(2);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int length = (int) Math.pow(2, n);
        long[] a = new long[length];
        for (int i = 0; i < length; i++) {
            long temp = scanner.nextLong();
            a[i] = temp;
        }
        int h = (int) Math.ceil(log2(length));
        int sizeTree = 2 * (int) Math.pow(2, h) - 1;
        long[] segTree = new long[sizeTree];
        buildTree(a, segTree, 0, length - 1, 0,0,h + 1);

        for (int i = 0; i < m; i++) {
            int pos = scanner.nextInt() - 1;
            long value = scanner.nextLong();
            updateQuery(segTree, a, 0, length - 1, 0, pos, value,0,h + 1);
            System.out.println(segTree[0]);
        }
    }

    private static void buildTree(long[] a, long[] segTree, int left, int right, int index, int currentDepth, int h) {
        if (left == right) {
            segTree[index] = a[left];
            return;
        }
        int mid = (left + right) / 2;
        buildTree(a, segTree, left, mid, index * 2 + 1, currentDepth + 1,h);
        buildTree(a, segTree, mid + 1, right, index * 2 + 2, currentDepth + 1,h);

        if (currentDepth % 2 == 0) {
            if(h == 2)
            {
                segTree[index] = segTree[index * 2 + 1] | segTree[index * 2 + 2];
            }else {
                segTree[index] = segTree[index * 2 + 1] ^ segTree[index * 2 + 2];
            }
        } else {
            segTree[index] = segTree[index * 2 + 1] | segTree[index * 2 + 2];
        }
    }

    private static void updateQuery(long[] segTree, long[] a, int left, int right, int index, int pos, long value, int currentDepth, int h) {
        if (pos < left || pos > right) {
            return;
        }
        if (left == right) {
            a[pos] = value;
            segTree[index] = value;
            return;
        }

        int mid = (left + right) / 2;
        if (pos <= mid) {
            updateQuery(segTree, a, left, mid, 2 * index + 1, pos, value, currentDepth + 1,h);
        } else {
            updateQuery(segTree, a, mid + 1, right, 2 * index + 2, pos, value, currentDepth + 1,h);
        }
        if (currentDepth % 2 == 0 ) {
            if(h == 2)
            {
                segTree[index] = segTree[index * 2 + 1] | segTree[index * 2 + 2];
            }else {
                segTree[index] = segTree[index * 2 + 1] ^ segTree[index * 2 + 2];
            }
        } else {
            segTree[index] = segTree[index * 2 + 1] | segTree[index * 2 + 2];
        }
    }

    int other(int operation)
    {
        return (operation^1);
    }

  /*  int perform(int a, int operation, int b)
    {
        switch(operation)
        {
            case OR  :  return (a|b);
            case XOR :  return (a^b);
        }
    }*/
}

/*
#include <bits/stdc++.h>
using namespace std;

int main() {
    int n, m;
    cin >> n >> m;
    vector<int> T(1 << (n + 1));
    for (int i = 1 << n; i < (1 << (n + 1)); ++i) {
        cin >> T[i];
        for (int p = i / 2, t = 1; p > 0; p /= 2, t ^= 1) {
            T[p] = t ? T[p << 1] | T[p << 1 | 1] : T[p << 1] ^ T[p << 1 | 1];
        }
    }

    while (m--) {
        int p, b;
        cin >> p >> b;
        p = (p + (1 << n) - 1);
        T[p] = b;
        p /= 2;
        for (int t = 1; p > 0; p /= 2, t ^= 1) {
            T[p] = t ? T[p << 1] | T[p << 1 | 1] : T[p << 1] ^ T[p << 1 | 1];
        }
        cout << T[1] << endl;
    }
}
 */

/*
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInta();
        int m = sc.nextInt();
        int[] T = new int[1 << (n + 1)];

        for (int i = 1 << n; i < (1 << (n + 1)); ++i) {
            T[i] = sc.nextInt();
            for (int p = i / 2, t = 1; p > 0; p /= 2, t ^= 1) {
                T[p] = (t == 1) ? T[p << 1] | T[p << 1 | 1] : T[p << 1] ^ T[p << 1 | 1];
            }
        }

        while (m-- > 0) {
            int p, b;
            p = sc.nextInt();
            b = sc.nextInt();
            p = (p + (1 << n) - 1);
            T[p] = b;
            p /= 2;
            for (int t = 1; p > 0; p /= 2, t ^= 1) {
                T[p] = (t == 1) ? T[p << 1] | T[p << 1 | 1] : T[p << 1] ^ T[p << 1 | 1];
            }

            System.out.print(T[1] + "\n");
        }
    }
}
 */