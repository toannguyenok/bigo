package toan.bigo.segementtree;

import java.util.Arrays;
import java.util.Scanner;

/*
Hướng dẫn giải
Đây là bài toán tương tự bài toán tính tổng trên đoạn, nhưng thay vì tính tổng chúng ta tính tích. Vì đề bài toán chỉ yêu cầu dấu của kết quả, nên những giá trị dương chúng ta sẽ thay bằng 11, âm sẽ thay bằng -1−1 và 00 thì giữ nguyên.

Bước 1: Đọc vào N,KN,K và mảng được cho bởi đề bài.
Bước 2: Viết các hàm cho buildTreebuildTree,updateQueryupdateQuery và productRangeproductRange:
buildTreebuildTree: Tương tự như buildTreebuildTree ở bài toán tính tổng trên đoạn nhưng có 22 thay đổi chính. Ở bước lưu giá trị vào Segment Tree, chúng ta lưu như đã nói ở trên. Ở bước lấy kết quả từ 22 node con, thay vì cộng, chúng ta nhân lại với nhau.
updateQueryupdateQuery: Tương tự như buildTreebuildTree, chúng ta có 22 thay đổi ở bước lưu giá trị và lấy kết quả từ 22 node con.
productRangeproductRange: Tương tự như hàm sumRangesumRange nhưng chúng ta lấy tích thay vì tổng.
Bước 2: Duyệt qua các lệnh theo đề bài, nếu là lệnh change (C) thì chúng ta gọi hàm updateQueryupdateQuery, nếu là lệnh product (P) thì chúng ta gọi hàm productRangeproductRange và dựa vào kết quả in ra theo yêu cầu của đề.
Độ phức tạp: O(N+KlogN)O(N+KlogN) với N,KN,K tương ứng trong đề bài.
 */
public class IntervalProduct {

    private static double log2(int number) {
        return Math.log(number) / Math.log(2);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            int k = scanner.nextInt();
            int[] a = new int[n];
            for (int i = 0; i < n; i++) {
                int value = scanner.nextInt();
                a[i] = fakeValue(value);
            }

            int h = (int) Math.ceil(log2(n));
            int sizeTree = 2 * (int) Math.pow(2, h) - 1;
            int[] segTree = new int[sizeTree];
            Arrays.fill(segTree, 1);
            buildTree(a, segTree, 0, n - 1, 0);

            for (int i = 0; i < k; i++) {
                String text = scanner.next();
                if (text.equals("C")) {
                    int pos = scanner.nextInt() - 1;
                    int v = scanner.nextInt();
                    updateQuery(segTree, a, 0, n - 1, 0, pos, fakeValue(v));
                } else {
                    int from = scanner.nextInt() - 1;
                    int to = scanner.nextInt() - 1;
                    int value = multiplyRange(segTree, 0, n - 1, from, to, 0);
                    if (value == 0) {
                        System.out.print("0");
                    } else if (value > 0) {
                        System.out.print("+");
                    } else {
                        System.out.print("-");
                    }
                }
            }
            System.out.println();
        }
    }

    private static int fakeValue(int realValue) {
        if (realValue == 0) {
            return 0;
        }
        if (realValue > 0) {
            return 1;
        }
        return -1;
    }

    private static void buildTree(int[] a, int[] segTree, int left, int right, int index) {
        if (left == right) {
            segTree[index] = a[left];
            return;
        }
        int mid = (left + right) / 2;
        buildTree(a, segTree, left, mid, index * 2 + 1);
        buildTree(a, segTree, mid + 1, right, index * 2 + 2);
        segTree[index] = segTree[index * 2 + 1] * segTree[index * 2 + 2];
    }

    private static void updateQuery(int[] segTree, int[] a, int left, int right, int index, int pos, int value) {
        if (pos < left || pos > right) {
            return;
        }
        if (left == right) {
            a[pos] = value;
            segTree[index] = value;
            return;
        }

        int mid = (left + right) / 2;
        if (pos <= mid) {
            updateQuery(segTree, a, left, mid, 2 * index + 1, pos, value);
        } else {
            updateQuery(segTree, a, mid + 1, right, 2 * index + 2, pos, value);
        }
        segTree[index] = segTree[2 * index + 1] * segTree[2 * index + 2];
    }

    private static int multiplyRange(int[] segtree, int left, int right, int from,
                                     int to, int index) {
        if (from <= left && to >= right) {
            return segtree[index];
        }

        if (from > right || to < left) {
            return 1;
        }
        int mid = (left + right) / 2;
        int a = multiplyRange(segtree, left, mid, from, to, 2 * index + 1);
        int b = multiplyRange(segtree, mid + 1, right, from, to, 2 * index + 2);
        return a * b;
    }
}

/*
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        while (in.hasNext()) {
            int n = in.nextInt();
            int k = in.nextInt();

            int[] a = new int[n];

            for (int i = 0; i < n; i++) {
                int x = in.nextInt();

                if (x < 0) {
                    a[i] = -1;
                }
                else if (x > 0) {
                    a[i] = 1;
                }
                else {
                    a[i] = 0;
                }
            }

            int h = (int) Math.ceil(Math.log(n) / Math.log(2));
            int treeSize = 2 * (int) Math.pow(2, h) - 1;
            int[] segTree = new int[treeSize];

            buildTree(a, segTree, 0, 0, n - 1);

            for (int i = 0; i < k; i++) {
                char c = in.next().charAt(0);

                if (c == 'C') {
                    int I = in.nextInt();
                    int V = in.nextInt();
                    --I;

                    int sign;

                    if (V > 0) {
                        sign = 1;
                    }
                    else if (V < 0) {
                        sign = -1;
                    }
                    else {
                        sign = 0;
                    }

                    updateQuery(segTree, 0, 0, n - 1, I, sign);
                }
                else if (c == 'P') {
                    int I = in.nextInt();
                    int J = in.nextInt();
                    --I; --J;

                    int product = productRange(segTree, 0, 0, n - 1, I, J);

                    if (product > 0) {
                        System.out.print('+');
                    }
                    else if (product < 0) {
                        System.out.print('-');
                    }
                    else {
                        System.out.print('0');
                    }
                }
            }
            System.out.println();
        }
    }

    private static int productRange(int[] segTree, int index, int left, int right, int from, int to) {
        if (from > right || to < left) {
            return 1;
        }

        if (from <= left && right <= to) {
            return segTree[index];
        }

        int mid = (left + right) / 2;

        return productRange(segTree, 2 * index + 1, left,    mid,   from, to)
                * productRange(segTree, 2 * index + 2, mid + 1, right, from, to);
    }

    private static void updateQuery(int[] segTree, int index, int left, int right, int pos, int val) {
        if (pos < left || pos > right) return;

        if (left == right) {
            if (left == pos) {
                segTree[index] = val;
            }
            return;
        }

        int mid = (left + right) / 2;

        updateQuery(segTree, 2 * index + 1, left,    mid,   pos, val);
        updateQuery(segTree, 2 * index + 2, mid + 1, right, pos, val);

        segTree[index] = segTree[2 * index + 1] * segTree[2 * index + 2];
    }

    private static void buildTree(int[] a, int[] segTree, int index, int left, int right) {
        if (left == right) {
            segTree[index] = a[left];
            return;
        }

        int mid = (left + right) / 2;

        buildTree(a, segTree, 2 * index + 1, left,    mid);
        buildTree(a, segTree, 2 * index + 2, mid + 1, right);

        segTree[index] = segTree[2 * index + 1] * segTree[2 * index + 2];
    }
}
 */

/*
#include <bits/stdc++.h>

using namespace std;

void buildTree(const vector<int> &a, vector<int> &segTree, int index, int left, int right) {
    if (left == right) {
        segTree[index] = a[left];
        return;
    }

    int mid = (left + right) / 2;

    buildTree(a, segTree, 2 * index + 1, left,    mid);
    buildTree(a, segTree, 2 * index + 2, mid + 1, right);

    segTree[index] = segTree[2 * index + 1] * segTree[2 * index + 2];
}

void updateQuery(vector<int> &segTree, int index, int left, int right, int pos, int val) {
    if (pos < left || pos > right) return;

    if (left == right) {
        if (left == pos) {
            segTree[index] = val;
        }
        return;
    }

    int mid = (left + right) / 2;

    updateQuery(segTree, 2 * index + 1, left,    mid,   pos, val);
    updateQuery(segTree, 2 * index + 2, mid + 1, right, pos, val);

    segTree[index] = segTree[2 * index + 1] * segTree[2 * index + 2];
}

int productRange(const vector<int> &segTree, int index, int left, int right, int from, int to) {
    if (from > right || to < left) {
        return 1;
    }

    if (from <= left && right <= to) {
        return segTree[index];
    }

    int mid = (left + right) / 2;

    return productRange(segTree, 2 * index + 1, left,    mid,   from, to)
         * productRange(segTree, 2 * index + 2, mid + 1, right, from, to);
}

int main() {
    int n, k;

    while (cin >> n >> k) {
        vector<int> a(n);

        for (int i = 0; i < n; i++) {
            int x;
            cin >> x;
            if (x < 0) {
                a[i] = -1;
            }
            else if (x > 0) {
                a[i] = 1;
            }
            else {
                a[i] = 0;
            }
        }

        int h = (int) ceil(log2(n));
        int treeSize = 2 * (int) pow(2, h) - 1;

        vector<int> segTree(treeSize);
        buildTree(a, segTree, 0, 0, n - 1);

        for (int i = 0; i < k; i++) {
            char c;
            cin >> c;

            if (c == 'C') {
                int I, V;
                cin >> I >> V;
                --I;

                int sign;

                if (V > 0) {
                    sign = 1;
                }
                else if (V < 0) {
                    sign = -1;
                }
                else {
                    sign = 0;
                }

                updateQuery(segTree, 0, 0, n - 1, I, sign);
            }
            else if (c == 'P') {
                int I, J;
                cin >> I >> J;
                --I, --J;

                int product = productRange(segTree, 0, 0, n - 1, I, J);

                if (product > 0) {
                    cout << '+';
                }
                else if (product < 0) {
                    cout << '-';
                }
                else {
                    cout << '0';
                }
            }
        }

        cout << '\n';
    }

    return 0;
}
 */