package toan.bigo.segementtree;

/*
Hướng dẫn giải
Giải thích ví dụ
Ví dụ trên gồm có 2 bộ test:

Bộ 1: Teresa không thể hái quá 100100 quả việt quất. Cách tối ưu mà cô bé có thể chọn là hái việt quất ở các bụi thứ 11 và 55. Tổng số quả việt quất hái được là 50 + 40 = 9050+40=90.
Bộ 2: Teresa không thể hái quá 8787 quả việt quất. Cách tối ưu mà cô bé có thể chọn là hái việt quất ở các bụi thứ 1, 31,3 và 55. Lưu ý rằng mặc dù hai bụi thứ 22 và 33 cho tổng số quả nhiều hơn và không vượt ngưỡng giới hạn nhưng hai bụi này nằm cạnh nhau nên không hợp lệ. Do đó, tổng số quả việt quất hái được là 21 + 30 + 14 = 6521+30+14=65.

Hướng dẫn giải
Nhận xét: Đề bài đặt ra tương đối giống với bài toán cái ba lô (dạng 0/1 Knapsack Problem) với ràng buộc số quả tối đa không được quá KK quả và đại lượng cần tối ưu là số quả hái được. Tuy nhiên, ta có thêm ràng buộc rằng không được hái cùng lúc hai bụi cây kề nhau.

Gọi f[i][j]f[i][j] là số việt quất tối đa có thể hái được tính tới bụi cây thứ ii, giới hạn là jj quả. Thực hiện tương tự như cách giải quyết bài toán cái ba lô, chỉ khác rằng khi ta chọn một bụi cây thứ ii thì giá trị WW giảm xuống còn W - w[i]W−w[i], tức ta phải chọn (n - 2)(n−2) bụi cây còn lại trong giới hạn W - w[i]W−w[i]. Kết quả của bài toán con này là p[i] + f[i - 2][W - w[i]]p[i]+f[i−2][W−w[i]].

Kết quả của cả bài toán sẽ nằm tại vị trí f[N][K]f[N][K].

Độ phức tạp: O(N \cdot K)O(N⋅K) với NN là số lượng bụi cây và KK là số quả tối đa được phép hái
 */
public class Blueberries {

}
/*
#include <bits/stdc++.h>
using namespace std;
const int MAX = 1005;

long long a[MAX];
long long f[MAX][MAX];

long long knapsack(int n, int w) {
    for (int i = 0; i <= n; i++) {
        f[i][0] = 0;
    }

    for (int i = 0; i <= w; i++) {
        f[0][i] = 0;
    }

    for (int i = a[0]; i <= w; i++) {
        f[1][i] = a[0];
    }

    for (int i = 2; i <= n; i++) {
        for (int j = 1; j <= w; j++) {
            if (a[i - 1] > j) {
                f[i][j] = f[i - 1][j];
            }
            else {
                long long tmp1 = a[i - 1] + f[i - 2][j - a[i - 1]];
                long long tmp2 = f[i - 1][j];
                f[i][j] = max(tmp1, tmp2);
            }
        }
    }

    return f[n][w];
}

int main() {
    int tc, n, k;
    cin >> tc;

    for (int t = 1; t <= tc; t++) {
        cin >> n >> k;

        for (int i = 0; i < n; i++) {
            cin >> a[i];
        }

        cout << "Scenario #" << t << ": " << knapsack(n, k);
        if (t != tc) {
            cout << endl;
        }
    }

    return 0;
}
 */

/*

 */