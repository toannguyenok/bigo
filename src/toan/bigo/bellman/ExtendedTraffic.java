package toan.bigo.bellman;

import java.util.ArrayList;
import java.util.Scanner;

/*
Giải thích ví dụ:

Lưu ý: Trọng số một cạnh được tính bằng (trọng số điểm đích – trọng số điểm nguồn)^3
​3
​​ . Do đó tùy thuộc vào thứ tự đề cho điểm nguồn và điểm đích mà trọng số của một cạnh có thể dương hoặc âm. Do đó tuy đề có đề cập chỉ có tối đa một đường đi nối trực tiếp giữa 2 điểm, đường đi đó có thể có tới 2 trọng số (một cách trực quan bạn có thể tưởng tượng mỗi trọng số đại diện cho một chiều của tuyến đường).
Ví dụ trên có 2 bộ test.

Bộ 1: Có 2 truy vấn yêu cầu tìm đường đi ngắn nhất từ:

Đỉnh 1 \rightarrow 41→4: Qua 1 \rightarrow 2 \rightarrow 3 \rightarrow 41→2→3→4 có trọng số là 1 + 1 + 1 = 31+1+1=3 không nhỏ hơn 33.
Đỉnh 1 \rightarrow 51→5: Qua 1 \rightarrow 2 \rightarrow 3 \rightarrow 4 \rightarrow 51→2→3→4→5 có trọng số là 1 + 1 + 1 + 1 + 1 = 41+1+1+1+1=4 không nhỏ hơn 33.


Bộ 2: Có 1 truy vấn yêu cầu tìm đường đi ngắn nhất từ đỉnh 1 \rightarrow 21→2: Đi trực tiếp từ 1 \rightarrow 21→2 có trọng số là 00, nhỏ hơn 33 nên in ra “?”.



**Hướng dẫn giải: **

Sử dụng công thức tính trọng số của mỗi cạnh được cho để xây dựng đồ thị và sử dụng thuật toán Bellman-Ford để tìm đường đi ngắn nhất từ đỉnh 11 đến qq đỉnh được chỉ định.

Độ phức tạp: O (T * V * E)O(T∗V∗E) với VV là số đỉnh (số điểm giao thông), EE là số đường đi, TT là số bộ test.
 */
public class ExtendedTraffic {

    private static class Edge {
        int source;
        int des;
        int weight;

        public Edge(int source, int des, int weight) {
            this.source = source;
            this.des = des;
            this.weight = weight;
        }
    }

    private static ArrayList<Integer> path = new ArrayList<>();
    private static ArrayList<Integer> dist = new ArrayList<>();
    private static ArrayList<Edge> edges = new ArrayList<>();

    public static void main(String[] args) {
        // write your code here
        int testCase;

        Scanner sc = new Scanner(System.in);
        testCase = sc.nextInt();

        ArrayList<ArrayList<String>> result = new ArrayList<>();
        for (int i = 0; i < testCase; i++) {
            edges.clear();
            path.clear();
            dist.clear();
            result.add(getResult(sc));
        }

        for (int i = 0;i <result.size();i++) {
            System.out.println("Case " + (i + 1) + ":");
            for(int j = 0;j<result.get(i).size();j++)
            {
                System.out.println(result.get(i).get(j));
            }
        }
    }

    private static ArrayList<String> getResult(Scanner sc) {
        int n;
        int m;
        ArrayList<String> result = new ArrayList<>();
        ArrayList<Integer> denoting = new ArrayList<>();
        n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            path.add(-1);
            dist.add(Integer.MAX_VALUE);
            denoting.add(sc.nextInt());
        }
        m = sc.nextInt();
        for (int i = 0; i < m; i++) {
            int start = sc.nextInt() - 1;
            int end = sc.nextInt() - 1;
            edges.add(new Edge(start, end, (int) Math.pow(denoting.get(end) - denoting.get(start),3)));
        }

        bellman(0, n, m);
        int queries = sc.nextInt();
        for (int i = 0; i < queries; i++) {
            int end = sc.nextInt() - 1;
            if (path.get(end) == -1 || dist.get(end) < 3) {
                result.add("?");
            } else {
                result.add(dist.get(end) + "");
            }
        }
        return result;
    }

    private static void bellman(int start, int n, int m) {

        int u;
        int v;
        int w;

        dist.set(start, 0);
        for (int i = 0; i < n - 2; i++) {
            for (int j = 0; j < m; j++) {
                u = edges.get(j).source;
                v = edges.get(j).des;
                w = edges.get(j).weight;
                if (dist.get(u) != Integer.MAX_VALUE && dist.get(u) + w < dist.get(v)) {
                    dist.set(v, dist.get(u) + w);
                    path.set(v, u);
                }
            }
        }
    }
}
/*
import java.util.*;
import java.io.*;

public class Main {
    static final int MAX_V = 205;
    static final int MAX_E = 205 * 204;
    static class Edge {
        int source, target, weight;

        public Edge(int _source, int _target, int _weight) {
            this.source = _source;
            this.target = _target;
            this.weight = _weight;
        }
    }

    static int n, m;
    static int[] weight = new int[MAX_V];
    static int[] dist = new int[MAX_V];
    static Edge[] graph = new Edge[MAX_E];

    public static void BellmanFord(int s) {
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[s] = 0;

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < m; j++) {
                int u = graph[j].source;
                int v = graph[j].target;
                int w = graph[j].weight;

                if (dist[u] != Integer.MAX_VALUE) {
                    dist[v] = Math.min(dist[v], dist[u] + w);
                }
            }
        }
    }

    public static void main(String[] agrs) {
        MyScanner in = new MyScanner();
        int T = in.nextInt();

        for (int t = 1; t <= T; t++) {
            n = in.nextInt();
            for (int i = 1; i <= n; i++) {
                weight[i] = in.nextInt();
            }

            m = in.nextInt();
            for (int i = 0; i < m; i++) {
                int u = in.nextInt();
                int v = in.nextInt();
                graph[i] = new Edge(u, v, (int)Math.pow(weight[v] - weight[u], 3));
            }

            BellmanFord(1);
            System.out.println("Case " + t + ":");
            int q = in.nextInt();

            for (int i = 0; i < q; i++) {
                int f = in.nextInt();
                System.out.println(dist[f] != Integer.MAX_VALUE && dist[f] >= 3 ? dist[f] : "?");
            }
        }
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                        st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
                return Integer.parseInt(next());
        }
        long nextLong() {
                return Long.parseLong(next());
        }
        double nextDouble() {
                return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
 */