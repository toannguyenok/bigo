package toan.bigo.bellman;

import java.util.ArrayList;
import java.util.Scanner;

public class MonkBusinessDay {

    private static class Node {
        int u;
        int v;
        int w;

        public Node(int u, int v, int w) {
            this.u = u;
            this.v = v;
            this.w = w;
        }
    }

    private static ArrayList<Node> graph = new ArrayList<>();
    private static ArrayList<Integer> dist = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int testcase = sc.nextInt();
        ArrayList<String> result = new ArrayList<>();
        for (int t = 0; t < testcase; t++) {
            graph.clear();
            dist.clear();
            int n = sc.nextInt();
            int m = sc.nextInt();
            for (int i = 0; i < n; i++) {
                dist.add(Integer.MAX_VALUE);
            }
            for (int i = 0; i < m; i++) {
                int u = sc.nextInt() - 1;
                int v = sc.nextInt() - 1;
                int w = sc.nextInt();
                graph.add(new Node(u, v, -w));
            }
            boolean flag = bellmanFord(0, n, m);
            if (flag) {
                result.add("Yes");
            } else {
                result.add("No");
            }
        }
        for (String ans : result) {
            System.out.println(ans);
        }
    }

    private static boolean bellmanFord(int s, int n, int m) {
        dist.set(s, 0);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int u = graph.get(j).u;
                int v = graph.get(j).v;
                int w = graph.get(j).w;

                if (dist.get(u) != Integer.MAX_VALUE && dist.get(u) + w < dist.get(v)) {
                    dist.set(v, dist.get(u) + w);
                }
            }
        }

        for (int i = 0; i < m; i++) {
            int u = graph.get(i).u;
            int v = graph.get(i).v;
            int w = graph.get(i).w;

            if (dist.get(u) != Integer.MAX_VALUE && dist.get(u) + w < dist.get(v)) {
                return true;
            }
        }
        return false;
    }
}
