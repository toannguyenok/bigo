package toan.bigo.bellman;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

/*
Ví dụ trên gồm 44 bộ test.

Bộ 1:



Ban đầu người chơi có mức năng lượng là 100100 và đứng tại phòng số 11. Người chơi cần đến căn phòng thứ 55, do đó phải đi qua căn phòng số 22. Tại đây mức năng lượng bị giảm 6060 \rightarrow→ mức năng lượng còn 4040. Tiếp tục đi đến căn phòng số 33. Mức năng lượng bị giảm 6060 \rightarrow→ còn -20−20. Không thể tiếp tục chơi nên kết quả là “hopeless”.

Bộ 2:



Mức năng lượng ban đầu 100100.
Mục tiêu: Đến căn phòng số 55.
Đi qua căn phòng số 22: năng lượng là 100 + 20 = 120100+20=120.
Đi qua căn phòng số 33: năng lượng là 120 - 60 = 60120−60=60.
Đi qua căn phòng số 44: năng lượng là 60 - 60 = 060−60=0. \rightarrow→ Dừng và in “hopeless”.
*** Lưu ý: Đề cho phép đi qua một căn phòng nhiều lần, ta nghĩ từ căn phòng số 33 có thể đi ngược lại về căn phòng số 22 để nạp năng lượng nhưng điều này là không được phép do từ căn phòng số 33, trò chơi chỉ cho phép mình đi tiếp đến căn phòng số 44. Do đó các cạnh trong đồ thị tạo là đường 11 chiều.

Thực hiện tương tự với các bộ test còn lại.

Hướng dẫn giải:

Điều ta có thể kết luận ngay: Nếu tìm được một đường đi từ phòng số 11 \rightarrow→ nn có mức năng lượng dương \rightarrow→ in ra “winnable”.

Tuy nhiên nếu ta không tìm được con đường như vậy, ta vẫn chưa thể kết luận điều gì vì có thể trên đường đi tồn tại một chu trình nào đó mà ta càng đi thì năng lượng sẽ càng tăng lên.

Do đó bài này ta sẽ xử lý như sau:

Dùng Bellman-Ford để tìm đường đi dài nhất từ 11 \rightarrow→ nn. Nếu khi đến phòng nn mà năng lượng dương thì in ra kết quả “winnable”.
Ngược lại, duyệt qua các cạnh trong đồ thị.
Nếu cạnh (u, v)(u,v) thuộc chu trình dương và từ uu có đường đi đến nn thì, không quan tâm mức năng lượng ta nhận thêm sau mỗi lần đi qua chu trình đó là bao nhiêu, điều chắc chắn là ta sẽ đi được đến nn với mức năng lượng dương, in kết quả “winnable”. \rightarrow→ Sử dụng BFS / DFS.
Lưu ý: Để tìm đường đi dài nhất bằng Bellman-Ford, thay vì đặt khoảng cách từ đỉnh bắt đầu đến các đỉnh khác trong đồ thị là dương vô cực, ta đặt là âm vô cực.

Độ phức tạp: độ phức tạp thuật toán Bellman Ford là O(V * E)O(V∗E). Với mỗi cạnh của đồ thị, ta có thể duyệt BFS lại một lần để kiểm tra chu trình thì độ phức tạp ở phần kiểm tra là O(E * (E + V))O(E∗(E+V)). Vậy độ phức tạp chung của cả chương trình là O(T * E * (E + V))O(T∗E∗(E+V)) với TT là số lượng bộ test đầu vào.
 */
public class XYZZY {

    private static class Edge {
        int source;
        int des;
        int weight;

        public Edge(int source, int des, int weight) {
            this.source = source;
            this.des = des;
            this.weight = weight;
        }
    }

    private static class Temp {
        ArrayList<Integer> b;

        public Temp(ArrayList<Integer> b) {
            this.b = b;
        }
    }

    private static ArrayList<Integer> path = new ArrayList<>();
    private static ArrayList<Integer> dist = new ArrayList<>();
    private static ArrayList<Edge> edges = new ArrayList<>();

    private static ArrayList<ArrayList<Integer>> graphDFS = new ArrayList<>();
    private static ArrayList<Boolean> visitedDFS = new ArrayList<>();
    private static ArrayList<Integer> pathDFS = new ArrayList<>();


    private static boolean cheat = false;

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        ArrayList<String> result = new ArrayList<>();
        int n = 0;
        while (n != -1) {
            n = sc.nextInt();
            if (n != -1) {
                edges.clear();
                path.clear();
                dist.clear();
                graphDFS.clear();
                visitedDFS.clear();
                pathDFS.clear();
                cheat = false;
                result.add(getResult(sc, n));
            }
        }

        for (String s : result) {
            System.out.println(s);
        }
    }

    private static String getResult(Scanner sc, int n) {
        int start = -1;
        int end = -1;
        ArrayList<Temp> tempArray = new ArrayList<>();
        ArrayList<Integer> energyArray = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graphDFS.add(new ArrayList<>());
            visitedDFS.add(false);
            pathDFS.add(-1);
            int energy = sc.nextInt();
            if (energy == 0) {
                if (start == -1) {
                    start = i;
                } else {
                    end = i;
                }
            }
            energyArray.add(energy);
            path.add(-1);
            dist.add(-Integer.MAX_VALUE);
            int door = sc.nextInt();
            ArrayList<Integer> edgeTemp = new ArrayList<>();
            for (int j = 0; j < door; j++) {
                int temp = sc.nextInt() - 1;
                edgeTemp.add(temp);
            }
            tempArray.add(new Temp(edgeTemp));
        }

        for (int i = 0; i < tempArray.size(); i++) {
            for (int j = 0; j < tempArray.get(i).b.size(); j++) {
                edges.add(new Edge(i, tempArray.get(i).b.get(j),
                        energyArray.get(tempArray.get(i).b.get(j))));
                graphDFS.get(i).add(tempArray.get(i).b.get(j));
            }
        }

        bellman(0, n, edges.size());
        if ((dist.get(end) > 0) || cheat) {
            return "winnable";
        } else {
            return "hopeless";
        }
    }


    private static void bellman(int start, int n, int m) {

        int u;
        int v;
        int w;

        dist.set(start, 100);
        for (int i = 0; i < n - 2; i++) {
            for (int j = 0; j < m; j++) {
                u = edges.get(j).source;
                v = edges.get(j).des;
                w = edges.get(j).weight;
                if (dist.get(u) != Integer.MAX_VALUE && dist.get(u) > 0 && dist.get(u) + w > dist.get(v)) {
                    dist.set(v, dist.get(u) + w);
                    path.set(v, u);
                }
            }
        }

        for (int i = 0; i < m; i++) {
            u = edges.get(i).source;
            v = edges.get(i).des;
            w = edges.get(i).weight;
            ArrayList<Integer> dfs = dfs(graphDFS, visitedDFS, pathDFS, u);
            if (dist.get(u) != -Integer.MAX_VALUE && dist.get(u) > 0 && dist.get(u) + w > dist.get(v)
                    && dfs.get(n - 1) != -1) {
                cheat = true;
            }
        }
    }

    public static ArrayList<Integer> dfs(ArrayList<ArrayList<Integer>> graph,
                                         ArrayList<Boolean> visited,
                                         ArrayList<Integer> path,
                                         int firstU) {
        Stack<Integer> stack = new Stack<>();
        stack.add(firstU);
        visited.set(firstU, true);
        while (!stack.isEmpty()) {
            int u = stack.pop();
            for (int i = 0; i < graph.get(u).size(); i++) {
                int nextU = graph.get(u).get(i);
                if (!visited.get(nextU)) {
                    visited.set(nextU, true);
                    path.set(nextU, u);
                    stack.add(nextU);
                }
            }
        }
        return path;
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX = 105;
    static int n, m;
    static class Edge {
        int source, target;

        public Edge(int _source, int _target) {
            this.source = _source;
            this.target = _target;
        }
    };

    static int[] energy = new int[MAX];
    static int[] dist = new int[MAX];
    static boolean[] visited = new boolean[MAX];
    static ArrayList<Edge> graph = new ArrayList<>();

    public static boolean hasPathBFS(int s, int f) {
        Arrays.fill(visited, false);
        Queue<Integer> q = new LinkedList<>();
        q.add(s);
        visited[s] = true;

        while (!q.isEmpty()) {
            int u = q.poll();

            for (Edge edge : graph) {
                if (edge.source == u) {
                    int v = edge.target;

                    if (!visited[v]) {
                        visited[v] = true;
                        q.add(v);
                    }

                    if (v == f) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean BellmanFord(int s, int f) {
        Arrays.fill(dist, Integer.MIN_VALUE);
        dist[1] = 100;

        for (int i = 0; i < n - 1; i++) {
            for (Edge edge : graph) {
                int u = edge.source;
                int v = edge.target;
                if (dist[u] > 0) {
                    dist[v] = Math.max(dist[v], dist[u] + energy[v]);
                }
            }
        }

        for (Edge edge : graph) {
            int u = edge.source;
            int v = edge.target;
            if (dist[u] > 0 && dist[u] + energy[v] > dist[v] && hasPathBFS(u, f)) {
                return true;
            }
        }

        return dist[f] > 0;
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);

        while (true) {
            graph.clear();

            n = sc.nextInt();
            if (n == -1) {
                break;
            }

            for (int u = 1; u <= n; u++) {
                energy[u] = sc.nextInt();
                m = sc.nextInt();

                for (int i = 0; i < m; i++) {
                    int v = sc.nextInt();
                    graph.add(new Edge(u, v));
                }
            }

            boolean canGo = BellmanFord(1, n);
            System.out.println(canGo ? "winnable" : "hopeless");
        }
    }
}
 */