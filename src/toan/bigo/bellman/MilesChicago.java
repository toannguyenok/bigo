package toan.bigo.bellman;

import java.util.ArrayList;
import java.util.Scanner;

/*
Ví dụ chỉ gồm 1 bộ test và được minh họa sau đây:



Để đi từ 1 \rightarrow 51→5, con đường an toàn và có xác suất cao nhất là 1 \rightarrow 4 \rightarrow 51→4→5 với xác suất là 0.85 * 0.9 * 0.8 = 0.612 = 61.2\%0.85∗0.9∗0.8=0.612=61.2%

Hướng dẫn giải:

Để giải quyết bài này, trước hết ta cần nhớ lại kiến thức về xác suất, cụ thể là quy tắc nhân được phát biểu như sau: “Nếu một công việc nào đó phải hoàn thành qua nn giai đoạn liên tiếp, trong đó:

Phương án thứ 11 có m_1m
​1
​​  cách thực hiện.
Phương án thứ 22 có m_2m
​2
​​  cách thực hiện.
…
Phương án thứ nn có m_nm
​n
​​  cách thực hiện.
Khi đó sẽ có m_1 * m_2 * ... * m_nm
​1
​​ ∗m
​2
​​ ∗...∗m
​n
​​  cách để hoàn thành công việc được cho.

Từ đó ta quy ra công thức tính xác suất đi từ uu đến vv qua nn tuyến đường chính bằng tích của nn tuyến đường đó với nhau, với trọng số của mỗi tuyến đường được quy về xác suất thay vì phần trăm như đề cho.

Sau đó sử dụng thuật toán Bellman-Ford để tìm tuyến đường có tích lớn nhất xuất phát từ đỉnh 11 và không cần thiết kiểm tra chu trình âm vì chắc chắn tích xác suất luôn lớn hơn hoặc bằng 00:

Gán xác suất để đi từ đỉnh 11 đến các đỉnh còn lại là -1.0−1.0 (nghĩa là ta chưa tìm được đường đi tới các đỉnh này).
Xác suất để đi từ đỉnh 11 đến chính nó bằng 1.01.0.
Với mỗi cạnh (u, v)(u,v):
Nếu tích xác suất của con đường mới từ 11 đến uu thông qua vv lớn hơn xác suất hiện tại đi từ 11 đến uu thì ta cập nhật lại.
Tương tự, nếu tích xác suất của con đường mới từ 11 đến vv thông qua uu lớn hơn xác suất hiện tại đi từ 11 đến vv thì cập nhật lại.
Cuối cùng in ra kết quả chính xác đến 66 chữ số thập phân.

Lưu ý: Thuật toán Bellman-Ford chỉ có thể áp dụng trên đồ thị vô hướng có trọng số dương vì một cạnh có trọng số âm trong đồ thị vô hướng tạo nên một chu trình âm.

Độ phức tạp: O(T * V * E)O(T∗V∗E) với TT là số lượng bộ test, O(V * E)O(V∗E) là độ phức tạp của Bellman-Ford.
 */
public class MilesChicago {

    private static class Node {
        int u;
        int v;
        double weight;

        public Node(int u, int v, double weight) {
            this.u = u;
            this.v = v;
            this.weight = weight;
        }
    }

    private static ArrayList<Node> graph = new ArrayList<>();
    private static ArrayList<Double> dist = new ArrayList<>();

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        ArrayList<Double> result = new ArrayList<>();
        while (flag) {
            int n = sc.nextInt();
            if (n == 0) {
                flag = false;
            } else {
                int m = sc.nextInt();
                graph.clear();
                dist.clear();
                for(int i = 0 ;i<n;i++)
                {
                    dist.add(-1.0);
                }
                for(int i = 0;i<m;i++)
                {
                    int u = sc.nextInt() - 1;
                    int v = sc.nextInt() - 1;
                    int weight = sc.nextInt();
                    graph.add(new Node(u,v,weight/100.0));
                }
                bellmanFord(0,n,m);
                result.add(dist.get(n  - 1));
            }
        }
        for (Double aDouble : result) {
            System.out.printf("%.6f percent\n", aDouble * 100.0);
        }
    }

    private static void bellmanFord(int dest, int n, int m)
    {
        dist.set(dest,1.0);
        for(int i = 0;i<n;i++)
        {
            for(int j =0 ;j<m;j++)
            {
                int u = graph.get(j).u;
                int v = graph.get(j).v;
                double w = graph.get(j).weight;
                dist.set(u, Math.max(dist.get(u), dist.get(v)*w));
                dist.set(v,Math.max(dist.get(v), dist.get(u) * w));
            }
        }
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX_N = 100 + 5;
    static final int MAX_M = 100 * 50 + 5;
    static class Edge {
        int source, target;
        double weight;

        public Edge(int _source, int _target, double _weight) {
            this.source = _source;
            this.target = _target;
            this.weight = _weight;
        }
    }

    static int n, m;
    static double[] prob = new double[MAX_N];
    static Edge[] graph = new Edge[MAX_M];

    public static void BellmanFord() {
        Arrays.fill(prob, -1.0);
        prob[1] = 1.0;

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < m; j++) {
                int u = graph[j].source;
                int v = graph[j].target;
                double w = graph[j].weight;

                prob[u] = Math.max(prob[u], prob[v] * w);
                prob[v] = Math.max(prob[v], prob[u] * w);
            }
        }
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);

        while (true) {
            n = sc.nextInt();
            if (n == 0) {
                break;
            }

            m = sc.nextInt();

            for (int i = 0; i < m; i++) {
                int u = sc.nextInt();
                int v = sc.nextInt();
                double w = sc.nextInt();
                graph[i] = new Edge(u, v, w / 100.0);
            }

            BellmanFord();
            System.out.printf("%.6f percent\n", prob[n] * 100.0);
        }
    }
}
 */