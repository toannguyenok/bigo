package toan.bigo.bellman;

import java.util.ArrayList;

public class Bellman {

    private static class Edge {
        int source;
        int des;
        int weight;

        public Edge(int source, int des, int weight) {
            this.source = source;
            this.des = des;
            this.weight = weight;
        }
    }

    private static ArrayList<Integer> path = new ArrayList<>();
    private static ArrayList<Integer> dist = new ArrayList<>();
    private static ArrayList<Edge> edges = new ArrayList<>();

    private static boolean bellman(int start, int n, int m) {

        int u;
        int v;
        int w;

        dist.set(start, 0);
        for (int i = 0; i < n - 2; i++) {
            for (int j = 0; j < m; j++) {
                u = edges.get(j).source;
                v = edges.get(j).des;
                w = edges.get(j).weight;
                if (dist.get(u) != Integer.MAX_VALUE && dist.get(u) + w < dist.get(v)) {
                    dist.set(v, dist.get(u) + w);
                    path.set(v, u);
                }
            }
        }

        for (int i = 0; i < m; i++) {
            u = edges.get(i).source;
            v = edges.get(i).des;
            w = edges.get(i).weight;
            if (dist.get(u) != Integer.MAX_VALUE && dist.get(u) + w < dist.get(v)) {
                return true;
            }
        }
        return false;
    }
}
