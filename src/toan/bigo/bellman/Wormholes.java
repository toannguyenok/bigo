package toan.bigo.bellman;

import java.util.ArrayList;
import java.util.Scanner;

/*
Giải thích ví dụ:

Trước hết ta định nghĩa lại chu trình âm: “Chu trình âm là một chu trình có tổng các trọng số là âm.”

Ví dụ trên gồm 2 bộ test:

Bộ 1: Tồn tại một chu trình âm, do đi từ 1 \rightarrow 2 \rightarrow 11→2→1 có tổng trọng số là 15 + (-42) = -27 < 015+(−42)=−27<0.



In ra “possible”.

Bộ 2: Không tồn tại chung trình âm nào hết nên in ra là “not possible”.



Lưu ý chu trình 0 \rightarrow 1 \rightarrow 2 \rightarrow 3 \rightarrow 00→1→2→3→0 có tổng trọng số là 10 + 20 + 30 + (-60) = 010+20+30+(−60)=0 nên không phải một chu trình âm.

Hướng dẫn giải:

Sử dụng thuật toán Bellman-Ford để phát hiện chu trình âm trong đồ thị.

Độ phức tạp: O (T * V * E)O(T∗V∗E) với TT là số lượng bộ test, VV là số lượng đỉnh và EE là số lượng cạnh của đồ thị.
 */
public class Wormholes {

    private static class Edge {
        int source;
        int des;
        int weight;

        public Edge(int source, int des, int weight) {
            this.source = source;
            this.des = des;
            this.weight = weight;
        }
    }

    private static ArrayList<Integer> path = new ArrayList<>();
    private static ArrayList<Integer> dist = new ArrayList<>();
    private static ArrayList<Edge> edges = new ArrayList<>();

    public static void main(String[] args) {
        // write your code here
        int testCase;

        Scanner sc = new Scanner(System.in);
        testCase = sc.nextInt();

        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < testCase; i++) {
            edges.clear();
            path.clear();
            dist.clear();
            result.add(getResult(sc));
        }

        for (String s : result) {
            System.out.println(s);
        }
    }

    private static String getResult(Scanner sc) {
        int n;
        int m;
        n = sc.nextInt();
        m = sc.nextInt();
        for (int i = 0; i < n; i++) {
            path.add(-1);
            dist.add(Integer.MAX_VALUE);
        }
        for (int i = 0; i < m; i++) {
            int start = sc.nextInt();
            int end = sc.nextInt();
            int weight = sc.nextInt();
            edges.add(new Edge(start, end, weight));
        }

        if (bellman(0, n, m)) {
            return "possible";

        }
        return "not possible";
    }

    private static boolean bellman(int start, int n, int m) {

        int u;
        int v;
        int w;

        dist.set(start, 0);
        for (int i = 0; i < n - 2; i++) {
            for (int j = 0; j < m; j++) {
                u = edges.get(j).source;
                v = edges.get(j).des;
                w = edges.get(j).weight;
                if (dist.get(u) != Integer.MAX_VALUE && dist.get(u) + w < dist.get(v)) {
                    dist.set(v, dist.get(u) + w);
                    path.set(v, u);
                }
            }
        }

        for (int i = 0; i < m; i++) {
            u = edges.get(i).source;
            v = edges.get(i).des;
            w = edges.get(i).weight;
            if (dist.get(u) != Integer.MAX_VALUE && dist.get(u) + w < dist.get(v)) {
                return true;
            }
        }
        return false;
    }
}
/*
import java.util.*;

public class Main {
    static final int MAX_V = 1005;
    static final int MAX_E = 2005;
    static class Edge {
        int source, target, weight;

        public Edge(int _source, int _target, int _weight) {
            this.source = _source;
            this.target = _target;
            this.weight = _weight;
        }
    };

    static int n, m;
    static int dist[] = new int[MAX_V];
    static Edge graph[] = new Edge[MAX_E];

    public static boolean BellmanFord(int s) {
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[s] = 0;

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < m; j++) {
                int u = graph[j].source;
                int v = graph[j].target;
                int w = graph[j].weight;

                if (dist[u] != Integer.MAX_VALUE && dist[u] + w < dist[v]) {
                    dist[v] = dist[u] + w;
                }
            }
        }

        for (int i = 0; i < m; i++) {
            int u = graph[i].source;
            int v = graph[i].target;
            int w = graph[i].weight;

            if (dist[u] != Integer.MAX_VALUE && dist[u] + w < dist[v]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();

        while (T-- > 0) {
            n = sc.nextInt();
            m = sc.nextInt();

            for (int i = 0; i < m; i++) {
                int x = sc.nextInt();
                int y = sc.nextInt();
                int t = sc.nextInt();
                graph[i] = new Edge(x, y, t);
            }

            System.out.println(!BellmanFord(0) ? "possible" : "not possible");
        }
    }
}
 */