package toan.bigo.bellman;

/*
Giữa hai đỉnh (u, v)(u,v) có thể xảy ra 33 trường hợp:

Tồn tại chu trình âm trên đường từ uu đến vv.
Từ uu không thể đến vv.
Từ uu đến được vv với đường đi ngắn nhất.
Để kiểm tra lần lượt các trường hợp trên, ta sử dụng thuật toán Bellman-Ford.

Trước tiên, ta sử dụng ma trận kề (adjacency matrix) hoặc danh sách cạnh (edge list) để lưu trọng số các cạnh (khuyến khích bạn lưu đồ thị dưới dạng danh sách cạnh để tránh lưu các thông tin thừa).

Xây dựng danh sách cạnh của đồ thị:
Nếu khoảng cách giữa tượng đài đến chính nó là dương, ta xem như khoảng cách này bằng 00 (như phát biểu của đề).
Nếu khoảng cách giữa tượng đài đến chính nó bằng âm, đồng nghĩa với việc ta có thể đi nhiều lần từ đỉnh đó tới chính nó để độ dài đường đi giảm vô hạn, lúc này ta gán lại khoảng cách bằng âm vô cực.
Sử dụng Bellman-Ford tìm đường đi ngắn nhất và phát hiện chu trình âm:
Vì có nhiều truy vấn nên thay vì với mỗi truy vấn, ta chạy lại Bellman-Ford với mỗi đỉnh nguồn, ta có thể chạy Bellman-Ford với tất cả các đỉnh trong đồ thị duy nhất một lần và lưu kết quả vào mảng 2 chiều.
Đồng thời sử dụng thêm một mảng 2 chiều khác đánh dấu từ đỉnh uu đến đỉnh vv bất kỳ có tồn tại chu trình âm hay không.
Với mỗi truy vấn, kiểm tra từng trường hợp nêu trên và in ra kết quả cần tìm.

Độ phức tạp: O(T * N^4)O(T∗N
​4
​​ ) với NN là số tượng đài – số đỉnh của đồ thị.
 */
public class AliceInWonderLand {
}

/*
import java.util.*;
import java.io.*;

public class Main {
   static final long INF = (1l << 30) * 100 + 7;
   static final int MAX = 105;

   static class Edge {
      int source, target;
      long weight;

      public Edge(int _source, int _target, long _weight) {
         this.source = _source;
         this.target = _target;
         this.weight = _weight;
      }
   };

   static int n, m;
   static long[][] dist = new long[MAX][MAX];
   static String[] monuments = new String[MAX];
   static Edge[] graph = new Edge[MAX * MAX];

   public static void BellmanFord(int s) {
      dist[s][s] = 0;

      for (int i = 0; i < n - 1; i++) {
         for (int j = 0; j < m; j++) {
            int u = graph[j].source;
            int v = graph[j].target;
            long w = graph[j].weight;
            if (dist[s][u] != INF && dist[s][u] + w < dist[s][v]) {
               dist[s][v] = dist[s][u] + w;
            }
         }
      }

      for (int i = 0; i < n - 1; i++) {
         for (int j = 0; j < m; j++) {
            int u = graph[j].source;
            int v = graph[j].target;
            long w = graph[j].weight;
            if (dist[s][u] != INF && dist[s][u] + w < dist[s][v]) {
               dist[s][v] = -INF;
            }
         }
      }
   }

   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      int tc = 1;

      while (true) {
         n = sc.nextInt();
         m = 0;
         if (n == 0) {
            break;
         }

         for (int i = 0; i < n; i++) {
            monuments[i] = sc.next();
            for (int j = 0; j < n; j++) {
               long w = sc.nextLong();
               dist[i][j] = INF;
               if (i != j && w == 0) {
                  continue;
               }
               if (i == j && w >= 0) {
                  w = 0;
               }
               graph[m++] = new Edge(i, j, w);
            }
         }

         for (int i = 0; i < n; i++) {
            BellmanFord(i);
         }

         int q = sc.nextInt();
         System.out.println("Case #" + tc++ + ":");

         while (q-- > 0) {
            int u = sc.nextInt();
            int v = sc.nextInt();
            if (dist[u][v] <= -INF) {       // Be careful here
               System.out.println("NEGATIVE CYCLE");
            }
            else {
               System.out.print(monuments[u] + "-" + monuments[v] + " ");
               System.out.println(dist[u][v] != INF ? dist[u][v] : "NOT REACHABLE");
            }
         }
      }
   }
}
 */