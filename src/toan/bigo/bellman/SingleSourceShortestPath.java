package toan.bigo.bellman;

import java.util.ArrayList;
import java.util.Scanner;

/*
Giải thích:

Ví dụ trên có 2 bộ test.

Bộ 1:



Đường đi ngắn nhất từ đỉnh 00 đến:

Đỉnh 1: Ta có thể đi 00 \rightarrow→ 11 \rightarrow→ 22 \rightarrow→ 11 \rightarrow→ ... và lặp lại vô tận. Do đó không thể xác định được trọng số nhỏ nhất.
Đỉnh 3: Đi thẳng từ 00 \rightarrow→ 33 là đường đi ngắn nhất với trọng số là 22.
Đỉnh 4: Không thể đi từ 00 đến 44.
Bộ 2:



Đường đi ngắn nhất từ 00 \rightarrow→ 11 có trọng số là -100−100.

Hướng dẫn giải:

Nhận xét: Nhiệm vụ của ta là tìm đường đi ngắn nhất từ một đỉnh đến các đỉnh còn lại trên đồ thị có cạnh có trọng số âm, do đó ta sẽ áp dụng thuật toán Bellman-Ford để giải quyết.

Sau khi chạy Bellman-Ford một lần để lấy được trọng số nhỏ nhất từ đỉnh nguồn đến các đỉnh còn lại trong đồ thị. Lúc này xét các khoảng cách thu được:

Nếu khoảng cách từ đỉnh nguồn ss tới đỉnh ii có giá trị là dương vô cực, nghĩa là không tồn tại đường đi từ ss \rightarrow→ ii.
Ngược lại, nếu khoảng cách này khác dương vô cực có thể xảy ra 22 trường hợp:
Đó là trọng số nhỏ nhất đi từ ss \rightarrow→ ii.
Đỉnh ii thuộc chu trình âm nên khảng cách này là không xác định.
Ta biết rằng thuật toán Bellman-Ford truyền thống chỉ cho phép ta pháp hiện có tồn tại chu trình âm bất kỳ trong đồ thị chứ không chỉ rõ các đỉnh thuộc các chu trình âm trong đồ thị đó. \rightarrow→ Chạy Bellman-Ford lần hai, nếu lại tìm được một khoảng cách từ ss \rightarrow→ vv tốt hơn thì chắc chắn vv thuộc chu trình âm. Ta gán khoảng cách là âm vô cực.

Cuối cùng với mỗi truy vấn, ta gọi đỉnh yêu cầu uu và so sánh:

Nếu khoảng cách từ ss \rightarrow→ uu là dương vô cực: in “Impossible”.
Nếu khoảng cách từ ss \rightarrow→ uu là âm vô cực: in “-Infinity”.
Ngược lại, khoảng cách từ ss \rightarrow→ uu xác định, ta in ra khoảng cách ấy.
Độ phức tạp: Ta sẽ phải chạy thuật Bellman-Ford 22 lần cho mỗi bộ test tốn O(n * m)O(n∗m) và đọc dữ liệu tốn O(m + q)O(m+q). Vậy độ phức tạp toàn bài là O(T * n * m)O(T∗n∗m) với TT là số lượng bộ test, nn là số đỉnh và mm là số cạnh của đồ thị.
 */
public class SingleSourceShortestPath {

    private static int INF = (int) 10e7 + 5;

    public static class Node {
        int u;
        int v;
        int w;

        public Node(int u, int v, int w) {
            this.u = u;
            this.v = v;
            this.w = w;
        }
    }

    private static ArrayList<Integer> dist = new ArrayList<>();
    private static ArrayList<Node> graph = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        ArrayList<ArrayList<String>> result = new ArrayList<>();
        while (flag) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            int q = sc.nextInt();
            int s = sc.nextInt();
            if (n == 0 && m == 0 && q == 0 && s == 0) {
                flag = false;
            } else {
                dist.clear();
                graph.clear();
                ArrayList<String> ans = new ArrayList<>();
                for (int i = 0; i < n; i++) {
                    dist.add(INF);
                }
                for (int i = 0; i < m; i++) {
                    int u = sc.nextInt();
                    int v = sc.nextInt();
                    int w = sc.nextInt();
                    graph.add(new Node(u, v, w));
                }

                bellmanFord(s, n, m);
                for (int i = 0; i < q; i++) {
                    int d = sc.nextInt();
                    if (dist.get(d) == INF) {
                        ans.add("Impossible");
                    } else if (dist.get(d) == -INF) {
                        ans.add("-Infinity");
                    } else {
                        ans.add(dist.get(d) + "");
                    }
                }
                result.add(ans);
            }
        }

        for (int i = 0; i < result.size(); i++) {
            for (int j = 0; j < result.get(i).size(); j++) {
                System.out.println(result.get(i).get(j));
            }
            System.out.println();
        }
    }

    private static void bellmanFord(int s, int n, int m) {
        dist.set(s, 0);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int u = graph.get(j).u;
                int v = graph.get(j).v;
                int w = graph.get(j).w;
                if (dist.get(u) != INF && dist.get(u) + w < dist.get(v)) {
                    dist.set(v, dist.get(u) + w);
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int u = graph.get(j).u;
                int v = graph.get(j).v;
                int w = graph.get(j).w;
                if (dist.get(u) != INF && dist.get(u) + w < dist.get(v)) {
                    dist.set(v, -INF);
                }
            }
        }
    }
}
/*
import java.util.*;
import java.io.*;

public class Main {
    static final int INF = (int)1e9 + 7;
    static final int MAX_N = 1005;
    static final int MAX_M = 5005;
    static class Edge {
        int source, target, weight;

        public Edge(int _source, int _target, int _weight) {
            this.source = _source;
            this.target = _target;
            this.weight = _weight;
        }
    }

    static int n, m;
    static int[] dist = new int[MAX_N];
    static Edge[] graph = new Edge[MAX_M];

    public static void BellmanFord(int s) {
        Arrays.fill(dist, INF);
        dist[s] = 0;

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < m; j++) {
                int u = graph[j].source;
                int v = graph[j].target;
                int w = graph[j].weight;

                if (dist[u] != INF && dist[u] + w < dist[v]) {
                    dist[v] = dist[u] + w;
                }
            }
        }

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < m; j++) {
                int u = graph[j].source;
                int v = graph[j].target;
                int w = graph[j].weight;

                if (dist[u] != INF && dist[u] + w < dist[v]) {
                    dist[v] = -INF;
                }
            }
        }
    }

    public static void main(String[] agrs) {
        MyScanner in = new MyScanner();
        while (true) {
            n = in.nextInt();
            m = in.nextInt();
            int q = in.nextInt();
            int s = in.nextInt();

            if (n == 0) {
                break;
            }

            for (int i = 0; i < m; i++) {
                int u = in.nextInt();
                int v = in.nextInt();
                int w = in.nextInt();
                graph[i] = new Edge(u, v, w);
            }

            BellmanFord(s);

            while (q-- > 0) {
                int f = in.nextInt();

                if (dist[f] == INF) {
                    System.out.println("Impossible");
                }
                else if (dist[f] == -INF) {
                    System.out.println("-Infinity");
                }
                else {
                    System.out.println(dist[f]);
                }
            }
            System.out.println();
        }
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                        st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
                return Integer.parseInt(next());
        }
        long nextLong() {
                return Long.parseLong(next());
        }
        double nextDouble() {
                return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
 */