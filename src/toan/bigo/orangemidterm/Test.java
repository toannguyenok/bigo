package toan.bigo.orangemidterm;

import java.util.ArrayList;
import java.util.Scanner;

public class Test {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCases = scanner.nextInt();
        ArrayList<Long> result = new ArrayList<>();
        for (int t = 0; t < testCases; t++) {
            long n = scanner.nextLong();
            if(n > 64)
            {
                result.add(n);
            }else {
                result.add(paper(n));
            }
        }
        for(int i = 0;i<result.size();i++)
        {
            System.out.println(result.get(i));
        }
    }

    public static long paper(long n) {
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 3;
        }
        return 1 + paper(n - 1) * 2;
    }
}
