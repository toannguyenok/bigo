package toan.bigo.orangemidterm;

import java.util.Scanner;

public class PalindoremicSeriesTest {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testcases = scanner.nextInt();
        char[] character = new char[10];
        char startChar = 'a';
        for (int i = 0; i < 10; i++) {
            character[i] = startChar;
            startChar++;
        }
        for (int t = 0; t < testcases; t++) {
            String lineNumber = scanner.next();

            int total = 0;
            String e = "";
            for (int i = 0; i < lineNumber.length(); i++) {
                int number = Integer.parseInt(Character.toString(lineNumber.charAt(i)));
                total += number;
            }
            int count = 0;
            for (int i = 1; i <= total; i++) {
                for (int j = 0; j < lineNumber.length(); j++) {
                    int number = Integer.parseInt(Character.toString(lineNumber.charAt(j)));
                    e += character[number];
                    count++;
                    if (count == total) {
                        break;
                    }
                }
                if (count == total) {
                    break;
                }
            }
            StringBuffer sb = new StringBuffer(e);
            StringBuffer sb1 = sb.reverse();
            if (e.equals(sb1.toString()))
                System.out.println("YES");
            else
                System.out.println("NO");
        }
    }
}
