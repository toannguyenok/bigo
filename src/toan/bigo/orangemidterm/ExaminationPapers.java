package toan.bigo.orangemidterm;

import java.util.ArrayList;
import java.util.Scanner;

public class ExaminationPapers {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCases = scanner.nextInt();
        ArrayList<Integer> result = new ArrayList<>();
        for (int t = 0; t < testCases; t++) {
            int n = scanner.nextInt();
            result.add(paper(n));
        }
        for(int i = 0;i<result.size();i++)
        {
            System.out.println(result.get(i));
        }
    }

    private static int paper(int n) {
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 3;
        }
        return 1 + paper(n - 1) * 2;
    }
}
