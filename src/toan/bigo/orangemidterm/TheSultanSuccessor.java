package toan.bigo.orangemidterm;

import java.util.ArrayList;
import java.util.Scanner;

public class TheSultanSuccessor {

    private static ArrayList<ArrayList<Integer>> board = new ArrayList<>();
    private static ArrayList<ArrayList<Boolean>> visited = new ArrayList<>();
    private static int ans = -1;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        for (int t = 0; t < k; t++) {
            board.clear();
            visited.clear();
            for (int i = 0; i < 8; i++) {
                board.add(new ArrayList<>());
                visited.add(new ArrayList<>());
                ans = -1;
                for (int j = 0; j < 8; j++) {
                    int number = scanner.nextInt();
                    board.get(i).add(number);
                    visited.get(i).add(false);
                }
            }
            queen(0, 0);
            System.out.println(ans);
        }
    }

    public static void queen(int row, int sum) {
        if (row == 8) {
            if (sum > ans) {
                ans = sum;
            }
            return;
        }
        for (int i = 0; i < 8; i++) {
            if (check(row, i)) {
                visited.get(row).set(i, true);
                queen(row + 1, sum + board.get(row).get(i));
                visited.get(row).set(i, false);
            }
        }
    }

    public static boolean check(int row, int col) {
        //check vertical
        for (int i = 0; i < row; i++) {
            if (visited.get(i).get(col)) {
                return false;
            }
        }
        //check main diagonal
        for (int i = row, j = col; i >= 0 && j >= 0; i--, j--) {
            if (visited.get(i).get(j)) {
                return false;
            }
        }

        //check secondary diagonal
        for (int i = row, j = col; i >= 0 && j < 8; i--, j++) {
            if (visited.get(i).get(j)) {
                return false;
            }
        }
        return true;
    }
}
