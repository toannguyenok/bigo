package toan.bigo.orangemidterm;

import java.util.Scanner;

public class PalindoremicSeries {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testcases = scanner.nextInt();
        for (int t = 0; t < testcases; t++) {
            String input = Integer.toString(scanner.nextInt());
            char c = 'a';
            String s[] = new String[26];
            String e = "";
            String f = "";
            int element, sum = 0, count = 0;
            for (int j = 0; j < 26; j++) {
                s[j] = Character.toString(c);
                c++;
            }
            for (int j = 0; j < input.length(); j++) {
                element = Integer.parseInt(Character.toString(input.charAt(j)));
                e += s[element];
                sum += element;
            }
            for (int k = 1; k <= sum; k++) {
                for (int j = 0; j < input.length(); j++) {
                    f += e.charAt(j);
                    count++;
                    if (count == sum) {
                        count = k ;
                        break;
                    }
                }
                if (k == count)
                    break;
            }
            StringBuffer sb = new StringBuffer(f);
            StringBuffer sb1 = sb.reverse();
            if (f.equals(sb1.toString()))
                System.out.println("YES");
            else
                System.out.println("NO");
        }
    }
}
